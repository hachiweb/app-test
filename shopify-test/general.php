<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
    <section class="container-fluid bg-light">
        <div class="row border-bottom p-2">
            <div class="col-xl-10 col-lg-9 col-sm-8 col-12 d-flex">
                <div class="p-1">
                    <button type="button" class="btn btn-white border2"><a href="index.php" alt=""
                            class="text-dark text-decoration-none">Dashboard</a></button>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Setting </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="general.php">General Settings</a>
                            <a class="dropdown-item" href="referral.php">Referral Program</a>
                        </div>
                    </div>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Data </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="birthday.php">Birthdays</a>
                            <a class="dropdown-item" href="product.php">Product Comments</a>
                            <a class="dropdown-item" href="purchase.php">Post Purchase Surveys</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-sm-4 col-12 text-right">
                <div class="border-back responsive-set p-1">
                    <button type="button" class="btn btn-white text-white ">Helps & Pricing</button>
                </div>
            </div>
        </div>


        <section class="container settings-two">
            <span class="settings"><a href="index.php"><i class="fas fa-less-than"></i> Back </a></span>
            <h3 class="">Settings</h3>
            <div class="row py-4">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Application status</h5>
                    <p>Enable or disable the app.</p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-sm-6 col-12 my-auto">
                                <span class="">Click the button to enable ReConvert</span>
                            </div>
                            <div class="col-sm-6 col-12 border-back text-right my-auto">
                                <button type="button" class="btn btn-white text-white responsive-set">Enable & ReConvert</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row py-4">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Language</h5>
                    <p>Set app admin language.</p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-sm-12">
                                <span class="">Select language</span>
                                <select class="form-control mt-2" name="app_language" onchange="">
                                    <option value="en" selected="selected">English</option>
                                    <option value="fil">Filipino</option>
                                    <option value="he_IL">עברית</option>
                                    <option value="sp">Español</option>
                                    <option value="fr">français</option>
                                    <option value="gr">Deutsche</option>
                                    <option value="jp">日本語</option>
                                    <option value="pt-BR">Português</option>
                                    <option value="zh-CN">简体中文</option>
                                    <option value="zh-TW">繁體中文</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row py-4">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Third party birthday integration</h5>
                    <p>In this section you can set up integrations with third party services for the birthday collector
                        widget, this will allow you to set up automated birthday messages to your customers.</p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="d-block">Select third party service</span>
                                <div class="checkbox my-4">
                                    <label><input type="checkbox" value="" checked class=""> Collect in the app </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control" id="integration_list" name="integration_list">
                                    <option value="0" selected="" disabled=""> Select more integration option </option>
                                    <option value="MailChimp">MailChimp</option>
                                    <option value="Klaviyo">Klaviyo</option>
                                    <option value="tobi">Tobi</option>
                                    <option value="SMSbump">SMSbump</option>
                                    <option value="SMSnotifications">SMS Notification</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-6">
                                <button type="button" class="btn btn-white text-dark border my-2">Cancel</button>
                            </div>
                            <div class="col-sm-6 col-6">
                                <div class="border-back text-right my-2">
                                    <button type="button" class="btn btn-white text-white">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row py-4">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Star reviews third party integration</h5>
                    <p>In this section, you can set an integration with product reviews apps, to display star ratings on
                        the product recommendations and product upsell widgets.</p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="d-block">Select third party review app</span>
                            </div>
                            <div class="col-sm-6">
                                <div class="d-flex">
                                <button type="button" class="btn btn-white text-dark border" data-toggle="modal" data-target="#myModal">Help
                                    Videos <i class="far fa-play-circle"></i></button>
                                      <!-- The Modal -->
                                      <div class="modal fade" id="myModal">
                                    <div class="modal-dialog modal-dialog-centered modal-lg">
                                        <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h5 class="modal-title">Star review third party integration</h5>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body p-5">
                                            Modal body..
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer border-back">
                                            <button type="button" class="btn btn-white text-white" data-dismiss="modal">Close</button>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                <select class="form-control w-50  mx-2" onchange="" id="reviewAppSelect" name="review_app">
                                    <option value="0" selected="selected">Select review app</option>
                                    <option value="1">Judge.me</option>
                                    <option value="2">Loox</option>
                                    <option value="3">Rivyo</option>
                                    <option value="4">Stamped.io</option>
                                </select>
                            </div>
                            </div>
                            <div class="col-12 my-3 border-top"></div>
                            <div class="col-sm-6 col-6">
                                <button type="button" class="btn btn-white text-dark border my-2">Cancel</button>
                            </div>
                            <div class="col-sm-6 col-6">
                                <div class="border-back text-right my-2">
                                    <button type="button" class="btn btn-white text-white">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row py-4">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Join our Facebook group</h5>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-6 col-12">
                                <span class="d-block">Get daily updates, tips & hacks to help you grow your eCommerce business with advice from other merchants and experts.</span>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-6 col-12">
                                <a href="" alt=""><img src="assets/images/facebook-button-join-group.png" alt="" class="w-100"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="row">
                <div class="m-auto">
                    <div class="FooterHelp__Content">
                        <span>ReConvert © 2019 - Developed by <a target="_blank"
                                href="https://www.stilyoapps.info/">Stilyo Apps</a> team </span>
                    </div>
            </footer>
        </section>
    </section>
</body>

</html>