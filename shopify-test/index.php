<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
    <section class="container-fluid bg-light">
        <div class="row border-bottom p-2">
            <div class="col-sm-6 d-flex">
                <div class="p-1">
                    <button type="button" class="btn btn-white border2"><a href="index.php" alt=""
                            class="text-dark text-decoration-none">Dashboard</a></button>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Setting </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="general.php">General Settings</a>
                            <a class="dropdown-item" href="referral.php">Referral Program</a>
                        </div>
                    </div>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Data </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="birthday.php">Birthdays</a>
                            <a class="dropdown-item" href="product.php">Product Comments</a>
                            <a class="dropdown-item" href="purchase.php">Post Purchase Surveys</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-right">
                <div class="border-back responsive-set p-1">
                    <button type="button" class="btn btn-white text-white">Helps & Pricing</button>
                </div>
            </div>
        </div>


        <section class="container">
            <div class="row py-4">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Application status</h5>
                    <p>Enable or disable the app.</p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-sm-6 col-12 my-auto">
                                <span class="">Click the button to enable ReConvert</span>
                            </div>
                            <div class="col-sm-6 col-12 border-back text-right my-auto">
                                <button type="button" class="btn btn-white text-white responsive-set">Enable & ReConvert</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row pt-5 pb-3">
                <div class="col-sm-4 col-6">
                    <h3 class="font-weight-normal">Templates</h3>
                </div>
                <div class="col-sm-8 col-6">
                    <div class="text-right">
                        <div class="border-back">
                            <button type="button" class="btn btn-white text-white">Enable & ReConvert</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row pt-5 pb-3">
                <div class="col-sm-4 col-12">
                    <h5 class="">Active template</h5>
                    <span class="my-auto">This is the current thank you page template that your customers see when they
                        complete a purchase</span>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row">
                            <div class="col-sm-6 col-12">
                                <h6>Empty template</h6>
                                <span class="my-auto">Last saved on Monday at 07:11am</span>
                            </div>
                            <div class="col-sm-6 col-12 border-back ">
                                <div class="form-inline float-right">
                                    <div class="dropdown show p-1">
                                        <a class="btn btn-white border2 dropdown-toggle" href="#" role="button"
                                            id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false"> Active </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Actions</a>
                                            <a class="dropdown-item" href="#">Preview</a>
                                            <a class="dropdown-item" href="#">Rename</a>
                                            <a class="dropdown-item" href="#">Duplicate</a>
                                            <a class="dropdown-item" href="#">Analytics</a>
                                        </div>
                                    </div>
                                    <div class="px-1">
                                        <button type="button" class="btn btn-white text-white"> Customize </button>
                                    </div>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <img src="assets/images/shopify.PNG" alt="" class="w-100 pt-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <div class="row pt-5 pb-3">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <h5 class="">Draft templates</h5>
                    <span class="my-auto font-weight-normal">Manage your store's draft templates. Add new drafts and
                        publish them to change your online store's thank you page appearance.</span>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="border2 p-3 padding-set bg-white">
                        <div class="row mx-1">
                            <div class="col-sm-4 col-12">
                                <h6>Template name</h6>
                                <!-- <span class="my-auto">Last saved on Monday at 07:11am</span> -->
                            </div>
                            <div class="col-sm-4 col-12">
                                <h6>Last saved</h6>
                                <!-- <span class="my-auto">Last saved on Monday at 07:11am</span> -->
                            </div>
                            <div class="col-sm-4 col-12">
                            </div>
                        </div>
                        <div class="border-top"></div>
                        <div class="row pt-3 pb-4">
                            <div class="col-sm-4 col-12">
                                <h6 class="font-weight-normal">Empty template_Copy</h6>
                                <!-- <span class="my-auto">Last saved on Monday at 07:11am</span> -->
                            </div>
                            <div class="col-sm-4 col-12">
                                <h6 class="font-weight-normal">Monday at 11:28am</h6>
                                <!-- <span class="my-auto">Last saved on Monday at 07:11am</span> -->
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-inline">
                                    <div class="dropdown show">
                                        <a class="btn btn-white border2 dropdown-toggle" href="#" role="button"
                                            id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false"> Active </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Actions</a>
                                            <a class="dropdown-item" href="#">Preview</a>
                                            <a class="dropdown-item" href="#">Rename</a>
                                            <a class="dropdown-item" href="#">Duplicate</a>
                                            <a class="dropdown-item" href="#">Analytics</a>
                                        </div>
                                    </div>
                                    <div class="border-back pl-3">
                                        <a href="">Customize</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- <div class="col-sm-6 col-12 border-back ">
                                <div class="form-inline float-right">
                                    <div class="dropdown show px-1">
                                        <a class="btn btn-white border dropdown-toggle" href="#" role="button"
                                            id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false"> Active </a>
                                        <div class="dropdown-menu"
                                            aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Actions</a>
                                            <a class="dropdown-item" href="#">Preview</a>
                                            <a class="dropdown-item" href="#">Rename</a>
                                            <a class="dropdown-item" href="#">Duplicate</a>
                                            <a class="dropdown-item" href="#">Analytics</a>
                                        </div>
                                    </div>
                                    <div  class="px-1">
                                    <button type="button" class="btn btn-white text-white"> Customize </button>
                                    </div>
                                </div>
                            </div> -->

                    </div>
                </div>
                <div class="col-sm-4 col-12">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12 mt-3">
                    <div class="border2 bg-white">
                        <div class="row  padding-set">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-6 text-center">
                                <img src="assets/images/img1.PNG" alt="" class="px-2">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-6 ">
                                <h6 class="">Free template</h6>
                                <span class="my-auto">Explore ReConvert's free templates, all designed to offer the best
                                    thank you page experience.</span>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">

                                <button type="button" class="btn btn-white text-dark border"> Explore Free Template
                                </button>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="border-top"></div>
            <section class='m-auto text-center'>
                <h4>----chart remaining------</h4>
            </section>
            <section class="row">
                <div class="col-md-4">
                    <div class="border-set">
                        <span class="set1 h5">ROI</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover"  data-content="Return on investment from ReConvert (revenue generated by the app / last payment for the app)*100"></i> </span>
                        <span class="set3"><i class="fas fa-infinity"></i></span>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="border-set">
                        <span class="set1 h5">ReConvert orders AOV</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="Average order value from ReConvert"></i> </span>
                        <span class="set4 h5 font-weight-bolder">€0</span>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="border-set">
                        <span class="set1 h5">Total store orders</span>
                        <span class="set2"></span>
                        <span class="set4 h5 font-weight-bolder">0</span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="border-set">
                        <span class="set1 h5">Thank you page impressions</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="This number is higher than store orders, since customers come back to this page from transactional emails (order confirmation, shipping updates, etc.)"></i> </span>
                        <span class="set4 h5 font-weight-bolder">0</span>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="border-set">
                        <span class="set1 h5">Thank you page conversion rate</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="The percentage of people who purchased out of the number of visitors to the thank you page: (number of purchases from the page / number of impressions to the page)*100"></i> </span>
                        <span class="set4 h5 font-weight-bolder">0%</span>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="border-set">
                        <span class="set1 h5">Bounce rate</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="Percentage of people who left the thank you page without taking any additional action"></i> </span>
                        <span class="set4 h5 font-weight-bolder">0%</span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="border-set">
                        <span class="set1 h5">Birthdays collected</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="Number of customer birthdays collected using the birthday collector widget"></i> </span>
                        <span class="set4 h5 font-weight-bolder">0</span>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="border-set">
                        <span class="set1 h5">Product comments collected</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="Number of product comments collected using the product comments widget"></i> </span>
                        <span class="set4 h5 font-weight-bolder">0</span>
                    </div>
                </div>
                <div class="col-md-4">
                <div class="border-set">
                        <span class="set1 h5">Surveys answered</span>
                        <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="Number of post-purchase surveys answeres collected using the post-purchase survey widget"></i> </span>
                        <span class="set4 h5 font-weight-bolder">0</span>
                    </div>
                </div>
            </section>
            <footer class="row">
                <div class="m-auto">
                <div class="FooterHelp__Content">
                       <span>ReConvert © 2019 - Developed by <a target="_blank" href="https://www.stilyoapps.info/">Stilyo Apps</a> team  </span> 
                </div>
            </footer>
        </section>
    </section>
    <script>
    $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
    });
</script>
</body>

</html>