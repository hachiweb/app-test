<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
    <section class="container-fluid bg-light">
        <div class="row border-bottom p-2">
            <div class="col-sm-6 d-flex">
                <div class="p-1">
                    <button type="button" class="btn btn-white border2"><a href="index.php" alt=""
                            class="text-dark text-decoration-none">Dashboard</a></button>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Setting </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="general.php">General Settings</a>
                            <a class="dropdown-item" href="referral.php">Referral Program</a>
                        </div>
                    </div>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="" role="button"id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Data </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="birthday.php">Birthdays</a>
                            <a class="dropdown-item" href="product.php">Product Comments</a>
                            <a class="dropdown-item" href="purchase.php">Post Purchase Surveys</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-right">
                <div class="border-back responsive-set p-1">
                    <button type="button" class="btn btn-white text-white">Helps & Pricing</button>
                </div>
            </div>
        </div>


        <section class="container-fluid settings-two">
            <div class="row">
                <div class="col-sm-11 m-auto">
                    <div class="border2 p-3 padding-set bg-white border-top-set">
                        <div class="row">
                            <div class="col-12 my-auto">
                                <h5>Referral Program</h5>
                                <span class="">This is where you can share ReConvert with other friendly stores and earn some money in the process. You'll get <strong>10% </strong>commission for life on whatever the stores referred by you will pay.</span>
                            </div>
                        </div>
                    </div>
                    <section class="border-top-set-one">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                            <div class="border3 p-3 padding-set bg-white">
                                <div class="border-back">
                                    <h6>Affiliate link</h6>
                                    <h5><input type="text" class="form-control w-75 d-inline" id="myInput" value="https://www.stilyoapps.com/reconvert/?ref=ST01-q4YsTgwsp3mFJix">
                                    <span class="tooltip1">
                                    <button type="button" class="btn btn-white text-white" id="myTooltip" data-toggle="popover" data-content="Copy to clipboard">
                                    <span class="tooltiptext"><i class="far fa-copy"></i></span>
                                    </button></h5>
                                    </span>
                                    <span>Your affiliate commission payout is 10% of whatever the store spends on a paid plan for ReConvert. Payouts are processed at the end of each month but not earlier than 2 weeks from the original charge.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                            <div class="border3 p-3 padding-set bg-white">
                                <div class="border-back">
                                <h6>PayPal email setup</h6>
                                <h5><input type="text" class="form-control w-75 d-inline" placeholder="Please enter your PayPal email address here."><button class="btn btn-white text-white">Save</button></h5>
                                <span>Please enter your PayPal email address here.</span>
                                </div>
                            </div>
                        </div>
                        </div>
                        <section class="row visitors-set">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="border-set">
                                    <span class="set1 h5">Visitors</span>
                                    <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover"  data-content="Number of visitors that came through your link"></i> </span>
                                    <span class="set3">0</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="border-set">
                                    <span class="set1 h5">Referrals</span>
                                    <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="Number of stores that installed ReConvert through your link"></i> </span>
                                    <span class="set4 h5 font-weight-bolder">0</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="border-set">
                                    <span class="set1 h5">Total earnings</span>
                                    <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="The amount of money you earned through commissions(minimum amount per payment: $50)"></i> </span>
                                    <span class="set4 h5 font-weight-bolder">$0</span>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="border-set">
                                    <span class="set1 h5">Total paid</span>
                                    <span class="set2"><i class="fas fa-exclamation-circle" data-toggle="popover" data-content="The amount of commission money already paid"></i> </span>
                                    <span class="set4 h5 font-weight-bolder">$0</span>
                                </div>
                            </div>
                        </section>
                        <section class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <h6 class="p-4">Referred stores</h6>
                            <div class="border3 bg-white">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr class="font-weight-normal">
                                    <th>Store name</th>
                                    <th>Store plan</th>
                                    <th>Current plan</th>
                                    <th>Date registered	</th>
                                    <th>Current commission	</th>
                                    <th>Total commission	</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr> -->
                                <tr>
                                    <td colspan="7">No affiliates stores found</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                            </div>
                        </section>
                        <section class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <h6 class="p-4">Payment history</h6>
                            <div class="border3 bg-white">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr class="font-weight-normal">
                                    <th>PayPal email</th>
                                    <th>Paid amount</th>
                                    <th>Date added</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr> -->
                                <tr>
                                    <td colspan="3">No data found</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                            </div>
                        </section>
                    </section>

                    <div class="border-top"></div>
                    <footer class="row">
                        <div class="m-auto">
                            <div class="FooterHelp__Content">
                                <span>ReConvert © 2019 - Developed by <a target="_blank"
                                        href="https://www.stilyoapps.info/">Stilyo Apps</a> team </span>
                            </div>
            </footer>
        </section>
        </div>
            </div>
    </section>


    <script>
    $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
    $(document).on("click","#myTooltip",function(){
        var copyText = document.getElementById("myInput");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        });
    });
</script>
</body>

</html>