<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
    <section class="container-fluid bg-light">
        <div class="row border-bottom p-2">
            <div class="col-sm-6 col-12 d-flex">
                <div class="p-1">
                    <button type="button" class="btn btn-white border2"><a href="index.php" alt=""
                            class="text-dark text-decoration-none">Dashboard</a></button>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Setting </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="general.php">General Settings</a>
                            <a class="dropdown-item" href="referral.php">Referral Program</a>
                        </div>
                    </div>
                </div>
                <div class="p-1">
                    <div class="dropdown show">
                        <a class="btn btn-white border2 dropdown-toggle" href="" role="button" id="dropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Data </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="birthday.php">Birthdays</a>
                            <a class="dropdown-item" href="product.php">Product Comments</a>
                            <a class="dropdown-item" href="purchase.php">Post Purchase Surveys</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-12 text-right">
                <div class="border-back responsive-set p-1">
                    <button type="button" class="btn btn-white text-white">Helps & Pricing</button>
                </div>
            </div>
        </div>


        <section class="container settings-two">
            <div class="row">
                <div class="col-sm-10 m-auto">
                    <div class="border2 p-3 padding-set bg-white pb-5">
                        <div class="row">
                            <div class="col-sm-6 col-6 my-auto">
                                <h6>Survey builder</h6>
                            </div>
                            <div class="col-sm-6 col-6 border-back  text-right">
                            </div>

                            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-6 my-auto">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button class="btn btn-white border" type="button"><i class="fas fa-search"></i></button>
                                </div>
                                <input type="text" class="form-control" placeholder="Search" aria-label="" aria-describedby="basic-addon1">
                            </div>
                                <span class="mt-0">Type at least 3 characters</span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 border-back">
                                <button type="button" class="btn btn-white text-white"><a href="survey.php" class="text-white text-decoration-none">Create a vew survey</a></button>
                            </div>
                        </div>
                        <div class="row py-4">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="border bg-white">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr class="font-weight-normal">
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- <tr>
                                    <td></td>
                                    <td></td>
                                </tr> -->
                                <tr>
                                    <td colspan="2">Could not find any record.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            <footer class="row">
                <div class="m-auto">
                    <div class="FooterHelp__Content">
                        <span>ReConvert © 2019 - Developed by <a target="_blank" href="https://www.stilyoapps.info/">Stilyo Apps</a> team </span>
                    </div>
                </div>
            </footer>
        </section>
</section>
</body>

</html>