<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Include PHPMailer library files
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

$mail = new PHPMailer;
// $mail->isSMTP();
$mail->Host     = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->Username = 'testhachiweb@gmail.com';
$mail->Password = 'Computer@25';
$mail->SMTPSecure = 'tls';
$mail->Port     = 587;
$mail->setFrom('testhachiweb@gmail.com', 'Anil Kumar');
$mail->addReplyTo('testhachiweb@gmail.com', 'Anil Kumar');

// Add a recipient
$mail->addAddress('test@hachiweb.com');

// Add cc or bcc 
$mail->addCC('test@hachiweb.com');
$mail->addBCC('test@hachiweb.com');

// Email subject
$mail->Subject = 'Send Email via SMTP using PHPMailer';

// Set email format to HTML
$mail->isHTML(true);

// Email body content
$mailContent = '
    <h2>Send HTML Email using SMTP in PHP</h2>
    <p>It is a test email by Anil Kumar, sent via SMTP server with PHPMailer using PHP.</p>';
$mail->Body = $mailContent;

// Send email
if(!$mail->send()){
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}else{
    echo 'Message has been sent';
}
?>