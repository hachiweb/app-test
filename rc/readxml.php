<?php 
$soap_response = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header />
   <soapenv:Body>
      <unirec:HotelCreateReservationReq xmlns:unirec="http://www.travelport.com/schema/universal_v41_0" xmlns:com="http://www.travelport.com/schema/common_v41_0" xmlns:hot="http://www.travelport.com/schema/hotel_v41_0" AuthorizedBy="user" ProviderCode="1V" TraceId="trace" UserAcceptance="true" TargetBranch="P3029488">
         <com:BillingPointOfSaleInfo OriginApplication="uAPI" />
         <com:BookingTraveler>
            <com:BookingTravelerName First="Charlie" Last="Davidson" />
            <com:PhoneNumber Number="3343720641" />
            <com:Email EmailID="Cdtigger82@hotmail.com" />
            <com:Address>
               <com:Street>111 Regency Court</com:Street>
               <com:City>Warner Robins</com:City>
               <com:PostalCode>31088</com:PostalCode>
               <com:Country>US</com:Country>
            </com:Address>
         </com:BookingTraveler>
         <hot:HotelRateDetail RatePlanType="KEN013A" />
         <hot:HotelProperty HotelChain="HI" HotelCode="14993" />
         <hot:HotelStay>
            <hot:CheckinDate>2019-04-27</hot:CheckinDate>
            <hot:CheckoutDate>2019-04-28</hot:CheckoutDate>
         </hot:HotelStay>
         <hot:HotelSpecialRequest />
         <com:PointOfSale ProviderCode="1V" PseudoCityCode="2GM4" IATA="34651842" />
         <hot:GuestInformation NumberOfRooms="1" />
      </unirec:HotelCreateReservationReq>
   </soapenv:Body>
</soapenv:Envelope>
XML;




$xml_nodes = new SimpleXMLElement($soap_response);

$nodes = $xml_nodes->xpath('//hot:HotelRateDetail[@RatePlanType = "KEN013A"]'); // Replace the ID value with whatever seatmap id you're trying to access
print_r($nodes); exit();
foreach($nodes as $seat)
{
    // You can then access: $seat['row'], $seat['seatnum'], $seat['available']
}
//your php code here to read any data fromt the response
//for example read if I would like to read RatePlanType I get KEN013A as the
// $xmlres = htmlentities($soap_response);
// var_dump(json_encode($soap_response));
$fileContents = str_replace(array("\n", "\r", "\t"), '', $soap_response);
$fileContents = trim(str_replace('"', "'", $fileContents));
$simpleXml = simplexml_load_string($fileContents);
echo $fileContents;
$xml = simplexml_load_string($soap_response);
// foreach($xml->hot[0]->attributes() as $attr => $val) {
//     echo $attr,'="',$val,"\"\n";
// }

