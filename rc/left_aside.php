<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    $link = "https://";
} else {
    $link = "http://";
}
$site_url = $link . $_SERVER['HTTP_HOST'].'/rc';
$username = $_SESSION["username"];
$alias = $_SESSION["alias"];
$role = $_SESSION["role"];
$created_at = $_SESSION["created_at"];
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <?php
        if($role=='student')
        {
          echo '<img src="'.$site_url.'/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">';
        }
        else{
          echo '<p style="height:20px;;"></p>';
        }
        ?>  
      </div>
      <div class="pull-left info">
        <p><?php echo $role;?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo $site_url ?>/"><i class="fa fa-circle-o"></i> Home</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="">
          <i class="fa fa-edit"></i> <span>Forms</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
         <li><a href="<?php echo $site_url ?>/pages/forms/admin.php"><i class="fa fa-circle-o"></i> Admin Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/school.php"><i class="fa fa-circle-o"></i> School Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/affiliation.php"><i class="fa fa-circle-o"></i> Affiliation Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/branch.php"><i class="fa fa-circle-o"></i> Branch Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/evaluator.php"><i class="fa fa-circle-o"></i> Evaluator Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/report-card.php"><i class="fa fa-circle-o"></i> Report Card Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/subject.php"><i class="fa fa-circle-o"></i> Subject Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/exam.php"><i class="fa fa-circle-o"></i> Exam Form</a></li> 
         <li><a href="<?php echo $site_url ?>/pages/forms/student.php"><i class="fa fa-circle-o"></i> Student Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/teacher.php"><i class="fa fa-circle-o"></i> Teacher Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/class-detail.php"><i class="fa fa-circle-o"></i> Class Detail</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/standard.php"><i class="fa fa-circle-o"></i> Standard Form</a></li>
          <li><a href="<?php echo $site_url ?>/pages/forms/principal.php"><i class="fa fa-circle-o"></i> Principal Form</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-table"></i> <span>Manage</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_school.php"><i class="fa fa-circle-o"></i> Manage School</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_branch.php"><i class="fa fa-circle-o"></i> Manage Branch</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_principal.php"><i class="fa fa-circle-o"></i> Manage Principal</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/manage_teacher.php"><i class="fa fa-circle-o"></i> Manage Teacher</a></li>
          <li><a href="<?php echo $site_url ?>/pages/tables/select_class.php"><i class="fa fa-circle-o"></i> Manage Student</a></li>
        </ul>
      </li>
      <li class="treeview">
       <a href="#">
         <i class="fa fa-table"></i> <span>ReportCard</span>
         <span class="pull-right-container">
           <i class="fa fa-angle-left pull-right"></i>
         </span>
       </a>
       <ul class="treeview-menu">
           <li><a href="<?php echo $site_url ?>/pages/forms/scholastic-page.php"><i class="fa fa-circle-o"></i> Register Marks</a></li>
           <li><a href="<?php echo $site_url ?>/pages/forms/select_report_card.php"><i class="fa fa-circle-o"></i> View Report Card</a></li>
       </ul>
     </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>