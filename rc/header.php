<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    $link = "https://";
} else {
    $link = "http://";
}
$site_url = $link . $_SERVER['HTTP_HOST'].'/rc';
include 'dbconnect.php';
$db = new DB();
$username = $_SESSION["username"];
$alias = $_SESSION["alias"];
$role = $_SESSION["role"];
$created_at = $_SESSION["created_at"];
$branch_id = $_SESSION['branch_id'];
$selected_branch_sql = "SELECT b.id, b.branch_code,b.branch_city,s.school_name FROM branch b LEFT JOIN school s ON b.school_id = s.id WHERE b.id='$branch_id' ORDER BY b.id";
$branch_result = $db->executeQuery($selected_branch_sql);
$branch = mysqli_fetch_assoc($branch_result);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Digital Report Card</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $site_url ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?php echo $site_url ?>/dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="<?php echo $site_url ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="<?php echo $site_url ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <link rel="stylesheet" href="<?php echo $site_url ?>/dist/css/custom.css">
   <!--JQuery 3-->
   <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
 </head>
 <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header main-header_dubb">
      <!-- Logo -->
      <a href="<?php echo $site_url ?>/index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>RC</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Digital Report Card</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo $site_url; ?>/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
          <span class="hidden-xs"><?php echo $alias; ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="<?php echo $site_url; ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            <p>
              <?php echo $alias.' - '.$role; ?>
              <small><b>Branch - <?php echo isset($branch_id)?$branch['school_name'].'('.$branch['branch_city'].')':'Not Available'; ?></b></small>
            </p>
          </li>
          <!-- Menu Body -->
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
              <div class="pull-left">
              <a href="https://www.myschoolreportcards.com/pages/auth/cpwd.php" class="btn btn-default btn-flat">Change Password</a>
            </div>
            <div class="pull-right">
              <a href="<?php echo $site_url ?>/pages/auth/signout.php" class="btn btn-default btn-flat">Logout</a>
            </div>
          </li>
        </ul>
      </li>
      <!-- Control Sidebar Toggle Button -->
    </ul>
  </div>
</nav>
</header>
<?php include('left_aside.php') ?>