<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    };
include('../../header.php');
?>    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage School
        <small>View records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">School</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">School Record</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Serial No.</th>
                  <th>Registration No.</th>
                  <th>School Code</th>
                  <th>School name</th>
                  <th>school logo</th>
                  <th>Status</th>
                </tr>
                <?php
                $db = new DB();
                //extract from school table
                $school_query = "SELECT * FROM school";
                $select_data = $db->executeQuery($school_query);
                $n = 1;
                while ($school_data = mysqli_fetch_array($select_data)) {
                ?>
                  <tr style="<?php if(isset($_GET['id'])){
                    if($_GET['id'] == $school_data['id']){
                      echo 'background: #00FFFF';
                    };
                  }; ?>">
                    <td><?php echo $n++; ?></td>
                    <td><?php echo $school_data['registration_no']; ?></td>
                    <td><?php echo $school_data['school_code']; ?></td>
                    <td><?php echo $school_data['school_name']; ?></td>
                    <td><img src="<?=$site_url."/pages/forms/assets/school/".$school_data['id']."/logo/".$school_data['school_logo']; ?>" width="20" height="20"></td>
                    <td><?php echo $school_data['status']; ?></td>
                    <td><a href="edit_school_details.php?id=<?php echo $school_data['id']; ?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"><a/></td>
                  </tr>
                <?php }; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>