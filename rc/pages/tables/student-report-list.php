<?php
// print_r($_POST); exit;
if (!isset($_SESSION)) {
    session_start();
}
$site_url = 'https://' . $_SERVER['HTTP_HOST'];
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
?>    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student Result Record List
        <small>View Result Records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Result Records</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Student Record</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Registration No</th>
                  <th>Name</th>
                  <th>DOB</th>
                  <th>Mother Name</th>
                  <th>Father Name</th>
                  <th>Exam Type</th>
                  <th style="width: 1px">View</th>
                </tr>
                <?php
                $db = new DB();
                $std_id = $_POST['standard_id'];
                $sec_id = $_POST['section_id'];
                $year   = $_POST['year'];
                $student_query = "SELECT * FROM `student` WHERE `standard_id` = '$std_id' AND `section_id` = '$sec_id' AND `academic_session` = '$year'";
                $select_data = $db->executeQuery($student_query);
                while ($student_data = mysqli_fetch_array($select_data)) {
                ?>
                  <tr>
                  <form action="a.php" method="post">
                    <td><?php echo $student_data['registration_id']; ?><input type="hidden" name="registration_id" value="<?php echo $student_data['registration_id'];?>"></td>
                    <td><?php echo $student_data['first_name'].' '.$student_data['last_name']; ?></td>
                    <td><span class="label label-success"><?php echo $student_data['dob']; ?></span></td>
                    <td><?php echo $student_data['mother_name']; ?></td>
                    <td><?php echo $student_data['father_name']; ?></td>
                    <td>
                      <select name="periodic_id" id="periodic_id" class="form-control">        
                        <option value="select" >Select</option>										
                          <?php 
                          $exam_sql = "SELECT * FROM `exam_meta` WHERE `branch_id`='$branch_id'";
                          $exam_raw = $db->executeQuery($exam_sql);
                          while($result = mysqli_fetch_assoc($exam_raw)){echo '<option value="'.$result['id'].'">'.$result['name'].'</option>';}?>
                      </select>
                    </td>
                    <td><button type="submit" id="submit_btn" class="btn btn_custom"><i class="fa fa-list-alt" aria-hidden="true"></i></button></td>
                    </tr>
                    </form>
                <?php }; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include('../../footer.php'); ?>