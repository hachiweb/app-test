<?php 
include('../../header.php'); 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Report Card
			<small>Preview</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">Report Card</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-primary pd-10 mt-5">
					<div class="logo-box text-center">
						<div class="img">
							<img src="http://p12cdn4static.sharpschool.com/UserFiles/Servers/Server_425395/Image/SchoolLogo.png" alt="School Logo">
						</div>
					</div><!-- logo-box -->
					<hr class="box box-primary">
					<div class="profile-data-bx">
						<div class="row">
							<div class="col-sm-4 pull-right profile-image-col">
								<div class="profile_img">
									<div class="img">
										<img src="http://hachiweb.com/reportcard/dist/img/user2-160x160.jpg" alt="Profile">
									</div>
								</div>
							</div><!-- col -->
							<div class="col-sm-8">
								<div class="profile-data text-capitalize">
									<div class="profile-row d-flex">
										<label>student name</label>:
										<span>Lorem Ipsum</span>
									</div>
									<div class="profile-row d-flex">
										<label>father name</label>:
										<span>Lorem Ipsum Lorem</span>
									</div>
									<div class="profile-row d-flex">
										<label>standard</label>:
										<span>Middle</span>
									</div>
									<div class="profile-row d-flex">
										<label>section roll</label>:
										<span>1400</span>
									</div>
									<div class="profile-row d-flex">
										<label>number</label>:
										<span>0123456789</span>
									</div>
									<div class="profile-row d-flex">
										<label>admission number</label>:
										<span>12345</span>
									</div>
								</div><!-- prodile-data -->
							</div><!-- col -->
						</div><!-- row -->
					</div><!-- profile data -->
				</div><!-- box-primary -->
			</div>
			<div class="col-md-10 col-md-offset-1">
				<!-- general form elements -->
				<div class="box box-primary mt-5">
					<div class="box-header with-border">
						<h3>Report Card</h3>
					</div>
					<!-- /.box-header -->
					<!-- table start -->
					<div class="custom-table pd-10">
						<h3 class="bg-blue pd-10">Part-1: Scholastic Area</h3>
						<h4 class="m0 text-center pd-10 bg-blue-light">Term -1 (100 Marks)</h4>
						<table class="table table-bordered scholastic_area_table">
							<thead>
								<tr class="text-center">
									<th>S.No</th>
									<th>Subject</th>
									<th>PA-1</th>
									<th>Note <br> Book</th>
									<th>Subject <br> Entertainment</th>
									<th>Total <br> (PA+NB+SE)</th>
									<th>Half Yearly <br> Examination</th>
									<th>Marks <br> Obtained</th>
									<th>Grade</th>
								</tr>
							</thead>
							<tbody>
								<tr class="text-center pd-10 bg-blue-light">
									<td></td>
									<td></td>
									<td>(10)</td>
									<td>(5)</td>
									<td>(5)</td>
									<td>(20)</td>
									<td>(80)</td>
									<td>(100)</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>English</td>
									<td></td>
									<td></td>									
									<td></td>									
									<td></td>
									<td></td>
									<td></td>
									<td></td>									
								</tr>
								<tr>
									<td>2</td>
									<td>Hindi</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>									
								</tr>
								<tr>
									<td>3</td>
									<td>Urdu</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td class="text-right" colspan="7">Total</td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered term-result max-width-600">
							<thead>
								<tr>
									<th>Subject</th>
									<th>Marks</th>
									<th>Grade</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Arabic</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>Deeniyat</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>Value Education</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>General Knowledge</td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table><!-- term-result -->
					</div><!-- table bx -->
					<div class="custom-table pd-10">
						<h3 class="bg-blue pd-10">Part-2: Co-Scholastic Areas (3-Point Grading Scale)</h3>
						<h4 class="m0 text-center pd-10 bg-blue-light">Term -1</h4>
						<table class="table table-bordered co_scholastic_table">
							<thead>
								<tr>
									<th class="bg-blue-light">Activity</th>
									<th width="150px">Grade</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="bg-blue-light">Arabic</td>
									<td></td>
								</tr>
								<tr>
									<td class="bg-blue-light">Deeniyat</td>
									<td></td>
								</tr>
								<tr>
									<td class="bg-blue-light">Value Education</td>
									<td></td>
								</tr>
							</tbody>
						</table><!-- term-result -->
					</div><!-- custom table bx -->
					<div class="custom-table pd-10">
						<h3 class="mb-0 bg-blue pd-10">Part-3: Discipline (3-Point Grading Scale)</h3>
						<h4 class="m0 pd-10">(Attendance: Sincerity: Behaviour: Values )</h4>
						<h4 class="m0 text-center pd-10 bg-blue-light max-width-600">Term -1</h4>
						<table class="table table-bordered discipline_table max-width-600">
							<thead>
								<tr>
									<th>Activity</th>
									<th>Grade</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Discipline</td>
									<td></td>
								</tr>
								<tr>
									<td>Discipline</td>
									<td></td>
								</tr>
								<tr>
									<td>Discipline</td>
									<td></td>
								</tr>
							</tbody>
						</table><!-- term-result -->
					</div><!-- custom table bx -->
				</div><!-- /.box -->
			</div><!-- col -->
		</div><!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>