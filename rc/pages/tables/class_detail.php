<?php
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
    $branch_id = $_SESSION["branch_id"];
}
include '../../header.php';
$selected_section_id = $_SESSION['section_id'];
$selected_standard_id = $_SESSION['standard_id'];
$db = new DB();
$teacher_sql = "SELECT * FROM `teacher` WHERE `branch_id`='$branch_id'";
$all_branch_teacher = "SELECT * FROM `teacher` WHERE `branch_id`='$branch_id'";
$current_subject_teacher = "SELECT * FROM `subject_teacher` WHERE `standard_id`='$selected_standard_id' AND `section_id` = '$selected_section_id' AND `branch_id`='$branch_id'";
$standard_sql = "SELECT * FROM `standard`";
$section_sql = "SELECT * FROM `section`";
$student_sql = "SELECT * FROM `student` WHERE `standard_id`='$selected_standard_id' AND `section_id` = '$selected_section_id' AND `branch_id`='$branch_id'";
$class_detail_sql = "SELECT * FROM `subject_teacher` WHERE `standard_id`='$selected_standard_id' AND `section_id` = '$selected_section_id' AND `branch_id`='$branch_id'";
$subject_query = "SELECT * FROM `subject` WHERE `standard_id`='$selected_standard_id'";
$subject_count = "SELECT COUNT(`id`) AS subject_count FROM `subject` WHERE `standard_id`='$selected_standard_id'";
$subject_count_raw = $db->executeQuery($subject_count);
$subject_count_result = mysqli_fetch_assoc($subject_count_raw);
$class_detail_raw = $db->executeQuery($class_detail_sql);
$class_detail_result = mysqli_fetch_assoc($class_detail_raw);
$class_teacher = "SELECT * FROM `class-teacher` WHERE `standard_id`='$selected_standard_id' AND `section_id` = '$selected_section_id' AND `branch_id`='$branch_id'";
$student_raw = $db->executeQuery($student_sql);
$result = $db->executeQuery($class_teacher);
$value = mysqli_fetch_assoc($result);
$subject_sql = "SELECT name, id FROM subject GROUP BY name";
$stu_strength_sql = "SELECT COUNT(`id`) AS strength FROM `student` WHERE `standard_id`='$selected_section_id' AND `section_id`='$selected_standard_id' AND `branch_id`='$branch_id'";
$teacher_raw = $db->executeQuery($teacher_sql);
$all_branch_teacher_raw = $db->executeQuery($all_branch_teacher);
$standard_raw = $db->executeQuery($standard_sql);
$section_raw = $db->executeQuery($section_sql);
$subject_raw = $db->executeQuery($subject_sql);
$stdsubject = $db->executeQuery($subject_query);
$stdsubject2 = $db->executeQuery($subject_query);
$all_branch_teacher_raw2 = $db->executeQuery($current_subject_teacher);
$student_strength = $db->executeQuery($stu_strength_sql);
$student_strength = mysqli_fetch_assoc($student_strength);
$student_strength = $student_strength['strength'];
$i = 0;
while ($sub_result2 = mysqli_fetch_assoc($stdsubject2)) {
    $currSub['id'][$i] = $sub_result2['id'];
    $currSub['name'][$i] = $sub_result2['name'];
    $i = $i + 1;
}
$i = 0;
while ($teacher_result2 = mysqli_fetch_assoc($all_branch_teacher_raw2)) {
    $id = $teacher_result2["teacher_id"];
    $current_subject_teacher_name = "SELECT * FROM `teacher` WHERE `id`='$id'";
    $current_subject_teacher_name_raw = $db->executeQuery($current_subject_teacher_name);
    $current_subject_teacher_detail = mysqli_fetch_assoc($current_subject_teacher_name_raw);
    $currTeacher['id'][$i] = $id;
    $currTeacher['name'][$i] = $current_subject_teacher_detail['first_name'] . ' ' . $current_subject_teacher_detail['last_name'] . '(Reg id-' . $id . ')';
    $i = $i + 1;
}
?>
<style>
/*body{*/
/*			margin-top: 100px;*/
/*			font-family: 'Trebuchet MS', serif;*/
/*			line-height: 1.6*/
/*		}*/
		.container{
			width: 800px;
			margin: 0 auto;
		}
		ul.tabs{
			margin: 0px;
			padding: 0px;
			list-style: none;
		}
		ul.tabs li{
			background: none;
			color: #222;
			display: inline-block;
			padding: 10px 15px;
			cursor: pointer;
		}
		ul.tabs li.current{
			background: #ededed;
			color: #222;
		}
		.tab-content{
			display: none;
			background: #ededed;
			padding: 15px;
		}
		.tab-content.current{
			display: inherit;
		}
 </style>		
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Class Details
        <small>preview of class details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Class Detail</li>
      </ol>
    <div id="message" class="alert"></div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Basic Detail</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <!--input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div-->
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th></th>
                  <th>Standard</th>
                  <th><select name="standard_id" id="standard_id" class="form-control" disabled>        <option value="select" >Select</option>
										<?php while($result = mysqli_fetch_assoc($standard_raw)){
										if($selected_standard_id==$result['id']){
										echo '
										<option selected="selected" value="'.$result['id'].'">'.$result['name'].'</option>'; }
										else{
										    	echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>';
										}
										}
										?>
									</select></th>
                  <th>Section</th>
                  <th><select name="section_id" id="section_id" class="form-control" disabled>        <option value="select" >Select                   </option>										<?php while($result = mysqli_fetch_assoc($section_raw)){ if($selected_section_id==$result['id']){
									    echo '
										<option selected="selected" value="'.$result['id'].'">'.$result['name'].'</option>'; }
										else{
										    echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>';
										}
                  }
										?>
									    </select></th>
                  <th>Academic Session</th>
                  <th>2018-19</th>
                </tr>
                 <tr class="">
                  <th></th>
                  <th colspan=2>Class Teacher</th>
                  <th>
                      <?php if(isset($value) and !empty($value)){
                        //   echo '<input type="text" disable="disabled" value="'.$value['teacher'].'" />';
                         echo '<select name="teacher_id" id="teacher_id" class="form-control">        <option value="select" >Select</option>';
									    while($result = mysqli_fetch_assoc($teacher_raw)){
									    if($value['teacher_id']==$result['id']){
									    echo '
										<option selected="selected" value="'.$result['id'].'">'.$result['first_name'].' '.$result['last_name'].'</option>'; }
										else{
									     echo '
										<option value="'.$result['id'].'">'.$result['first_name'].' '.$result['last_name'].'</option>'; }}
											echo '</select></th>';
									} 
                      else{
                          echo '<select name="teacher_id" id="teacher_id" class="form-control">        <option value="select" >Select</option>';
									    while($result = mysqli_fetch_assoc($teacher_raw)){echo '
										<option value="'.$result['id'].'">'.$result['first_name'].' '.$result['last_name'].'</option>'; }
									echo '</select></th>';
                      }
                      ?>
                  <th colspan=2>Total Students</th>
                  <th>	<?php echo $student_strength; ?></th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>
                    <button id="basic_detail_btn" type="button" class="btn btn-block btn-success">Save</button>
                  </td>
                </tr>
                <?php /* <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-success">Approved</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
                <tr>
                  <td>219</td>
                  <td>Alexander Pierce</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-warning">Pending</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
                <tr>
                  <td>657</td>
                  <td>Bob Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-primary">Approved</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
                <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-danger">Denied</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr> */?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
       <div class="container">
</div><!-- container -->
  <ul class="tabs">
    <li class="tab-link current" data-tab="tab-1">Student</li>
    <li class="tab-link" data-tab="tab-2">Register Student</li>
    <li class="tab-link" data-tab="tab-3">Subject Teacher</li>
  </ul>
	<div id="tab-1" class="tab-content current clearfix">
	<div class="col-md-6">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Student Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed ">
                <tbody>
                  <tr>
                    <th style="width: 10px"></th>
                    <th>Admission No</th>
                    <th>Name</th>
                    <th>Picture</th>
                   </tr>
                <?php
                    while($result = mysqli_fetch_assoc($student_raw)){
                        //  print_r($result);
                        echo '<tr>'.'<td></td><td>'.$result['registration_id'].'</td>'.'<td>'.$result['first_name'].' '.$result['last_name'].'</td>'.'<td>'.'<img src="'.$result['picture'].'" width="40" height="40"/>'.'</td>'.'</tr>';
                    }
				          ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td>
                      <!--div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                      </div-->
                    </td>
                    <td><!--span class="badge bg-red">55%</span--></td>
                  </tr>
                <!--tr>
                  <td></td>
                  <td>Clean database</td>
                  <td>
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-yellow">70%</span></td>
                </tr>
                <tr>
                  <td></td>
                  <td>Cron job running</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-light-blue">30%</span></td>
                </tr>
                <tr>
                  <td></td>
                  <td>Fix and squish bugs</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-success" style="width: 90%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-green">90%</span></td>
                </tr-->
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
	</div>
    <div id="tab-2" class="tab-content clearfix">
	<div class="col-md-6">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Register Student</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
            <form id="student_reg_form" >
              <table class="table table-condensed ">
                <tbody>
                  <tr>
                    <th style="width: 10px"></th>
                    <th></th>
                        <th><input type ="number" class="form-control" id="student_reg_no" name="student_reg_no" placeholder="Student Registration number" /></th>
                        <th><button id="student_reg_btn" type="button" class="btn btn-block btn-success">Register</button></th>
                    </form>
                   </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td>
                    </td>
                    <td><!--span class="badge bg-red">55%</span--></td>
                  </tr>
              </tbody></table>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
	</div>
	<div id="tab-3" class="tab-content clearfix">
		  <div class="col-md-6">  
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Subject-Teacher Detail</h3>
            </div>
            <!-- /.box-header -->
            <form id="subject_teacher_form">
              <div class="box-body no-padding">
                <table class="table table-striped ">
                  <tbody><tr>
                    <th style="width: 10px"></th>
                    <th>Subject</th>
                    <th>Teacher</th>
                    <?php 
                      if(!empty($class_detail_result))
                      {
                        echo '<th>Edit</th>';
                      }
                    ?> 
                  </tr>
                    <input type="hidden" id="getStandardId" name="standard_id"/>
                    <input type="hidden" id="getSectionId" name="section_id"/>
                  <?php
                    if(!empty($class_detail_result))
                    {
                        // iterate over your results once and collect them to a string
                      $stdSubjectOptions = '';
                      while($subject_result = mysqli_fetch_assoc($stdsubject)){
                          $stdSubjectOptions .= '<option value="'.$subject_result['id'].'">'.$subject_result['name'].'</option>';
                      }
                      // iterate over your results once and collect them to a string
                      $teachers = '';
                      while($teacher_result = mysqli_fetch_assoc($all_branch_teacher_raw)){
                          $teachers .= '<option value="'.$teacher_result['id'].'">'.$teacher_result['first_name'].' '.$teacher_result['last_name'].'</option>';                         
                      } 
                      for($i=0; $i<$subject_count_result['subject_count']; $i++){
                          if(!empty($currTeacher['name'][$i])){
                            echo '<tr><td></td><td><input type="text" disabled value="';
                            // just output previously formed string
                            echo $currSub['name'][$i];    
                            echo  '"/>';
                            echo '<input type="hidden" name="subject_id[]" id="subject_id[]" value="';
                            echo $currSub['id'][$i];    
                            echo  '"/>';
                            echo '</td><td>';
                            echo '<input type="text" disabled value="';                    
                            echo $currTeacher['name'][$i];                            
                            echo '"/>';
                            echo '<input type="hidden" name="teacher_id[]" id="teacher_id[]" value="';                    
                            echo $currTeacher['id'][$i];                            
                            echo '"/>';
                            echo '</td><td><img src="https://cdn4.iconfinder.com/data/icons/web-ui-color/128/Edit-512.png" height="20" width="20"/></td><td></td></tr>';
                          }
                          else{
                            echo '<tr><td></td><td><input type="text" disabled value="';
                            echo $currSub['name'][$i];  
                            echo  '"/>';
                            echo '<input type="hidden" name="subject_id[]" id="subject_id[]" value="';
                            echo $currSub['id'][$i]; 
                            echo  '"/>';
                            echo '</td><td>';
                            echo '<select name="teacher_id[]" id="teacher_id[]" class="form-control"><option value="select" >Select</option>';
                            // just output previously formed string
                            echo $teachers;
                            echo '</select>';
                            echo '</td><td></td></tr>';
                          }  
                        }
                          echo '<tr><td></td><td></td><td>
                          <input id="subject_techer_detail_btn" type="submit" class="btn btn-block btn-success" value="Save" />
                          </td></tr>';    
                    }
                    else{
                        // iterate over your results once and collect them to a string
                        $stdSubjectOptions = '';
                        while($subject_result = mysqli_fetch_assoc($stdsubject)){
                            $stdSubjectOptions .= '<option value="'.$subject_result['id'].'">'.$subject_result['name'].'</option>';
                        }
                      // iterate over your results once and collect them to a string
                      $teachers = '';
                      while($teacher_result = mysqli_fetch_assoc($all_branch_teacher_raw)){
                          $teachers .= '<script>console.log('.$teacher_result['id'].')</script>';
                          $teachers .= '<option value="'.$teacher_result['id'].'">'.$teacher_result['first_name'].' '.$teacher_result['last_name'].'</option>';                         
                      } 
                      for($i=0; $i<$subject_count_result['subject_count']; $i++){
                          echo '<tr><td></td><td><select name="subject_id[]" id="subject_id[]" class="form-control"><option value="select" >Select</option>';
                          // just output previously formed string
                          echo $stdSubjectOptions;    
                          echo '</select></td><td>';
                          echo '<select name="teacher_id[]" id="teacher_id[]" class="form-control"><option value="select" >Select</option>';
                          // just output previously formed string
                          echo $teachers;
                          echo '</select>';
                          echo '</td><td></td></tr>';
                      }
                          echo '<tr><td></td><td></td><td>
                          <input id="subject_techer_detail_btn" type="submit" class="btn btn-block btn-success" value="Save" />
                          </td></tr>';    
                    }
                  ?>
                  </tbody>
                </table>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
	</div>
      <!--div class="row">
      </div-->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script type="text/javascript">
    $(document).ready(function(){
         $("#basic_detail_btn").click(function(e){
            var curr = $(this);
            if( $("#standard_id").val()=="select"){
                alert('Please select a valid section');
            }
            else if ($("#section_id").val()=="select"){
                alert('Please select a valid standard');
            }
             else if ($("#teacher_id").val()=="select"){
                alert('Please select a valid teacher');
            }
            else{
                $("successMessage").removeClass('alert-danger');
                  section_id = $("#section_id").val();
                  standard_id = $("#standard_id").val();
                  teacher_id = $("#teacher_id").val();
                  // Call ajax for pass data to other place
                  $.ajax({
                      type: 'POST',
                      url: 'process_class_teacher_submission.php',
                      data: {
                            teacher_id:teacher_id,
                            standard_id:standard_id,
                            section_id:section_id
                    }, 
                      success: function (data) {
                        $("#message").addClass('alert-success');
                        $("#message").html(data);
                        alert(data);
                  },  
              });
            }
        });
        $("form").submit(function(e){
          var curr = $(this);
          e.preventDefault();
          $("successMessage").removeClass('alert-danger');
          section_id = $("#section_id").val();
          standard_id = $("#standard_id").val();
          $("#getSectionId").val(section_id);
          $("#getStandardId").val(standard_id);
          var formData = new FormData(this);
          $.ajax({
              type: 'POST',
              url: 'process_subject_teacher.php',
              data: formData, 
              processData: false,
              contentType: false,
              success: function (data) {
                $("#message").addClass('alert-success');
                $("#message").html(data);
                alert(data);
              },  
          });
        });
      $('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');
    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');
    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
	   });
       $("#student_reg_btn").click(function(e){
          reg_no = $("#student_reg_no").val();
          section_id = $("#section_id").val();
          standard_id = $("#standard_id").val();
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: 'register_student.php',
              data: {
                reg_no:reg_no,
                standard_id:standard_id,
                section_id:section_id
                }, 
              success: function (data) {
                $("#message").addClass('alert-success');
                $("#message").html(data);
                alert(data);
              },  
          });
        });
    });
    </script>
<?php include('../../footer.php'); ?>