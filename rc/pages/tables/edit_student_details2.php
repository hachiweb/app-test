<?php
include('../../header.php');
?>    

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Student
        <small>View and edit records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Student Record</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form method="POST" action="edit_details.php">
                <table>
  
                <?php 
                require_once "../../dbconnect.php";
                $db           = new DB();
                $id = $_GET['id'];
                $select_data  = "SELECT * FROM student WHERE id = '$id'";
                $student_data = $db->executeQuery($select_data);
                $student_data = mysqli_fetch_assoc($student_data);
                ?>


                  <tr>
                  <td><label class="m-2" for="">Registration No</label></td>
                  <td><input type="text" name="reg_no" value="<?php echo $student_data['registration_id']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">First Name</label></td>
                  <td><input type="text" name="fname" value="<?php echo $student_data['first_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Last Name</label></td>
                  <td><input type="text" name="lname" value="<?php echo $student_data['last_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">UIDAI</label></td>
                  <td><input type="text" name="uidai" value="<?php echo $student_data['UIDAI']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">DOB</label></td>
                  <td><input type="text" name="dob" value="<?php echo $student_data['dob']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Email</label></td>
                  <td><input type="text" name="email" value="<?php echo $student_data['email']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Address</label></td>
                  <td><input type="text" name="addr" value="<?php echo $student_data['address']; ?>"></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Mother Name</label></td>
                  <td><input type="text" value="<?php echo $student_data['mother_name']; ?>" readonly></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Father Name</label></td>
                  <td><input type="text" value="<?php echo $student_data['father_name']; ?>" readonly></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Guardian Name</label></td>
                  <td><input type="text" value="<?php echo $student_data['guardian_name']; ?>" readonly></td>
                </tr>
                <tr>
                  <td><label class="m-2" for="">Blood Group</label></td>
                  <td><input type="text" value="<?php echo $student_data['blood_group']; ?>" readonly></td>
                </tr>
                <input type="hidden" value="<?php echo $student_data['id']; ?>" name="id">
                </table>

                <div><input class="ml-15 m-3" type="submit" value="Update"></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include('../../footer.php'); ?>
 