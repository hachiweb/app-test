<?php
if (!isset($_SESSION)) {
    session_start();
}
$site_url = 'https://' . $_SERVER['HTTP_HOST'];
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
?>    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Teacher
        <small>View records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">manage-teacher</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Teacher Record</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Serial No.</th>
                  <th>Allotted ID</th>
                  <th>Name</th>
                  <th>Prefix</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Brunch Code</th>
                  <th>edit</th>
                </tr>
                <?php
                $branch_id = $_SESSION["branch_id"];
                $db = new DB();
                //branch code
                $branch_id_name = "SELECT * FROM branch WHERE id='$branch_id'";
                $select_branch_name = $db->executeQuery($branch_id_name);
                $branch_name = mysqli_fetch_array($select_branch_name);
                //extract from teacher table
                $teacher_query = "SELECT * FROM teacher WHERE branch_id='$branch_id'";
                $select_data = $db->executeQuery($teacher_query);
                $n = 1;
                while ($teacher_data = mysqli_fetch_array($select_data)) {
                ?>
                  <tr style="<?php if(isset($_GET['id'])){
                    if($_GET['id'] == $teacher_data['id']){
                      echo 'background: #00FFFF';
                    };
                  }; ?>">
                    <td><?php echo $n++ ?></td>
                    <td><?php echo $teacher_data['allotted_id']; ?></td>
                    <td><?php echo $teacher_data['first_name'].' '.$teacher_data['last_name']; ?></td>
                    <td><?php echo $teacher_data['prefix']; ?></td>
                    <td><?php echo $teacher_data['email']; ?></td>
                    <td><?php echo $teacher_data['contact']; ?></td>
                    <td><?php echo $branch_name['branch_code']; ?></td>
                    <td><a href="edit_teacher_details.php?id=<?php echo $teacher_data['id']; ?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"><a/></td>
                  </tr>
                <?php }; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>