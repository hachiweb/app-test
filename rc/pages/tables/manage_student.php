<?php
if (!isset($_SESSION)) {
    session_start();
}
$site_url = 'https://' . $_SERVER['HTTP_HOST'];
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
?>    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Student
        <small>View and edit records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Student Record</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Registration No</th>
                  <th>Name</th>
                  <th>UIDAI</th>
                  <th>DOB</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Mother Name</th>
                  <th>Father Name</th>
                  <th>Guardian Name</th>
                  <th>Blood Group</th>
                  <th>Edit</th>
                </tr>
                <?php
                $db = new DB();
                $student_query = "SELECT * FROM student";
                $select_data = $db->executeQuery($student_query);
                while ($student_data = mysqli_fetch_array($select_data)) {
                ?>
                  <tr style="">
                    <td><?php echo $student_data['registration_id']; ?></td>
                    <td><?php echo $student_data['first_name'].' '.$student_data['last_name']; ?></td>
                    <td><?php echo $student_data['UIDAI']; ?></td>
                    <td><span class="label label-success"><?php echo $student_data['dob']; ?></span></td>
                    <td><?php echo $student_data['email']; ?></td>
                    <td><?php echo $student_data['address']; ?></td>
                    <td><?php echo $student_data['mother_name']; ?></td>
                    <td><?php echo $student_data['father_name']; ?></td>
                    <td><?php echo $student_data['guardian_name']; ?></td>
                    <td><?php echo $student_data['blood_group']; ?></td>
                    <td><a href="edit_student_details.php?id=<?php echo $student_data['id']; ?>"><img src="https://image.flaticon.com/icons/png/512/97/97841.png" width="20" height="20"><a/></td>
                  </tr>
                <?php }; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include('../../footer.php'); ?>