<?php
require_once "../../dbconnect.php";
$db = new DB();
$contact = $_POST['status'];
$id = $_POST['id'];
$registration_no = $_POST['registration_no'];
$school_code = $_POST['school_code'];
$school_name = $_POST['school_name'];
$school_logo = basename($_FILES['school_logo']['name']);
$dir = '../forms/assets/school/' . $id . '/logo';
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}
$school_logomove = $dir . '/' . $school_logo;
if (move_uploaded_file($_FILES['school_logo']['tmp_name'], $school_logomove)) {
    $school_data = "UPDATE school SET registration_no='$registration_no', school_code='$school_code', school_name='$school_name', school_logo='$school_logo', status='$status' WHERE id='$id' ";
    $school_query = $db->executeQuery($school_data);
}
header("location: manage_school.php?id=$id");
?>