<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_POST['section_id'])) {
    $_SESSION['section_id'] = $_POST['section_id'];
}
;
if (isset($_POST['standard_id'])) {
    $_SESSION['standard_id'] = $_POST['standard_id'];
}
;
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
    $branch_id = $_SESSION["branch_id"];
}
include '../../header.php';
$db = new DB();
$teacher_sql = "SELECT * FROM `teacher`";
$standard_sql = "SELECT * FROM `standard`";
$section_sql = "SELECT * FROM `section`";
$class_teacher = "SELECT * FROM `class-teacher` WHERE `standard_id`=1 AND `section_id` = 1";
$result = $db->executeQuery($class_teacher);
$value = mysqli_fetch_assoc($result);
$subject_sql = "SELECT name, id FROM subject GROUP BY name";
$stu_strength = "SELECT COUNT(`id`) FROM `student` WHERE `standard_id`=1 AND `section_id`=2";
$teacher_raw = $db->executeQuery($teacher_sql);
$standard_raw = $db->executeQuery($standard_sql);
$section_raw = $db->executeQuery($section_sql);
$subject_raw = $db->executeQuery($subject_sql);
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Select Class
        <small>Select Standard and Section</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Class Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Basic Detail</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <!--input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div-->
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th></th>
                  <th>Standard</th>
                  <th><select name="standard_id" id="standard_id" class="form-control">        <option value="select" >Select</option>
										<?php while($result = mysqli_fetch_assoc($standard_raw)){echo '
										<option  value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									</select></th>
                  <th>Section</th>
                  <th><select name="section_id" id="section_id" class="form-control">        <option value="select" >Select                   </option>										<?php while($result = mysqli_fetch_assoc($section_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									    </select></th>
                  <th>Academic Session</th>
                  <th>2018-19</th>
                </tr>
                 <tr class="toggle-basic-table">
                  <th></th>
                  <th colspan=2>Class Teacher</th>
                  <th>
                      <?php if(isset($value) and !empty($value)){
                          echo '<input type="text" disable="disabled" value="'.$value['teacher'].'" />';
                      }
                      else{
                          echo '<select name="teacher_id" id="teacher_id" class="form-control">        <option value="select" >Select</option>';
									    while($result = mysqli_fetch_assoc($teacher_raw)){echo '
										<option value="'.$result['id'].'">'.$result['first_name'].' '.$result['last_name'].'</option>'; }
									echo '</select></th>';
                      }
                      ?>
                  <th colspan=2>Total Students</th>
                  <th>9</th>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <!-- /.box -->
          <div class="box toggle-student-table">
            <div class="box-header">
              <h3 class="box-title">Student Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed ">
                <tbody><tr>
                  <th style="width: 10px"></th>
                  <th>Roll No</th>
                  <th>Name</th>
                  <th>Admission No</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td>
                  </td>
                  <td>
                  </td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <!-- /.box -->
          <div class="box toggle-subject-table">
            <div class="box-header">
              <h3 class="box-title">Subject-Teacher Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped toggle-subject-table">
                <tbody><tr>
                  <th style="width: 10px"></th>
                  <th>Subject</th>
                  <th>Teacher</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td>
                  </td>
                  <td>
                  </td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script type="text/javascript">
    $(document).ready(function(){
         $("#section_id, #standard_id").change(function(e){
            if($("#section_id").val()=="select"){
                // alert('Please select a valid section');
            }
            else if($("#standard_id").val()=="select"){
                // alert('Please select a valid standard');
            }
             else {
                var section_id = $("#section_id").val();
                var standard_id = $("#standard_id").val();
                <?php  ?>
                 $.ajax({
                  type: 'POST',
                  url: 'class-detail.php',
                  data: {
                        section_id:section_id,
                        standard_id:standard_id
                      }, 
                  success: function (data) {
                      window.location.href = 'manage_student.php';
                  },  
               });
            }
        });
        $("form").submit(function(e){
            var curr = $(this);
          e.preventDefault();
          $("successMessage").removeClass('alert-danger');
          teacher_id = $("#teacher_id").val();
          standard_id = $("#standard_id").val();
          section_id = $("#section_id").val();
          subject_id = $("#subject_id").val();
          // Call ajax for pass data to other place
          $.ajax({
              type: 'POST',
              url: 'process_subject_teacher_submission.php',
              data: {
                    teacher_id:teacher_id,
                    standard_id:standard_id,
                    section_id:section_id,
                    subject_id:subject_id
              }, 
              success: function (data) {
                $("#successMessage").addClass('alert-success');
                $("#successMessage").html(data);
                alert(data);
              },  
          });
      });
    });
    </script>
<?php include('../../footer.php'); ?>