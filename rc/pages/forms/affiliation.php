<?php 
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
?> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Affiliation Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Affiliation Form</li>
    </ol>
  </section>
    <div class="alert">
        <span id="successMessage"> </span>
        <span id="errorMessage"> </span>
    </div>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Affiliation Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="" onsubmit="submitAffiliation();">
            <div class="box-body">
              <div class="form-group">
                <label for="affiliation_code">Affiliation Code</label>
                <input type="text" class="form-control" id="affiliation_code" name="affiliation_code" required>
              </div>
              <div class="form-group">
                <label for="affiliation_body">Affiliation Body</label>
                <input type="text" class="form-control" id="affiliation_body" name="affiliation_body" required>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn_custom">Submit</button>
            </div>                
          </form>
        </div><!-- /.box -->
      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
 <script type="text/javascript">
    $(document).ready(function(){
        $("form").submit(function(e){
          e.preventDefault();
          $("successMessage").removeClass('alert-success');
          var affiliation_code = $('#affiliation_code').val();
          var affiliation_body = $('#affiliation_body').val();
          // Call ajax for pass data to other place
          $.ajax({
              type: 'POST',
              url: 'process_affiliation_submission.php',
              data: {
                affiliation_code:affiliation_code,
                affiliation_body:affiliation_body
              },
              success: function (data) {
                $("#successMessage").addClass('alert-success');
                $("#successMessage").html(data);
                alert(data);
              },  
          });      
      });
    });
</script>
<?php include('../../footer.php'); ?>