<?php
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
    $branch_id = $_SESSION["branch_id"];
}
include '../../header.php';
$db = new DB();
$teacher_sql = "SELECT * FROM `teacher`";
$standard_sql = "SELECT * FROM `standard`";
$section_sql = "SELECT * FROM `section`";
$exam_sql = "SELECT * FROM `exam_meta` WHERE `branch_id`='$branch_id'";
$class_teacher = "SELECT * FROM `class-teacher` WHERE `standard_id`=1 AND `section_id` = 1";
$result = $db->executeQuery($class_teacher);
$value = mysqli_fetch_assoc($result);
$subject_sql = "SELECT name, id FROM subject WHERE `branch_id`='$branch_id' GROUP BY name";
$stu_strength = "SELECT COUNT(`id`) FROM `student` WHERE `standard_id`=1 AND `section_id`=2";
$teacher_raw = $db->executeQuery($teacher_sql);
$standard_raw = $db->executeQuery($standard_sql);
$section_raw = $db->executeQuery($section_sql);
$subject_raw = $db->executeQuery($subject_sql);
$exam_raw = $db->executeQuery($exam_sql);
$year = date("Y")."-".(date("y")+1);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Select Report Card
      <small>preview of class details</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Select Report Card</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Basic Detail</h3>
            <?php /* ?>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
            <?php */ ?>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
          <form action="../tables/student-report-list.php" method="post">
            <table class="table table-hover">
                  <tr>
                  <th>Standard</th>
                  <th>Section</th>
                  <th>Academic Session</th>
                </tr>
                <tr>
                  <td><select name="standard_id" id="standard_id" class="form-control"><option value="select" >Select</option><?php while($result = mysqli_fetch_assoc($standard_raw)){echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?></select></td>
                  <td><select name="section_id" id="section_id" class="form-control"><option value="select" >Select</option><?php while($result = mysqli_fetch_assoc($section_raw)){ echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?></select></td>
                  <td><select name="year" id="year" class="form-control" style="width:200px;"><?php $j="1";for ($i=2000; $i < ($year+1); $i++) { 
                    if ($j<10) {
                      $j="0".$j;
                    }
                    ?><option value="<?=$i."-".$j;?>" <?php if ($i==$year) {
                    echo "selected";
                  }?>><?php echo $i."-".$j;?></option><?php $j++ ; } ?>
                  </select></td>
                </tr>
            </table>
            <div class="text-center">
            <button type="submit" id="submit_btn" class="btn btn_custom">Submit</button>
            </div>
            </form>
          </div><!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div><!-- row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->     
<?php include('../../footer.php'); ?>