<?php 
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
$db = new DB();
$branch_query = "SELECT * FROM `branch`";
$branch = $db->executeQuery($branch_query);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Standard Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Standard Form</li>
    </ol>
    <div id="successMessage" class="alert"></div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Standard Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST">
            <div class="box-body">
              <div class="form-group">
                <label for="standard_name">Name</label>
                <input type="text" class="form-control" id="standard_name" name="standard_name" required>
              </div>
              <?php /*<div class="form-group">
                <label for="branch_id">Branch</label>
                    <select id="branch_id" name="branch_id" class="form-control">
                      <?php while($result = mysqli_fetch_assoc($branch)){
                      echo '<option value="'.$result['id'].'">'.$result['branch_code'].'</option>'; }?>
                    </select> 
              </div>*/?>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn_custom">Submit</button>
            </div>                
          </form>
        </div><!-- /.box -->
      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(){
        $("form").submit(function(e){
          e.preventDefault();
          $("successMessage").removeClass('alert-danger');
          var standard_name = $('#standard_name').val();
          var branch_name = $('#branch_id').val();
          // Call ajax for pass data to other place
          $.ajax({
              type: 'POST',
              url: 'process_standard_submission.php',
              data: {
                standard_name:standard_name,
                branch_name:branch_name               
              },
              success: function (data) {
                $("#successMessage").addClass('alert-success');
                $("#successMessage").html(data);
                alert(data);
              },  
          });      
      });
    });
</script>
<?php include('../../footer.php'); ?>