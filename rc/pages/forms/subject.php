<?php 
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
$db = new DB();
$standard_query = "SELECT * FROM `standard`";
$section_query = "SELECT * FROM `section`";
$standard = $db->executeQuery($standard_query);
$section = $db->executeQuery($section_query);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subject Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Subject Form</li>
        </ol>
        <div id="successMessage" class="alert"></div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8 col-md-offset-2">
                <!-- general form elements -->
                <div class="box box-primary mt-5">
                    <div class="box-header with-border">
                        <h3>Subject Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="subject_code">Code</label>
                                <input type="text" class="form-control" id="subject_code" name="code" required>
                            </div>
                            <div class="form-group">
                                <label for="subject_name">Name</label>
                                <input type="text" class="form-control" id="subject_name" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="subject_subcatogery">Subcategory</label>
                                <input type="text" class="form-control" id="subject_subcatogery" name="subcategory"
                                    required>
                            </div>
                            <!-- <div class="form-group">
                                <p>Select subject type</p>
                                <span><input type="radio" name="is_scholastic" class="is_scholastic"
                                        value="1" />Scholastic</span>
                                <span><input type="radio" name="is_scholastic" class="is_scholastic"
                                        value="0" />Co-Scholastic</span><br />
                            </div> -->
                            <div class="form-group">
                                <label for="subject_description">Description</label>
                                <textarea class="form-control" id="description" name="description" rows="5"></textarea>
                            </div>
                            <div class="form-group" id="standard">
                                <label for="standard_id">Standard</label><br>
                                <?php while($result = mysqli_fetch_assoc($standard)){
                                    if (is_numeric($result['name'])) {
                                      if (($result['name'])<11) {
                                        echo '<input type="checkbox" class="standard_id" name="standard_id[]" style="margin-left:10px;" value="'.$result['id'].'">'.$result['name'];
                                      }
                                    }
                                  }
                                  ?>
                            </div>
                            <input type="hidden" id="process_standard_id" />
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn_custom">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!-- col -->
        </div><!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function() {
    $(".standard_id").click(function() {
        var curr = $(this);
        var standard_id = '';
        if ($('#standard input:checked').length == 0) {
            alert('Please select atleast one standard');
            curr.prop('checked', true);
        } else {
            if (curr.is(":checked")) {
                var n = $("#process_standard_id").val().indexOf(curr.val());
                if (n == -1) {
                    standard_id = $("#process_standard_id").val();
                    if (standard_id == '') {
                        $("#process_standard_id").val(curr.val());
                    } else {
                        $("#process_standard_id").val(standard_id + ',' + curr.val());
                    }
                }
            } else {
                $("#process_standard_id").val($("#process_standard_id").val().replace(curr.val() + ',',
                    ''));
            }
        }
        // alert($("#process_standard_id").val());          
    });
    $("form").submit(function(e) {
        e.preventDefault();
        $("successMessage").removeClass('alert-danger');
        if ($('#standard input:checked').length == 0) {
            alert('Please select atleast one standard');
        } else {
            var code = $('#subject_code').val();
            var name = $('#subject_name').val();
            var standard_id = $('#process_standard_id').val();
            var description = $('#description').val();
            var subcategory = $('#subject_subcatogery').val();
            var is_scholastic = $('input[name=is_scholastic]:checked').val();
            $.ajax({
                type: 'POST',
                url: 'process_subject_submission.php',
                data: {
                    code: code,
                    name: name,
                    description: description,
                    standard_id: standard_id,
                    subcategory: subcategory,
                    is_scholastic: is_scholastic,
                },
                success: function(data) {
                    $("#successMessage").addClass('alert-success');
                    $("#successMessage").html(data);
                    alert(data);
                },
            });
        }
    });
});
</script>
<?php include('../../footer.php'); ?>