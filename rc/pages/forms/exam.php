<?php 
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
    
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
    include('../../header.php'); 
    $db = new DB();

    $sql="SELECT b.id, b.branch_code,b.branch_city,s.school_name FROM branch b LEFT JOIN school s ON b.school_id = s.id ORDER BY b.id";

    $raw = $db->executeQuery($sql);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Exam Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Exam Form</li>
    </ol>
    <div id="successMessage" class="alert"></div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Exam Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

          <div class="form-group">
              
          <form role="form">
            <div class="box-body">
            <label for="branch_id">Branch</label>
            <input type="hidden" name="branch_id" id="branch_id" value="echo $_SESSION['branch_id'];?>" />
              <div class="form-group">
                <label for="exam_name">Name</label>
                <input type="text" class="form-control" id="exam_name"  name="name" required>
              </div> 


              


              <div class="form-group">
                <label for="weightage"z>weightage</label>
                <input type="text" class="form-control" id="weightage" name="weightage" required>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn_custom">Submit</button>
            </div>                
          </form>
        </div><!-- /.box -->

      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">

    $(document).ready(function(){

        $("form").submit(function(e){

          e.preventDefault();
          var formData = new FormData(this);
          $("successMessage").removeClass('alert-danger');

          $.ajax({

              type: 'POST',
              url: 'process_exam_submission.php',
              data: formData,              

              success: function (data) {

                $("#successMessage").addClass('alert-success');

                $("#successMessage").html(data);

                alert(data);

                document.getElementById("schoolRegistration").reset();

              }, 
              cache: false,
              contentType: false,
              processData: false 

              

          });

      });

    });

</script>
<?php include('../../footer.php'); ?>