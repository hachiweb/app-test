<?php include('../../header.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Evaluator Details
      <small>preview of class details</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Evaluator Detail</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">   
    <div class="row">
      <div class="col-md-8">        
        <div class="box student-table-bx">
          <div class="box-header">
            <h3 class="box-title">Student Detail</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <table class="table table-condensed">
              <tr>
                <th class="col-xs-3">Students Name</th>
                <th class="col-xs-3">Subject Marks</th>
                <th class="col-xs-3">Maximum Marks</th>
                <th class="col-xs-3">Notebook & subject Enrichment</th>
              </tr>
              <tr>
                <td><div class="custom_td_bx">Rajesh Kumar</div></td> 
                <td><div class="custom_td_bx">100</div></td>
                <td><div class="custom_td_bx">100</div></td>
                <td><div class="custom_td_bx">Value</div></td>
              </tr>
              <tr>
                <td><div class="custom_td_bx">Rohit Kumar</div></td>
                <td><div class="custom_td_bx">100</div></td>
                <td><div class="custom_td_bx">100</div></td>
                <td><div class="custom_td_bx">Value</div></td>
              </tr>
              <tr>
                <td><div class="custom_td_bx">Sandeep Singh</div></td>
                <td><div class="custom_td_bx">100</div></td>
                <td><div class="custom_td_bx">100</div></td>
                <td><div class="custom_td_bx">Value</div></td>
              </tr>
            </table>
          </div>            
        </div><!-- /.box -->
      </div><!-- /.col -->        
    </div><!-- row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include('../../footer.php'); ?>