<?php include('../../header.php'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Annual Details
      <small>preview of class details</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Annual Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Periodic Test 1</h3>
          </div>
          <!-- /.box-header -->
          <!-- table start -->
          <div class="custom-table annual_info_table pd-10">
            <h3 class="bg-blue pd-10">Part-1: Scholastic Area</h3>
            <table class="table table-bordered scholastic_area_table">
              <thead>                
                <tr class="text-center">
                  <th rowspan="2">S.No</th>
                  <th rowspan="2">Subjects</th>
                  <th class="text-center bg-blue-light">Periodic Test</th>
                  <th rowspan="2">Notebook & subject Enrichment</th>
                  <th rowspan="2">Total</th>
                  <th rowspan="2">Grand Total</th>
                  <th rowspan="2">Grade</th>
                </tr>
                <tr>
                  <th>PT 1</th>
                </tr>
              </thead>
              <tbody>
                <tr class="text-center pd-10 bg-blue-light">
                  <td></td>
                  <td></td>
                  <td>(10)</td>
                  <td>(8=10)</td>
                  <td>(A+B=20)</td>
                  <td>(A+B=100)</td>
                  <td></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>English</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Hindi / Urdu / Arabic</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Mathematics</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Science</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Social Science</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div><!-- table bx -->
        </div><!-- /.box -->

      </div><!-- col -->
    </div><!-- row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include('../../footer.php'); ?>