<?php
require_once '../../dbconnect.php';
$db = new DB();
$registration_no = $_POST['school_registration_no'];
$school_code = $_POST['school_code'];
$school_name = $_POST['school_name'];
$affiliation_id = $_POST['affiliation_id'];
$school_logo = basename($_FILES['school_logo']['name']);
$school_status = $_POST['school_status'];
$last_id = "SELECT id FROM `school` ORDER BY id DESC";
$last_id = $db->executeQuery($last_id);
$last_id = mysqli_fetch_assoc($last_id);
$current_id = (int) $last_id['id'] + 1;
$dir = 'assets/school/' . $current_id . '/logo';
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}
$school_logomove = $dir . '/' . $school_logo;
if (move_uploaded_file($_FILES['school_logo']['tmp_name'], $school_logomove)) {
    $sql = "INSERT INTO `school`(`registration_no`,`school_code`,`school_name`,`affiliation_id`,`school_logo`,`status`) VALUES ('$registration_no','$school_code','$school_name','$affiliation_id','$school_logo','$school_status')";
    $result = $db->executeQuery($sql);
    if ($result) {
        $message = "School has successfully been registered";
    } else {
        $message = "School could not be registered ";
    }
} else {
    $message = "Logo upload failed";
}

echo $message;
?>    