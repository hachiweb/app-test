<?php
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
}
include '../../header.php';
$db = new DB();
$teacher_sql = "SELECT * FROM `teacher`";
$standard_sql = "SELECT * FROM `standard`";
$section_sql = "SELECT * FROM `section`";
$subject_sql = "SELECT name, id FROM subject GROUP BY name";
$teacher_raw = $db->executeQuery($teacher_sql);
$standard_raw = $db->executeQuery($standard_sql);
$section_raw = $db->executeQuery($section_sql);
$subject_raw = $db->executeQuery($subject_sql);
?>
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Subject Teacher Form
				<small>Preview</small>
			</h1>
			<ol class="breadcrumb">
				<li>
					<a href="#">
						<i class="fa fa-dashboard"></i> Home
					</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Subject Teacher Form</li>
			</ol>
			<div id="successMessage" class="alert"></div>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-8 col-md-offset-2">
					<!-- general form elements -->
					<div class="box box-primary mt-5">
						<div class="box-header with-border">
							<h3>Subject Teacher Form</h3>
						</div>
						<!-- /.box-header -->
						<!-- form start -->
						<form role="form">
							<div class="box-body">
								<div class="form-group">
									<label for="st_teacher_id">Teacher</label>
									<select name="teacher_id" id="teacher_id" class="form-control">
										<?php while($result = mysqli_fetch_assoc($teacher_raw)){echo '
										<option value="'.$result['id'].'">'.$result['first_name'].' '.$result['last_name'].'</option>'; }?>
									</select>
								</div>
								<div class="form-group">
									<label for="standard_id">Standard</label>
									<select name="standard_id" id="standard_id" class="form-control">
										<?php while($result = mysqli_fetch_assoc($standard_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									</select>
									</div>
									<div class="form-group">
										<label for="st_section_id">Section</label>
										<select name="section_id" id="section_id" class="form-control">
										<?php while($result = mysqli_fetch_assoc($section_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									    </select>
									</div>
									<div class="form-group">
										<label for="st_subject_id">Subject</label>
										<select name="subject_id" id="subject_id" class="form-control">
										<?php while($result = mysqli_fetch_assoc($subject_raw)){echo '
										<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
									    </select>
									</div>
										</div>
										<!-- /.box-body -->
										<div class="box-footer">
											<button type="submit" class="btn btn_custom">Submit</button>
										</div>
									</form>
								</div>
								<!-- /.box -->
							</div>
							<!-- col -->
						</div>
						<!-- /.row -->
					</section>
					<!-- /.content -->
				</div>
				<!-- /.content-wrapper -->
	<script type="text/javascript">
    $(document).ready(function(){
        $("form").submit(function(e){
            var curr = $(this);
          e.preventDefault();
          $("successMessage").removeClass('alert-danger');
          teacher_id = $("#teacher_id").val();
          standard_id = $("#standard_id").val();
          section_id = $("#section_id").val();
          subject_id = $("#subject_id").val();
          // Call ajax for pass data to other place
          $.ajax({
              type: 'POST',
              url: 'process_subject_teacher_submission.php',
              data: {
                    teacher_id:teacher_id,
                    standard_id:standard_id,
                    section_id:section_id,
                    subject_id:subject_id
              }, 
              success: function (data) {
                $("#successMessage").addClass('alert-success');
                $("#successMessage").html(data);
                alert(data);
              },  
          });
      });
    });
    </script>
<?php include('../../footer.php'); ?>