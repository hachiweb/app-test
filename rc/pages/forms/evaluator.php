<?php 
if (!isset($_SESSION)) {
    session_start();
}
if (empty($_SESSION["username"])) {
    header("location:../auth/login.php");
    exit();
} else {
    $username = $_SESSION["username"];
    $alias = $_SESSION["alias"];
    $role = $_SESSION["role"];
    $branch_id = '1';
}
include '../../header.php';
$db = new DB();
$subject_sql = "SELECT name, id FROM subject GROUP BY name";
$subject = $db->executeQuery($subject_sql);
$exam_sql = "SELECT * FROM `exam_meta`";
$exam = $db->executeQuery($exam_sql);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Evaluator Form
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Evaluator Form</li>
    </ol>
    <div class="alert" id="successMessage" ></div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary mt-5">
          <div class="box-header with-border">
            <h3>Evaluator Form</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form">
            <div class="box-body">
              <div class="form-group">
                <label for="exam">Exam</label>
                <select name="exam" id="exam" class="form-control" required>
                  <?php while($result = mysqli_fetch_assoc($exam)){
                   echo '<option value="'.$result['id'].'">'.$result['name'].'(Max Marks: '.$result['weightage'].')'.'</option>'; }?>
                </select>
              </div>
              </div>
              <div class="form-group" id="subject">
                <label for="exam">Subjects</label>
                 <?php while($result = mysqli_fetch_assoc($subject)){
                      echo '<input type="checkbox" class="subject" name="subject[]" style="margin-left:10px;" value="'.$result['name'].'">'.$result['name'];
                    }
                    ?>
                <input type="hidden" id="process_subject" name="process_subject"/>
              </div>
              <!--div class="form-group">
                <label for="language">Language</label>
                <select class="form-control" id="language" name="language" required>
                  <option value="Englis">English</option>
                  <option value="Hindi">Hindi</option>
                  <option value="Urdu">Urdu</option>
                  <option value="Telgu">Telgu</option>
                </select> 
              </div-->
              <div id="evaluatorContainer" class="box-body">
                <div class="table table-responsive">
              <table class="table table-responsive table-striped table-bordered">
                <thead>
                  <tr>
                    <!--td>Standard</td>
                    <td>Section</td-->
                    <td>Parameter</td>
                    <td>Is Grade</td>
                    <td>Maximum Marks</td>
                    <!--td>Grade</td-->
                    <td>BTN</td>
                  </tr>
                </thead>
                <tbody id="EvaluatorContainer">
                    <?php /*td>
                     <select name="standard_id[]" class="form-control"> 
                        <?php while($result = mysqli_fetch_assoc($standard)){ echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
                     </select>
                     </td>
                     <td>
                     <select name="section_id[]" class="form-control"> 
                        <?php while($result = mysqli_fetch_assoc($section)){ echo '<option value="'.$result['id'].'">'.$result['name'].'</option>'; }?>
                     </select>
                    </td */?> 
                    <td><input type="text" class="form-control parameter" id="parameter" name="parameter[]" required></td> 
        		    <td><select class="form-control is_grade" name="is_grade[]">
            		      <option value="1">Yes</option>
            		      <option value="0" selected>No</option>
            		    </select></td>
            		 <td><input type="number" class="form-control maximum_marks" id="maximum_marks[]" name="maximum_marks[]" ></td> 
                     <input type="hidden" id="process_subject" />
            		 <!--td><input type="text" class="form-control grade" id="grade[]" name="grade[]" disabled="disabled"></td-->
            		 <td><button type="button" class="btn btn-danger" disabled="disabled"><i class="glyphicon glyphicon-remove-sign"></i></button></td>
                </tbody>
                <tfoot>
                  <!--tr>
                    <th colspan="5">
                      <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls">
                        <i class="glyphicon glyphicon-plus-sign"></i>
                        &nbsp; Add&nbsp;
                      </button>
                    </th>
                  </tr-->
                </tfoot>
              </table>
            </div>  
              </div>
              <div style="padding: 10px;">
                <div><label>To add fiels click on Add Button</label></div>
                <button id="evaluatorBtnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls">
                  <i class="glyphicon glyphicon-plus-sign"></i>
                  &nbsp; Add&nbsp;
                </button>
              </div>      
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn_custom">Submit</button>
            </div>                
          </form>
        </div><!-- /.box -->
      </div><!-- col -->
    </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('change','.is_grade', function(){
        var curr = $(this);
      if(curr.val()=='1'){
        curr.parent().next().children().val('');
        curr.parent().next().children().prop("disabled", true);
      }
      else{
        curr.parent().next().children().prop("disabled", false);
      }
    });
       $(".subject").click(function(){
        var  curr = $(this);
        var subject = '';
        if($('#subject input:checked').length==0){
          alert('Please select atleast one subject');
          curr.prop('checked', true);
        }
        else{         
          if(curr.is(":checked")){
            var n = $("#process_subject").val().indexOf(curr.val());
            if(n==-1){
              subject = $("#process_subject").val();  
              if(subject==''){
                $("#process_subject").val(curr.val());
              }
              else{                    
                $("#process_subject").val(subject+','+curr.val());
              }   
            }
          }  
          else{
            $("#process_subject").val($("#process_subject").val().replace(curr.val()+',',''));
          }
        }      
      });
    $("form").submit(function(e){
    e.preventDefault();
    $("#successMessage").html('');
    $("successMessage").removeClass('alert-danger');
    var formData = new FormData(this);
    var parameter = [];
    var exam = $('#exam').val();
    var subjects = $("#process_subject").val();
    $("input[name='parameter[]']").each(function() {
          parameter.push($(this).val());
      });
      var maximum_marks = [];
      var maximum_marks_validate = [];
      $("input[name='maximum_marks[]']").each(function() {
          if($(this).val()!='')
          maximum_marks_validate.push($(this).val());
          maximum_marks.push($(this).val());
      });
       var is_grade = [];
      $("select[name='is_grade[]']").each(function() {
          is_grade.push($(this).val());
      });
      var total = 0;
      $.each(maximum_marks_validate,function() {
        total += parseInt(this);
      });
      if(total==100){
          $.ajax({
              type: 'POST',
              url: 'process_evaluator_submission.php',
              data: {
                  exam:exam,
                  is_grade:is_grade,
                  parameter:parameter,
                  maximum_marks:maximum_marks,
                  subjects:subjects
                },              
              success: function (data) {
                $("#successMessage").addClass('alert-success');
                $("#successMessage").html(data);
                alert(data);
              }
        });
      }
      else{
        message = 'Sum of all maximum_marks must be equal to 100'; 
        $("#successMessage").addClass('alert-danger');
        $("#successMessage").html(message);
        alert(message);
      }
  });
});
</script>
<?php include('../../footer.php'); ?>