<?php 
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
  //  
    if(empty($_SESSION["username"])){
        header("location:../auth/login.php");
        exit();
    }
    else{
        $username = $_SESSION["username"];
        $alias = $_SESSION["alias"];
        $role = $_SESSION["role"];
    }
  include('../../header.php');
  $db = new DB();
  $sql="SELECT b.id, b.branch_code,b.branch_city,s.school_name FROM branch b LEFT JOIN school s ON b.school_id = s.id ORDER BY b.id";
  $raw = $db->executeQuery($sql);
?>



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Admin Form

      <small>Preview</small>

    </h1>

    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

      <li><a href="#">Forms</a></li>

      <li class="active">admin Form</li>

    </ol>
    <!--<div class="alert" id="message"></div> -->

  </section>



  <!-- Main content -->

  <section class="content">

    <div class="row">

      <!-- left column -->

      <div class="col-md-8 col-md-offset-2">

        <!-- general form elements -->

        <div class="box box-primary mt-5">

          <div class="box-header with-border">

            <h3>Admin Form</h3>

          </div>

          <!-- /.box-header -->

          <!-- form start -->

          <form role="form">

            <div class="box-body">

            <div class="form-group">

            <label for="branch_id">Branch</label>
            <input type="text" class="form-control" name="branch_id" id="branch_id" value="<?php echo $_SESSION['branch_id'];?>" />

            </div>

              <div class="form-group">

                <label for="name">Name</label>

                <input type="text" class="form-control" id="name" name="name" required>

              </div>

              <div class="form-group">

                <label for="email">Email</label>

                <input type="email" class="form-control" id="email" name="email" required>

              </div>

              <div class="form-group">

                <label for="phone">Phone</label>

                <input type="text" class="form-control" id="phone" name="phone">

              </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">

              <button type="submit" class="btn btn_custom">Submit</button>

            </div>                

          </form>

          </div>



      </div><!-- col -->

    </div><!-- /.row -->

  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->

<script type="text/javascript">

    $(document).ready(function(){
          jQuery('#phone').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g,'');
        });
        jQuery("#phone").keypress(function (e) {
            var length = jQuery(this).val().length;
            if (length > 9) {
                return false;
            }
        })

        $("form").submit(function(e){

          e.preventDefault();

          $("successMessage").removeClass('alert-danger');

          //var allotted_id = $('#allotted_id').val();

          var name = $('#name').val();

          //var last_name = $('#last_name').val();

          var branch_id = $('#branch_id').val();

          var email = $('#email').val();

          var phone = $('#phone').val();

      

        //   alert(allotted_id+first_name+last_name+branch_id+email+phone);

          // Call ajax for pass data to other place

          $.ajax({

              type: 'POST',

              url: 'process_admin_submission.php',

              data: {

               // allotted_id:allotted_id,

                name:name,

                branch_id:branch_id,

                email:email,

                contact:phone               

              },

              success: function (data) {

                $("#message").addClass('alert-success');

                $("#message").html(data);

                alert(data);

                

               /* console.log(data);   

                var parsed_data = JSON.parse(data);

                console.log(parsed_data.success); */

              },  

          });

      });

    });

</script>

<?php include('../../footer.php'); ?>
