<?php
require_once '../../dbconnect.php';
session_start();
$registration_id = $_POST['registration_no'];
$UIDAI = (int) trim(preg_replace('/\s+/', '', $_POST['uidai']));
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$student_picture = basename($_FILES['student_picture']['name']);
$dob = $_POST['dob'];
$academic_session = $_POST['academic_session'];
$date1 = new DateTime($dob);
$curr_date = date('Y-m-d');
$curr_date = new DateTime($curr_date);
$diff = $curr_date->diff($date1)->format("%a");
if ($diff <= 1277) {
    $data['flag'] = 0;
    $data['msg'] = "Student age must at least 3 years and 6 months";
}
$yoa = $_POST['yoa'];
$standard_id = $_POST['standard_id'];
$section_id = $_POST['section_id'];
$blood_group = $_POST['blood_group'];
$mother_name = $_POST['mother_name'];
$father_name = $_POST['father_name'];
$guardian_name = $_POST['guardian_name'];
$address = $_POST['address'];
$contact_no = (int) $_POST['contact'];
$email = $_POST['email'];
$branch_id = $_SESSION['branch_id'];
$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < 10; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}
$pw = $randomString;
$hashedpw = md5($pw);
$db = new DB();
$last_id = "SELECT id FROM `student` ORDER BY id DESC";
$last_id = $db->executeQuery($last_id);
$last_id = mysqli_fetch_assoc($last_id);
$current_id = (int) $last_id['id'] + 1;
$dir = 'assets/student/' . $current_id . '/picture';
if (!file_exists($dir)) {
    mkdir($dir, 0777, true);
}

$student_picturemove = $dir . '/' . $student_picture;
$data = array();
if (move_uploaded_file($_FILES['student_picture']['tmp_name'], $student_picturemove)) {
    $create_student_query = "INSERT INTO `users`(`username`,`alias`,`password`,`role`,`branch_id`) VALUES ('$registration_id','$first_name','$hashedpw','student','$branch_id')";
    $createStudent = $db->executeQuery($create_student_query);
    $sql = "INSERT INTO `student`(`registration_id`,`UIDAI`,`year_of_admission`,`first_name`,`last_name`,`picture`,`dob`,`blood_group`,`mother_name`,`father_name`,`guardian_name`,`address`,`contact_no`,`email`,`branch_id`,`standard_id`,`section_id`,`academic_session`) VALUES ('$registration_id','$UIDAI','$yoa','$first_name','$last_name','$student_picture','$dob','$blood_group','$mother_name','$father_name','$guardian_name','$address','$contact_no','$email','$branch_id','$standard_id','$section_id','$academic_session')";
    $result = $db->executeQuery($sql);
    if (isset($result) && isset($createStudent) && !empty($result) && !empty($createStudent)) {
        $success = "Student has successfully been registered. Please save the credentials- Username: " . $registration_id . ' Password: ' . $pw;
    } else {
        $success = "Student could not be registered";
    }
} else {
    $success = "Student picture upload failed";
}
echo $success;
?>