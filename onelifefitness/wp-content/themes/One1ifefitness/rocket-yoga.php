<?php /* Template Name: rocket-yoga*/
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
    <div class="_1vdzHPH">
        <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
            <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
                <h2>Rocket Yoga</h2>
                <div class="_1-4gF4V">
                    <p>Burning out your fuse at both ends? You wanna try Rocket, man. Developed in 80s San Fran, this is
                        a more playful way to yogi – riding a cosmic wave of energy to a stronger, more centred you. The
                        flow may be faster, but it’s the kind of escape you need.</p>
                </div>
                <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Cannon Street</span><span
                                class="Nfa775m">Ealing</span><span class="Nfa775m">Elephant and Castle</span><span
                                class="Nfa775m">Farringdon</span><span class="Nfa775m">Holborn</span><span
                                class="Nfa775m">Old Street</span><span class="Nfa775m">Victoria</span><span
                                class="Nfa775m">Westfield London</span></div>
                    </li>
                </ul>
                <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Select gym</option>
                        <option value="Westfield London">Westfield London</option>
                        <option value="Ealing">Ealing</option>
                        <option value="Old Street">Old Street</option>
                        <option value="Farringdon">Farringdon</option>
                        <option value="Cannon Street">Cannon Street</option>
                        <option value="Holborn">Holborn</option>
                        <option value="Victoria">Victoria</option>
                    </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Choose time</option>
                        <option value="1292582" disabled="">10:00 Sunday 16th (60 minutes) - Bookable 7am day before
                        </option>
                        <option value="1290867" disabled="">07:15 Wednesday 19th (60 minutes) - Bookable 7am day before
                        </option>
                    </select><a
                        href=""
                        target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
                <div class="_3srnCE8">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                    <h2>Your <br>instructors</h2>
                    <div class="Sa1FFQw">
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Fleur_Poad.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Fleur</h3>
                            <div>
                                <p>Fleur has trained in various yoga styles including Ashtanga,
                                    Mandala Vinyasa Flow, Yin and pre and post natal, and is always working on
                                    enhancing her knowledge. Fleur’s classes are fun and challenging, and open to
                                    both beginners and those at a more advanced level. </p>
                            </div>
                        </div>
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Saori_Funawatatri.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Saori </h3>
                            <div>
                                <p>After discovering yoga in 2002, Saori went on to train with Yoga London and Yoga
                                    People to become a qualified instructor. She teaches various types of yoga, from
                                    Rocket Yoga to Ashtanga, and aims to help her clients counteract the stresses of
                                    daily life in her classes.&nbsp; </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wqBYWJi">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Rocket2.jpg');"></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
   