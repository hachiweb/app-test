<?php /* Template Name: functional-training */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<section class="_1J0RKGy _2rbE6TC _2v5bHvx">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                </ol>
                <div class="carousel-inner" role="listbox">
                <div class="carousel-item active"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="_130-KDr">
        <div class="">
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">funct1onal circuit</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">combines cardiovascular conditioning (heart and lung endurance), functional strength training, balance activities, and flexibility and stretching exercises in an intense and encouraging atmosphere.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/b-o-l-t"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">tabata</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Tabata - is a high-intensity interval training that consists of eight sets of fast-paced exercises each performed for 20 seconds interspersed with a brief rest of 10 seconds.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/barfly"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">h11t</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">This Caveman-style circuit class goes back to basics – using intense based HIIT  training to give you hunter-gatherer agility, animalistic speed and the strength to take down an angry mammoth if required. All essential skills for Cork life.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/bartendaz"></a>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>