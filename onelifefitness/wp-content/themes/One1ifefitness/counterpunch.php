<?php /* Template Name: counterpunch */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
            <h2>Boxing circuit</h2>
            <div class="_1-4gF4V">
                <p>I would loose  </p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                    <span class="Nfa775m">Onelifefitness Camden quay</span>
                      <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                        </div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
            <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
                <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1296026">18:00 Friday 14th (45 minutes)</option>
                    <option value="1304602" disabled="">07:00 Monday 17th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1304091" disabled="">13:00 Tuesday 18th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1305719" disabled="">18:00 Tuesday 18th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1256071" disabled="">13:00 Thursday 20th (45 minutes) - Bookable 7am day before
                    </option>
                </select>
                <a href="https://indma05.clubwise.com/onelifefitness/index.html" target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book
                        class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Natalia_Kotowska_Extra_Option_1.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Natalia</h3>
                        <div>
                            <p>Originally from
                                Poland,&nbsp;Natalia&nbsp;has represented London and Poland internationally in
                                dance battles, panels and workshops, and in 2015&nbsp;gained&nbsp;a degree in
                                Dance Urban Practice.&nbsp;Natalia teaches across our Look Better Naked, Combat
                                Sports and School of Dance categories. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Daniel_Terry.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Daniel</h3>
                        <div>
                            <p>Having started training in&nbsp;martial arts at 9 years old, Daniel Terry is a
                                lifelong student of combat sports.&nbsp;After joining the onelifefitness team in 2007, he
                                soon fell in love with Muay Thai.&nbsp;Under the tutelage of his coach, Philip Tieu,
                                Daniel has earned himself a reputation as an elite A class professional fighter. He
                                has fought on some of the sport’s most prestigious events and has won regional,
                                English, British &amp; European championship titles.</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hosam_Radwan.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hosam</h3>
                        <div>
                            <p>Hosam has been&nbsp;training in Muay Thai and Boxing for over 10 years. He has
                                achieved various national fighting titles in Italy as an amateur and as an A class
                                Professional fighter. He also achieved an ISKA European Championship belt in
                                Germany, 2014.&nbsp;Come and join him for a sweat drenched workout in the ring! See
                                you there!</p>
                            <p>&nbsp;
                            </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Rochelle_Balen.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Rochelle</h3>
                        <div>
                            <p>With a degree in Sports Therapy and a qualified Personal Trainer, Rochelle thrives to
                                get the best out of people and push them to their limits. Rochelle also has
                                experience in martial arts -&nbsp;in particular karate, starting when she was just 5
                                years old. Her style of teaching is challenging yet rewarding at the same time. Many
                                refer to her as Eagle-Eyed. So be prepared to work and don’t get caught slacking in
                                her classes!</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>