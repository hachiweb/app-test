<?php /* Template Name: paddleboard-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_14BrxaV">
    <div class="_1vdzHPH">
        <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
            <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
                <h2>Paddleboard Yoga</h2>
                <div class="_1-4gF4V">
                    <p>Hang zen, dude. If you’re a yogi who wants to get wild, forget about a traditional flow – try
                        riding the wave of a Paddleboard workout instead. With the instability challenging the strength
                        of your posture and core, it’s still about finding balance… but nobody said we had to make it
                        easy.
                    </p>
                </div>
                <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span
                                class="Nfa775m">Cannon Street</span><span class="Nfa775m">Covent Garden</span><span
                                class="Nfa775m">Ealing</span><span class="Nfa775m">Holborn</span></div>
                    </li>
                </ul>
                <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Select gym</option>
                        <option value="Bank">Bank</option>
                        <option value="Cannon Street">Cannon Street</option>
                        <option value="Ealing">Ealing</option>
                        <option value="Holborn">Holborn</option>
                        <option value="Covent Garden">Covent Garden</option>
                    </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Choose time</option>
                        <option value="1330499" disabled="">12:00 Tuesday 18th (45 minutes) - Bookable 7am day before
                        </option>
                    </select><a
                        href=""
                        target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
                <div class="_3srnCE8">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                    <h2>Your <br>instructors</h2>
                    <div class="Sa1FFQw">
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Fleur_Poad.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Fleur</h3>
                            <div>
                                <p>Fleur has trained in various yoga styles including Ashtanga,
                                    Mandala Vinyasa Flow, Yin and pre and post natal, and is always working on
                                    enhancing her knowledge. Fleur’s classes are fun and challenging, and open to
                                    both beginners and those at a more advanced level. </p>
                            </div>
                        </div>
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Paulius_Savickas.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Paulius</h3>
                            <div>
                                <p>Paulius is a multi-style yogi teacher from Lithuania, explorer of the body, mind and
                                    the creative spirit. His yoga classes are the extensions of his own practice of
                                    yoga, meditation and the healing arts. Paulius believes that if we take a good care
                                    of our bodies and minds – our inner spirit can express itself in the infinite ways
                                    of happiness. His mission is to deliver you a safe and informed guidance to reach
                                    that goal.</p>
                            </div>
                        </div>
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Clare_Nagarajan.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Clare</h3>
                            <div>
                                <p>Clare is a yoga instructor who specialises in Vinyasa, Ashtanga, Yin, pregnancy and
                                    children’s yoga. She loves to bring elements of her training in meditation and
                                    mindfulness to her teaching, and her classes provide a moment for you take a break
                                    from everyday life.
                                </p>
                                <p><br></p>
                            </div>
                        </div>
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Beko_Kaygee.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Beko</h3>
                            <div>
                                <p>As an international presenter, Beko has been promoting
                                    dance, martial arts and holistic health for over 22 years, and has been at Gymbox
                                    since its opening day. Accredited with the creation of sword-fighting inspired
                                    workout ‘Blade’, Beko’s sessions are anything but boring.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wqBYWJi">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Paddleboard.jpg');"></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    