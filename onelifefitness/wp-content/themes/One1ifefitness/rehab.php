<?php /* Template Name: rehab */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
            <h2>Rehab</h2>
            <div class="_1-4gF4V">
                <p>Sure, we know you’re an absolute beast at the rack and a terror on the track… but how are you
                    REALLY? If you're battling weights addiction it's time to check into Rehab – a chance to scan
                    your body for imbalances, focus on maintenance and prescribe the best course to recovery. Take
                    the first step.</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Ealing</span><span
                            class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Westfield
                            London</span><span class="Nfa775m">Westfield Stratford</span></div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Select gym</option>
                    <option value="Elephant and Castle">Elephant and Castle</option>
                    <option value="Ealing">Ealing</option>
                    <option value="Westfield London">Westfield London</option>
                </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1309455" disabled="">13:15 Sunday 16th (45 minutes) - Bookable 7am day before
                    </option>
                </select><a
                    href=""
                    target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ashley_Ahrens.jpg?mtime=20180905112243"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ashley</h3>
                        <div>
                            <p>Ashley’s classes are disciplined, yet nurturing. She places a great deal of emphasis
                                on connecting deep within your own body, using the breath as a tool to distract the
                                mind, paired with fascial tissue and energetic lines of awareness to understand
                                postures on a very personal level. With 9 years of dedicated practice, 400-hours of
                                yoga training, and a Biology degree, she has a lot of inspiration and knowledge to
                                share!
                            </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ben_Harrison_Extra_Option_2.jpg?mtime=20180425160644"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ben</h3>
                        <div>
                            <p>Ben teaches modern, efficient,
                                anatomical and science based yoga. He focuses on building strength, often with hand
                                balancing - but with progressions so that every class is suitable
                                for beginners.<br></p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                        <div>
                            <p>Through a combination of tough-love and motivation, Stevie will push you to your
                                limits with high-energy metabolic conditioning workouts that will leave you in a
                                pool of sweat and a big smile on your face.&nbsp;</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                        <div>
                            <p>Firas found his passion for American Football 15 years ago, and has since won
                                numerous championships as both a player and coach. Firas brings his enthusiasim and
                                camaraderie to all his classes, and will undoubtedly bring out the inner athlete in
                                you. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                        <div>
                            <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                                &nbsp;classes range from Gains to Escalate, and are based around personal
                                development in a motivational team environment. His love for fitness is second only
                                to his love of the onelifefitness team.
                            </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                        <div>
                            <p>First qualified in aqua fitness aged 17, Hester worked in film production and then
                                re-trained as a contemporary dancer. Instructing pilates, fitness &amp; dance
                                supported her through BA &amp; MSc studies at Laban, and brought her to onelifefitness in
                                2004. Fitness, wellness and aerial arts have guided Hester throughout her career.
                                Possibly unequalled in her passion for teaching and sharing the knowledge
                                accumulated over more than 3 decades in the fitness world, Hester is onelifefitness Aerial
                                Series Master Trainer and creator of CircusFit Academy.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Rehab2.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>