<?php /* Template Name: aerial-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
                <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
                    <h2>Ride</h2>
                    <div class="_1-4gF4V">
                        <p>R1DE is the class to make spin everyone’s thing, with playlists to get you pumped, and climbs, sprints and jumps to seriously crank your cardio training up a gear. Whether you’re more used to pushing pedals or pounding the dancefloor, you’ll soon find your rhythm at this multicoloured mash up.</p>
                    </div>
                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                            <span class="Nfa775m">Onelifefitness Camden quay</span>
                            <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                            </div>
                        </li>
                    </ul>
                    <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
                        <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
                        <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                            <option value="" disabled="">Choose time</option>
                            <option value="1303165">19:30 Friday 14th (60 minutes)</option>
                            <option value="1284494" disabled="">12:15 Wednesday 19th (45 minutes) - Bookable 7am day
                                before</option>
                        </select><a
                            href="https://indma05.clubwise.com/onelifefitness/index.html"
                            target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a>
                    </div>
                    <div class="_3srnCE8">
                        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                            <div class="_2Tb-We1 sDHKtsB">
                                <ul class="_2J71_L4 sDHKtsB">
                                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                        <h2>Your <br>instructors</h2>
                        <div class="Sa1FFQw">
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hattie_Grover_Extra_Option_2.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hattie</h3>
                                <div>
                                    <p>Hattie's training began at the age
                                        of 3 when she started taking ballet lessons. As her
                                        love for dance blossomed she began performing, and immersed herself in a variety
                                        of different dance styles specialising in contemporary and breaking. Whilst at
                                        onelifefitness she has broadened her knowledge in the Aerial Arts and is fully committed
                                        to sharing her love for everything aerial, acrobatic and upside
                                        down!</p>
                                </div>
                            </div>
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Adrienn_Dombi.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Adrienn</h3>
                                <div>
                                    <p>Having grown up playing basketball and swimming competitively, Adrienn began a
                                        career in makeup and hairdressing before rediscovering her love for fitness. She
                                        then followed her passion to became an instructor, and now specialises in
                                        gymnastics, aerial fitness and yoga.</p>
                                </div>
                            </div>
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                                <div>
                                    <p>First qualified in aqua fitness aged 17, Hester worked in film production and
                                        then re-trained as a contemporary dancer. Instructing pilates, fitness &amp;
                                        dance supported her through BA &amp; MSc studies at Laban, and brought her to
                                        onelifefitness in 2004. Fitness, wellness and aerial arts have guided Hester throughout
                                        her career. Possibly unequalled in her passion for teaching and sharing the
                                        knowledge accumulated over more than 3 decades in the fitness world, Hester is
                                        onelifefitness Aerial Series Master Trainer and creator of CircusFit Academy.</p>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="wqBYWJi">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"
                                        data-interval="3000">
                                        <!-- <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                                class="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        </ol> -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item item_left active"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/one-Annotation.jpg');">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>