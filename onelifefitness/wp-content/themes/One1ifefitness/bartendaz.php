<?php /* Template Name: bartendaz */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
            <h2>H11t</h2>
            <div class="_1-4gF4V">
                <p>This Caveman-style circuit class goes back to basics – using intense based HIIT  training to give you hunter-gatherer agility, animalistic speed and the strength to take down an angry mammoth if required. All essential skills for Cork life.</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                    <span class="Nfa775m">Onelifefitness Camden quay</span>
                    <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                    </div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
            <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
                <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1289361" disabled="">12:30 Monday 17th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1306016" disabled="">19:15 Tuesday 18th (45 minutes) - Bookable 7am day
                        before</option>
                </select><a
                    href="https://indma05.clubwise.com/onelifefitness/index.html"
                    target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a>
            </div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jasmine_Lake.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jasmine</h3>
                        <div>
                            <p>Gymbox’s first female Bartendaz instructor, Jasmine has
                                trained extensively in calisthenics, and she believes in the ability of the
                                Bartendaz method to transform not just your body, but your mind too. Train with
                                Jasmine to discover your full potential, one pull up at a time. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lawrence_Cain.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Lawrence</h3>
                        <div>
                            <p>Lawrence focuses on calisthenics, strength training and body building in his
                                training, and teaches Frame Fitness, Ripped &amp; Stripped, Drill Sergeant and
                                Kettlebells. Lawrence’s no-nonsense, hardcore attitude is sure to push you
                                beyond your limits.</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>