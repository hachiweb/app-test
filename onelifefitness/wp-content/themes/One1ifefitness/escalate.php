<?php /* Template Name: escalate */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Escalate</h2>
        <div class="_1-4gF4V">
            <p>Functional, HIIT, circuits and track sprints? Ouch. If you’re looking for a class that’ll leave you
                knackered, aching and hilariously sweaty, you’ve hit the jackpot. Move from station to station as the
                intensity kicks up a gear each round. You'll wonder how things escalated so quickly.
            </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Cannon Street</span><span
                        class="Nfa775m">Ealing</span><span class="Nfa775m">Elephant and Castle</span><span
                        class="Nfa775m">Farringdon</span><span class="Nfa775m">Victoria</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Ealing">Ealing</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Victoria">Victoria</option>
                <option value="Cannon Street">Cannon Street</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1332335">11:00 Sunday 16th (45 minutes)</option>
                <option value="1258003" disabled="">06:15 Monday 17th (45 minutes) - Bookable 7am day before</option>
                <option value="1293464" disabled="">06:15 Thursday 20th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Mark_Lenihan.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Mark</h3>
                    <div>
                        <p>Mark has been in the fitness industry since 2010, and
                            specialises in bodyweight calisthenics, gymnastics and HIIT. He believes that
                            the first step to any transformation is being aware of the choices you make,
                            and how they effect yourself and others. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                    <div>
                        <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                            &nbsp;classes range from Gains to Escalate, and are based around personal development in a
                            motivational team environment. His love for fitness is second only to his love of the Gymbox
                            team.
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_James_Dawkins.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">James - Master Trainer</h3>
                    <div>
                        <p>London based House DJ and our Look Better Naked Master Trainer, James is passionate about
                            leading a team that will help you not only look great naked, but also feel great naked. Hop
                            into any of his classes and you'll quickly learn why we're the antidote to boring gyms! </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Flat_Line_Class_Crop_-resize.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>