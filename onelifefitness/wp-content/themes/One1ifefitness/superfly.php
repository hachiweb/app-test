<?php /* Template Name: superfly */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
                <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
                    <h2>Superfly</h2>
                    <div class="_1-4gF4V">
                        <p>There are many ways to fly (and not many of them are legal) but at Superfly, it’s your body
                            not your brain doing all the hard work. When you strap into that harness, you’re a
                            zero-gravity force of nature where you'll jump, float and glide – no pushing required.<br>
                        </p>
                    </div>
                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Cannon Street</span><span
                                    class="Nfa775m">Ealing</span><span class="Nfa775m">Westfield Stratford</span></div>
                        </li>
                    </ul>
                    <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="" disabled="">Select gym</option>
                            <option value="Westfield Stratford">Westfield Stratford</option>
                            <option value="Ealing">Ealing</option>
                            <option value="Cannon Street">Cannon Street</option>
                        </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                            <option value="" disabled="">Choose time</option>
                            <option value="1330036">10:30 Saturday 15th (45 minutes)</option>
                        </select>
                        <a href="" target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a>
                    </div>
                    <div class="_3srnCE8">
                        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                            <div class="_2Tb-We1 sDHKtsB">
                                <ul class="_2J71_L4 sDHKtsB">
                                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                        <h2>Your <br>instructors</h2>
                        <div class="Sa1FFQw">
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hattie_Grover_Extra_Option_2.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hattie</h3>
                                <div>
                                    <p>Hattie's training began at the age
                                        of 3 when she started taking ballet lessons. As her
                                        love for dance blossomed she began performing, and immersed herself in a variety
                                        of different dance styles specialising in contemporary and breaking. Whilst at
                                        onelifefitness she has broadened her knowledge in the Aerial Arts and is fully committed
                                        to sharing her love for everything aerial, acrobatic and upside
                                        down!</p>
                                </div>
                            </div>
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                                <div>
                                    <p>First qualified in aqua fitness aged 17, Hester worked in film production and
                                        then re-trained as a contemporary dancer. Instructing pilates, fitness &amp;
                                        dance supported her through BA &amp; MSc studies at Laban, and brought her to
                                        onelifefitness in 2004. Fitness, wellness and aerial arts have guided Hester throughout
                                        her career. Possibly unequalled in her passion for teaching and sharing the
                                        knowledge accumulated over more than 3 decades in the fitness world, Hester is
                                        onelifefitness Aerial Series Master Trainer and creator of CircusFit Academy.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wqBYWJi">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                            style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/GMYBOX-SUPERFLY-DSC_7238.jpg');"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="_3reSXV1 _4zWqqYK sDHKtsB">Loading: 100%</div>
        <div class="_12c1-Jf sDHKtsB">
            <div class="LN-nIKU I7PwxVS">
                <div class="_1L5csiF">
                    <div class="_1US2FDM _2rbE6TC _2v5bHvx VBD7Ow3">
                        <h3 class="LsKezX_">FREE PASS</h3><span class="_2va2FpG"></span>
                    </div>
                    <div class="_3ITUOra _2rbE6TC _2v5bHvx VBD7Ow3"
                        style="width: 5366.38px; transform: translateX(0px);">
                        <form>
                            <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                                <div class="Zd4QDDO">
                                    <h4 class="Zd4QDDO">1/6</h4>
                                </div>
                                <h2 class="_1_KNBvC">What's your name?</h2>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="fromName"></label><input type="text"
                                        name="fromName" value="" placeholder="Name"></div>
                            </div>
                            <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                                <div class="Zd4QDDO">
                                    <h4 class="Zd4QDDO">2/6</h4>
                                </div>
                                <h2 class="_1_KNBvC">What's your email?</h2>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="fromEmail"></label><input type="email"
                                        name="fromEmail" value="" placeholder="Email"></div>
                            </div>
                            <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                                <div class="Zd4QDDO">
                                    <h4 class="Zd4QDDO">3/6</h4>
                                </div>
                                <h2 class="_1_KNBvC">What's your phone?</h2>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="message[mobile]"></label><input type="text"
                                        name="message[mobile]" value="" placeholder="Phone"></div>
                            </div>
                            <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                                <div class="Zd4QDDO">
                                    <h4 class="Zd4QDDO">4/6</h4>
                                </div>
                                <h2 class="_1_KNBvC">Select club</h2>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="message[gym]"></label><select
                                        name="message[gym]">
                                        <option value="select">Select</option>
                                        <option value="Bank">Bank</option>
                                        <option value="Cannon Street">Cannon Street</option>
                                        <option value="Covent Garden">Covent Garden</option>
                                        <option value="Ealing">Ealing</option>
                                        <option value="Elephant and Castle">Elephant and Castle</option>
                                        <option value="Farringdon">Farringdon</option>
                                        <option value="Finsbury Park">Finsbury Park</option>
                                        <option value="Holborn">Holborn</option>
                                        <option value="Old Street">Old Street</option>
                                        <option value="Victoria">Victoria</option>
                                        <option value="Westfield London">Westfield London</option>
                                        <option value="Westfield Stratford">Westfield Stratford</option>
                                    </select></div>
                            </div>
                            <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                                <div class="Zd4QDDO">
                                    <h4 class="Zd4QDDO">5/6</h4>
                                </div>
                                <h2 class="_1_KNBvC">Time to submit!</h2>
                                <p>I understand that by submitting my details I will be contacted by onelifefitness with
                                    information about their services and membership options (not with spam, we promise)
                                </p><input type="submit" value="Book your free tour!">
                            </div>
                            <div class="_3ipyBdB">
                                <h2>Thanks - chat soon</h2>
                            </div>
                        </form>
                    </div>
                    <div class="_2bIo9Yp _2rbE6TC _2v5bHvx VBD7Ow3"><button
                            class="_3pXyaiA _2S_CT_r"><span>&nbsp;</span></button><button><span>Next</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>