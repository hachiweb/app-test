<?php /* Template Name: power-battle */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
            <h2>Booty Blast</h2>
            <div class="_1-4gF4V">
                <p>Blitz your lower body in a session that goes down a storm with your glutes. Combining bodyweight
                    booty-busters and ruthless resistance work, it's the conditioning class where the thigh's the limit.
                    Come on, giz us a squeeze.</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                        <span class="Nfa775m">Onelifefitness Camden quay</span>
                        <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                    </div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
                <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                        Fitness
                    </option>
                    <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                    </option>
                </select>
                <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1242745" disabled="">10:30 Sunday 16th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1333590" disabled="">07:00 Wednesday 19th (45 minutes) - Bookable 7am day before
                    </option>
                </select><a href="https://indma05.clubwise.com/onelifefitness/index.html" target="_blank"
                    class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jenny_Lawford_1.2.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jenny - Master Trainer</h3>
                        <div>
                            <p>With a background in music, Jenny fell in love with the Cycle Club category after
                                realising the impact a banging playlist could have on a class! From Bike &amp; Beats to
                                Connect, she always brings a smile and her goofy sense of humour to the studio and
                                promises that you’ll never leave her class feeling anything less than a champion,
                                rockstar or diva.</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Raquel_Banuls.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Raquel</h3>
                        <div>
                            <p>Raquel is a PT and studio instructor who specialises in
                                pole, aerial, indoor cycling and circuit training. Raquel’s passion for fitness
                                is contagious, and nothing makes her smile more than seeing the smiles and
                                satisfaction of her clients after a workout. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Abbey_Price.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Abigail</h3>
                        <div>
                            <p>A studio cycling instructor with a neverending supply of
                                enthusiasm, Abigail’s classes are guaranteed to get you as addicted to life in
                                the saddle as she is. With multiple qualifications under her belt, Abi will
                                take you from part time Boris-biker to spinning pro.</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jason_Mellars.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jason</h3>
                        <div>
                            <p>Jason has over 20 years experience in the dance and fitness
                                industry as an international fitness presenter, master trainer and
                                choreographer. With specialities including pilates, spinning and dance, Jason’s
                                classes celebrate having fun whilst pushing you to your limits.</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>