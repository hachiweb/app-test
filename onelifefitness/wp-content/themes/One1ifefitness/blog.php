<?php /* Template Name: blog */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */?>
<?php 
    $title = isset($_GET['post'])?$_GET['post']:'';
    $title_parts = array_filter(explode('/',$title));
    $category_name = "";
    $post_name = "";
    if(!empty($title_parts) && sizeof($title_parts) == 2){
        if( $title_parts[1] == 'category'){
            $category_name = $title_parts[2];
        }
    }
    else if (!empty($title_parts)){
        $post_name = $title_parts[1];
    }
    // print_r($title_parts);
    // print_r($post_name);
    // exit();
    get_header(); 
 ?>
<div class="_1vdzHPH">
    <div class="_3l5upd8 _2rbE6TC _2v5bHvx">
        <div class="_2DStQxp _1G7RCdW _2v5bHvx VBD7Ow3">
            <h4>Categories</h4>
            <?php $categories = get_categories();
            foreach($categories as $category) {
            echo '<div class="col-md-4"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';
            } ?>
        </div>
        <div class="_2uaX48K _1G7RCdW _2v5bHvx VBD7Ow3">
            <div class="_1bpafaq <?php if(!empty($post_name)){echo 'd-none';} ?>">
                <div class="Qp6qIPZ">
                    <div>
                        <div class="M5KqVYK _2rbE6TC _2v5bHvx">
                            <h3 class="_2Ii007W">Instagram</h3><a class="zxpnPmO _25LPQhk"
                                href="https://www.instagram.com/one_life_fitness_/">@onelifefitnessofficial</a>
                        </div>
                        <div class="_2lcgMde _2rbE6TC _2v5bHvx">
                            <div class="_1fy4wK0 _2rbE6TC _2v5bHvx">
                                <div class="_1yfuZQ0 _1XSaRkn">
                                    <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                        <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE41976D15-4EBF-47AA-8413-C81599A1FAE7.JPG');"></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="_1XSaRkn">
                                    <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                        <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE41C1-A0A0-B2DDC64C498D.JPG');"></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="_1yfuZQ0 _1XSaRkn">
                                    <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                        <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONEIMG_1075.JPG');"></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="_1XSaRkn">
                                    <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                        <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-A07717B5E353.JPG');"></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="_1XSaRkn">
                                    <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                        <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONEE1BF360F2569.JPG');"></span>
                                        </div>
                                    </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="_2bqWHNj _2rbE6TC _2v5bHvx">Articles</h3>
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                // echo($paged);
                    // define query arguments
                    if(!empty(trim($category_name))){
                        $args = array (
                            'nopaging'               => false,
                            'paged'                  => $paged,
                            'posts_per_page'         => '10',
                            'post_type'              => 'post',
                            'cat' => get_cat_ID($category_name),
                        );
                    }
                    else{
                        $args = array (
                            'nopaging'               => false,
                            'paged'                  => $paged,
                            'posts_per_page'         => '10',
                            'post_type'              => 'post',
                        );
                    }

                    // set up new query
                    $tyler_query = new WP_Query( $args );

                    // loop through found posts
                    if ($tyler_query->have_posts()) {
                        while ( $tyler_query->have_posts() ) {
                            $tyler_query->the_post();
                            //Post item -->
                    ?>
                <div class="_1o0O21S _2rbE6TC _2v5bHvx _3HiqiFI">
                    <a href="<?php the_permalink();?>">
                        <div class="_2gpiPPm _2kezaiW _2rbE6TC _2v5bHvx">
                            <span class="_3h_jpHd _3sQgXbr sDHKtsB"
                                style="background-image: url(<?php echo catch_that_image() ?>)">
                            </span>
                        </div>
                    </a>
                    <div class="uerild2 _2rbE6TC _2v5bHvx">
                        <p class="_1FS5y40"><?php the_date();?></p>
                        <a class="Rh4zxCS _1Sq1pn3" href="<?php the_permalink();?>"><?php the_title();?></a>
                    </div>
                </div>
                <?php
                }
            }
            // reset post data
            wp_reset_postdata();
            ?>
            <div class="_lMnZEd VBD7Ow3 undefined">
            <?php
            // set the "paged" parameter (use 'page' if the query is on a static front page)
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
            if(!empty($category_name)){
                $args = array (
                    'nopaging'               => false,
                    'paged'                  => $paged,
                    'posts_per_page'         => '10',
                    'post_type'              => 'post',
                    'cat' => get_cat_ID($category_name),
                );
            }
            else{
                $args = array (
                    'nopaging'               => false,
                    'paged'                  => $paged,
                    'posts_per_page'         => '10',
                    'post_type'              => 'post',
                );
            }
            // The Query
            $query = new WP_Query( $args );
            // The Loop
            if ( $query->have_posts() ) {

                previous_posts_link( '« Newer Posts &nbsp;&nbsp;&nbsp;&nbsp;' );
                echo "       ";
                // while ( $query->have_posts() ) {
                //     $query->the_post();
                //     echo '<div class="news-item">';
                //         // post stuff here
                //             echo '<h1 class="page-title screen-reader-text">' . the_title() . '</h1>';
                //     echo '</div>';
                // }
                next_posts_link( ' &nbsp;&nbsp;&nbsp;&nbsp; Older Posts »', $query->max_num_pages );
            } else {
                // no posts found
                echo '<h1 class="page-title screen-reader-text">No Posts Found</h1>';
            }
            // Restore original Post Data
            wp_reset_postdata();
            ?>
        </div>
            </div>



            
    <!-- post-detail section -->
            <div class="_2fGVUXV _2rbE6TC _2v5bHvx _3HiqiFI <?php if(empty($post_name)){echo 'd-none';} ?>">
            <?php
                if(!empty($post_name)){
                    $args = array(
                        'name'        => $post_name,
                        'post_type'   => 'post',
                        'post_status' => 'publish',
                        'numberposts' => 1
                      );
                      $my_posts = get_posts($args);
                      if( $my_posts ) :
                        // print_r($my_posts);
                        // echo 'ID on the first post found ' . $my_posts[0]->ID;
                        echo '
                        <div>
                        <h2 class="_3L7FtF7">'.$my_posts[0]->post_title.'</h2>
                        <p>'.$my_posts[0]->post_date_gmt.'</p>
                        </div>
                        <div>'.$my_posts[0]->post_content.'</div>';
                      endif;

                    // $wp_query = new WP_Query( $args );
                    
                } ?>
                <div>
                
                   
                </div>
            </div><!-- end post-detail section -->
        </div>

    </div>
</div>
<?php
// get_sidebar();
get_footer();
?>