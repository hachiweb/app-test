<?php /* Template Name: balates */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
            <h2>abs circuit</h2>
            <div class="_1-4gF4V">
                <p>If you thought the name was inspired by the killer abs you’re going to get from this class, we’ve got bad news. Try the killer workout you crunch, twist and plank through to get there. This is one mad assault on your middle that definitely earns that 6 pack. (And the one in the fridge.)
                </p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                    <span class="Nfa775m">Onelifefitness Camden quay</span>
                <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                    </div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
            <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
                <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1330220" disabled="">12:00 Wednesday 19th (45 minutes) - Bookable 7am day before
                    </option>
                </select>
                <a href="https://indma05.clubwise.com/onelifefitness/index.html" target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ophelie_Griffond.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ophelie</h3>
                        <div>
                            <p>Having been born with defects in both her legs, Ophelie wishes to show her clients that
                                nothing is impossible, and that with belief and commitment you can achieve anything. As
                                well as an instructor and PT at onelifefitness, Ophelie is a circus and burlesque artist.&nbsp;
                            </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                        <div>
                            <p>First qualified in aqua fitness aged 17, Hester worked in film production and then
                                re-trained as a contemporary dancer. Instructing pilates, fitness &amp; dance supported
                                her through BA &amp; MSc studies at Laban, and brought her to onelifefitness in 2004. Fitness,
                                wellness and aerial arts have guided Hester throughout her career. Possibly unequalled
                                in her passion for teaching and sharing the knowledge accumulated over more than 3
                                decades in the fitness world, Hester is onelifefitness Aerial Series Master Trainer and creator
                                of CircusFit Academy.</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>