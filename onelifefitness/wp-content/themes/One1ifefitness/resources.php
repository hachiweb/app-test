<?php /* Template Name: resources */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div class="_2i1VlSl _2rbE6TC _2v5bHvx">
                <div class="_3JE8_m6">
                    <div class="_2PvrzEw _2rbE6TC _2v5bHvx">
                        <h1 class="ke23QbA">Sitemap</h1>
                    </div>
                    <div class="_2p2lQy5 _2rbE6TC _2v5bHvx VBD7Ow3">
                        <div class="_3B4_uN4">
                            <div class="_2X4DPBY">
                                <div>
                                    <p><br><br></p>
                                    <h2>Our Gym Locations</h2>
                                    <p><br></p>
                                    <p>
                                    </p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/one-life-fitness">Onelifefitness Camden quay</a>
                                    </p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/riversedgefitness" target=""
                                            class="go" rel="noreferrer noopener">Onelifefitness Sullivans quay </a>
                                    </p>
                                
                                    <p><br></p>
                                    <h2>Our Gym Classes</h2>
                                    <p><br></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/lift">Lift<br></a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/box">Box</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/ride">Ride</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/olf45">OLF 45</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/strong">Strong</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/boxing-circuit">Box1ng Circuit</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/legs-bums-tums">Legs, bums + tums</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/hips-bums-tums">h1ps, bums, & tums</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/booty-blast">booty blast</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/functional-circuit">Funct1onal Circuit</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/tabata">Tabata</a></p>
                                    <p>&gt; <a href="<?= get_site_url(); ?>/hiit">h11t</a></p>
                                    <p><br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>