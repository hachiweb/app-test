<?php /* Template Name: contact-us */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
?>
<?php get_header(); ?>

<div class="_1vdzHPH">
    <div class="_1Z3ea-t _2rbE6TC _2v5bHvx">
        <div class="_3JE8_m6">
            <div class="_2PvrzEw _2rbE6TC _2v5bHvx">
                <h1 class="ke23QbA">Get Involved</h1>
            </div>
            <div class="_2DxRudK _2rbE6TC _2v5bHvx">
                <div class="top-padd _2JiiLfi _2rbE6TC _2v5bHvx">
                    <div>
                        <p class="text-justify">Working with a onelifefitness Coach is the best investment you can make in both your health
                            and achieving your goals. So whatever that improvement looks like to you, it all
                            starts by dropping your deets into the form below…
                        </p>
                    </div>
                    <div class="HRKJIWa VBD7Ow3">
                        <form class="pt-enquiry-form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>"
                            method="POST">
                            <input type="hidden" name="action" value="my_simple_form">
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="fromName">Name</label>
                                <input name="fromName" placeholder="Name" type="text" value="" required>
                                </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="fromEmail">Email</label>
                                <input name="fromEmail" placeholder="Email" type="email" value="" required>
                                </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="mobile_number">Phone</label>
                                <input name="mobile_number" placeholder="Phone" type="text" value="" required>
                                </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="message_goals">Additional information about training goals</label>
                                    <textarea name="message_goals" placeholder="Additional information about training goals" required></textarea>
                                </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="club_intrested">Club interested in</label>
                                    <select name="club_intrested" placeholder="Club interested in" required>
                                    <option value="" selected disabled>Select Club</option>
                                    <option value="onelife fitness sullivans quay">onelife fitness sullivans quay</option>
                                    <option value="onelife fitness camden quay">onelife fitness camden quay</option>
                                </select>
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="check" name="check" required>
                                    <label class="custom-control-label" for="check">I understand that by submitting
                                    my details I will be contacted by a onelifefitness freelance personal trainer with
                                    information about their services.</label>
                                </div>
                            </div>
                            <input type="submit" name="contact_submit" class="submit-pt-enquiry" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>