<?php /* Template Name: gymboxing */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
            <h2>Gymboxing</h2>
            <div class="_1-4gF4V">
                <p>Starting boxing from scratch? The gloves are on and London's pros are primed to pass on all the tips
                    and techniques you need to come out swinging. Get to grips with the rules. Do your thing in the
                    ring. Start your journey to every world title going. Ding, ding – ROUND ONE. BYO Boxing gloves/bag
                    mitts.</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span
                            class="Nfa775m">Covent Garden</span><span class="Nfa775m">Ealing</span><span
                            class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Farringdon</span><span
                            class="Nfa775m">Holborn</span><span class="Nfa775m">Old Street</span><span
                            class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                            class="Nfa775m">Westfield Stratford</span></div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Select gym</option>
                    <option value="Westfield London">Westfield London</option>
                    <option value="Farringdon">Farringdon</option>
                    <option value="Victoria">Victoria</option>
                    <option value="Ealing">Ealing</option>
                    <option value="Covent Garden">Covent Garden</option>
                    <option value="Bank">Bank</option>
                    <option value="Holborn">Holborn</option>
                    <option value="Westfield Stratford">Westfield Stratford</option>
                    <option value="Elephant and Castle">Elephant and Castle</option>
                    <option value="Old Street">Old Street</option>
                </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1301038">11:00 Saturday 15th (60 minutes)</option>
                    <option value="1298948" disabled="">06:45 Tuesday 18th (60 minutes) - Bookable 7am day before
                    </option>
                    <option value="1290503" disabled="">18:00 Tuesday 18th (60 minutes) - Bookable 7am day before
                    </option>
                    <option value="1300669" disabled="">18:30 Thursday 20th (60 minutes) - Bookable 7am day before
                    </option>
                </select><a
                    href="https://gymbox.legendonlineservices.co.uk/enterprise/Basket/AddPublicClassBooking?bookingId="
                    target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Aleksandar_Toskov.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Aleksandar</h3>
                        <div>
                            <p>Aleksandar has been a&nbsp;professional Muay thai &amp; K1 kickboxing fighter for over 13
                                years as well as a personal trainer for over 10 years. As an active fighter he is
                                passionate, dedicated and wants to help people train smarter to reach a
                                higher&nbsp;level of fitness.&nbsp;</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Natalia_Kotowska_Extra_Option_1.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Natalia</h3>
                        <div>
                            <p>Originally from
                                Poland,&nbsp;Natalia&nbsp;has represented London and Poland internationally in
                                dance battles, panels and workshops, and in 2015&nbsp;gained&nbsp;a degree in
                                Dance Urban Practice.&nbsp;Natalia teaches across our Look Better Naked, Combat
                                Sports and School of Dance categories. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Gymboxing_NEw.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>