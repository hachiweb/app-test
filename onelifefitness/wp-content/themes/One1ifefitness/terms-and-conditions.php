<?php /* Template Name: terms-and-conditions */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_2dDJkQd VBD7Ow3">
        <div>
            <div>
                <h2>
                    1. GENERAL TERMS AND CONDITIONS</h2>
                <p>In these Terms &amp; Conditions and the <a href="">Club
                        Rules</a>, "onelifefitness" means Sparring Partners Ltd, company number 4204345 and whose
                    registered office is at Unit 7, 38 New Kent Road, London, SE1 6TJ and/or onelifefitness Black Ltd;
                    "Member" means the member named on the Application Form, and "Membership" is the membership
                    by a Member of a Club, which starts when onelifefitness accepts the proposed Member's Application
                    Form; "Club" means the onelifefitness gym at which the Member applied for membership and any other
                    onelifefitness gym the Member may attend; "Contract" means the contract between onelifefitness and the
                    Member subject to these Terms &amp; Conditions and Club Rules; "Application Form" means the
                    application form completed by the Member to join the Club; "Club Rules" means the terms set
                    out in the onelifefitness Club Rules.
                </p>
                <h2>
                    2. MEMBERSHIP APPLICATION</h2>
                <p>Membership is subject to these Terms &amp; Conditions as amended from time to time.
                    Submission of an Application Form is an offer to onelifefitness to become a Member of the Club
                    subject to these Terms &amp; Conditions and the club rules. onelifefitness reserves the right to
                    reject any application for Membership.
                </p>
                <p>Membership is available to individuals of 18 years of age and over, subject to status.
                </p>
                <p>Upon being accepted to the Club’s Membership scheme, the Member consents to having their
                    photograph taken by onelifefitness to confirm their identity upon entry, and consents to having
                    their photograph taken at any time whilst using the facilities, excluding changing rooms.
                    onelifefitness reserves the right to use any such photographs for press and/or promotional purposes.
                </p>
                <h2>
                    3. MEMBERSHIP TYPES</h2>
                <p>onelifefitness offers various Membership packages, offering a range of price versus flexibility, so
                    that Members have choice.</p>
                <p><strong>a) No Commitment Memberships</strong>
                </p>
                <p>For these package types, payment is collected on or around the 1<sup>st</sup> of each month.
                    There is no minimum term for these Memberships and will continue to roll on a monthly basis
                    only until the Member cancels their direct debit directly with their bank. See Point 6,
                    below, for further information on how to cancel a No Commitment Membership. </p>
                <p><strong>b) Monthly Minimum Term Memberships</strong>
                </p>
                <p>For these package types, payment is collected on or around the 1st of each month. These
                    Memberships each have a minimum contract term, and Membership continues to roll on a monthly
                    basis after the minimum term has expired. See Point 6, below, on how to cancel a Monthly
                    Minimum Term Membership.
                </p>
                <ul>
                    <li>"Pay as You Go"– minimum term of one full calendar month.
                    </li>
                    <li>"3 Month Monthly" – minimum term of three full calendar months.
                    </li>
                    <li>"6 Month Monthly" – minimum term of six full calendar months.
                    </li>
                    <li>"1 Year Monthly" – minimum term of twelve full calendar months.
                    </li>
                    <li>"18 Month Monthly" – minimum term of eighteen full calendar months.
                    </li>
                </ul>
                <p>The minimum term of all Monthly Minimum Term Memberships starts as of the 1st day of the
                    first full paid calendar month. For example: if a Membership starts on 15th September, the
                    minimum term would begin as of 1st October.
                </p>
                <p><strong>c) Paid in Full Memberships</strong>
                </p>
                <p>For these package types, full payment is required upfront, and the Membership will
                    automatically expire at the end of the term. Members may then renew their Membership at the
                    prevailing rate for their package type, or alternatively may switch to a rolling Monthly
                    Membership as per above.
                </p>
                <ul>
                    <li>"3 Months Paid in Full" – three months of Membership from selected start date.
                    </li>
                    <li>"6 Months Paid in Full" – six months of Membership from selected start date.
                    </li>
                    <li>"12 Months Paid in Full" – twelve months of Membership from selected start date.
                    </li>
                    <li>"18 Months Up Front" – eighteen months of Membership from selected start date.
                    </li>
                </ul>
                <p>onelifefitness reserves the right to introduce, withdraw and vary categories of Membership.
                </p>
                <p><strong>d) Switching</strong>
                </p>
                <p>A Member may apply to switch to a different Membership type. A Member cannot switch to a
                    Membership type with a shorter minimum term than their current contract. Any months
                    completed on a previous Membership type will not be credited towards the minimum term of the
                    new contract. A Member may only move onto a Membership type and rate available at the time
                    of upgrading. These may vary from Membership types available at the Member's point of
                    joining. </p>
                <h2>
                    4. MEMBERSHIP CARD</h2>
                <p>A Membership card will be issued to each Member upon joining the Club. Members must present
                    their card for security and verification on each visit to the Club; Members without a valid
                    Membership card may be asked for photo identification, and will be admitted at the absolute
                    discretion of onelifefitness staff.
                </p>
                <p>A Member may not loan their Membership card or permit its use by any other person; allowing
                    such misuse of a Membership card may result in Membership being terminated with no refund of
                    fees already paid.
                </p>
                <p>Any lost or mislaid Membership cards will be replaced by onelifefitness for a nominal charge. </p>
                <h2>5. INITIAL JOINING FEE AND MEMBERSHIP DUES</h2>
                <p>All Members shall pay an initial Joining Fee as per the Club's current price schedule, which
                    will be made available to you before you join as a Member. The Joining Fee is not refundable
                    under any circumstances.
                </p>
                <p>Membership is payable in advance, either fully or monthly as per the Membership type selected
                    (see Point 3, above). Monthly fees will be debited on or around the 1st of each month. In
                    the event that the Member falls into arrears in respect of any fees payable, all arrears
                    must be settled before the Member can use the Club.
                </p>
                <p>Monthly Membership fees may be increased at the discretion of and at any time by onelifefitness, to
                    take effect after the Member's minimum term subject to at least one calendar month's written
                    notice. Changes to pricing for new joiners may be made without notice.
                </p>
                <p>Any discounted Membership options are offered on the basis that the Member can provide proof
                    of eligibility; Members should be aware that the full fee for their Membership type may be
                    applied until such proof is provided.
                </p>
                <p>All Members who opt for a Monthly Minimum Term Membership paid by Direct Debit, are required
                    to provide onelifefitness with debit or credit card details as a secondary means of payment
                    ("Payment Guarantee"). If any Membership payment remains outstanding beyond the due date,
                    the Member's signature on the Payment Guarantee constitutes the Member's unconditional and
                    irrevocable authority to debit the nominated card for the total amount due without notice to
                    the Member.
                </p>
                <p>onelifefitness reserves the right to levy an administration fee of £35 if it forwards the account to
                    a 3rd party debt collection agency in the event of non-payment of fees when due. Any lapses
                    in Membership, including but not limited to non-payment of fees or failure to renew a Paid
                    in Full Membership, may result in a new Joining Fee being charged should the Member reapply
                    for Membership.</p>
                <h2>
                    6. CANCELLING YOUR MEMBERSHIP</h2>
                <p>Members on a Monthly Minimum Term Membership contract may cancel their Membership after or
                    with effect from the end of the minimum term, by giving one full calendar month's advance
                    written notice, effective from the 1st of the following month. This should be done by
                    completing a Cancellation Request using the onelifefitness Online Member Services function. This can
                    be found on the onelifefitness website using the following link <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>.<br></p>
                <p>A onelifefitness team member will respond to all cancellation requests. If confirmation of
                    cancellation is not received, the Member is responsible for re-submitting the cancellation
                    request.
                </p>
                <p>Where the payment method is Direct Debit, the Member must advise their bank to cancel the
                    Direct Debit instruction after the final payment has been made. onelifefitness is not obliged to
                    refund any fees where the Member has not cancelled the instruction and cannot provide proof
                    of a valid cancellation request. All fees must be paid to date at the time of cancellation.
                </p>
                <p>Paid in Full Memberships will expire automatically at the end of the term, unless the Member
                    chooses to renew their Membership. No refunds are applicable for any portion of fees paid
                    upfront, excluding in the following circumstances: redundancy, permanent illness and
                    permanent injury preventing gym usage. Valid documentation will be requested as proof in
                    such circumstances.
                </p>
                <p>All Members who opt for a No Commitment Membership can terminate their Membership by
                    cancelling the direct debit authorisation directly through their bank or building society.
                    It is the Members responsibility to ensure enough notice is provided to the bank/building
                    society to prevent the next payment. onelifefitness recommends at least 5 working days.
                    Cancellations cannot be backdated and payments taken successfully will not be refunded. When
                    a direct debit payment is unsuccessful, access will be denied with immediate effect and the
                    Membership will be terminated within two working days. Should a Member wish to continue a
                    Membership following a failed direct debit they may contact the membership team via <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a> to
                    discuss available options.<br></p>
                <p>Should a Member be unable to use the Club for the remainder of the term, the Member may apply
                    to transfer the Contract to another person. The Member is responsible for finding someone,
                    who is not already a Member of a onelifefitness Club, to take over the balance of the Membership. A
                    Transfer Fee will apply as per the Club's current price schedule. Both the existing Member
                    (where possible) and the new Member must attend the Club to complete the transfer
                    application, which will not be deemed complete until a Membership Application Form has been
                    completed and signed by the new proposed Member and onelifefitness has accepted that person as a
                    Member.
                </p>
                <p>Members still within the minimum contractual period may reduce their cancellation notice to
                    one full calendar month in the following circumstances: Pregnancy, Redundancy, Permanent
                    injury and Permanent illness preventing gym usage. Valid documentation will be requested as
                    proof in such circumstances.
                </p>
                <p>Any Member with three or more months remaining of the minimum term may terminate their
                    Membership at the end of the then current month by paying in full 50% of the total future
                    fees due until the end of the minimum term.
                </p>
                <p>Please contact the onelifefitness membership team via the following link for more information: <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>.</p>
                <h2>7. PRESALE - 14 DAY MONEY BACK GUARANTEE</h2>
                <p>A "14 Day Money Back Guarantee" is applicable when a Member signs up to a new Club during its
                    presale period. A presale period is defined as the time in which a Membership can be
                    purchased for a new Club prior to the opening date.
                </p>
                <p>During the designated 14 days, a Member may request immediate termination of their new
                    Membership, along with a refund of fees paid upon joining (including the joining fee).
                </p>
                <p>The 14 days is effective from, and including, the agreed original start date of their
                    Membership.
                </p>
                <p>A Member can only have a maximum of one visit of the new Club recorded on their Membership to
                    be eligible for this 14 day money back offer. Upon visiting the facilities and using the new
                    Club a second time, the member is confirming they will commit to the full terms of their
                    chosen Membership.
                </p>
                <p>In cases where the Member has opted to use an existing onelifefitness site prior to the new location
                    opening, the 14 day money back offer will not apply.</p>
                <h2>8. TRANSFERRING MEMBERSHIP TO ANOTHER CLUB </h2>
                <p>A Member can apply to move their Membership to another Club after the first four weeks of
                    membership is complete. A price increase or package change may apply depending on the
                    prevailing rate for their Membership at the requested Club.
                </p>
                <h2>9. FREEZING YOUR MEMBERSHIP </h2>
                <p>Any Member may apply for their Membership to be frozen or unfrozen by completing a request
                    via the onelifefitness Online Member Services function using the following link, <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>.</p>
                <p>The minimum freeze period is one full calendar month, commencing on the 1<sup>st</sup> day of
                    the month, with a maximum of six full consecutive calendar months.
                </p>
                <p>A Member must request the freeze by the 20<sup>th</sup> day of the month prior to the
                    requested freeze start date. For example, a Member who wishes to freeze commencing
                    1<sup>st</sup> June, must complete the online request by 20<sup>th</sup> May. A freeze
                    request completed on 21<sup>st</sup> May would only be eligible for a freeze start date as
                    of 1<sup>st</sup> July.
                </p>
                <p>A fee of £20 per month applies throughout the period of frozen Membership. Freeze periods
                    will not count towards a Member's contractual term, and will extend any applicable minimum
                    term by the number of frozen months taken. Membership may not be frozen during the notice
                    period of cancellation.
                </p>
                <p>Members on a Monthly contract type will have their freeze fee(s) taken via Direct Debit;
                    Members on a Paid in Full contract type must settle their freeze fee(s) upfront in advance.
                </p>
                <p>Freezing for reasons of illness, injury or pregnancy will be reviewed and leniency may be
                    applied in relation to the fee and notice period; valid medical documentation must be
                    attached to the request for this to be considered. If valid dated documentation is provided,
                    a freeze due to medical/injury reasons may be backdated, providing no usage has been
                    registered on the Member’s account. In such circumstances no refund would be due and any
                    fees paid would be credited to future Membership payments.
                </p>
                <p>Any Member who requests to freeze their Membership under the Terms and Conditions will not be
                    able to access the Club during the frozen period, either via their Membership or purchase of
                    a Guest Pass. A Member must apply to unfreeze their Membership if wishing to use the Club
                    during the frozen period. In order to unfreeze a Membership, a pro rata fee for the
                    remainder of the then current month will be payable. The freeze fee of £20 is not payable
                    towards this pro rata fee.</p>
                <h2>10. CLUB FACILITIES </h2>
                <p>Full details of normal opening hours are available upon request at each Club. onelifefitness reserves
                    the right to vary normal opening hours, temporarily remove the access to certain equipment,
                    or to temporarily close certain areas of any Club from time to time without notice for
                    various purpose including for cleaning, decorating, repairs, refurbishment, or for special
                    functions and holidays. </p>
                <h2>11. HEALTH AND SAFETY</h2>
                <p>All Club users must complete a health questionnaire (PARQ) before entry. If any medical
                    conditions are disclosed, the user must sign the back of the PARQ stating that they have
                    sought medical advice or wish to use the Club without doing so, and that they take full
                    responsibility for any injury or health condition sustained whilst using the facilities
                    arising from their medical condition. onelifefitness staff are not medically trained and are
                    therefore not qualified to assess whether the Club user is in appropriate physical condition
                    to use the facilities. onelifefitness advises all Club users to take medical advice prior to
                    starting any exercise programme if they are in any doubt as to their ability to do so.
                </p>
                <p>The Member warrants and represents upon their Membership Application Form, and repeats such
                    warranty upon each visit to the Club, that they are in good physical condition and know of
                    no medical or other reason why they should not engage in any form of exercise, and that such
                    exercise would not be detrimental to their health, safety, comfort or physical condition.
                </p>
                <h2>12. PERSONAL TRAINING</h2>
                <p>The Very Personal Trainers (VPT) that operate within Club are not employees of onelifefitness, but
                    are self-employed, independent freelance trainers. In using a VPT you are entering into an
                    agreement with the VPT alone and not with onelifefitness.
                </p>
                <h2>13. RELOCATION OF A CLUB BY onelifefitness</h2>
                <p>onelifefitness may re-locate a Club, or open a new Club near to the Member's then current Club. In
                    such case, onelifefitness may, on no less than 4 weeks' written notice to the Member, notify the
                    Member that the relocated Club or the new Club is to be the Club in respect of which the
                    Member has their Membership.
                </p>
                <p>a) If the re-located Club or new Club is one third of a mile or less from the Member's
                    current Club, the Member's membership will automatically transfer to the re-located Club or
                    new Club. In such case, all terms and conditions relating to the Member’s current membership
                    will continue in full force and effect, applying to the relocated or new Club
                </p>
                <p>b) If the re-located Club or new Club is more than one third of a mile from the Member's
                    current Club, and the Member does not agree to such relocation, the Member may terminate
                    this Contract, effective on the date the Member’s current Club closes. In such cases onelifefitness
                    will notify the Member of the relocation in writing, no less than 2 full calendar month’s in
                    advance of the relocation date. The member must give one full calendar month’s advance
                    notice, effective on the first of the following month, by completing a Cancellation Request
                    using the onelifefitness online Member Services function. <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>. Should
                    the member accept the relocation, the member may, during the first 10 days of the new or
                    relocated club opening, cancel their current contract with one full calendar months written
                    notice. In such case, from the 11th day of the new club opening, all terms and conditions
                    relating to the Member’s current membership will continue in full force and effect, applying
                    to the relocated or new Club. </p>
                <h2>14. TERMINATION OF MEMBERSHIP BY onelifefitness</h2>
                <p>onelifefitness may terminate a Membership without notice and with immediate effect if:
                </p>
                <ul>
                    <li>The Member breaches these Terms &amp; Conditions or the Club Rules, either repeatedly or
                        because of one serious breach;
                    </li>
                    <li>Any due fees remain unpaid after any request for payment by onelifefitness;
                    </li>
                    <li>onelifefitness is of the opinion (acting in its discretion) that the Member is not suitable for
                        continued Membership;
                    </li>
                    <li>The Member puts the health, safety or well-being of staff or other Members or Guests at
                        risk.
                    </li>
                </ul>
                <p>All decisions made by onelifefitness under this clause are final and binding. onelifefitness is unlikely to
                    accept a new application for Membership from someone whose Membership has been terminated in
                    accordance with this clause.</p>
                <h2>15. YOUR PERSONAL INFORMATION</h2>
                <p>onelifefitness acknowledge that the security of a Members personal information is of high importance.
                    onelifefitness Privacy Policy conforms to the European Union General Data Protection Regulation (EU
                    GDPR) effective as of 25th May 2018 and explains how a Members information is processed at
                    onelifefitness and the rights a Member has as an individual under the GDPR.
                </p>
                <p>Members are responsible for advising onelifefitness of changes to their personal information. Should
                    a Member wish to update their personal information and/or request a change of Membership,
                    this must be requested using the registered email address within the onelifefitness database for
                    that Member. Where onelifefitness is required to provide any written or email notification, onelifefitness
                    will send the notice to the contact information on the Application Form, or any updated
                    contact information the Member has provided since joining.
                </p>
                <p>Please ensure you have read and understand the onelifefitness <a
                        href="https://onelifefitness.ie/">Privacy Policy</a> prior to accepting the Terms
                    of your Agreement.</p>
                <h2>16. LIMITATION OF LIABILITY</h2>
                <p>Nothing in these Terms &amp; Conditions shall limit or exclude onelifefitness’s liability for:
                </p>
                <p>(i) death or personal injury caused by its negligence, or the negligence of its personnel or
                    agents;
                </p>
                <p>(ii) fraud or fraudulent misrepresentation; or
                </p>
                <p>(iii) any other liability which cannot be limited or excluded by applicable law.
                </p>
                <p>Subject to that:
                </p>
                <p>(a) onelifefitness shall have no liability to the Member or Guest, whether in contract, tort
                    (including negligence), breach of statutory duty, or otherwise, for any indirect or
                    consequential loss arising under or in connection with these Terms &amp; Conditions; and
                </p>
                <p>(b) onelifefitness’s total liability to the Member or Guest, whether in contract, tort (including
                    negligence), breach of statutory duty, or otherwise, arising under or in connection with
                    these Terms &amp; Conditions shall be limited to £250. </p>
                <h2>17. CHANGES TO onelifefitness TERMS &amp; CONDITIONS </h2>
                <p>onelifefitness may from time to time amend these Terms &amp; Conditions, including the introduction
                    of any additional terms and conditions, and will notify Members by placing a notice on the
                    Club noticeboard. Any changes will be effective immediately.
                </p>
                <h2>18. THE TERMS OF YOUR CONTRACT</h2>
                <p>A signed Application Form, these Terms &amp; Conditions, the Club Rules and the PARQ make up
                    a binding contract of Membership with onelifefitness. Members are advised to read the Terms &amp;
                    Conditions and Club Rules in full before signing the Application Form.
                </p>
                <p>The failure of onelifefitness to enforce any of its rights at any time for any period shall not be
                    construed as a waiver of those rights. Any failure to identify or act upon a breach of the
                    Terms &amp; Conditions or Club Rules shall not be deemed to be an affirmation by onelifefitness that
                    the behaviour of the Member or Guest is acceptable.
                </p>
                <p>Except where permitted by this Contract, neither onelifefitness nor the Member may alter the terms of
                    this Contract without the express agreement of the other.</p>
                <h2>19. GOVERNING LAW AND JURISDICTION</h2>
                <p>These Terms &amp; Conditions, and any dispute or claim arising out of or in connection with
                    them or their subject matter or formation (including non-contractual disputes or claims),
                    shall be governed by, and construed in accordance with the law of England and Wales.
                </p>
                <p>Each party irrevocably agrees that the courts of England and Wales shall have exclusive
                    jurisdiction to settle any dispute or claim arising out of or in connection with these Terms
                    &amp; Conditions or their subject matter or formation (including non-contractual disputes or
                    claims).<br></p>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>