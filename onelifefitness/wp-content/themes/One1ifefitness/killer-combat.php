<?php /* Template Name: killer-combat */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
            <h2>Killer Combat</h2>
            <div class="_1-4gF4V">
                <p>If training montages have taught us anything, it’s that putting yourself through anaerobic hell is a
                    the best way to win. Whether you’re kicking training up a few notches in prep for a fight or just
                    like more workouts a bit tougher than most, try a combat circuit that would make even Rocky cry. BYO
                    Boxing Gloves/Bag Mitts</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Holborn</span></div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Select gym</option>
                    <option value="Holborn">Holborn</option>
                </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1213553" disabled="">18:00 Tuesday 18th (45 minutes) - Bookable 7am day before
                    </option>
                </select><a
                    href=""
                    target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/CombatConditioning_New.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>