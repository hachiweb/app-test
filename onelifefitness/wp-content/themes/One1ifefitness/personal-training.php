<?php /* Template Name: personal-training */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_1Z3ea-t _2rbE6TC _2v5bHvx">
        <div class="_3JE8_m6">
            <div class="_2PvrzEw _2rbE6TC _2v5bHvx">
                <h1 class="ke23QbA">Personal Training</h1>
            </div>
            <div class="_2jiohKc _2_4LOuF _2rbE6TC _2v5bHvx VBD7Ow3">
                <div class="_1jUu-8N">
                    <div class="_1QMkbpI">
                        <div>
                            <p><strong>Why work with a One Life Fitness PT?</strong>
                            </p>
                            <p class="text-justify">The onelifefitness PTs work with some of the best coaches in Ireland – each one bringing their unique personalities, specialist insight and expertise in club upon finishing training. Armed with the sharpest skills and latest techniques, choosing a onelifefitness Personal Trainer means that whatever your goal, you’ll be in the safest (and strongest) hands around.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="_9QOA4oa">
                    <div class="_2akYpU-">
                        <div class="_2gpiPPm _12G1ncO" data-aos="zoom-in-down">
                            <img src="<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png"
                                class="_3h_jpHd"></div>
                    </div>
                </div>
            </div>
            <div class="_2_4LOuF _2rbE6TC _2v5bHvx VBD7Ow3">
                <div class="_1jUu-8N">
                    <div class="_1QMkbpI">
                        <div>
                            <p><strong>Why work with a coach?</strong> </p>
                            <p class="text-justify">Teaming up with your PT, you can expect tailored assessments and programmes to guide you. Access to more knowledge than Google and Stephen Fry combined. And plenty of professional expertise in weight management, strength training, rehabilitation, body composition, combat sports, movement health and functional training.
                            </p>
                            <p class="text-justify">Whether you’re following their tailored or online training plan, your coach will make sure you’re performing all exercises correctly and keep pushing yourself. After all, if you want dem gains, you need to be held accountable – and that takes constant feedback, motivation and support. 
                            </p>
                            <p>If you’re interested in becoming one of our Personal Trainers, please <a
                                    href="<?= get_site_url(); ?>/career">click here</a>.
                            </p>
                            <p><br></p>
                        </div>
                    </div>
                </div>
                <div class="_9QOA4oa">
                    <div class="_2akYpU-">
                        <div class="_2gpiPPm _12G1ncO" data-aos="zoom-in-down"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png" class="_3h_jpHd">
                        </div>
                    </div>
                </div>
            </div>
            <div class="_2PvrzEw _2rbE6TC _2v5bHvx">
                <h1 class="ke23QbA">Get Involved</h1>
            </div>
            <div class="_2DxRudK _2rbE6TC _2v5bHvx">
                <div class="_2JiiLfi _2rbE6TC _2v5bHvx">
                    <div>
                        <p class="text-justify">Working with a onelifefitness Coach is the best investment you can make in both your health
                            and achieving your goals. So whatever that improvement looks like to you, it all
                            starts by dropping your deets into the form below…
                        </p>
                    </div>
                    <div class="HRKJIWa VBD7Ow3">
                        <form class="pt-enquiry-form">
                            <div class="_2nxeEK_ _14NUrhZ"><label for="fromName">Name</label><input name="fromName"
                                    placeholder="Name" type="text" value=""></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="fromEmail">Email</label><input name="fromEmail"
                                    placeholder="Email" type="email" value=""></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="message[mobile]">Phone</label><input
                                    name="message[mobile]" placeholder="Phone" type="text" value=""></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="message[goals]">Additional information
                                    about training goals</label><textarea name="message[goals]"
                                    placeholder="Additional information about training goals"></textarea></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="message[gym]">Club interested
                                    in</label>
                                    <select name="message[gym]" placeholder="Club interested in">
                                    <option value="select">Select</option>
                                    <option value="One Life Fitness">One Life Fitness</option>
                                    <option value="Rivers Edge Fitness">Rivers Edge Fitness</option>
                                </select>
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="ticket[charge]">I understand that by submitting
                                    my details I will be contacted by a onelifefitness freelance personal trainer with
                                    information about their services.</label>
                                <select name="ticket[charge]" placeholder="I understand that by submitting my details I will be contacted by a onelifefitness freelance personal trainer with information about their services.">
                                    <option value="select">Select</option>
                                    <option value="Agreed">Agreed</option>
                                </select>
                            </div>
                            <input type="hidden" name="subject" value="New PT enquiry">
                            <input type="hidden" name="ptEnquiry" value="true">
                            <input type="hidden" name="onelifefitness"
                                value=""><input type="submit" class="submit-pt-enquiry" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>