<?php /* Template Name: faqs */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_2ii-XsZ _2rbE6TC _2v5bHvx">
        <div class="_3xI4F15 VBD7Ow3">
            <h3 class="_32KNnSS _2rbE6TC _2v5bHvx">Frequently Asked Questions</h3>
            <div class="_2DQBLZb">
                <div class="_1U5x0G0 _1MNkAOW _2v5bHvx">
                    <a class="_3r1Y9_D _Y55-bj _2rbE6TC _2v5bHvx faqc1" href="javascript:void(0);">Booking Studio Classes</a>
                    <!-- <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc2" href="javascript:void(0);">Towels</a> -->
                    <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc3" href="javascript:void(0);">Lockers</a>
                    <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc4" href="javascript:void(0);">Linked Membership</a>
                    <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc5" href="javascript:void(0);">Personal Training</a>
                    <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc6" href="javascript:void(0);">Guest Passes</a>
                    <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc7" href="javascript:void(0);">Freezing Your Membership</a>
                    <!-- <a class="_Y55-bj _2rbE6TC _2v5bHvx faqc8" href="javascript:void(0);">Feedback</a> -->
                </div>
                <div class="_2x_S9gP _2eBzx2S _2v5bHvx">
                    <div class="_81firkY faq1">
                        <p>onelifefitness is famous for running the best and most creative studio timetable in the
                            country. To secure a place on your favourite class we offer advanced booking through
                            the onelifefitness App.<br></p>
                        <p>Members can book online from 7am the previous day.<br></p>
                        <p>The booking system will not allow a member to book onto classes that overlap each
                            other. </p>
                        <p>Please note that to ensure full attendance, we operate a "2 strikes" booking system,
                            whereby two no-shows in any 30 day period will result in a one-week booking
                            restriction; please always ensure you cancel your class booking no later than one
                            hour in advance of the class start time if you are no longer able to attend. </p>
                        <p>If you gain entry to the club without presenting your physical membership card this
                            may result in an unrecorded attendance and a class booking strike will be applied.
                        </p>
                        <p>If a class is full you will be given the option to book onto a waiting list. Once you
                            have been booked onto the waiting list you will receive an email to confirm. When a
                            space becomes available you will be automatically moved into the class and notified
                            via email up until an hour before the class start time. </p>
                        <p>Once on the waiting list you are committing to attend and could be auto booked into
                            the class up to 1 hour before the class start time. If you decide you can no longer
                            attend please remove yourself from the waiting list as a strike will be applied in
                            line with our class cancellation policy. 　
                        </p>
                        <p><strong><br></strong></p>
                        <p><strong>Registering for the first time?</strong><br></p>
                        <p><strong><br></strong></p>
                        <p>Visit the members' section on the website and you will be required to verify your
                            details. <a href="<?= get_site_url(); ?>/join-us"><u>Members
                                    Area</u></a>
                        </p>
                        <p><br>
                        </p>
                        <p><br>
                        </p>
                        <p><strong>Difficulty registering?
                            </strong></p>
                        <p><strong><br></strong></p>
                        <ul>
                            <li>Your email, postcode, mobile number and date of birth must match the ones
                                provided at sign up. </li>
                        </ul>
                        <p><br>
                        </p>
                        <ul>
                            <li>You will need your member number in order to register. This can be found on your
                                membership card or by calling your home club. <a href="<?= get_site_url(); ?>/join-us"><u>Club Contact Details</u></a>
                            </li>
                        </ul>
                        <p><br>
                        </p>
                        <ul>
                            <li>If you have any further issues please ask at the reception desk or call the club
                                to assist you. <a href="<?= get_site_url(); ?>/join-us"><u>Club Contacts</u></a></li>
                        </ul>
                        <p><br></p>
                        <p><br>
                        </p>
                    </div>

                    <div class="_81firkY dis-none faq2">
                        <p>Club users are entitled to one towel per visit. Additional towels are available at £1.50
                            each.</p>
                    </div>

                    <div class="_81firkY dis-none faq3">
                        <p>Lockers are provided to store your personal belongings in whilst training. Members must
                            provide their own padlock; we recommend a combination lock, to avoid carrying a key around
                            whilst using the gym. You may supply your own padlock, or you can purchase one at reception.
                            If you are tired of having to lug the old gym bag to work each day, Permanent Lockers are
                            available to hire (subject to availability). Please speak to reception to find out about
                            locker availability at your club. For more information please contact the member services
                            team here: <u><a href="<?= get_site_url(); ?>/join-us">member-services</a></u>
                        </p>
                    </div>

                    <div class="_81firkY dis-none faq4">
                        <p>Experience everything that onelifefitness has to offer by upgrading to a Linked Membership. For just
                            a small added fee per month you can use all of the clubs in the onelifefitness group. See the
                            website for information about all of the <u><a href="" target="_blank"
                                    rel="noreferrer noopener">onelifefitness sites</a></u>. For more information please contact
                            the member services team here: <u><a
                                    href="<?= get_site_url(); ?>/join-us">member-services</a></u>
                        </p>
                    </div>

                    <div class="_81firkY dis-none faq5">
                        <p>onelifefitness VPTs (Very Personal Trainers) are the very best in the business and guarantee results.
                            If you have problems achieving your exercise goals then Personal Training is definitely the
                            recommended solution. If you have questions on how Personal Training can benefit you, please
                            feel free to speak to any Trainer directly, or alternatively contact them directly using the
                            details on the interactive digital noticeboards (located on the gym floor). Note that the
                            VPTs are not employees or agents of onelifefitness, but are independent professionals.</p>
                    </div>


                    <div class="_81firkY dis-none faq6">
                        <p>Non-members can use onelifefitness on a day pass for a cost of 10€ or register for a free pass. All non-members will be asked for
                            a valid form of ID and to complete an exercise waiver prior to entry, and all Club Rules
                            must be followed whilst using the gym. If a guest wants to use onelifefitness on a more frequent
                            basis, it may be more cost effective to take out a membership; speak to reception for more
                            information.</p>
                    </div>

                    <div class="_81firkY dis-none faq7">
                        <p>A membership can be frozen if you are unable to use onelifefitness for a specific period of time, and
                            instead of paying your normal monthly rates we reduce this to just a £20 monthly holding fee
                            whilst you are away. To freeze your membership we would require notice by the 20th day of
                            the month prior to the freeze start date. The freeze period must always start on the 1st day
                            of a month and you may freeze for between one and six full months at a time. onelifefitness will
                            freeze free of charge if a member is sick, injured or pregnant (medical proof will be
                            required). For more information please contact the member services team here: <u><a
                                    href="<?= get_site_url(); ?>/join-us">member-services</a></u>
                        </p>
                    </div>


                    <div class="_81firkY dis-none faq8">
                        <p>With around 100 different classes every week and the coolest, sweatiest venues in town we
                            reckon that we’re getting pretty close. But the perfect gym still doesn’t exist. So if
                            there’s something you want to tell us, please contact the general manager of your club.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function(){
        $("._Y55-bj").click(function(){
            $("._2rbE6TC").removeClass("_3r1Y9_D");
            $(this).addClass("_3r1Y9_D");
        });
        $(".faqc1").click(function(){
            $(".faq1").show();
            $(".faq2").hide();
            $(".faq3").hide();
            $(".faq4").hide();
            $(".faq5").hide();
            $(".faq6").hide();
            $(".faq7").hide();
            $(".faq8").hide();
        });
        $(".faqc2").click(function(){
            $(".faq1").hide();
            $(".faq2").show();
            $(".faq3").hide();
            $(".faq4").hide();
            $(".faq5").hide();
            $(".faq6").hide();
            $(".faq7").hide();
            $(".faq8").hide();
        });
        $(".faqc3").click(function(){
            $(".faq1").hide();
            $(".faq2").hide();
            $(".faq3").show();
            $(".faq4").hide();
            $(".faq5").hide();
            $(".faq6").hide();
            $(".faq7").hide();
            $(".faq8").hide();
        });
        $(".faqc4").click(function(){
            $(".faq4").show();
            $(".faq1").hide();
            $(".faq2").hide();
            $(".faq3").hide();
            $(".faq5").hide();
            $(".faq6").hide();
            $(".faq7").hide();
            $(".faq8").hide();
        });
        $(".faqc5").click(function(){
            $(".faq4").hide();
            $(".faq1").hide();
            $(".faq2").hide();
            $(".faq3").hide();
            $(".faq5").show();
            $(".faq6").hide();
            $(".faq7").hide();
            $(".faq8").hide();
        });
        $(".faqc6").click(function(){
            $(".faq4").hide();
            $(".faq1").hide();
            $(".faq2").hide();
            $(".faq3").hide();
            $(".faq5").hide();
            $(".faq6").show();
            $(".faq7").hide();
            $(".faq8").hide();
        });
        $(".faqc7").click(function(){
            $(".faq4").hide();
            $(".faq1").hide();
            $(".faq2").hide();
            $(".faq3").hide();
            $(".faq5").hide();
            $(".faq6").hide();
            $(".faq7").show();
            $(".faq8").hide();
        });
        $(".faqc8").click(function(){
            $(".faq4").hide();
            $(".faq1").hide();
            $(".faq2").hide();
            $(".faq3").hide();
            $(".faq5").hide();
            $(".faq6").hide();
            $(".faq7").hide();
            $(".faq8").show();
        });
    });
</script>
<?php
// get_sidebar();
get_footer();
?>