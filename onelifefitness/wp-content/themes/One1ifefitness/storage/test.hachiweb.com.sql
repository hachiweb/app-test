-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 08, 2020 at 07:46 AM
-- Server version: 10.2.30-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u565879516_olf`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(0, 2, '_wp_trash_meta_status', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-02-04 04:12:57', '2020-02-04 04:12:57', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 1, 'admin', 'testhachiweb@gmail.com', '', '::1', '2020-02-27 06:57:54', '2020-02-27 06:57:54', 'helloooo', 0, 'trash', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/82.0.4068.5 Safari/537.36', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_contact`
--

CREATE TABLE `wp_contact` (
  `id` int(11) NOT NULL,
  `fromName` text NOT NULL,
  `fromEmail` text NOT NULL,
  `mobile_number` text NOT NULL,
  `message_goals` text NOT NULL,
  `club_intrested` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_contact`
--

INSERT INTO `wp_contact` (`id`, `fromName`, `fromEmail`, `mobile_number`, `message_goals`, `club_intrested`) VALUES
(1, 'Ronan Sexton', 'juceq@mailinator.com', '846', 'Ducimus et consequa', 'onelife fitness sullivans quay'),
(2, 'Ronan Sexton', 'juceq@mailinator.com', '846', 'Ducimus et consequa', 'onelife fitness sullivans quay'),
(3, 'Ronan Sexton', 'juceq@mailinator.com', '846', 'Ducimus et consequa', 'onelife fitness sullivans quay'),
(4, 'Omar Sears', 'xoxudeg@mailinator.net', '911', 'Dolor do a molestias', 'onelife fitness camden quay');

-- --------------------------------------------------------

--
-- Table structure for table `wp_freepass`
--

CREATE TABLE `wp_freepass` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `choose_gym` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_freepass`
--

INSERT INTO `wp_freepass` (`id`, `name`, `email`, `phone`, `choose_gym`) VALUES
(1, 'Sybil Anderson', 'gabadupafu@mailinator.net', '+1 (392) 252-9118', 'Rivers Edge Fitness'),
(2, 'Sybil Anderson', 'gabadupafu@mailinator.net', '+1 (392) 252-9118', 'Rivers Edge Fitness'),
(3, 'Sybil Anderson', 'gabadupafu@mailinator.net', '+1 (392) 252-9118', 'Rivers Edge Fitness'),
(4, 'Sybil Anderson', 'gabadupafu@mailinator.net', '+1 (392) 252-9118', 'Rivers Edge Fitness'),
(5, 'Dexter Perkins', 'tawositu@mailinator.com', '+1 (147) 692-4427', 'Rivers Edge Fitness'),
(6, 'Lavinia Anthony', 'korejifule@mailinator.net', '+1 (292) 382-2978', 'Rivers Edge Fitness'),
(7, 'Yoshio Roman', 'xigefiwe@mailinator.com', '+1 (947) 985-9556', 'Rivers Edge Fitness'),
(8, 'Zorita Bowers', 'juhybyja@mailinator.com', '+1 (967) 235-8323', 'Rivers Edge Fitness'),
(9, 'Aphrodite Leonard', 'bezopem@mailinator.com', '+1 (318) 298-9158', 'One Life Fitness'),
(10, 'Bertha Riggs', 'kyhatiloli@mailinator.net', '+1 (521) 956-2579', 'Rivers Edge Fitness'),
(11, 'Ashton Barry', 'hyrupeqe@mailinator.net', '+1 (663) 297-1428', 'One Life Fitness'),
(12, 'Richard Mcfadden', 'danemom@mailinator.net', '+1 (575) 635-8066', 'Rivers Edge Fitness'),
(13, 'Kelly Heath', 'pydukybeto@mailinator.com', '+1 (681) 768-8574', 'Rivers Edge Fitness'),
(14, 'Martina Solomon', 'cuputa@mailinator.com', '+1 (489) 741-3777', 'One Life Fitness'),
(15, 'Nelle Walter', 'turosise@mailinator.com', '+1 (986) 249-4303', 'Rivers Edge Fitness'),
(16, 'Mallory Boyd', 'zibyhecu@mailinator.com', '9968123375', 'onelife fitness sullivans quay'),
(17, 'Jarrod Kirby', 'lytywywo@mailinator.com', '+1 (749) 659-9414', 'onelife fitness sullivans quay'),
(18, 'Merrill Benton', 'halusywyr@mailinator.com', '+1 (972) 982-1235', 'onelife fitness camden quay'),
(19, 'Adrian Oneil', 'pawuzyfoxu@mailinator.net', '+1 (914) 526-1407', 'onelife fitness camden quay'),
(20, 'Nomlanga Barrera', 'kegyt@mailinator.com', '+1 (663) 247-8131', 'onelife fitness camden quay'),
(21, 'Cain Miller', 'cebyz@mailinator.com', '+1 (741) 917-6301', 'onelife fitness camden quay'),
(22, 'Bertha Sargent', 'dizy@mailinator.com', '+1 (948) 223-1019', 'onelife fitness camden quay'),
(23, 'Oscar Willis', 'betesaduty@mailinator.com', '+1 (578) 721-3963', 'onelife fitness camden quay'),
(24, 'Donovan Vazquez', 'quqaripe@mailinator.com', '+1 (816) 597-7306', 'onelife fitness camden quay'),
(25, 'Ignacia Tanner', 'hycozuryd@mailinator.net', '+1 (197) 855-2034', 'onelife fitness sullivans quay'),
(26, 'Anne Baird', 'hyjatytimy@mailinator.com', '+1 (851) 507-5758', 'onelife fitness sullivans quay');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_membership`
--

CREATE TABLE `wp_membership` (
  `id` int(11) NOT NULL,
  `select_club` text NOT NULL,
  `user_name` text NOT NULL,
  `email` text NOT NULL,
  `user_phone` text NOT NULL,
  `membership_number` text NOT NULL,
  `ticket_reasontype` text NOT NULL,
  `ticket_reasonforfreeze` text NOT NULL,
  `ticket_freeze_startdate` text NOT NULL,
  `ticket_freeze_enddate` text NOT NULL,
  `ticket_comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_membership`
--

INSERT INTO `wp_membership` (`id`, `select_club`, `user_name`, `email`, `user_phone`, `membership_number`, `ticket_reasontype`, `ticket_reasonforfreeze`, `ticket_freeze_startdate`, `ticket_freeze_enddate`, `ticket_comments`) VALUES
(1, 'One Life Fitness', 'posewes', 'sinigirate@mailinator.com', '+1 (399) 222-7409', '928', 'Request a freeze', 'Financial', '01 August 2020', '30 April 2020', 'Consequatur libero q'),
(2, 'One Life Fitness', 'posewes', 'sinigirate@mailinator.com', '+1 (399) 222-7409', '928', 'Request a freeze', 'Financial', '01 August 2020', '30 April 2020', 'Consequatur libero q'),
(3, 'One Life Fitness', 'posewes', 'sinigirate@mailinator.com', '+1 (399) 222-7409', '928', 'Request a freeze', 'Financial', '01 August 2020', '30 April 2020', 'Consequatur libero q'),
(4, 'One Life Fitness', 'posewes', 'sinigirate@mailinator.com', '+1 (399) 222-7409', '928', 'Request a freeze', 'Financial', '01 August 2020', '30 April 2020', 'Consequatur libero q'),
(5, 'One Life Fitness', 'donuwy', 'jocoguzul@mailinator.net', '+1 (264) 663-3165', '441', 'Request a freeze', 'Season', '01 August 2020', '31 July 2020', 'Aliquid consequat D'),
(6, 'Rivers Edge Fitness', 'jawomyxabo', 'qubawuge@mailinator.net', '+1 (841) 772-7685', '542', 'Request a freeze', 'Medical/Injury', '01 July 2020', '31 July 2020', 'Do quas non quae qui'),
(7, 'Rivers Edge Fitness', 'tawatihi', 'kubiluqy@mailinator.net', '+1 (799) 822-6592', '526', 'Request to unfreeze', 'Work relocation', '01 April 2020', '30 April 2020', 'Rerum ex elit volup'),
(8, 'Rivers Edge Fitness', 'defoca', 'pekyn@mailinator.net', '+1 (406) 911-5819', '516', 'Request to unfreeze', 'Medical/Injury', '01 April 2020', '31 August 2020', 'Laboriosam consecte'),
(9, 'Rivers Edge Fitness', 'piqevobivo', 'fajisa@mailinator.net', '+1 (317) 673-2582', '497', 'Request a freeze', 'Pregnancy', '01 September 2020', '30 June 2020', 'Lorem duis assumenda'),
(10, 'One Life Fitness', 'zurygode', 'womo@mailinator.com', '+1 (606) 121-2819', '858', 'Request to unfreeze', 'Financial', '01 May 2020', '31 May 2020', 'Omnis distinctio No'),
(11, 'One Life Fitness', 'rugufoba', 'fupep@mailinator.com', '+1 (496) 164-7053', '47', 'Request to unfreeze', 'Pregnancy', '01 September 2020', '30 June 2020', 'Nulla recusandae Re'),
(12, 'One Life Fitness', 'xytaso', 'qynyk@mailinator.net', '+1 (762) 289-7512', '172', 'Request to unfreeze', 'Season', '01 April 2020', '31 May 2020', 'Est iusto quam quam '),
(13, 'One Life Fitness', 'lozat', 'huhami@mailinator.net', '+1 (518) 152-1013', '557', 'Request to unfreeze', 'Pregnancy', '01 June 2020', '30 April 2020', 'Omnis qui repellendu');

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://test.hachiweb.com/onelifefitness', 'yes'),
(2, 'home', 'https://test.hachiweb.com/onelifefitness', 'yes'),
(3, 'blogname', 'ONE L1FE FITNESS', 'yes'),
(4, 'blogdescription', 'A New Breed Of Gym In Cork City', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'testhachiweb@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '6', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/blog?post=/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:87:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:58:\"blog?post=/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:53:\"blog?post=/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:34:\"blog?post=/category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:46:\"blog?post=/category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:28:\"blog?post=/category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:55:\"blog?post=/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:50:\"blog?post=/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:31:\"blog?post=/tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:43:\"blog?post=/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:25:\"blog?post=/tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:56:\"blog?post=/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:51:\"blog?post=/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:32:\"blog?post=/type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:44:\"blog?post=/type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:26:\"blog?post=/type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:58:\"blog?post=/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:53:\"blog?post=/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:34:\"blog?post=/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:46:\"blog?post=/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:28:\"blog?post=/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:80:\"blog?post=/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:75:\"blog?post=/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:56:\"blog?post=/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:68:\"blog?post=/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:50:\"blog?post=/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:67:\"blog?post=/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:62:\"blog?post=/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:43:\"blog?post=/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:55:\"blog?post=/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:37:\"blog?post=/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:54:\"blog?post=/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:49:\"blog?post=/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:30:\"blog?post=/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:42:\"blog?post=/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:24:\"blog?post=/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:38:\"blog?post=/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"blog?post=/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"blog?post=/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"blog?post=/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"blog?post=/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"blog?post=/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"blog?post=/([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:31:\"blog?post=/([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:51:\"blog?post=/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:46:\"blog?post=/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:39:\"blog?post=/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:46:\"blog?post=/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:35:\"blog?post=/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:27:\"blog?post=/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"blog?post=/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"blog?post=/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"blog?post=/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"blog?post=/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"blog?post=/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:0:{}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:93:\"/home/u565879516/domains/hachistaging.com/public_html/olf/wp-content/themes/gym-box/style.css\";i:2;s:60:\"D:\\Server\\htdocs\\gym_box/wp-content/themes/gym-box/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'One1ifefitness-child', 'yes'),
(41, 'stylesheet', 'One1ifefitness-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '45805', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:27:\"wp-pagenavi/wp-pagenavi.php\";s:14:\"__return_false\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1596341572', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:6:{i:1583655179;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1583683979;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1583727178;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583727203;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1583727398;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:1:{s:22:\"MYZmNWALo1KYry75kJ8EvH\";a:2:{s:10:\"hashed_key\";s:34:\"$P$BW.2xAmsDFvyyYR6.92zJH3N7uR3iT/\";s:10:\"created_at\";i:1583599101;}}', 'yes'),
(115, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1583650264;s:15:\"version_checked\";s:5:\"5.3.2\";s:12:\"translations\";a:0:{}}', 'no'),
(116, 'theme_mods_twentytwenty', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1583651963;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-1\";a:3:{i:1;s:10:\"archives-2\";i:2;s:12:\"categories-2\";i:3;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(126, 'can_compress_scripts', '1', 'no'),
(157, 'theme_mods_gym-box', 'a:4:{s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:329;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1583651950;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-1\";a:3:{i:1;s:10:\"archives-2\";i:2;s:12:\"categories-2\";i:3;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(158, 'current_theme', 'One1ifefitness Child Theme', 'yes'),
(159, 'theme_switched', '', 'yes'),
(160, 'theme_switched_via_customizer', '', 'yes'),
(161, 'customize_stashed_theme_mods', 'a:0:{}', 'no'),
(162, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1583651950;s:7:\"checked\";a:7:{s:20:\"One1ifefitness-child\";s:5:\"1.0.0\";s:14:\"One1ifefitness\";s:5:\"1.0.0\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:18:\"twentytwenty-child\";s:3:\"1.0\";s:12:\"twentytwenty\";s:3:\"1.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(165, 'nav_menu_options', 'a:1:{s:8:\"auto_add\";a:0:{}}', 'yes'),
(208, 'WPLANG', '', 'yes'),
(209, 'new_admin_email', 'testhachiweb@gmail.com', 'yes'),
(628, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1582793524;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(645, 'recently_activated', 'a:2:{s:47:\"one-life-popup-banner/one-life-popup-banner.php\";i:1583293468;s:41:\"popup-contact-form/popup-contact-form.php\";i:1583293352;}', 'yes'),
(717, 'category_children', 'a:0:{}', 'yes'),
(785, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1583650266;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"ari-adminer/ari-adminer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/ari-adminer\";s:4:\"slug\";s:11:\"ari-adminer\";s:6:\"plugin\";s:27:\"ari-adminer/ari-adminer.php\";s:11:\"new_version\";s:5:\"1.2.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/ari-adminer/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/ari-adminer.1.2.3.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:62:\"https://s.w.org/plugins/geopattern-icon/ari-adminer_7a2e4b.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(786, 'pagenavi_options', 'a:15:{s:10:\"pages_text\";s:36:\"Page %CURRENT_PAGE% of %TOTAL_PAGES%\";s:12:\"current_text\";s:13:\"%PAGE_NUMBER%\";s:9:\"page_text\";s:13:\"%PAGE_NUMBER%\";s:10:\"first_text\";s:8:\"« First\";s:9:\"last_text\";s:7:\"Last »\";s:9:\"prev_text\";s:2:\"«\";s:9:\"next_text\";s:2:\"»\";s:12:\"dotleft_text\";s:3:\"...\";s:13:\"dotright_text\";s:3:\"...\";s:9:\"num_pages\";i:5;s:23:\"num_larger_page_numbers\";i:3;s:28:\"larger_page_numbers_multiple\";i:10;s:11:\"always_show\";i:0;s:16:\"use_pagenavi_css\";i:1;s:5:\"style\";i:1;}', 'yes'),
(789, 'recovery_mode_email_last_sent', '1583599101', 'yes'),
(861, '_site_transient_timeout_browser_e3168ce13635383f9e47b430e0930917', '1584015216', 'no'),
(862, '_site_transient_browser_e3168ce13635383f9e47b430e0930917', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:11:\"82.0.4068.5\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(863, '_site_transient_timeout_php_check_78e1776a2900a8656cebe7d7ea2a07cc', '1584015218', 'no'),
(864, '_site_transient_php_check_78e1776a2900a8656cebe7d7ea2a07cc', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(881, '_site_transient_timeout_browser_c11f2719a05815529ee87ba2eb5b4881', '1584092691', 'no'),
(882, '_site_transient_browser_c11f2719a05815529ee87ba2eb5b4881', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:11:\"82.0.4077.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(0, 416, '_edit_lock', '1583590275:1'),
(17, 9, '_edit_lock', '1581997897:1'),
(18, 9, '_edit_last', '1'),
(19, 9, '_wp_page_template', 'index.php'),
(41, 20, '_edit_lock', '1580878066:1'),
(42, 20, '_wp_page_template', 'laughing-therapy.php'),
(45, 25, '_edit_lock', '1580880062:1'),
(46, 25, '_wp_page_template', 'regeneration-z.php'),
(47, 27, '_edit_lock', '1580884116:1'),
(48, 27, '_wp_page_template', 'rampant-rehab.php'),
(49, 29, '_edit_lock', '1580881766:1'),
(50, 29, '_wp_page_template', 'sword-play.php'),
(52, 32, '_edit_lock', '1580882209:1'),
(53, 32, '_wp_page_template', 'rolling-with-my-yogis.php'),
(55, 34, '_edit_lock', '1580886638:1'),
(56, 34, '_wp_page_template', 'career.php'),
(57, 36, '_edit_lock', '1580894661:1'),
(58, 36, '_wp_page_template', 'services.php'),
(60, 39, '_edit_lock', '1580900808:1'),
(61, 39, '_wp_page_template', 'faqs.php'),
(64, 43, '_edit_lock', '1580902651:1'),
(65, 43, '_wp_page_template', 'refer.php'),
(66, 45, '_edit_lock', '1580902675:1'),
(67, 45, '_wp_page_template', 'club-rules.php'),
(68, 47, '_edit_lock', '1580903476:1'),
(69, 47, '_wp_page_template', 'terms-and-conditions.php'),
(70, 49, '_edit_lock', '1580904380:1'),
(71, 49, '_wp_page_template', 'privacy-policy.php'),
(72, 49, '_edit_last', '1'),
(76, 52, '_edit_lock', '1581060529:1'),
(77, 52, '_wp_page_template', 'bank.php'),
(78, 54, '_edit_lock', '1580969840:1'),
(79, 54, '_wp_page_template', 'login.php'),
(80, 56, '_edit_lock', '1580982351:1'),
(83, 59, '_edit_lock', '1580988918:1'),
(84, 59, '_edit_last', '1'),
(85, 59, '_wp_page_template', 'timetable.php'),
(86, 62, '_edit_lock', '1580989116:1'),
(87, 62, '_edit_last', '1'),
(88, 62, '_wp_page_template', 'join-us.php'),
(89, 64, '_edit_lock', '1580990182:1'),
(90, 64, '_edit_last', '1'),
(91, 64, '_wp_page_template', 'personal-training.php'),
(92, 66, '_edit_lock', '1581047892:1'),
(93, 66, '_edit_last', '1'),
(94, 66, '_wp_page_template', 'about.php'),
(95, 68, '_edit_lock', '1581052066:1'),
(96, 68, '_edit_last', '1'),
(97, 68, '_wp_page_template', 'corporate.php'),
(99, 71, '_edit_lock', '1581509771:1'),
(102, 77, '_edit_lock', '1581509739:1'),
(105, 80, '_edit_lock', '1581510392:1'),
(114, 83, '_wp_trash_meta_status', 'publish'),
(115, 83, '_wp_trash_meta_time', '1581510463'),
(116, 85, '_wp_trash_meta_status', 'publish'),
(117, 85, '_wp_trash_meta_time', '1581510518'),
(118, 86, '_edit_lock', '1581576258:1'),
(119, 88, '_edit_lock', '1581576974:1'),
(120, 88, '_edit_last', '1'),
(121, 88, '_wp_page_template', 'categories.php'),
(122, 90, '_edit_lock', '1581581261:1'),
(123, 90, '_edit_last', '1'),
(124, 90, '_wp_page_template', 'classes.php'),
(125, 92, '_edit_lock', '1583410714:1'),
(126, 92, '_edit_last', '1'),
(127, 92, '_wp_page_template', 'aerial-hoop.php'),
(128, 94, '_edit_lock', '1583410604:1'),
(129, 94, '_edit_last', '1'),
(130, 94, '_wp_page_template', 'aerial-pilates.php'),
(131, 96, '_edit_lock', '1583410756:1'),
(132, 96, '_edit_last', '1'),
(133, 96, '_wp_page_template', 'aerial-yoga.php'),
(134, 98, '_edit_lock', '1581594858:1'),
(135, 98, '_edit_last', '1'),
(136, 98, '_wp_page_template', 'cirqueit.php'),
(137, 100, '_edit_lock', '1581595423:1'),
(138, 100, '_edit_last', '1'),
(139, 100, '_wp_page_template', 'cocoon.php'),
(140, 102, '_edit_lock', '1581596053:1'),
(141, 102, '_edit_last', '1'),
(142, 102, '_wp_page_template', 'false-grip.php'),
(143, 104, '_edit_lock', '1581652977:1'),
(144, 104, '_edit_last', '1'),
(145, 104, '_wp_page_template', 'invert-yourself.php'),
(146, 106, '_edit_lock', '1581653388:1'),
(147, 106, '_edit_last', '1'),
(148, 106, '_wp_page_template', 'superfly.php'),
(149, 108, '_edit_lock', '1581654472:1'),
(150, 108, '_edit_last', '1'),
(151, 108, '_wp_page_template', 'trapeze.php'),
(152, 110, '_edit_lock', '1581655591:1'),
(153, 110, '_edit_last', '1'),
(154, 110, '_wp_page_template', 'combat-sports.php'),
(155, 112, '_edit_lock', '1583410793:1'),
(156, 112, '_edit_last', '1'),
(157, 112, '_wp_page_template', 'brazilian-jui-jitsu.php'),
(158, 114, '_edit_lock', '1583410820:1'),
(159, 114, '_edit_last', '1'),
(160, 114, '_wp_page_template', 'counterkick.php'),
(162, 117, '_edit_lock', '1583410878:1'),
(165, 117, '_edit_last', '1'),
(166, 117, '_wp_page_template', 'counterpunch.php'),
(167, 121, '_edit_lock', '1581658068:1'),
(168, 121, '_edit_last', '1'),
(169, 121, '_wp_page_template', 'gymboxing.php'),
(170, 123, '_edit_lock', '1581658579:1'),
(171, 123, '_edit_last', '1'),
(172, 123, '_wp_page_template', 'gymboxing-sparring.php'),
(173, 125, '_edit_lock', '1581659038:1'),
(174, 125, '_edit_last', '1'),
(175, 125, '_wp_page_template', 'kickboxing.php'),
(176, 127, '_edit_lock', '1581659441:1'),
(177, 127, '_edit_last', '1'),
(178, 127, '_wp_page_template', 'kickboxing-fighters-club.php'),
(179, 129, '_edit_lock', '1581659943:1'),
(180, 129, '_edit_last', '1'),
(181, 129, '_wp_page_template', 'killer-combat.php'),
(182, 131, '_edit_lock', '1581660128:1'),
(183, 131, '_edit_last', '1'),
(184, 131, '_wp_page_template', 'krav-maga.php'),
(186, 134, '_edit_lock', '1581660321:1'),
(187, 134, '_edit_last', '1'),
(188, 134, '_wp_page_template', 'l1-white-collar-fight-club.php'),
(189, 136, '_edit_lock', '1581660464:1'),
(190, 136, '_edit_last', '1'),
(191, 136, '_wp_page_template', 'l2-white-collar-fight-club.php'),
(192, 138, '_edit_lock', '1581660619:1'),
(193, 138, '_edit_last', '1'),
(194, 138, '_wp_page_template', 'ladies-only-bjj.php'),
(195, 140, '_edit_lock', '1581662182:1'),
(196, 140, '_edit_last', '1'),
(197, 140, '_wp_page_template', 'mma.php'),
(198, 142, '_edit_lock', '1581662482:1'),
(200, 142, '_edit_last', '1'),
(201, 142, '_wp_page_template', 'muay-thai.php'),
(202, 145, '_edit_lock', '1581663093:1'),
(203, 145, '_edit_last', '1'),
(204, 145, '_wp_page_template', 'muay-thai-sparring-2.php'),
(205, 147, '_edit_lock', '1581663334:1'),
(206, 147, '_edit_last', '1'),
(207, 147, '_wp_page_template', 'muay-thai-beginners.php'),
(208, 149, '_edit_lock', '1581663516:1'),
(209, 149, '_edit_last', '1'),
(210, 149, '_wp_page_template', 'muay-thai-fighters-club.php'),
(211, 151, '_edit_lock', '1581663699:1'),
(212, 151, '_edit_last', '1'),
(213, 151, '_wp_page_template', 'muay-thai-int-adv.php'),
(214, 153, '_edit_lock', '1581663974:1'),
(215, 153, '_edit_last', '1'),
(216, 153, '_wp_page_template', 'muay-thai-sparring.php'),
(217, 155, '_edit_lock', '1581664421:1'),
(218, 155, '_edit_last', '1'),
(219, 155, '_wp_page_template', 'no-gi.php'),
(220, 157, '_edit_lock', '1581677137:1'),
(221, 157, '_edit_last', '1'),
(222, 157, '_wp_page_template', 'sluggers-club.php'),
(223, 159, '_edit_lock', '1581665254:1'),
(224, 159, '_edit_last', '1'),
(225, 159, '_wp_page_template', 'cycle-club.php'),
(226, 161, '_edit_lock', '1583410948:1'),
(227, 161, '_edit_last', '1'),
(228, 161, '_wp_page_template', 'bike-beats.php'),
(229, 163, '_edit_lock', '1583411014:1'),
(230, 163, '_edit_last', '1'),
(231, 163, '_wp_page_template', 'cyclone.php'),
(232, 165, '_edit_lock', '1583411093:1'),
(233, 165, '_edit_last', '1'),
(234, 165, '_wp_page_template', 'power-battle.php'),
(235, 167, '_edit_lock', '1581670100:1'),
(236, 167, '_edit_last', '1'),
(237, 167, '_wp_page_template', 'functional-training.php'),
(239, 170, '_edit_lock', '1581671007:1'),
(241, 172, '_edit_lock', '1583411141:1'),
(242, 172, '_edit_last', '1'),
(243, 172, '_wp_page_template', 'b-o-l-t.php'),
(244, 174, '_edit_lock', '1583411180:1'),
(245, 174, '_edit_last', '1'),
(246, 174, '_wp_page_template', 'barfly.php'),
(247, 176, '_edit_lock', '1583411214:1'),
(248, 176, '_edit_last', '1'),
(249, 176, '_wp_page_template', 'bartendaz.php'),
(250, 178, '_edit_lock', '1583411694:1'),
(251, 178, '_edit_last', '1'),
(252, 178, '_wp_page_template', 'battleballs.php'),
(253, 180, '_edit_lock', '1581673068:1'),
(254, 180, '_edit_last', '1'),
(255, 180, '_wp_page_template', 'bodyweight-bandits.php'),
(256, 182, '_edit_lock', '1581673625:1'),
(257, 182, '_edit_last', '1'),
(258, 182, '_wp_page_template', 'cavemen-and-neandergals.php'),
(259, 184, '_edit_lock', '1581673879:1'),
(260, 184, '_edit_last', '1'),
(261, 184, '_wp_page_template', 'functional-bodybuilding.php'),
(262, 186, '_edit_lock', '1581674151:1'),
(263, 186, '_edit_last', '1'),
(264, 186, '_wp_page_template', 'gymnastic-conditioning.php'),
(265, 188, '_edit_lock', '1581674557:1'),
(266, 188, '_edit_last', '1'),
(267, 188, '_wp_page_template', 'ocr-training.php'),
(268, 190, '_edit_lock', '1581674760:1'),
(269, 190, '_edit_last', '1'),
(270, 190, '_wp_page_template', 'prehab.php'),
(271, 192, '_edit_lock', '1581675504:1'),
(272, 192, '_edit_last', '1'),
(273, 192, '_wp_page_template', 'rehab.php'),
(274, 194, '_edit_lock', '1581675831:1'),
(275, 194, '_edit_last', '1'),
(276, 194, '_wp_page_template', 'rowingwod.php'),
(277, 196, '_edit_lock', '1581676068:1'),
(278, 196, '_edit_last', '1'),
(279, 196, '_wp_page_template', 'strongman.php'),
(280, 198, '_edit_lock', '1581676327:1'),
(281, 198, '_edit_last', '1'),
(282, 198, '_wp_page_template', 'suspenders.php'),
(283, 200, '_edit_lock', '1581677146:1'),
(284, 200, '_edit_last', '1'),
(285, 200, '_wp_page_template', 'swingers-club.php'),
(286, 202, '_edit_lock', '1581677574:1'),
(287, 202, '_edit_last', '1'),
(288, 202, '_wp_page_template', 'the-fundamentals.php'),
(289, 204, '_edit_lock', '1581677880:1'),
(290, 204, '_edit_last', '1'),
(291, 204, '_wp_page_template', 'wod-squad.php'),
(292, 206, '_edit_lock', '1581678351:1'),
(293, 206, '_edit_last', '1'),
(294, 206, '_wp_page_template', 'holistic-retreat.php'),
(295, 208, '_edit_lock', '1583411256:1'),
(296, 208, '_edit_last', '1'),
(297, 208, '_wp_page_template', 'ashtanga-yoga.php'),
(298, 210, '_edit_lock', '1583411309:1'),
(300, 210, '_edit_last', '1'),
(301, 210, '_wp_page_template', 'balates.php'),
(302, 213, '_edit_lock', '1583411359:1'),
(303, 213, '_edit_last', '1'),
(304, 213, '_wp_page_template', 'budokon-yoga.php'),
(305, 215, '_edit_lock', '1581681020:1'),
(306, 215, '_edit_last', '1'),
(307, 215, '_wp_page_template', 'buti-yoga.php'),
(308, 217, '_edit_lock', '1581681372:1'),
(309, 217, '_edit_last', '1'),
(310, 217, '_wp_page_template', 'contortion.php'),
(311, 219, '_edit_lock', '1581681828:1'),
(313, 219, '_edit_last', '1'),
(314, 219, '_wp_page_template', 'hatha-yoga.php'),
(315, 222, '_edit_lock', '1581682409:1'),
(316, 222, '_edit_last', '1'),
(317, 222, '_wp_page_template', 'paddleboard-yoga.php'),
(318, 224, '_edit_lock', '1581682697:1'),
(319, 224, '_edit_last', '1'),
(320, 224, '_wp_page_template', 'pilates.php'),
(321, 226, '_edit_lock', '1581683016:1'),
(322, 226, '_edit_last', '1'),
(323, 226, '_wp_page_template', 'rocket-yoga.php'),
(324, 228, '_edit_lock', '1581739880:1'),
(325, 228, '_edit_last', '1'),
(326, 228, '_wp_page_template', 'vinyasa-yoga.php'),
(327, 230, '_edit_lock', '1581740535:1'),
(328, 230, '_edit_last', '1'),
(329, 230, '_wp_page_template', 'yin-yoga.php'),
(330, 232, '_edit_lock', '1581740878:1'),
(331, 232, '_edit_last', '1'),
(332, 232, '_wp_page_template', 'yoga-for-lifting.php'),
(333, 234, '_edit_lock', '1581741013:1'),
(334, 234, '_edit_last', '1'),
(335, 234, '_wp_page_template', 'yogangster.php'),
(336, 236, '_edit_lock', '1581741374:1'),
(337, 236, '_edit_last', '1'),
(338, 236, '_wp_page_template', 'look-better-naked.php'),
(339, 238, '_edit_lock', '1583411400:1'),
(340, 238, '_edit_last', '1'),
(341, 238, '_wp_page_template', 'badass.php'),
(342, 240, '_edit_lock', '1583411456:1'),
(343, 240, '_edit_last', '1'),
(344, 240, '_wp_page_template', 'gain-train.php'),
(345, 242, '_edit_lock', '1583411494:1'),
(346, 242, '_edit_last', '1'),
(347, 242, '_wp_page_template', 'hardcore.php'),
(348, 244, '_edit_lock', '1581743504:1'),
(349, 244, '_edit_last', '1'),
(350, 244, '_wp_page_template', 'reppin.php'),
(351, 246, '_edit_lock', '1581745398:1'),
(352, 246, '_edit_last', '1'),
(353, 246, '_wp_page_template', 'sweat-to-the-beat.php'),
(354, 248, '_edit_lock', '1581745628:1'),
(355, 248, '_edit_last', '1'),
(356, 248, '_wp_page_template', 'thunder.php'),
(357, 250, '_edit_lock', '1581745898:1'),
(358, 250, '_edit_last', '1'),
(359, 250, '_wp_page_template', 'school-dance.php'),
(360, 252, '_edit_lock', '1583411540:1'),
(361, 252, '_edit_last', '1'),
(362, 252, '_wp_page_template', 'callback.php'),
(363, 254, '_edit_lock', '1583411586:1'),
(364, 254, '_edit_last', '1'),
(365, 254, '_wp_page_template', 'dancehall.php'),
(366, 256, '_edit_lock', '1583411621:1'),
(367, 256, '_edit_last', '1'),
(368, 256, '_wp_page_template', 'femme.php'),
(369, 258, '_edit_lock', '1581746784:1'),
(370, 258, '_edit_last', '1'),
(371, 258, '_wp_page_template', 'no-holds-barre.php'),
(372, 260, '_edit_lock', '1581746977:1'),
(373, 260, '_edit_last', '1'),
(374, 260, '_wp_page_template', 'gymbox-pole-dance.php'),
(375, 262, '_edit_lock', '1581747228:1'),
(376, 262, '_edit_last', '1'),
(377, 262, '_wp_page_template', 'shway.php'),
(378, 264, '_edit_lock', '1581747461:1'),
(379, 264, '_edit_last', '1'),
(380, 264, '_wp_page_template', 'sweat-drench.php'),
(381, 266, '_edit_lock', '1583411668:1'),
(382, 266, '_edit_last', '1'),
(383, 266, '_wp_page_template', 'amrap.php'),
(384, 268, '_edit_lock', '1583411718:1'),
(385, 268, '_edit_last', '1'),
(386, 268, '_wp_page_template', 'beastmode.php'),
(387, 270, '_edit_lock', '1581748623:1'),
(388, 270, '_edit_last', '1'),
(389, 270, '_wp_page_template', 'deathrow.php'),
(390, 272, '_edit_lock', '1581748871:1'),
(391, 272, '_edit_last', '1'),
(392, 272, '_wp_page_template', 'drill-sergeant.php'),
(393, 274, '_edit_lock', '1581749053:1'),
(394, 274, '_edit_last', '1'),
(395, 274, '_wp_page_template', 'escalate.php'),
(396, 276, '_edit_lock', '1581749233:1'),
(397, 276, '_edit_last', '1'),
(398, 276, '_wp_page_template', 'flatline.php'),
(399, 278, '_edit_lock', '1581749450:1'),
(400, 278, '_edit_last', '1'),
(401, 278, '_wp_page_template', 'metcon.php'),
(402, 280, '_edit_lock', '1581749751:1'),
(403, 280, '_edit_last', '1'),
(404, 280, '_wp_page_template', 'pound.php'),
(405, 282, '_edit_lock', '1581750153:1'),
(406, 282, '_edit_last', '1'),
(407, 282, '_edit_last', '1'),
(408, 282, '_wp_page_template', 'threshold.php'),
(409, 284, '_edit_lock', '1581750352:1'),
(410, 284, '_edit_last', '1'),
(411, 284, '_wp_page_template', 'trilactic.php'),
(412, 286, '_edit_lock', '1581751098:1'),
(413, 286, '_edit_last', '1'),
(414, 286, '_wp_page_template', 'ufc-fit.php'),
(415, 288, '_edit_lock', '1581751259:1'),
(416, 288, '_edit_last', '1'),
(417, 288, '_wp_page_template', 'whiplash.php'),
(418, 290, '_edit_lock', '1581765959:1'),
(419, 292, '_edit_lock', '1581765793:1'),
(420, 290, '_edit_last', '1'),
(421, 290, '_wp_page_template', 'default'),
(422, 290, '_wp_trash_meta_status', 'publish'),
(423, 290, '_wp_trash_meta_time', '1581765994'),
(424, 290, '_wp_desired_post_slug', 'timetable-blank'),
(425, 292, '_wp_trash_meta_status', 'publish'),
(426, 292, '_wp_trash_meta_time', '1581765997'),
(427, 292, '_wp_desired_post_slug', 'timetable-blank-2'),
(428, 294, '_edit_lock', '1581766034:1'),
(429, 294, '_edit_last', '1'),
(430, 294, '_wp_page_template', 'timetable-bank.php'),
(431, 296, '_edit_lock', '1581929628:1'),
(432, 296, '_edit_last', '1'),
(433, 296, '_wp_page_template', 'gym.php'),
(434, 298, '_edit_lock', '1581997721:1'),
(435, 298, '_wp_trash_meta_status', 'publish'),
(436, 298, '_wp_trash_meta_time', '1581997902'),
(437, 298, '_wp_desired_post_slug', 'home-page'),
(438, 300, '_edit_lock', '1582005660:1'),
(439, 300, '_edit_last', '1'),
(440, 300, '_wp_page_template', 'map-box.php'),
(441, 303, '_edit_lock', '1582351353:1'),
(442, 303, '_edit_last', '1'),
(443, 303, '_wp_page_template', 'cannon-street.php'),
(444, 305, '_edit_lock', '1582351630:1'),
(445, 305, '_edit_last', '1'),
(446, 305, '_wp_page_template', 'covent-garden.php'),
(447, 307, '_edit_lock', '1582351944:1'),
(448, 307, '_edit_last', '1'),
(449, 307, '_wp_page_template', 'ealing.php'),
(450, 309, '_edit_lock', '1582352130:1'),
(451, 309, '_edit_last', '1'),
(452, 309, '_wp_page_template', 'elephant-castle.php'),
(453, 311, '_edit_lock', '1582352460:1'),
(454, 311, '_edit_last', '1'),
(455, 311, '_wp_page_template', 'farringdon.php'),
(456, 313, '_edit_lock', '1582352707:1'),
(457, 313, '_edit_last', '1'),
(458, 313, '_wp_page_template', 'finsbury-park.php'),
(459, 315, '_edit_lock', '1582352920:1'),
(460, 315, '_edit_last', '1'),
(461, 315, '_wp_page_template', 'holborn.php'),
(462, 317, '_edit_lock', '1582353084:1'),
(463, 317, '_edit_last', '1'),
(464, 317, '_wp_page_template', 'old-street.php'),
(465, 319, '_edit_lock', '1582353233:1'),
(466, 319, '_edit_last', '1'),
(467, 319, '_wp_page_template', 'victoria.php'),
(468, 321, '_edit_lock', '1582353432:1'),
(469, 321, '_edit_last', '1'),
(470, 321, '_wp_page_template', 'westfield-london.php'),
(471, 323, '_edit_lock', '1582353641:1'),
(472, 323, '_edit_last', '1'),
(473, 323, '_wp_page_template', 'westfield-stratford.php'),
(474, 325, '_edit_lock', '1582371953:1'),
(475, 325, '_edit_last', '1'),
(476, 325, '_wp_page_template', 'resources.php'),
(477, 327, '_wp_attached_file', '2020/02/Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png'),
(478, 327, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:140;s:6:\"height\";i:30;s:4:\"file\";s:64:\"2020/02/Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(479, 328, '_edit_lock', '1582535846:1'),
(480, 329, '_wp_attached_file', '2020/02/cropped-Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png'),
(481, 329, '_wp_attachment_context', 'custom-logo'),
(482, 329, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:140;s:6:\"height\";i:30;s:4:\"file\";s:72:\"2020/02/cropped-Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(483, 328, '_wp_trash_meta_status', 'publish'),
(484, 328, '_wp_trash_meta_time', '1582535859'),
(485, 330, '_edit_lock', '1582690401:1'),
(486, 330, '_edit_last', '1'),
(487, 330, '_wp_page_template', 'one-life-fitness.php'),
(488, 332, '_edit_lock', '1582690930:1'),
(489, 332, '_edit_last', '1'),
(490, 332, '_wp_page_template', 'riversedge-fitness.php'),
(492, 335, '_edit_lock', '1582715832:1'),
(493, 335, '_wp_trash_meta_status', 'publish'),
(494, 335, '_wp_trash_meta_time', '1582716014'),
(495, 335, '_wp_desired_post_slug', 'test-test'),
(501, 342, '_edit_lock', '1583224545:1'),
(502, 342, '_edit_last', '1'),
(503, 342, '_wp_page_template', 'blog.php'),
(504, 344, '_edit_lock', '1582786454:1'),
(508, 348, '_edit_lock', '1582787470:1'),
(512, 353, '_edit_lock', '1582797813:1'),
(513, 354, '_wp_attached_file', '2020/02/Andreea-Yoga-Tile.png'),
(514, 354, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:959;s:6:\"height\";i:805;s:4:\"file\";s:29:\"2020/02/Andreea-Yoga-Tile.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"Andreea-Yoga-Tile-300x252.png\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"Andreea-Yoga-Tile-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"Andreea-Yoga-Tile-768x645.png\";s:5:\"width\";i:768;s:6:\"height\";i:645;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(516, 357, '_wp_trash_meta_status', 'publish'),
(517, 357, '_wp_trash_meta_time', '1582793473'),
(519, 359, '_edit_lock', '1582805846:1'),
(521, 361, '_wp_attached_file', '2020/02/Andreea-Yoga-Tile-1.png'),
(522, 361, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:959;s:6:\"height\";i:805;s:4:\"file\";s:31:\"2020/02/Andreea-Yoga-Tile-1.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"Andreea-Yoga-Tile-1-300x252.png\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Andreea-Yoga-Tile-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"Andreea-Yoga-Tile-1-768x645.png\";s:5:\"width\";i:768;s:6:\"height\";i:645;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(524, 9, '_wp_trash_meta_status', 'publish'),
(525, 9, '_wp_trash_meta_time', '1582881474'),
(526, 9, '_wp_desired_post_slug', 'home'),
(527, 364, '_edit_lock', '1582973742:1'),
(529, 366, '_edit_lock', '1582973848:1'),
(531, 368, '_edit_lock', '1582974220:1'),
(534, 368, '_wp_old_slug', 'gymbox'),
(535, 370, '_edit_lock', '1582974391:1'),
(538, 372, '_edit_lock', '1582974730:1'),
(540, 374, '_edit_lock', '1582977531:1'),
(542, 376, '_edit_lock', '1582976721:1'),
(543, 376, '_edit_last', '1'),
(544, 376, '_wp_page_template', 'post-details.php'),
(546, 1, '_edit_lock', '1582977831:1'),
(548, 380, '_edit_lock', '1583208924:1'),
(550, 382, '_edit_lock', '1583236135:1'),
(551, 383, '_wp_attached_file', '2020/03/Vinyasa.jpg'),
(552, 383, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1800;s:6:\"height\";i:1201;s:4:\"file\";s:19:\"2020/03/Vinyasa.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"Vinyasa-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"Vinyasa-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Vinyasa-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"Vinyasa-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:21:\"Vinyasa-1536x1025.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1025;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(554, 385, '_edit_lock', '1583293343:1'),
(555, 386, '_wp_attached_file', '2020/03/One-Life-Personal-Trainer-scaled.jpeg'),
(556, 386, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1707;s:4:\"file\";s:45:\"2020/03/One-Life-Personal-Trainer-scaled.jpeg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"One-Life-Personal-Trainer-300x200.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:39:\"One-Life-Personal-Trainer-1024x683.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"One-Life-Personal-Trainer-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"One-Life-Personal-Trainer-768x512.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:40:\"One-Life-Personal-Trainer-1536x1024.jpeg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:40:\"One-Life-Personal-Trainer-2048x1365.jpeg\";s:5:\"width\";i:2048;s:6:\"height\";i:1365;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"2.8\";s:6:\"credit\";s:29:\"Jacob Lund  - stock.adobe.com\";s:6:\"camera\";s:15:\"Canon EOS 5DS R\";s:7:\"caption\";s:132:\"Fitness woman doing sit-ups exercises with male instructor in gym. Female doing abs workout with  personal trainer holding her legs.\";s:17:\"created_timestamp\";s:10:\"1511081456\";s:9:\"copyright\";s:31:\"©Jacob Lund  - stock.adobe.com\";s:12:\"focal_length\";s:2:\"35\";s:3:\"iso\";s:3:\"200\";s:13:\"shutter_speed\";s:5:\"0.002\";s:5:\"title\";s:50:\"Woman doing sit-ups exercises with male instructor\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:78:{i:0;s:7:\"abdomen\";i:1;s:3:\"abs\";i:2;s:7:\"agility\";i:3;s:10:\"assistance\";i:4;s:4:\"body\";i:5;s:5:\"class\";i:6;s:5:\"coach\";i:7;s:6:\"effort\";i:8;s:14:\"exercise class\";i:9;s:10:\"exercising\";i:10;s:6:\"female\";i:11;s:3:\"fit\";i:12;s:7:\"fitness\";i:13;s:3:\"gym\";i:14;s:7:\"healthy\";i:15;s:7:\"helping\";i:16;s:7:\"holding\";i:17;s:10:\"instructor\";i:18;s:3:\"leg\";i:19;s:9:\"lifestyle\";i:20;s:4:\"male\";i:21;s:3:\"man\";i:22;s:8:\"muscular\";i:23;s:6:\"people\";i:24;s:8:\"personal\";i:25;s:10:\"practicing\";i:26;s:4:\"side\";i:27;s:7:\"sit-ups\";i:28;s:15:\"sports training\";i:29;s:8:\"strength\";i:30;s:10:\"stretching\";i:31;s:7:\"support\";i:32;s:7:\"trainer\";i:33;s:8:\"training\";i:34;s:3:\"two\";i:35;s:4:\"view\";i:36;s:5:\"woman\";i:37;s:7:\"workout\";i:38;s:5:\"young\";i:39;s:15:\"sports training\";i:40;s:7:\"sit-ups\";i:41;s:7:\"abdomen\";i:42;s:3:\"abs\";i:43;s:7:\"agility\";i:44;s:10:\"assistance\";i:45;s:4:\"body\";i:46;s:5:\"class\";i:47;s:5:\"coach\";i:48;s:6:\"effort\";i:49;s:14:\"exercise class\";i:50;s:10:\"exercising\";i:51;s:6:\"female\";i:52;s:3:\"fit\";i:53;s:7:\"fitness\";i:54;s:3:\"gym\";i:55;s:7:\"healthy\";i:56;s:7:\"helping\";i:57;s:7:\"holding\";i:58;s:10:\"instructor\";i:59;s:3:\"leg\";i:60;s:9:\"lifestyle\";i:61;s:4:\"male\";i:62;s:3:\"man\";i:63;s:8:\"muscular\";i:64;s:6:\"people\";i:65;s:8:\"personal\";i:66;s:10:\"practicing\";i:67;s:4:\"side\";i:68;s:8:\"strength\";i:69;s:10:\"stretching\";i:70;s:7:\"support\";i:71;s:7:\"trainer\";i:72;s:8:\"training\";i:73;s:3:\"two\";i:74;s:4:\"view\";i:75;s:5:\"woman\";i:76;s:7:\"workout\";i:77;s:5:\"young\";}}s:14:\"original_image\";s:30:\"One-Life-Personal-Trainer.jpeg\";}'),
(560, 414, '_edit_lock', '1583491504:1'),
(561, 414, '_edit_last', '1'),
(562, 414, '_wp_page_template', 'contact-us.php');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-02-04 04:12:57', '2020-02-04 04:12:57', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2020-02-29 12:06:12', '2020-02-29 12:06:12', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=1', 0, 'post', '', 1),
(9, 1, '2020-02-04 04:57:58', '2020-02-04 04:57:58', '', 'Home', '', 'trash', 'closed', 'closed', '', 'home__trashed', '', '', '2020-02-28 09:17:54', '2020-02-28 09:17:54', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=9', 0, 'page', '', 0),
(10, 1, '2020-02-04 04:57:58', '2020-02-04 04:57:58', '', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2020-02-04 04:57:58', '2020-02-04 04:57:58', '', 9, 'https://test.hachiweb.com/onelifefitness/2020/02/04/9-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2020-02-05 04:44:19', '2020-02-05 04:44:19', '', 'laughing-therapy', '', 'publish', 'closed', 'closed', '', 'laughing-therapy', '', '', '2020-02-13 06:19:16', '2020-02-13 06:19:16', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=20', 0, 'page', '', 0),
(21, 1, '2020-02-05 04:44:19', '2020-02-05 04:44:19', '', 'laughing-therapy', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2020-02-05 04:44:19', '2020-02-05 04:44:19', '', 20, 'https://test.hachiweb.com/onelifefitness/2020/02/05/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2020-02-05 04:51:52', '2020-02-05 04:51:52', '', 'Home', '', 'inherit', 'closed', 'closed', '', '9-autosave-v1', '', '', '2020-02-05 04:51:52', '2020-02-05 04:51:52', '', 9, 'https://test.hachiweb.com/onelifefitness/2020/02/05/9-autosave-v1/', 0, 'revision', '', 0),
(25, 1, '2020-02-05 05:23:24', '2020-02-05 05:23:24', '', 'regeneration-z', '', 'publish', 'closed', 'closed', '', 'regeneration-z', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=25', 0, 'page', '', 0),
(26, 1, '2020-02-05 05:23:24', '2020-02-05 05:23:24', '', 'regeneration-z', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2020-02-05 05:23:24', '2020-02-05 05:23:24', '', 25, 'https://test.hachiweb.com/onelifefitness/2020/02/05/25-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2020-02-05 05:39:47', '2020-02-05 05:39:47', '', 'rampant-rehab', '', 'publish', 'closed', 'closed', '', 'rampant-rehab', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=27', 0, 'page', '', 0),
(28, 1, '2020-02-05 05:39:47', '2020-02-05 05:39:47', '', 'rampant-rehab', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2020-02-05 05:39:47', '2020-02-05 05:39:47', '', 27, 'https://test.hachiweb.com/onelifefitness/2020/02/05/27-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2020-02-05 05:51:47', '2020-02-05 05:51:47', '', 'sword-play', '', 'publish', 'closed', 'closed', '', 'sword-play', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=29', 0, 'page', '', 0),
(30, 1, '2020-02-05 05:51:47', '2020-02-05 05:51:47', '', 'sword-play', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-02-05 05:51:47', '2020-02-05 05:51:47', '', 29, 'https://test.hachiweb.com/onelifefitness/2020/02/05/29-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2020-02-05 05:59:11', '2020-02-05 05:59:11', '', 'rolling-with-my-yogis', '', 'publish', 'closed', 'closed', '', 'rolling-with-my-yogis', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=32', 0, 'page', '', 0),
(33, 1, '2020-02-05 05:59:11', '2020-02-05 05:59:11', '', 'rolling-with-my-yogis', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2020-02-05 05:59:11', '2020-02-05 05:59:11', '', 32, 'https://test.hachiweb.com/onelifefitness/2020/02/05/32-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2020-02-05 06:32:33', '2020-02-05 06:32:33', '', 'career', '', 'publish', 'closed', 'closed', '', 'career', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=34', 0, 'page', '', 0),
(35, 1, '2020-02-05 06:32:33', '2020-02-05 06:32:33', '', 'career', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2020-02-05 06:32:33', '2020-02-05 06:32:33', '', 34, 'https://test.hachiweb.com/onelifefitness/2020/02/05/34-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2020-02-05 07:34:46', '2020-02-05 07:34:46', '', 'services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=36', 0, 'page', '', 0),
(37, 1, '2020-02-05 07:34:46', '2020-02-05 07:34:46', '', 'services', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2020-02-05 07:34:46', '2020-02-05 07:34:46', '', 36, 'https://test.hachiweb.com/onelifefitness/2020/02/05/36-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2020-02-05 10:12:22', '2020-02-05 10:12:22', '', 'faqs', '', 'publish', 'closed', 'closed', '', 'faqs', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=39', 0, 'page', '', 0),
(40, 1, '2020-02-05 10:12:22', '2020-02-05 10:12:22', '', 'faqs', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2020-02-05 10:12:22', '2020-02-05 10:12:22', '', 39, 'https://test.hachiweb.com/onelifefitness/2020/02/05/39-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2020-02-05 11:16:00', '2020-02-05 11:16:00', '', 'refer', '', 'publish', 'closed', 'closed', '', 'refer', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=43', 0, 'page', '', 0),
(44, 1, '2020-02-05 11:16:00', '2020-02-05 11:16:00', '', 'refer', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2020-02-05 11:16:00', '2020-02-05 11:16:00', '', 43, 'https://test.hachiweb.com/onelifefitness/2020/02/05/43-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-02-05 11:40:11', '2020-02-05 11:40:11', '', 'club-rules', '', 'publish', 'closed', 'closed', '', 'club-rules', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=45', 0, 'page', '', 0),
(46, 1, '2020-02-05 11:40:11', '2020-02-05 11:40:11', '', 'club-rules', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2020-02-05 11:40:11', '2020-02-05 11:40:11', '', 45, 'https://test.hachiweb.com/onelifefitness/2020/02/05/45-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2020-02-05 11:53:38', '2020-02-05 11:53:38', '', 'terms-and-conditions', '', 'publish', 'closed', 'closed', '', 'terms-and-conditions', '', '', '2020-02-05 12:18:56', '2020-02-05 12:18:56', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=47', 0, 'page', '', 0),
(48, 1, '2020-02-05 11:53:38', '2020-02-05 11:53:38', '', 'terms-and-conditions', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2020-02-05 11:53:38', '2020-02-05 11:53:38', '', 47, 'https://test.hachiweb.com/onelifefitness/2020/02/05/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2020-02-05 12:02:40', '2020-02-05 12:02:40', '', 'privacy-policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=49', 0, 'page', '', 0),
(50, 1, '2020-02-05 12:02:40', '2020-02-05 12:02:40', '', 'privacy-policy', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2020-02-05 12:02:40', '2020-02-05 12:02:40', '', 49, 'https://test.hachiweb.com/onelifefitness/2020/02/05/49-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2020-02-05 12:26:24', '2020-02-05 12:26:24', '', 'bank', '', 'publish', 'closed', 'closed', '', 'bank', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=52', 0, 'page', '', 0),
(53, 1, '2020-02-05 12:26:24', '2020-02-05 12:26:24', '', 'bank', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2020-02-05 12:26:24', '2020-02-05 12:26:24', '', 52, 'https://test.hachiweb.com/onelifefitness/2020/02/05/52-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2020-02-06 04:49:40', '2020-02-06 04:49:40', '', 'login', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=54', 0, 'page', '', 0),
(55, 1, '2020-02-06 04:49:40', '2020-02-06 04:49:40', '', 'login', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2020-02-06 04:49:40', '2020-02-06 04:49:40', '', 54, 'https://test.hachiweb.com/onelifefitness/2020/02/06/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2020-02-06 08:59:07', '2020-02-06 08:59:07', '', 'title', '', 'publish', 'closed', 'closed', '', 'title', '', '', '2020-02-06 08:59:07', '2020-02-06 08:59:07', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=56', 0, 'page', '', 0),
(57, 1, '2020-02-06 08:59:07', '2020-02-06 08:59:07', '', 'title', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2020-02-06 08:59:07', '2020-02-06 08:59:07', '', 56, 'https://test.hachiweb.com/onelifefitness/2020/02/06/56-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2020-02-06 11:25:09', '2020-02-06 11:25:09', '', 'timetable', '', 'publish', 'closed', 'closed', '', 'timetable', '', '', '2020-02-06 11:35:18', '2020-02-06 11:35:18', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=59', 0, 'page', '', 0),
(60, 1, '2020-02-06 11:25:09', '2020-02-06 11:25:09', '', 'categories', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-02-06 11:25:09', '2020-02-06 11:25:09', '', 59, 'https://test.hachiweb.com/onelifefitness/2020/02/06/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2020-02-06 11:35:18', '2020-02-06 11:35:18', '', 'timetable', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-02-06 11:35:18', '2020-02-06 11:35:18', '', 59, 'https://test.hachiweb.com/onelifefitness/2020/02/06/59-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2020-02-06 11:38:22', '2020-02-06 11:38:22', '', 'join-us', '', 'publish', 'closed', 'closed', '', 'join-us', '', '', '2020-02-13 06:19:16', '2020-02-13 06:19:16', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=62', 0, 'page', '', 0),
(63, 1, '2020-02-06 11:38:22', '2020-02-06 11:38:22', '', 'join-us', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2020-02-06 11:38:22', '2020-02-06 11:38:22', '', 62, 'https://test.hachiweb.com/onelifefitness/2020/02/06/62-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2020-02-06 11:56:02', '2020-02-06 11:56:02', '', 'personal-training', '', 'publish', 'closed', 'closed', '', 'personal-training', '', '', '2020-02-13 06:19:17', '2020-02-13 06:19:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=64', 0, 'page', '', 0),
(65, 1, '2020-02-06 11:56:02', '2020-02-06 11:56:02', '', 'personal-training', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2020-02-06 11:56:02', '2020-02-06 11:56:02', '', 64, 'https://test.hachiweb.com/onelifefitness/2020/02/06/64-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2020-02-07 03:57:53', '2020-02-07 03:57:53', '', 'about', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=66', 0, 'page', '', 0),
(67, 1, '2020-02-07 03:57:53', '2020-02-07 03:57:53', '', 'about', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2020-02-07 03:57:53', '2020-02-07 03:57:53', '', 66, 'https://test.hachiweb.com/onelifefitness/2020/02/07/66-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2020-02-07 05:07:32', '2020-02-07 05:07:32', '', 'corporate', '', 'publish', 'closed', 'closed', '', 'corporate', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=68', 0, 'page', '', 0),
(69, 1, '2020-02-07 05:07:32', '2020-02-07 05:07:32', '', 'corporate', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2020-02-07 05:07:32', '2020-02-07 05:07:32', '', 68, 'https://test.hachiweb.com/onelifefitness/2020/02/07/68-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-02-07 06:33:52', '2020-02-07 06:33:52', '', 'blank', '', 'publish', 'closed', 'closed', '', 'blank', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=71', 0, 'page', '', 0),
(72, 1, '2020-02-07 06:33:52', '2020-02-07 06:33:52', '', 'blank', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2020-02-07 06:33:52', '2020-02-07 06:33:52', '', 71, 'https://test.hachiweb.com/onelifefitness/2020/02/07/71-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2020-02-07 07:36:13', '2020-02-07 07:36:13', '', 'blank', '', 'inherit', 'closed', 'closed', '', '71-autosave-v1', '', '', '2020-02-07 07:36:13', '2020-02-07 07:36:13', '', 71, 'https://test.hachiweb.com/onelifefitness/2020/02/07/71-autosave-v1/', 0, 'revision', '', 0),
(77, 1, '2020-02-12 12:17:28', '2020-02-12 12:17:28', '', 'Example', '', 'publish', 'open', 'open', '', 'example', '', '', '2020-02-12 12:17:28', '2020-02-12 12:17:28', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=77', 0, 'post', '', 0),
(78, 1, '2020-02-12 12:17:28', '2020-02-12 12:17:28', '', 'Example', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2020-02-12 12:17:28', '2020-02-12 12:17:28', '', 77, 'https://test.hachiweb.com/onelifefitness/2020/02/12/77-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2020-02-12 12:25:45', '2020-02-12 12:25:45', '', 'blank2', '', 'publish', 'closed', 'closed', '', 'blank2', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=80', 0, 'page', '', 0),
(81, 1, '2020-02-12 12:25:45', '2020-02-12 12:25:45', '', 'blank2', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2020-02-12 12:25:45', '2020-02-12 12:25:45', '', 80, 'https://test.hachiweb.com/onelifefitness/2020/02/12/80-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2020-02-12 12:26:09', '2020-02-12 12:26:09', '', 'blank2', '', 'inherit', 'closed', 'closed', '', '80-autosave-v1', '', '', '2020-02-12 12:26:09', '2020-02-12 12:26:09', '', 80, 'https://test.hachiweb.com/onelifefitness/2020/02/12/80-autosave-v1/', 0, 'revision', '', 0),
(83, 1, '2020-02-12 12:27:42', '2020-02-12 12:27:42', '{\n    \"blogname\": {\n        \"value\": \"GYM BOX\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-12 12:27:42\"\n    },\n    \"nav_menu[-6188613310814620000]\": {\n        \"value\": {\n            \"name\": \"home\",\n            \"description\": \"\",\n            \"parent\": 0,\n            \"auto_add\": false\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-12 12:27:42\"\n    },\n    \"nav_menu_item[-4453985421865453600]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"custom\",\n            \"title\": \"Home\",\n            \"url\": \"https://test.hachiweb.com/onelifefitness\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Home\",\n            \"nav_menu_term_id\": -6188613310814620000,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-12 12:27:42\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'ef51232c-d3d7-4801-8178-8880b84625d6', '', '', '2020-02-12 12:27:42', '2020-02-12 12:27:42', '', 0, 'https://test.hachiweb.com/onelifefitness/2020/02/12/ef51232c-d3d7-4801-8178-8880b84625d6/', 0, 'customize_changeset', '', 0),
(85, 1, '2020-02-12 12:28:37', '2020-02-12 12:28:37', '{\n    \"nav_menu_item[19]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-12 12:28:37\"\n    },\n    \"nav_menu_item[84]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-12 12:28:37\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '166ef863-5210-410d-85af-119be4840c76', '', '', '2020-02-12 12:28:37', '2020-02-12 12:28:37', '', 0, 'https://test.hachiweb.com/onelifefitness/2020/02/12/166ef863-5210-410d-85af-119be4840c76/', 0, 'customize_changeset', '', 0),
(86, 1, '2020-02-13 06:46:40', '2020-02-13 06:46:40', '', 'Blank2', '', 'publish', 'closed', 'closed', '', 'blank2-2', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=86', 0, 'page', '', 0),
(87, 1, '2020-02-13 06:46:40', '2020-02-13 06:46:40', '', 'Blank2', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-02-13 06:46:40', '2020-02-13 06:46:40', '', 86, 'https://test.hachiweb.com/onelifefitness/2020/02/13/86-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2020-02-13 06:55:52', '2020-02-13 06:55:52', '', 'categories', '', 'publish', 'closed', 'closed', '', 'categories', '', '', '2020-02-14 05:11:00', '2020-02-14 05:11:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=88', 0, 'page', '', 0),
(89, 1, '2020-02-13 06:55:52', '2020-02-13 06:55:52', '', 'categories', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2020-02-13 06:55:52', '2020-02-13 06:55:52', '', 88, 'https://test.hachiweb.com/onelifefitness/2020/02/13/88-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2020-02-13 08:07:18', '2020-02-13 08:07:18', '', 'classes', '', 'publish', 'closed', 'closed', '', 'classes', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=90', 0, 'page', '', 0),
(91, 1, '2020-02-13 08:07:18', '2020-02-13 08:07:18', '', 'classes', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2020-02-13 08:07:18', '2020-02-13 08:07:18', '', 90, 'https://test.hachiweb.com/onelifefitness/2020/02/13/90-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2020-02-13 09:54:37', '2020-02-13 09:54:37', '', 'lift', '', 'publish', 'closed', 'closed', '', 'lift', '', '', '2020-03-05 12:18:34', '2020-03-05 12:18:34', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=92', 0, 'page', '', 0),
(93, 1, '2020-02-13 09:54:37', '2020-02-13 09:54:37', '', 'aerial-hoop', '', 'inherit', 'closed', 'closed', '', '92-revision-v1', '', '', '2020-02-13 09:54:37', '2020-02-13 09:54:37', '', 92, 'https://test.hachiweb.com/onelifefitness/2020/02/13/92-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2020-02-13 11:00:51', '2020-02-13 11:00:51', '', 'box', '', 'publish', 'closed', 'closed', '', 'box', '', '', '2020-03-05 12:16:44', '2020-03-05 12:16:44', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=94', 0, 'page', '', 0),
(95, 1, '2020-02-13 11:00:51', '2020-02-13 11:00:51', '', 'aerial-pilates', '', 'inherit', 'closed', 'closed', '', '94-revision-v1', '', '', '2020-02-13 11:00:51', '2020-02-13 11:00:51', '', 94, 'https://test.hachiweb.com/onelifefitness/2020/02/13/94-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2020-02-13 11:10:01', '2020-02-13 11:10:01', '', 'ride', '', 'publish', 'closed', 'closed', '', 'ride', '', '', '2020-03-05 12:19:16', '2020-03-05 12:19:16', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=96', 0, 'page', '', 0),
(97, 1, '2020-02-13 11:10:01', '2020-02-13 11:10:01', '', 'aerial-yoga', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2020-02-13 11:10:01', '2020-02-13 11:10:01', '', 96, 'https://test.hachiweb.com/onelifefitness/2020/02/13/96-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2020-02-13 11:53:58', '2020-02-13 11:53:58', '', 'cirqueit', '', 'publish', 'closed', 'closed', '', 'cirqueit', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=98', 0, 'page', '', 0),
(99, 1, '2020-02-13 11:53:58', '2020-02-13 11:53:58', '', 'cirqueit', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2020-02-13 11:53:58', '2020-02-13 11:53:58', '', 98, 'https://test.hachiweb.com/onelifefitness/2020/02/13/98-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2020-02-13 12:03:26', '2020-02-13 12:03:26', '', 'cocoon', '', 'publish', 'closed', 'closed', '', 'cocoon', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=100', 0, 'page', '', 0),
(101, 1, '2020-02-13 12:03:26', '2020-02-13 12:03:26', '', 'cocoon', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2020-02-13 12:03:26', '2020-02-13 12:03:26', '', 100, 'https://test.hachiweb.com/onelifefitness/2020/02/13/100-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2020-02-13 12:13:57', '2020-02-13 12:13:57', '', 'false-grip', '', 'publish', 'closed', 'closed', '', 'false-grip', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=102', 0, 'page', '', 0),
(103, 1, '2020-02-13 12:13:57', '2020-02-13 12:13:57', '', 'false-grip', '', 'inherit', 'closed', 'closed', '', '102-revision-v1', '', '', '2020-02-13 12:13:57', '2020-02-13 12:13:57', '', 102, 'https://test.hachiweb.com/onelifefitness/2020/02/13/102-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2020-02-14 04:02:16', '2020-02-14 04:02:16', '', 'invert-yourself', '', 'publish', 'closed', 'closed', '', 'invert-yourself', '', '', '2020-02-14 04:02:57', '2020-02-14 04:02:57', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=104', 0, 'page', '', 0),
(105, 1, '2020-02-14 04:02:16', '2020-02-14 04:02:16', '', 'invert-yourself', '', 'inherit', 'closed', 'closed', '', '104-revision-v1', '', '', '2020-02-14 04:02:16', '2020-02-14 04:02:16', '', 104, 'https://test.hachiweb.com/onelifefitness/2020/02/14/104-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2020-02-14 04:09:26', '2020-02-14 04:09:26', '', 'superfly', '', 'publish', 'closed', 'closed', '', 'superfly', '', '', '2020-02-14 04:09:48', '2020-02-14 04:09:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=106', 0, 'page', '', 0),
(107, 1, '2020-02-14 04:09:26', '2020-02-14 04:09:26', '', 'superfly', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2020-02-14 04:09:26', '2020-02-14 04:09:26', '', 106, 'https://test.hachiweb.com/onelifefitness/2020/02/14/106-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2020-02-14 04:27:30', '2020-02-14 04:27:30', '', 'trapeze', '', 'publish', 'closed', 'closed', '', 'trapeze', '', '', '2020-02-14 04:27:52', '2020-02-14 04:27:52', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=108', 0, 'page', '', 0),
(109, 1, '2020-02-14 04:27:30', '2020-02-14 04:27:30', '', 'trapeze', '', 'inherit', 'closed', 'closed', '', '108-revision-v1', '', '', '2020-02-14 04:27:30', '2020-02-14 04:27:30', '', 108, 'https://test.hachiweb.com/onelifefitness/2020/02/14/108-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2020-02-14 04:46:10', '2020-02-14 04:46:10', '', 'combat-sports', '', 'publish', 'closed', 'closed', '', 'combat-sports', '', '', '2020-02-14 05:11:01', '2020-02-14 05:11:01', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=110', 0, 'page', '', 0),
(111, 1, '2020-02-14 04:46:10', '2020-02-14 04:46:10', '', 'combat-sports', '', 'inherit', 'closed', 'closed', '', '110-revision-v1', '', '', '2020-02-14 04:46:10', '2020-02-14 04:46:10', '', 110, 'https://test.hachiweb.com/onelifefitness/2020/02/14/110-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2020-02-14 04:58:39', '2020-02-14 04:58:39', '', 'olf45', '', 'publish', 'closed', 'closed', '', 'olf45', '', '', '2020-03-05 12:19:53', '2020-03-05 12:19:53', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=112', 0, 'page', '', 0),
(113, 1, '2020-02-14 04:58:39', '2020-02-14 04:58:39', '', 'brazilian-jui-jitsu', '', 'inherit', 'closed', 'closed', '', '112-revision-v1', '', '', '2020-02-14 04:58:39', '2020-02-14 04:58:39', '', 112, 'https://test.hachiweb.com/onelifefitness/2020/02/14/112-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2020-02-14 05:05:12', '2020-02-14 05:05:12', '', 'strong', '', 'publish', 'closed', 'closed', '', 'strong', '', '', '2020-03-05 12:20:20', '2020-03-05 12:20:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=114', 0, 'page', '', 0),
(115, 1, '2020-02-14 05:05:12', '2020-02-14 05:05:12', '', 'counterkick', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2020-02-14 05:05:12', '2020-02-14 05:05:12', '', 114, 'https://test.hachiweb.com/onelifefitness/2020/02/14/114-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2020-02-14 05:21:25', '2020-02-14 05:21:25', '', 'boxing-circuit', '', 'publish', 'closed', 'closed', '', 'boxing-circuit', '', '', '2020-03-05 12:21:17', '2020-03-05 12:21:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=117', 0, 'page', '', 0),
(118, 1, '2020-02-14 05:21:25', '2020-02-14 05:21:25', '', 'counterpunch', '', 'inherit', 'closed', 'closed', '', '117-revision-v1', '', '', '2020-02-14 05:21:25', '2020-02-14 05:21:25', '', 117, 'https://test.hachiweb.com/onelifefitness/2020/02/14/117-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2020-02-14 05:27:30', '2020-02-14 05:27:30', '', 'gymboxing', '', 'publish', 'closed', 'closed', '', 'gymboxing', '', '', '2020-02-14 05:27:48', '2020-02-14 05:27:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=121', 0, 'page', '', 0),
(122, 1, '2020-02-14 05:27:30', '2020-02-14 05:27:30', '', 'gymboxing', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2020-02-14 05:27:30', '2020-02-14 05:27:30', '', 121, 'https://test.hachiweb.com/onelifefitness/2020/02/14/121-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2020-02-14 05:35:48', '2020-02-14 05:35:48', '', 'gymboxing-sparring', '', 'publish', 'closed', 'closed', '', 'gymboxing-sparring', '', '', '2020-02-14 05:36:19', '2020-02-14 05:36:19', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=123', 0, 'page', '', 0),
(124, 1, '2020-02-14 05:35:48', '2020-02-14 05:35:48', '', 'gymboxing-sparring', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2020-02-14 05:35:48', '2020-02-14 05:35:48', '', 123, 'https://test.hachiweb.com/onelifefitness/2020/02/14/123-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2020-02-14 05:43:16', '2020-02-14 05:43:16', '', 'kickboxing', '', 'publish', 'closed', 'closed', '', 'kickboxing', '', '', '2020-02-14 05:43:58', '2020-02-14 05:43:58', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=125', 0, 'page', '', 0),
(126, 1, '2020-02-14 05:43:16', '2020-02-14 05:43:16', '', 'kickboxing', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-02-14 05:43:16', '2020-02-14 05:43:16', '', 125, 'https://test.hachiweb.com/onelifefitness/2020/02/14/125-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2020-02-14 05:50:13', '2020-02-14 05:50:13', '', 'kickboxing-fighters-club', '', 'publish', 'closed', 'closed', '', 'kickboxing-fighters-club', '', '', '2020-02-14 05:50:41', '2020-02-14 05:50:41', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=127', 0, 'page', '', 0),
(128, 1, '2020-02-14 05:50:13', '2020-02-14 05:50:13', '', 'kickboxing-fighters-club', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2020-02-14 05:50:13', '2020-02-14 05:50:13', '', 127, 'https://test.hachiweb.com/onelifefitness/2020/02/14/127-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2020-02-14 05:58:43', '2020-02-14 05:58:43', '', 'killer-combat', '', 'publish', 'closed', 'closed', '', 'killer-combat', '', '', '2020-02-14 05:59:03', '2020-02-14 05:59:03', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=129', 0, 'page', '', 0),
(130, 1, '2020-02-14 05:58:43', '2020-02-14 05:58:43', '', 'killer-combat', '', 'inherit', 'closed', 'closed', '', '129-revision-v1', '', '', '2020-02-14 05:58:43', '2020-02-14 05:58:43', '', 129, 'https://test.hachiweb.com/onelifefitness/2020/02/14/129-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2020-02-14 06:01:46', '2020-02-14 06:01:46', '', 'krav-maga', '', 'publish', 'closed', 'closed', '', 'krav-maga', '', '', '2020-02-14 06:02:08', '2020-02-14 06:02:08', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=131', 0, 'page', '', 0),
(132, 1, '2020-02-14 06:01:46', '2020-02-14 06:01:46', '', 'krav-maga', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2020-02-14 06:01:46', '2020-02-14 06:01:46', '', 131, 'https://test.hachiweb.com/onelifefitness/2020/02/14/131-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2020-02-14 06:05:04', '2020-02-14 06:05:04', '', 'l1-white-collar-fight-club', '', 'publish', 'closed', 'closed', '', 'l1-white-collar-fight-club', '', '', '2020-02-14 06:05:20', '2020-02-14 06:05:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=134', 0, 'page', '', 0),
(135, 1, '2020-02-14 06:05:04', '2020-02-14 06:05:04', '', 'l1-white-collar-fight-club', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2020-02-14 06:05:04', '2020-02-14 06:05:04', '', 134, 'https://test.hachiweb.com/onelifefitness/2020/02/14/134-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2020-02-14 06:07:29', '2020-02-14 06:07:29', '', 'l2-white-collar-fight-club', '', 'publish', 'closed', 'closed', '', 'l2-white-collar-fight-club', '', '', '2020-02-14 06:07:43', '2020-02-14 06:07:43', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=136', 0, 'page', '', 0),
(137, 1, '2020-02-14 06:07:29', '2020-02-14 06:07:29', '', 'l2-white-collar-fight-club', '', 'inherit', 'closed', 'closed', '', '136-revision-v1', '', '', '2020-02-14 06:07:29', '2020-02-14 06:07:29', '', 136, 'https://test.hachiweb.com/onelifefitness/2020/02/14/136-revision-v1/', 0, 'revision', '', 0),
(138, 1, '2020-02-14 06:10:00', '2020-02-14 06:10:00', '', 'ladies-only-bjj', '', 'publish', 'closed', 'closed', '', 'ladies-only-bjj', '', '', '2020-02-14 06:10:19', '2020-02-14 06:10:19', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=138', 0, 'page', '', 0),
(139, 1, '2020-02-14 06:10:00', '2020-02-14 06:10:00', '', 'ladies-only-bjj', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2020-02-14 06:10:00', '2020-02-14 06:10:00', '', 138, 'https://test.hachiweb.com/onelifefitness/2020/02/14/138-revision-v1/', 0, 'revision', '', 0),
(140, 1, '2020-02-14 06:35:46', '2020-02-14 06:35:46', '', 'mma', '', 'publish', 'closed', 'closed', '', 'mma', '', '', '2020-02-14 06:36:22', '2020-02-14 06:36:22', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=140', 0, 'page', '', 0),
(141, 1, '2020-02-14 06:35:46', '2020-02-14 06:35:46', '', 'mma', '', 'inherit', 'closed', 'closed', '', '140-revision-v1', '', '', '2020-02-14 06:35:46', '2020-02-14 06:35:46', '', 140, 'https://test.hachiweb.com/onelifefitness/2020/02/14/140-revision-v1/', 0, 'revision', '', 0),
(142, 1, '2020-02-14 06:40:04', '2020-02-14 06:40:04', '', 'muay-thai', '', 'publish', 'closed', 'closed', '', 'muay-thai', '', '', '2020-02-14 06:41:22', '2020-02-14 06:41:22', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=142', 0, 'page', '', 0),
(143, 1, '2020-02-14 06:40:04', '2020-02-14 06:40:04', '', 'muay-thai', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2020-02-14 06:40:04', '2020-02-14 06:40:04', '', 142, 'https://test.hachiweb.com/onelifefitness/2020/02/14/142-revision-v1/', 0, 'revision', '', 0),
(145, 1, '2020-02-14 06:51:11', '2020-02-14 06:51:11', '', 'muay-thai-sparring-2', '', 'publish', 'closed', 'closed', '', 'muay-thai-sparring-2', '', '', '2020-02-14 06:51:32', '2020-02-14 06:51:32', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=145', 0, 'page', '', 0),
(146, 1, '2020-02-14 06:51:11', '2020-02-14 06:51:11', '', 'muay-thai-sparring-2', '', 'inherit', 'closed', 'closed', '', '145-revision-v1', '', '', '2020-02-14 06:51:11', '2020-02-14 06:51:11', '', 145, 'https://test.hachiweb.com/onelifefitness/2020/02/14/145-revision-v1/', 0, 'revision', '', 0),
(147, 1, '2020-02-14 06:55:08', '2020-02-14 06:55:08', '', 'muay-thai-beginners', '', 'publish', 'closed', 'closed', '', 'muay-thai-beginners', '', '', '2020-02-14 06:55:34', '2020-02-14 06:55:34', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=147', 0, 'page', '', 0),
(148, 1, '2020-02-14 06:55:08', '2020-02-14 06:55:08', '', 'muay-thai-beginners', '', 'inherit', 'closed', 'closed', '', '147-revision-v1', '', '', '2020-02-14 06:55:08', '2020-02-14 06:55:08', '', 147, 'https://test.hachiweb.com/onelifefitness/2020/02/14/147-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2020-02-14 06:58:13', '2020-02-14 06:58:13', '', 'muay-thai-fighters-club', '', 'publish', 'closed', 'closed', '', 'muay-thai-fighters-club', '', '', '2020-02-14 06:58:36', '2020-02-14 06:58:36', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=149', 0, 'page', '', 0),
(150, 1, '2020-02-14 06:58:13', '2020-02-14 06:58:13', '', 'muay-thai-fighters-club', '', 'inherit', 'closed', 'closed', '', '149-revision-v1', '', '', '2020-02-14 06:58:13', '2020-02-14 06:58:13', '', 149, 'https://test.hachiweb.com/onelifefitness/2020/02/14/149-revision-v1/', 0, 'revision', '', 0),
(151, 1, '2020-02-14 07:01:16', '2020-02-14 07:01:16', '', 'muay-thai-int-adv', '', 'publish', 'closed', 'closed', '', 'muay-thai-int-adv', '', '', '2020-02-14 07:01:39', '2020-02-14 07:01:39', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=151', 0, 'page', '', 0),
(152, 1, '2020-02-14 07:01:16', '2020-02-14 07:01:16', '', 'muay-thai-int-adv', '', 'inherit', 'closed', 'closed', '', '151-revision-v1', '', '', '2020-02-14 07:01:16', '2020-02-14 07:01:16', '', 151, 'https://test.hachiweb.com/onelifefitness/2020/02/14/151-revision-v1/', 0, 'revision', '', 0),
(153, 1, '2020-02-14 07:05:43', '2020-02-14 07:05:43', '', 'muay-thai-sparring', '', 'publish', 'closed', 'closed', '', 'muay-thai-sparring', '', '', '2020-02-14 07:06:13', '2020-02-14 07:06:13', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=153', 0, 'page', '', 0),
(154, 1, '2020-02-14 07:05:43', '2020-02-14 07:05:43', '', 'muay-thai-sparring', '', 'inherit', 'closed', 'closed', '', '153-revision-v1', '', '', '2020-02-14 07:05:43', '2020-02-14 07:05:43', '', 153, 'https://test.hachiweb.com/onelifefitness/2020/02/14/153-revision-v1/', 0, 'revision', '', 0),
(155, 1, '2020-02-14 07:13:08', '2020-02-14 07:13:08', '', 'no-gi', '', 'publish', 'closed', 'closed', '', 'no-gi', '', '', '2020-02-14 07:13:41', '2020-02-14 07:13:41', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=155', 0, 'page', '', 0),
(156, 1, '2020-02-14 07:13:08', '2020-02-14 07:13:08', '', 'no-gi', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2020-02-14 07:13:08', '2020-02-14 07:13:08', '', 155, 'https://test.hachiweb.com/onelifefitness/2020/02/14/155-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2020-02-14 07:15:54', '2020-02-14 07:15:54', '', 'sluggers-club', '', 'publish', 'closed', 'closed', '', 'sluggers-club', '', '', '2020-02-14 10:45:36', '2020-02-14 10:45:36', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=157', 0, 'page', '', 0),
(158, 1, '2020-02-14 07:15:54', '2020-02-14 07:15:54', '', 'sluggers-club', '', 'inherit', 'closed', 'closed', '', '157-revision-v1', '', '', '2020-02-14 07:15:54', '2020-02-14 07:15:54', '', 157, 'https://test.hachiweb.com/onelifefitness/2020/02/14/157-revision-v1/', 0, 'revision', '', 0),
(159, 1, '2020-02-14 07:26:45', '2020-02-14 07:26:45', '', 'cycle-club', '', 'publish', 'closed', 'closed', '', 'cycle-club', '', '', '2020-02-14 07:27:34', '2020-02-14 07:27:34', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=159', 0, 'page', '', 0),
(160, 1, '2020-02-14 07:26:45', '2020-02-14 07:26:45', '', 'cycle-club', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2020-02-14 07:26:45', '2020-02-14 07:26:45', '', 159, 'https://test.hachiweb.com/onelifefitness/2020/02/14/159-revision-v1/', 0, 'revision', '', 0),
(161, 1, '2020-02-14 07:33:56', '2020-02-14 07:33:56', '', 'legs-bums-tums', '', 'publish', 'closed', 'closed', '', 'legs-bums-tums', '', '', '2020-03-05 12:22:27', '2020-03-05 12:22:27', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=161', 0, 'page', '', 0),
(162, 1, '2020-02-14 07:33:56', '2020-02-14 07:33:56', '', 'bike-beats', '', 'inherit', 'closed', 'closed', '', '161-revision-v1', '', '', '2020-02-14 07:33:56', '2020-02-14 07:33:56', '', 161, 'https://test.hachiweb.com/onelifefitness/2020/02/14/161-revision-v1/', 0, 'revision', '', 0),
(163, 1, '2020-02-14 08:36:54', '2020-02-14 08:36:54', '', 'hips-bums-tums', '', 'publish', 'closed', 'closed', '', 'hips-bums-tums', '', '', '2020-03-05 12:23:34', '2020-03-05 12:23:34', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=163', 0, 'page', '', 0),
(164, 1, '2020-02-14 08:36:54', '2020-02-14 08:36:54', '', 'cyclone', '', 'inherit', 'closed', 'closed', '', '163-revision-v1', '', '', '2020-02-14 08:36:54', '2020-02-14 08:36:54', '', 163, 'https://test.hachiweb.com/onelifefitness/2020/02/14/163-revision-v1/', 0, 'revision', '', 0),
(165, 1, '2020-02-14 08:41:51', '2020-02-14 08:41:51', '', 'booty-blast', '', 'publish', 'closed', 'closed', '', 'booty-blast', '', '', '2020-03-05 12:24:53', '2020-03-05 12:24:53', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=165', 0, 'page', '', 0),
(166, 1, '2020-02-14 08:41:51', '2020-02-14 08:41:51', '', 'power-battle', '', 'inherit', 'closed', 'closed', '', '165-revision-v1', '', '', '2020-02-14 08:41:51', '2020-02-14 08:41:51', '', 165, 'https://test.hachiweb.com/onelifefitness/2020/02/14/165-revision-v1/', 0, 'revision', '', 0),
(167, 1, '2020-02-14 08:47:57', '2020-02-14 08:47:57', '', 'functional-training', '', 'publish', 'closed', 'closed', '', 'functional-training', '', '', '2020-02-14 08:48:20', '2020-02-14 08:48:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=167', 0, 'page', '', 0),
(168, 1, '2020-02-14 08:47:57', '2020-02-14 08:47:57', '', 'functional-training', '', 'inherit', 'closed', 'closed', '', '167-revision-v1', '', '', '2020-02-14 08:47:57', '2020-02-14 08:47:57', '', 167, 'https://test.hachiweb.com/onelifefitness/2020/02/14/167-revision-v1/', 0, 'revision', '', 0),
(170, 1, '2020-02-14 08:53:36', '2020-02-14 08:53:36', '', 'text', '', 'publish', 'open', 'open', '', 'text', '', '', '2020-02-14 08:53:36', '2020-02-14 08:53:36', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=170', 0, 'post', '', 0),
(171, 1, '2020-02-14 08:53:36', '2020-02-14 08:53:36', '', 'text', '', 'inherit', 'closed', 'closed', '', '170-revision-v1', '', '', '2020-02-14 08:53:36', '2020-02-14 08:53:36', '', 170, 'https://test.hachiweb.com/onelifefitness/2020/02/14/170-revision-v1/', 0, 'revision', '', 0),
(172, 1, '2020-02-14 09:06:06', '2020-02-14 09:06:06', '', 'functional-circuit', '', 'publish', 'closed', 'closed', '', 'functional-circuit', '', '', '2020-03-05 12:25:41', '2020-03-05 12:25:41', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=172', 0, 'page', '', 0),
(173, 1, '2020-02-14 09:06:06', '2020-02-14 09:06:06', '', 'b-o-l-t', '', 'inherit', 'closed', 'closed', '', '172-revision-v1', '', '', '2020-02-14 09:06:06', '2020-02-14 09:06:06', '', 172, 'https://test.hachiweb.com/onelifefitness/2020/02/14/172-revision-v1/', 0, 'revision', '', 0),
(174, 1, '2020-02-14 09:20:58', '2020-02-14 09:20:58', '', 'tabata', '', 'publish', 'closed', 'closed', '', 'tabata', '', '', '2020-03-05 12:26:20', '2020-03-05 12:26:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=174', 0, 'page', '', 0),
(175, 1, '2020-02-14 09:20:58', '2020-02-14 09:20:58', '', 'barfly', '', 'inherit', 'closed', 'closed', '', '174-revision-v1', '', '', '2020-02-14 09:20:58', '2020-02-14 09:20:58', '', 174, 'https://test.hachiweb.com/onelifefitness/2020/02/14/174-revision-v1/', 0, 'revision', '', 0),
(176, 1, '2020-02-14 09:24:12', '2020-02-14 09:24:12', '', 'hiit', '', 'publish', 'closed', 'closed', '', 'hiit', '', '', '2020-03-05 12:26:54', '2020-03-05 12:26:54', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=176', 0, 'page', '', 0),
(177, 1, '2020-02-14 09:24:12', '2020-02-14 09:24:12', '', 'bartendaz', '', 'inherit', 'closed', 'closed', '', '176-revision-v1', '', '', '2020-02-14 09:24:12', '2020-02-14 09:24:12', '', 176, 'https://test.hachiweb.com/onelifefitness/2020/02/14/176-revision-v1/', 0, 'revision', '', 0),
(178, 1, '2020-02-14 09:32:21', '2020-02-14 09:32:21', '', 'pilates', '', 'publish', 'closed', 'closed', '', 'pilates-2', '', '', '2020-03-05 12:34:54', '2020-03-05 12:34:54', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=178', 0, 'page', '', 0),
(179, 1, '2020-02-14 09:32:21', '2020-02-14 09:32:21', '', 'battleballs', '', 'inherit', 'closed', 'closed', '', '178-revision-v1', '', '', '2020-02-14 09:32:21', '2020-02-14 09:32:21', '', 178, 'https://test.hachiweb.com/onelifefitness/2020/02/14/178-revision-v1/', 0, 'revision', '', 0),
(180, 1, '2020-02-14 09:37:36', '2020-02-14 09:37:36', '', 'bodyweight-bandits', '', 'publish', 'closed', 'closed', '', 'bodyweight-bandits', '', '', '2020-02-14 09:37:48', '2020-02-14 09:37:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=180', 0, 'page', '', 0),
(181, 1, '2020-02-14 09:37:36', '2020-02-14 09:37:36', '', 'bodyweight-bandits', '', 'inherit', 'closed', 'closed', '', '180-revision-v1', '', '', '2020-02-14 09:37:36', '2020-02-14 09:37:36', '', 180, 'https://test.hachiweb.com/onelifefitness/2020/02/14/180-revision-v1/', 0, 'revision', '', 0),
(182, 1, '2020-02-14 09:46:48', '2020-02-14 09:46:48', '', 'cavemen-and-neandergals', '', 'publish', 'closed', 'closed', '', 'cavemen-and-neandergals', '', '', '2020-02-14 09:47:05', '2020-02-14 09:47:05', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=182', 0, 'page', '', 0),
(183, 1, '2020-02-14 09:46:48', '2020-02-14 09:46:48', '', 'cavemen-and-neandergals', '', 'inherit', 'closed', 'closed', '', '182-revision-v1', '', '', '2020-02-14 09:46:48', '2020-02-14 09:46:48', '', 182, 'https://test.hachiweb.com/onelifefitness/2020/02/14/182-revision-v1/', 0, 'revision', '', 0),
(184, 1, '2020-02-14 09:51:01', '2020-02-14 09:51:01', '', 'functional-bodybuilding', '', 'publish', 'closed', 'closed', '', 'functional-bodybuilding', '', '', '2020-02-14 09:51:19', '2020-02-14 09:51:19', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=184', 0, 'page', '', 0),
(185, 1, '2020-02-14 09:51:01', '2020-02-14 09:51:01', '', 'functional-bodybuilding', '', 'inherit', 'closed', 'closed', '', '184-revision-v1', '', '', '2020-02-14 09:51:01', '2020-02-14 09:51:01', '', 184, 'https://test.hachiweb.com/onelifefitness/2020/02/14/184-revision-v1/', 0, 'revision', '', 0),
(186, 1, '2020-02-14 09:55:27', '2020-02-14 09:55:27', '', 'gymnastic-conditioning', '', 'publish', 'closed', 'closed', '', 'gymnastic-conditioning', '', '', '2020-02-14 09:55:51', '2020-02-14 09:55:51', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=186', 0, 'page', '', 0),
(187, 1, '2020-02-14 09:55:27', '2020-02-14 09:55:27', '', 'gymnastic-conditioning', '', 'inherit', 'closed', 'closed', '', '186-revision-v1', '', '', '2020-02-14 09:55:27', '2020-02-14 09:55:27', '', 186, 'https://test.hachiweb.com/onelifefitness/2020/02/14/186-revision-v1/', 0, 'revision', '', 0),
(188, 1, '2020-02-14 10:02:01', '2020-02-14 10:02:01', '', 'ocr-training', '', 'publish', 'closed', 'closed', '', 'ocr-training', '', '', '2020-02-14 10:02:37', '2020-02-14 10:02:37', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=188', 0, 'page', '', 0),
(189, 1, '2020-02-14 10:02:01', '2020-02-14 10:02:01', '', 'ocr-training', '', 'inherit', 'closed', 'closed', '', '188-revision-v1', '', '', '2020-02-14 10:02:01', '2020-02-14 10:02:01', '', 188, 'https://test.hachiweb.com/onelifefitness/2020/02/14/188-revision-v1/', 0, 'revision', '', 0),
(190, 1, '2020-02-14 10:05:33', '2020-02-14 10:05:33', '', 'prehab', '', 'publish', 'closed', 'closed', '', 'prehab', '', '', '2020-02-14 10:06:00', '2020-02-14 10:06:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=190', 0, 'page', '', 0),
(191, 1, '2020-02-14 10:05:33', '2020-02-14 10:05:33', '', 'prehab', '', 'inherit', 'closed', 'closed', '', '190-revision-v1', '', '', '2020-02-14 10:05:33', '2020-02-14 10:05:33', '', 190, 'https://test.hachiweb.com/onelifefitness/2020/02/14/190-revision-v1/', 0, 'revision', '', 0),
(192, 1, '2020-02-14 10:17:57', '2020-02-14 10:17:57', '', 'rehab', '', 'publish', 'closed', 'closed', '', 'rehab', '', '', '2020-02-14 10:18:24', '2020-02-14 10:18:24', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=192', 0, 'page', '', 0),
(193, 1, '2020-02-14 10:17:57', '2020-02-14 10:17:57', '', 'rehab', '', 'inherit', 'closed', 'closed', '', '192-revision-v1', '', '', '2020-02-14 10:17:57', '2020-02-14 10:17:57', '', 192, 'https://test.hachiweb.com/onelifefitness/2020/02/14/192-revision-v1/', 0, 'revision', '', 0),
(194, 1, '2020-02-14 10:23:26', '2020-02-14 10:23:26', '', 'rowingwod', '', 'publish', 'closed', 'closed', '', 'rowingwod', '', '', '2020-02-14 10:23:51', '2020-02-14 10:23:51', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=194', 0, 'page', '', 0),
(195, 1, '2020-02-14 10:23:26', '2020-02-14 10:23:26', '', 'rowingwod', '', 'inherit', 'closed', 'closed', '', '194-revision-v1', '', '', '2020-02-14 10:23:26', '2020-02-14 10:23:26', '', 194, 'https://test.hachiweb.com/onelifefitness/2020/02/14/194-revision-v1/', 0, 'revision', '', 0),
(196, 1, '2020-02-14 10:27:19', '2020-02-14 10:27:19', '', 'strongman', '', 'publish', 'closed', 'closed', '', 'strongman', '', '', '2020-02-14 10:27:48', '2020-02-14 10:27:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=196', 0, 'page', '', 0),
(197, 1, '2020-02-14 10:27:19', '2020-02-14 10:27:19', '', 'strongman', '', 'inherit', 'closed', 'closed', '', '196-revision-v1', '', '', '2020-02-14 10:27:19', '2020-02-14 10:27:19', '', 196, 'https://test.hachiweb.com/onelifefitness/2020/02/14/196-revision-v1/', 0, 'revision', '', 0),
(198, 1, '2020-02-14 10:31:47', '2020-02-14 10:31:47', '', 'suspenders', '', 'publish', 'closed', 'closed', '', 'suspenders', '', '', '2020-02-14 10:32:07', '2020-02-14 10:32:07', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=198', 0, 'page', '', 0),
(199, 1, '2020-02-14 10:31:47', '2020-02-14 10:31:47', '', 'suspenders', '', 'inherit', 'closed', 'closed', '', '198-revision-v1', '', '', '2020-02-14 10:31:47', '2020-02-14 10:31:47', '', 198, 'https://test.hachiweb.com/onelifefitness/2020/02/14/198-revision-v1/', 0, 'revision', '', 0),
(200, 1, '2020-02-14 10:43:37', '2020-02-14 10:43:37', '', 'swingers-club', '', 'publish', 'closed', 'closed', '', 'swingers-club', '', '', '2020-02-14 10:45:46', '2020-02-14 10:45:46', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=200', 0, 'page', '', 0),
(201, 1, '2020-02-14 10:43:37', '2020-02-14 10:43:37', '', 'swingers-club', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2020-02-14 10:43:37', '2020-02-14 10:43:37', '', 200, 'https://test.hachiweb.com/onelifefitness/2020/02/14/200-revision-v1/', 0, 'revision', '', 0),
(202, 1, '2020-02-14 10:52:28', '2020-02-14 10:52:28', '', 'the-fundamentals', '', 'publish', 'closed', 'closed', '', 'the-fundamentals', '', '', '2020-02-14 10:52:54', '2020-02-14 10:52:54', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=202', 0, 'page', '', 0),
(203, 1, '2020-02-14 10:52:28', '2020-02-14 10:52:28', '', 'the-fundamentals', '', 'inherit', 'closed', 'closed', '', '202-revision-v1', '', '', '2020-02-14 10:52:28', '2020-02-14 10:52:28', '', 202, 'https://test.hachiweb.com/onelifefitness/2020/02/14/202-revision-v1/', 0, 'revision', '', 0),
(204, 1, '2020-02-14 10:57:42', '2020-02-14 10:57:42', '', 'wod-squad', '', 'publish', 'closed', 'closed', '', 'wod-squad', '', '', '2020-02-14 10:57:59', '2020-02-14 10:57:59', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=204', 0, 'page', '', 0),
(205, 1, '2020-02-14 10:57:42', '2020-02-14 10:57:42', '', 'wod-squad', '', 'inherit', 'closed', 'closed', '', '204-revision-v1', '', '', '2020-02-14 10:57:42', '2020-02-14 10:57:42', '', 204, 'https://test.hachiweb.com/onelifefitness/2020/02/14/204-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2020-02-14 11:05:30', '2020-02-14 11:05:30', '', 'holistic-retreat', '', 'publish', 'closed', 'closed', '', 'holistic-retreat', '', '', '2020-02-14 11:05:51', '2020-02-14 11:05:51', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=206', 0, 'page', '', 0),
(207, 1, '2020-02-14 11:05:30', '2020-02-14 11:05:30', '', 'holistic-retreat', '', 'inherit', 'closed', 'closed', '', '206-revision-v1', '', '', '2020-02-14 11:05:30', '2020-02-14 11:05:30', '', 206, 'https://test.hachiweb.com/onelifefitness/2020/02/14/206-revision-v1/', 0, 'revision', '', 0),
(208, 1, '2020-02-14 11:31:53', '2020-02-14 11:31:53', '', 'body-tone', '', 'publish', 'closed', 'closed', '', 'body-tone', '', '', '2020-03-05 12:27:36', '2020-03-05 12:27:36', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=208', 0, 'page', '', 0),
(209, 1, '2020-02-14 11:31:53', '2020-02-14 11:31:53', '', 'ashtanga-yoga', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-02-14 11:31:53', '2020-02-14 11:31:53', '', 208, 'https://test.hachiweb.com/onelifefitness/2020/02/14/208-revision-v1/', 0, 'revision', '', 0),
(210, 1, '2020-02-14 11:39:28', '2020-02-14 11:39:28', '', 'abs-circuit', '', 'publish', 'closed', 'closed', '', 'abs-circuit', '', '', '2020-03-05 12:28:29', '2020-03-05 12:28:29', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=210', 0, 'page', '', 0),
(211, 1, '2020-02-14 11:39:28', '2020-02-14 11:39:28', '', 'balates', '', 'inherit', 'closed', 'closed', '', '210-revision-v1', '', '', '2020-02-14 11:39:28', '2020-02-14 11:39:28', '', 210, 'https://test.hachiweb.com/onelifefitness/2020/02/14/210-revision-v1/', 0, 'revision', '', 0),
(213, 1, '2020-02-14 11:44:47', '2020-02-14 11:44:47', '', 'box-abs-circuit', '', 'publish', 'closed', 'closed', '', 'box-abs-circuit', '', '', '2020-03-05 12:29:18', '2020-03-05 12:29:18', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=213', 0, 'page', '', 0),
(214, 1, '2020-02-14 11:44:47', '2020-02-14 11:44:47', '', 'budokon-yoga', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2020-02-14 11:44:47', '2020-02-14 11:44:47', '', 213, 'https://test.hachiweb.com/onelifefitness/2020/02/14/213-revision-v1/', 0, 'revision', '', 0),
(215, 1, '2020-02-14 11:49:58', '2020-02-14 11:49:58', '', 'buti-yoga', '', 'publish', 'closed', 'closed', '', 'buti-yoga', '', '', '2020-02-14 11:50:19', '2020-02-14 11:50:19', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=215', 0, 'page', '', 0),
(216, 1, '2020-02-14 11:49:58', '2020-02-14 11:49:58', '', 'buti-yoga', '', 'inherit', 'closed', 'closed', '', '215-revision-v1', '', '', '2020-02-14 11:49:58', '2020-02-14 11:49:58', '', 215, 'https://test.hachiweb.com/onelifefitness/2020/02/14/215-revision-v1/', 0, 'revision', '', 0),
(217, 1, '2020-02-14 11:55:48', '2020-02-14 11:55:48', '', 'contortion', '', 'publish', 'closed', 'closed', '', 'contortion', '', '', '2020-02-14 11:56:12', '2020-02-14 11:56:12', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=217', 0, 'page', '', 0),
(218, 1, '2020-02-14 11:55:48', '2020-02-14 11:55:48', '', 'contortion', '', 'inherit', 'closed', 'closed', '', '217-revision-v1', '', '', '2020-02-14 11:55:48', '2020-02-14 11:55:48', '', 217, 'https://test.hachiweb.com/onelifefitness/2020/02/14/217-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(219, 1, '2020-02-14 12:03:20', '2020-02-14 12:03:20', '', 'hatha-yoga', '', 'publish', 'closed', 'closed', '', 'hatha-yoga', '', '', '2020-02-14 12:03:48', '2020-02-14 12:03:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=219', 0, 'page', '', 0),
(220, 1, '2020-02-14 12:03:20', '2020-02-14 12:03:20', '', 'hatha-yoga', '', 'inherit', 'closed', 'closed', '', '219-revision-v1', '', '', '2020-02-14 12:03:20', '2020-02-14 12:03:20', '', 219, 'https://test.hachiweb.com/onelifefitness/2020/02/14/219-revision-v1/', 0, 'revision', '', 0),
(222, 1, '2020-02-14 12:12:56', '2020-02-14 12:12:56', '', 'paddleboard-yoga', '', 'publish', 'closed', 'closed', '', 'paddleboard-yoga', '', '', '2020-02-14 12:13:29', '2020-02-14 12:13:29', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=222', 0, 'page', '', 0),
(223, 1, '2020-02-14 12:12:56', '2020-02-14 12:12:56', '', 'paddleboard-yoga', '', 'inherit', 'closed', 'closed', '', '222-revision-v1', '', '', '2020-02-14 12:12:56', '2020-02-14 12:12:56', '', 222, 'https://test.hachiweb.com/onelifefitness/2020/02/14/222-revision-v1/', 0, 'revision', '', 0),
(224, 1, '2020-02-14 12:17:42', '2020-02-14 12:17:42', '', 'pilates', '', 'publish', 'closed', 'closed', '', 'pilates', '', '', '2020-02-14 12:18:17', '2020-02-14 12:18:17', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=224', 0, 'page', '', 0),
(225, 1, '2020-02-14 12:17:42', '2020-02-14 12:17:42', '', 'pilates', '', 'inherit', 'closed', 'closed', '', '224-revision-v1', '', '', '2020-02-14 12:17:42', '2020-02-14 12:17:42', '', 224, 'https://test.hachiweb.com/onelifefitness/2020/02/14/224-revision-v1/', 0, 'revision', '', 0),
(226, 1, '2020-02-14 12:23:06', '2020-02-14 12:23:06', '', 'rocket-yoga', '', 'publish', 'closed', 'closed', '', 'rocket-yoga', '', '', '2020-02-14 12:23:36', '2020-02-14 12:23:36', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=226', 0, 'page', '', 0),
(227, 1, '2020-02-14 12:23:06', '2020-02-14 12:23:06', '', 'rocket-yoga', '', 'inherit', 'closed', 'closed', '', '226-revision-v1', '', '', '2020-02-14 12:23:06', '2020-02-14 12:23:06', '', 226, 'https://test.hachiweb.com/onelifefitness/2020/02/14/226-revision-v1/', 0, 'revision', '', 0),
(228, 1, '2020-02-15 04:10:53', '2020-02-15 04:10:53', '', 'vinyasa-yoga', '', 'publish', 'closed', 'closed', '', 'vinyasa-yoga', '', '', '2020-02-15 04:11:20', '2020-02-15 04:11:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=228', 0, 'page', '', 0),
(229, 1, '2020-02-15 04:10:53', '2020-02-15 04:10:53', '', 'vinyasa-yoga', '', 'inherit', 'closed', 'closed', '', '228-revision-v1', '', '', '2020-02-15 04:10:53', '2020-02-15 04:10:53', '', 228, 'https://test.hachiweb.com/onelifefitness/2020/02/15/228-revision-v1/', 0, 'revision', '', 0),
(230, 1, '2020-02-15 04:21:39', '2020-02-15 04:21:39', '', 'yin-yoga', '', 'publish', 'closed', 'closed', '', 'yin-yoga', '', '', '2020-02-15 04:22:15', '2020-02-15 04:22:15', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=230', 0, 'page', '', 0),
(231, 1, '2020-02-15 04:21:39', '2020-02-15 04:21:39', '', 'yin-yoga', '', 'inherit', 'closed', 'closed', '', '230-revision-v1', '', '', '2020-02-15 04:21:39', '2020-02-15 04:21:39', '', 230, 'https://test.hachiweb.com/onelifefitness/2020/02/15/230-revision-v1/', 0, 'revision', '', 0),
(232, 1, '2020-02-15 04:27:19', '2020-02-15 04:27:19', '', 'yoga-for-lifting', '', 'publish', 'closed', 'closed', '', 'yoga-for-lifting', '', '', '2020-02-15 04:27:58', '2020-02-15 04:27:58', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=232', 0, 'page', '', 0),
(233, 1, '2020-02-15 04:27:19', '2020-02-15 04:27:19', '', 'yoga-for-lifting', '', 'inherit', 'closed', 'closed', '', '232-revision-v1', '', '', '2020-02-15 04:27:19', '2020-02-15 04:27:19', '', 232, 'https://test.hachiweb.com/onelifefitness/2020/02/15/232-revision-v1/', 0, 'revision', '', 0),
(234, 1, '2020-02-15 04:29:58', '2020-02-15 04:29:58', '', 'yogangster', '', 'publish', 'closed', 'closed', '', 'yogangster', '', '', '2020-02-15 04:30:12', '2020-02-15 04:30:12', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=234', 0, 'page', '', 0),
(235, 1, '2020-02-15 04:29:58', '2020-02-15 04:29:58', '', 'yogangster', '', 'inherit', 'closed', 'closed', '', '234-revision-v1', '', '', '2020-02-15 04:29:58', '2020-02-15 04:29:58', '', 234, 'https://test.hachiweb.com/onelifefitness/2020/02/15/234-revision-v1/', 0, 'revision', '', 0),
(236, 1, '2020-02-15 04:35:50', '2020-02-15 04:35:50', '', 'look-better-naked', '', 'publish', 'closed', 'closed', '', 'look-better-naked', '', '', '2020-02-15 04:36:14', '2020-02-15 04:36:14', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=236', 0, 'page', '', 0),
(237, 1, '2020-02-15 04:35:50', '2020-02-15 04:35:50', '', 'look-better-naked', '', 'inherit', 'closed', 'closed', '', '236-revision-v1', '', '', '2020-02-15 04:35:50', '2020-02-15 04:35:50', '', 236, 'https://test.hachiweb.com/onelifefitness/2020/02/15/236-revision-v1/', 0, 'revision', '', 0),
(238, 1, '2020-02-15 04:44:23', '2020-02-15 04:44:23', '', 'box-xpress', '', 'publish', 'closed', 'closed', '', 'box-xpress', '', '', '2020-03-05 12:30:00', '2020-03-05 12:30:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=238', 0, 'page', '', 0),
(239, 1, '2020-02-15 04:44:23', '2020-02-15 04:44:23', '', 'badass', '', 'inherit', 'closed', 'closed', '', '238-revision-v1', '', '', '2020-02-15 04:44:23', '2020-02-15 04:44:23', '', 238, 'https://test.hachiweb.com/onelifefitness/2020/02/15/238-revision-v1/', 0, 'revision', '', 0),
(240, 1, '2020-02-15 04:56:17', '2020-02-15 04:56:17', '', 'ride-xpress', '', 'publish', 'closed', 'closed', '', 'ride-xpress', '', '', '2020-03-05 12:30:55', '2020-03-05 12:30:55', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=240', 0, 'page', '', 0),
(241, 1, '2020-02-15 04:56:17', '2020-02-15 04:56:17', '', 'gain-train', '', 'inherit', 'closed', 'closed', '', '240-revision-v1', '', '', '2020-02-15 04:56:17', '2020-02-15 04:56:17', '', 240, 'https://test.hachiweb.com/onelifefitness/2020/02/15/240-revision-v1/', 0, 'revision', '', 0),
(242, 1, '2020-02-15 05:01:51', '2020-02-15 05:01:51', '', 'kid-boxing', '', 'publish', 'closed', 'closed', '', 'kid-boxing', '', '', '2020-03-05 12:31:34', '2020-03-05 12:31:34', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=242', 0, 'page', '', 0),
(243, 1, '2020-02-15 05:01:51', '2020-02-15 05:01:51', '', 'hardcore', '', 'inherit', 'closed', 'closed', '', '242-revision-v1', '', '', '2020-02-15 05:01:51', '2020-02-15 05:01:51', '', 242, 'https://test.hachiweb.com/onelifefitness/2020/02/15/242-revision-v1/', 0, 'revision', '', 0),
(244, 1, '2020-02-15 05:10:54', '2020-02-15 05:10:54', '', 'reppin', '', 'publish', 'closed', 'closed', '', 'reppin', '', '', '2020-02-15 05:11:43', '2020-02-15 05:11:43', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=244', 0, 'page', '', 0),
(245, 1, '2020-02-15 05:10:54', '2020-02-15 05:10:54', '', 'reppin', '', 'inherit', 'closed', 'closed', '', '244-revision-v1', '', '', '2020-02-15 05:10:54', '2020-02-15 05:10:54', '', 244, 'https://test.hachiweb.com/onelifefitness/2020/02/15/244-revision-v1/', 0, 'revision', '', 0),
(246, 1, '2020-02-15 05:42:59', '2020-02-15 05:42:59', '', 'sweat-to-the-beat', '', 'publish', 'closed', 'closed', '', 'sweat-to-the-beat', '', '', '2020-02-15 05:43:18', '2020-02-15 05:43:18', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=246', 0, 'page', '', 0),
(247, 1, '2020-02-15 05:42:59', '2020-02-15 05:42:59', '', 'sweat-to-the-beat', '', 'inherit', 'closed', 'closed', '', '246-revision-v1', '', '', '2020-02-15 05:42:59', '2020-02-15 05:42:59', '', 246, 'https://test.hachiweb.com/onelifefitness/2020/02/15/246-revision-v1/', 0, 'revision', '', 0),
(248, 1, '2020-02-15 05:46:48', '2020-02-15 05:46:48', '', 'thunder', '', 'publish', 'closed', 'closed', '', 'thunder', '', '', '2020-02-15 05:47:08', '2020-02-15 05:47:08', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=248', 0, 'page', '', 0),
(249, 1, '2020-02-15 05:46:48', '2020-02-15 05:46:48', '', 'thunder', '', 'inherit', 'closed', 'closed', '', '248-revision-v1', '', '', '2020-02-15 05:46:48', '2020-02-15 05:46:48', '', 248, 'https://test.hachiweb.com/onelifefitness/2020/02/15/248-revision-v1/', 0, 'revision', '', 0),
(250, 1, '2020-02-15 05:51:17', '2020-02-15 05:51:17', '', 'school-dance', '', 'publish', 'closed', 'closed', '', 'school-dance', '', '', '2020-02-15 05:51:38', '2020-02-15 05:51:38', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=250', 0, 'page', '', 0),
(251, 1, '2020-02-15 05:51:17', '2020-02-15 05:51:17', '', 'school-dance', '', 'inherit', 'closed', 'closed', '', '250-revision-v1', '', '', '2020-02-15 05:51:17', '2020-02-15 05:51:17', '', 250, 'https://test.hachiweb.com/onelifefitness/2020/02/15/250-revision-v1/', 0, 'revision', '', 0),
(252, 1, '2020-02-15 05:54:19', '2020-02-15 05:54:19', '', 'step-lift', '', 'publish', 'closed', 'closed', '', 'step-lift', '', '', '2020-03-05 12:32:20', '2020-03-05 12:32:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=252', 0, 'page', '', 0),
(253, 1, '2020-02-15 05:54:19', '2020-02-15 05:54:19', '', 'callback', '', 'inherit', 'closed', 'closed', '', '252-revision-v1', '', '', '2020-02-15 05:54:19', '2020-02-15 05:54:19', '', 252, 'https://test.hachiweb.com/onelifefitness/2020/02/15/252-revision-v1/', 0, 'revision', '', 0),
(254, 1, '2020-02-15 05:56:44', '2020-02-15 05:56:44', '', 'abs-core', '', 'publish', 'closed', 'closed', '', 'abs-core', '', '', '2020-03-05 12:33:06', '2020-03-05 12:33:06', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=254', 0, 'page', '', 0),
(255, 1, '2020-02-15 05:56:44', '2020-02-15 05:56:44', '', 'dancehall', '', 'inherit', 'closed', 'closed', '', '254-revision-v1', '', '', '2020-02-15 05:56:44', '2020-02-15 05:56:44', '', 254, 'https://test.hachiweb.com/onelifefitness/2020/02/15/254-revision-v1/', 0, 'revision', '', 0),
(256, 1, '2020-02-15 06:02:16', '2020-02-15 06:02:16', '', 'body-bootcamp', '', 'publish', 'closed', 'closed', '', 'body-bootcamp', '', '', '2020-03-05 12:33:41', '2020-03-05 12:33:41', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=256', 0, 'page', '', 0),
(257, 1, '2020-02-15 06:02:16', '2020-02-15 06:02:16', '', 'femme', '', 'inherit', 'closed', 'closed', '', '256-revision-v1', '', '', '2020-02-15 06:02:16', '2020-02-15 06:02:16', '', 256, 'https://test.hachiweb.com/onelifefitness/2020/02/15/256-revision-v1/', 0, 'revision', '', 0),
(258, 1, '2020-02-15 06:06:04', '2020-02-15 06:06:04', '', 'no-holds-barre', '', 'publish', 'closed', 'closed', '', 'no-holds-barre', '', '', '2020-02-15 06:06:24', '2020-02-15 06:06:24', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=258', 0, 'page', '', 0),
(259, 1, '2020-02-15 06:06:04', '2020-02-15 06:06:04', '', 'no-holds-barre', '', 'inherit', 'closed', 'closed', '', '258-revision-v1', '', '', '2020-02-15 06:06:04', '2020-02-15 06:06:04', '', 258, 'https://test.hachiweb.com/onelifefitness/2020/02/15/258-revision-v1/', 0, 'revision', '', 0),
(260, 1, '2020-02-15 06:09:17', '2020-02-15 06:09:17', '', 'gymbox-pole-dance', '', 'publish', 'closed', 'closed', '', 'gymbox-pole-dance', '', '', '2020-02-15 06:09:37', '2020-02-15 06:09:37', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=260', 0, 'page', '', 0),
(261, 1, '2020-02-15 06:09:17', '2020-02-15 06:09:17', '', 'gymbox-pole-dance', '', 'inherit', 'closed', 'closed', '', '260-revision-v1', '', '', '2020-02-15 06:09:17', '2020-02-15 06:09:17', '', 260, 'https://test.hachiweb.com/onelifefitness/2020/02/15/260-revision-v1/', 0, 'revision', '', 0),
(262, 1, '2020-02-15 06:13:23', '2020-02-15 06:13:23', '', 'shway', '', 'publish', 'closed', 'closed', '', 'shway', '', '', '2020-02-15 06:13:48', '2020-02-15 06:13:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=262', 0, 'page', '', 0),
(263, 1, '2020-02-15 06:13:23', '2020-02-15 06:13:23', '', 'shway', '', 'inherit', 'closed', 'closed', '', '262-revision-v1', '', '', '2020-02-15 06:13:23', '2020-02-15 06:13:23', '', 262, 'https://test.hachiweb.com/onelifefitness/2020/02/15/262-revision-v1/', 0, 'revision', '', 0),
(264, 1, '2020-02-15 06:17:10', '2020-02-15 06:17:10', '', 'sweat-drench', '', 'publish', 'closed', 'closed', '', 'sweat-drench', '', '', '2020-02-15 06:17:40', '2020-02-15 06:17:40', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=264', 0, 'page', '', 0),
(265, 1, '2020-02-15 06:17:10', '2020-02-15 06:17:10', '', 'sweat-drench', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2020-02-15 06:17:10', '2020-02-15 06:17:10', '', 264, 'https://test.hachiweb.com/onelifefitness/2020/02/15/264-revision-v1/', 0, 'revision', '', 0),
(266, 1, '2020-02-15 06:29:21', '2020-02-15 06:29:21', '', 'zumba', '', 'publish', 'closed', 'closed', '', 'zumba', '', '', '2020-03-05 12:34:28', '2020-03-05 12:34:28', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=266', 0, 'page', '', 0),
(267, 1, '2020-02-15 06:29:21', '2020-02-15 06:29:21', '', 'amrap', '', 'inherit', 'closed', 'closed', '', '266-revision-v1', '', '', '2020-02-15 06:29:21', '2020-02-15 06:29:21', '', 266, 'https://test.hachiweb.com/onelifefitness/2020/02/15/266-revision-v1/', 0, 'revision', '', 0),
(268, 1, '2020-02-15 06:32:40', '2020-02-15 06:32:40', '', 'yoga', '', 'publish', 'closed', 'closed', '', 'yoga', '', '', '2020-03-05 12:35:18', '2020-03-05 12:35:18', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=268', 0, 'page', '', 0),
(269, 1, '2020-02-15 06:32:40', '2020-02-15 06:32:40', '', 'beastmode', '', 'inherit', 'closed', 'closed', '', '268-revision-v1', '', '', '2020-02-15 06:32:40', '2020-02-15 06:32:40', '', 268, 'https://test.hachiweb.com/onelifefitness/2020/02/15/268-revision-v1/', 0, 'revision', '', 0),
(270, 1, '2020-02-15 06:36:48', '2020-02-15 06:36:48', '', 'deathrow', '', 'publish', 'closed', 'closed', '', 'deathrow', '', '', '2020-02-15 06:37:03', '2020-02-15 06:37:03', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=270', 0, 'page', '', 0),
(271, 1, '2020-02-15 06:36:48', '2020-02-15 06:36:48', '', 'deathrow', '', 'inherit', 'closed', 'closed', '', '270-revision-v1', '', '', '2020-02-15 06:36:48', '2020-02-15 06:36:48', '', 270, 'https://test.hachiweb.com/onelifefitness/2020/02/15/270-revision-v1/', 0, 'revision', '', 0),
(272, 1, '2020-02-15 06:40:43', '2020-02-15 06:40:43', '', 'drill-sergeant', '', 'publish', 'closed', 'closed', '', 'drill-sergeant', '', '', '2020-02-15 06:41:11', '2020-02-15 06:41:11', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=272', 0, 'page', '', 0),
(273, 1, '2020-02-15 06:40:43', '2020-02-15 06:40:43', '', 'drill-sergeant', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2020-02-15 06:40:43', '2020-02-15 06:40:43', '', 272, 'https://test.hachiweb.com/onelifefitness/2020/02/15/272-revision-v1/', 0, 'revision', '', 0),
(274, 1, '2020-02-15 06:43:54', '2020-02-15 06:43:54', '', 'escalate', '', 'publish', 'closed', 'closed', '', 'escalate', '', '', '2020-02-15 06:44:13', '2020-02-15 06:44:13', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=274', 0, 'page', '', 0),
(275, 1, '2020-02-15 06:43:54', '2020-02-15 06:43:54', '', 'escalate', '', 'inherit', 'closed', 'closed', '', '274-revision-v1', '', '', '2020-02-15 06:43:54', '2020-02-15 06:43:54', '', 274, 'https://test.hachiweb.com/onelifefitness/2020/02/15/274-revision-v1/', 0, 'revision', '', 0),
(276, 1, '2020-02-15 06:46:51', '2020-02-15 06:46:51', '', 'flatline', '', 'publish', 'closed', 'closed', '', 'flatline', '', '', '2020-02-15 06:47:13', '2020-02-15 06:47:13', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=276', 0, 'page', '', 0),
(277, 1, '2020-02-15 06:46:51', '2020-02-15 06:46:51', '', 'flatline', '', 'inherit', 'closed', 'closed', '', '276-revision-v1', '', '', '2020-02-15 06:46:51', '2020-02-15 06:46:51', '', 276, 'https://test.hachiweb.com/onelifefitness/2020/02/15/276-revision-v1/', 0, 'revision', '', 0),
(278, 1, '2020-02-15 06:50:36', '2020-02-15 06:50:36', '', 'metcon', '', 'publish', 'closed', 'closed', '', 'metcon', '', '', '2020-02-15 06:50:50', '2020-02-15 06:50:50', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=278', 0, 'page', '', 0),
(279, 1, '2020-02-15 06:50:36', '2020-02-15 06:50:36', '', 'metcon', '', 'inherit', 'closed', 'closed', '', '278-revision-v1', '', '', '2020-02-15 06:50:36', '2020-02-15 06:50:36', '', 278, 'https://test.hachiweb.com/onelifefitness/2020/02/15/278-revision-v1/', 0, 'revision', '', 0),
(280, 1, '2020-02-15 06:55:25', '2020-02-15 06:55:25', '', 'pound', '', 'publish', 'closed', 'closed', '', 'pound', '', '', '2020-02-15 06:55:51', '2020-02-15 06:55:51', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=280', 0, 'page', '', 0),
(281, 1, '2020-02-15 06:55:25', '2020-02-15 06:55:25', '', 'pound', '', 'inherit', 'closed', 'closed', '', '280-revision-v1', '', '', '2020-02-15 06:55:25', '2020-02-15 06:55:25', '', 280, 'https://test.hachiweb.com/onelifefitness/2020/02/15/280-revision-v1/', 0, 'revision', '', 0),
(282, 1, '2020-02-15 07:02:08', '2020-02-15 07:02:08', '', 'threshold', '', 'publish', 'closed', 'closed', '', 'threshold', '', '', '2020-02-15 07:02:33', '2020-02-15 07:02:33', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=282', 0, 'page', '', 0),
(283, 1, '2020-02-15 07:02:08', '2020-02-15 07:02:08', '', 'threshold', '', 'inherit', 'closed', 'closed', '', '282-revision-v1', '', '', '2020-02-15 07:02:08', '2020-02-15 07:02:08', '', 282, 'https://test.hachiweb.com/onelifefitness/2020/02/15/282-revision-v1/', 0, 'revision', '', 0),
(284, 1, '2020-02-15 07:05:30', '2020-02-15 07:05:30', '', 'trilactic', '', 'publish', 'closed', 'closed', '', 'trilactic', '', '', '2020-02-15 07:05:52', '2020-02-15 07:05:52', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=284', 0, 'page', '', 0),
(285, 1, '2020-02-15 07:05:30', '2020-02-15 07:05:30', '', 'trilactic', '', 'inherit', 'closed', 'closed', '', '284-revision-v1', '', '', '2020-02-15 07:05:30', '2020-02-15 07:05:30', '', 284, 'https://test.hachiweb.com/onelifefitness/2020/02/15/284-revision-v1/', 0, 'revision', '', 0),
(286, 1, '2020-02-15 07:17:57', '2020-02-15 07:17:57', '', 'ufc-fit', '', 'publish', 'closed', 'closed', '', 'ufc-fit', '', '', '2020-02-15 07:18:18', '2020-02-15 07:18:18', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=286', 0, 'page', '', 0),
(287, 1, '2020-02-15 07:17:57', '2020-02-15 07:17:57', '', 'ufc-fit', '', 'inherit', 'closed', 'closed', '', '286-revision-v1', '', '', '2020-02-15 07:17:57', '2020-02-15 07:17:57', '', 286, 'https://test.hachiweb.com/onelifefitness/2020/02/15/286-revision-v1/', 0, 'revision', '', 0),
(288, 1, '2020-02-15 07:20:39', '2020-02-15 07:20:39', '', 'whiplash', '', 'publish', 'closed', 'closed', '', 'whiplash', '', '', '2020-02-15 07:20:59', '2020-02-15 07:20:59', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=288', 0, 'page', '', 0),
(289, 1, '2020-02-15 07:20:39', '2020-02-15 07:20:39', '', 'whiplash', '', 'inherit', 'closed', 'closed', '', '288-revision-v1', '', '', '2020-02-15 07:20:39', '2020-02-15 07:20:39', '', 288, 'https://test.hachiweb.com/onelifefitness/2020/02/15/288-revision-v1/', 0, 'revision', '', 0),
(290, 1, '2020-02-15 11:24:54', '2020-02-15 11:24:54', '', 'timetable-blank', '', 'trash', 'closed', 'closed', '', 'timetable-blank__trashed', '', '', '2020-02-15 11:26:34', '2020-02-15 11:26:34', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=290', 0, 'page', '', 0),
(291, 1, '2020-02-15 11:24:54', '2020-02-15 11:24:54', '', 'timetable-blank', '', 'inherit', 'closed', 'closed', '', '290-revision-v1', '', '', '2020-02-15 11:24:54', '2020-02-15 11:24:54', '', 290, 'https://test.hachiweb.com/onelifefitness/2020/02/15/290-revision-v1/', 0, 'revision', '', 0),
(292, 1, '2020-02-15 11:25:34', '2020-02-15 11:25:34', '', 'timetable-blank', '', 'trash', 'closed', 'closed', '', 'timetable-blank-2__trashed', '', '', '2020-02-15 11:26:37', '2020-02-15 11:26:37', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=292', 0, 'page', '', 0),
(293, 1, '2020-02-15 11:25:34', '2020-02-15 11:25:34', '', 'timetable-blank', '', 'inherit', 'closed', 'closed', '', '292-revision-v1', '', '', '2020-02-15 11:25:34', '2020-02-15 11:25:34', '', 292, 'https://test.hachiweb.com/onelifefitness/2020/02/15/292-revision-v1/', 0, 'revision', '', 0),
(294, 1, '2020-02-15 11:26:50', '2020-02-15 11:26:50', '', 'timetable-bank', '', 'publish', 'closed', 'closed', '', 'timetable-bank', '', '', '2020-02-15 11:27:14', '2020-02-15 11:27:14', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=294', 0, 'page', '', 0),
(295, 1, '2020-02-15 11:26:50', '2020-02-15 11:26:50', '', 'timetable-bank', '', 'inherit', 'closed', 'closed', '', '294-revision-v1', '', '', '2020-02-15 11:26:50', '2020-02-15 11:26:50', '', 294, 'https://test.hachiweb.com/onelifefitness/2020/02/15/294-revision-v1/', 0, 'revision', '', 0),
(296, 1, '2020-02-17 08:53:26', '2020-02-17 08:53:26', '', 'gym', '', 'publish', 'closed', 'closed', '', 'gym', '', '', '2020-02-17 08:53:48', '2020-02-17 08:53:48', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=296', 0, 'page', '', 0),
(297, 1, '2020-02-17 08:53:26', '2020-02-17 08:53:26', '', 'gym', '', 'inherit', 'closed', 'closed', '', '296-revision-v1', '', '', '2020-02-17 08:53:26', '2020-02-17 08:53:26', '', 296, 'https://test.hachiweb.com/onelifefitness/2020/02/17/296-revision-v1/', 0, 'revision', '', 0),
(298, 1, '2020-02-18 03:51:03', '2020-02-18 03:51:03', '', 'home-page', '', 'trash', 'closed', 'closed', '', 'home-page__trashed', '', '', '2020-02-18 03:51:42', '2020-02-18 03:51:42', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=298', 0, 'page', '', 0),
(299, 1, '2020-02-18 03:51:03', '2020-02-18 03:51:03', '', 'home-page', '', 'inherit', 'closed', 'closed', '', '298-revision-v1', '', '', '2020-02-18 03:51:03', '2020-02-18 03:51:03', '', 298, 'https://test.hachiweb.com/onelifefitness/2020/02/18/298-revision-v1/', 0, 'revision', '', 0),
(300, 1, '2020-02-18 06:00:36', '2020-02-18 06:00:36', '', 'map-box', '', 'publish', 'closed', 'closed', '', 'map-box', '', '', '2020-02-18 06:01:00', '2020-02-18 06:01:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=300', 0, 'page', '', 0),
(301, 1, '2020-02-18 06:00:36', '2020-02-18 06:00:36', '', 'map-box', '', 'inherit', 'closed', 'closed', '', '300-revision-v1', '', '', '2020-02-18 06:00:36', '2020-02-18 06:00:36', '', 300, 'https://test.hachiweb.com/onelifefitness/2020/02/18/300-revision-v1/', 0, 'revision', '', 0),
(303, 1, '2020-02-22 06:02:15', '2020-02-22 06:02:15', '', 'cannon-street', '', 'publish', 'closed', 'closed', '', 'cannon-street', '', '', '2020-02-22 06:02:33', '2020-02-22 06:02:33', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=303', 0, 'page', '', 0),
(304, 1, '2020-02-22 06:02:15', '2020-02-22 06:02:15', '', 'cannon-street', '', 'inherit', 'closed', 'closed', '', '303-revision-v1', '', '', '2020-02-22 06:02:15', '2020-02-22 06:02:15', '', 303, 'https://test.hachiweb.com/onelifefitness/2020/02/22/303-revision-v1/', 0, 'revision', '', 0),
(305, 1, '2020-02-22 06:06:45', '2020-02-22 06:06:45', '', 'covent-garden', '', 'publish', 'closed', 'closed', '', 'covent-garden', '', '', '2020-02-22 06:07:10', '2020-02-22 06:07:10', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=305', 0, 'page', '', 0),
(306, 1, '2020-02-22 06:06:45', '2020-02-22 06:06:45', '', 'covent-garden', '', 'inherit', 'closed', 'closed', '', '305-revision-v1', '', '', '2020-02-22 06:06:45', '2020-02-22 06:06:45', '', 305, 'https://test.hachiweb.com/onelifefitness/2020/02/22/305-revision-v1/', 0, 'revision', '', 0),
(307, 1, '2020-02-22 06:12:06', '2020-02-22 06:12:06', '', 'ealing', '', 'publish', 'closed', 'closed', '', 'ealing', '', '', '2020-02-22 06:12:24', '2020-02-22 06:12:24', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=307', 0, 'page', '', 0),
(308, 1, '2020-02-22 06:12:06', '2020-02-22 06:12:06', '', 'ealing', '', 'inherit', 'closed', 'closed', '', '307-revision-v1', '', '', '2020-02-22 06:12:06', '2020-02-22 06:12:06', '', 307, 'https://test.hachiweb.com/onelifefitness/2020/02/22/307-revision-v1/', 0, 'revision', '', 0),
(309, 1, '2020-02-22 06:15:10', '2020-02-22 06:15:10', '', 'elephant-castle', '', 'publish', 'closed', 'closed', '', 'elephant-castle', '', '', '2020-02-22 06:15:30', '2020-02-22 06:15:30', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=309', 0, 'page', '', 0),
(310, 1, '2020-02-22 06:15:10', '2020-02-22 06:15:10', '', 'elephant-castle', '', 'inherit', 'closed', 'closed', '', '309-revision-v1', '', '', '2020-02-22 06:15:10', '2020-02-22 06:15:10', '', 309, 'https://test.hachiweb.com/onelifefitness/2020/02/22/309-revision-v1/', 0, 'revision', '', 0),
(311, 1, '2020-02-22 06:20:41', '2020-02-22 06:20:41', '', 'farringdon', '', 'publish', 'closed', 'closed', '', 'farringdon', '', '', '2020-02-22 06:21:00', '2020-02-22 06:21:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=311', 0, 'page', '', 0),
(312, 1, '2020-02-22 06:20:41', '2020-02-22 06:20:41', '', 'farringdon', '', 'inherit', 'closed', 'closed', '', '311-revision-v1', '', '', '2020-02-22 06:20:41', '2020-02-22 06:20:41', '', 311, 'https://test.hachiweb.com/onelifefitness/2020/02/22/311-revision-v1/', 0, 'revision', '', 0),
(313, 1, '2020-02-22 06:24:46', '2020-02-22 06:24:46', '', 'finsbury-park', '', 'publish', 'closed', 'closed', '', 'finsbury-park', '', '', '2020-02-22 06:25:07', '2020-02-22 06:25:07', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=313', 0, 'page', '', 0),
(314, 1, '2020-02-22 06:24:46', '2020-02-22 06:24:46', '', 'finsbury-park', '', 'inherit', 'closed', 'closed', '', '313-revision-v1', '', '', '2020-02-22 06:24:46', '2020-02-22 06:24:46', '', 313, 'https://test.hachiweb.com/onelifefitness/2020/02/22/313-revision-v1/', 0, 'revision', '', 0),
(315, 1, '2020-02-22 06:28:15', '2020-02-22 06:28:15', '', 'holborn', '', 'publish', 'closed', 'closed', '', 'holborn', '', '', '2020-02-22 06:28:40', '2020-02-22 06:28:40', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=315', 0, 'page', '', 0),
(316, 1, '2020-02-22 06:28:15', '2020-02-22 06:28:15', '', 'holborn', '', 'inherit', 'closed', 'closed', '', '315-revision-v1', '', '', '2020-02-22 06:28:15', '2020-02-22 06:28:15', '', 315, 'https://test.hachiweb.com/onelifefitness/2020/02/22/315-revision-v1/', 0, 'revision', '', 0),
(317, 1, '2020-02-22 06:31:02', '2020-02-22 06:31:02', '', 'old-street', '', 'publish', 'closed', 'closed', '', 'old-street', '', '', '2020-02-22 06:31:23', '2020-02-22 06:31:23', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=317', 0, 'page', '', 0),
(318, 1, '2020-02-22 06:31:02', '2020-02-22 06:31:02', '', 'old-street', '', 'inherit', 'closed', 'closed', '', '317-revision-v1', '', '', '2020-02-22 06:31:02', '2020-02-22 06:31:02', '', 317, 'https://test.hachiweb.com/onelifefitness/2020/02/22/317-revision-v1/', 0, 'revision', '', 0),
(319, 1, '2020-02-22 06:33:31', '2020-02-22 06:33:31', '', 'victoria', '', 'publish', 'closed', 'closed', '', 'victoria', '', '', '2020-02-22 06:33:53', '2020-02-22 06:33:53', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=319', 0, 'page', '', 0),
(320, 1, '2020-02-22 06:33:31', '2020-02-22 06:33:31', '', 'victoria', '', 'inherit', 'closed', 'closed', '', '319-revision-v1', '', '', '2020-02-22 06:33:31', '2020-02-22 06:33:31', '', 319, 'https://test.hachiweb.com/onelifefitness/2020/02/22/319-revision-v1/', 0, 'revision', '', 0),
(321, 1, '2020-02-22 06:36:49', '2020-02-22 06:36:49', '', 'westfield-london', '', 'publish', 'closed', 'closed', '', 'westfield-london', '', '', '2020-02-22 06:37:11', '2020-02-22 06:37:11', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=321', 0, 'page', '', 0),
(322, 1, '2020-02-22 06:36:49', '2020-02-22 06:36:49', '', 'westfield-london', '', 'inherit', 'closed', 'closed', '', '321-revision-v1', '', '', '2020-02-22 06:36:49', '2020-02-22 06:36:49', '', 321, 'https://test.hachiweb.com/onelifefitness/2020/02/22/321-revision-v1/', 0, 'revision', '', 0),
(323, 1, '2020-02-22 06:40:20', '2020-02-22 06:40:20', '', 'westfield-stratford', '', 'publish', 'closed', 'closed', '', 'westfield-stratford', '', '', '2020-02-22 06:40:41', '2020-02-22 06:40:41', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=323', 0, 'page', '', 0),
(324, 1, '2020-02-22 06:40:20', '2020-02-22 06:40:20', '', 'westfield-stratford', '', 'inherit', 'closed', 'closed', '', '323-revision-v1', '', '', '2020-02-22 06:40:20', '2020-02-22 06:40:20', '', 323, 'https://test.hachiweb.com/onelifefitness/2020/02/22/323-revision-v1/', 0, 'revision', '', 0),
(325, 1, '2020-02-22 11:45:27', '2020-02-22 11:45:27', '', 'resources', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2020-02-22 11:45:52', '2020-02-22 11:45:52', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=325', 0, 'page', '', 0),
(326, 1, '2020-02-22 11:45:27', '2020-02-22 11:45:27', '', 'resources', '', 'inherit', 'closed', 'closed', '', '325-revision-v1', '', '', '2020-02-22 11:45:27', '2020-02-22 11:45:27', '', 325, 'https://test.hachiweb.com/onelifefitness/2020/02/22/325-revision-v1/', 0, 'revision', '', 0),
(327, 1, '2020-02-24 09:16:55', '2020-02-24 09:16:55', '', 'Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile', '', 'inherit', 'open', 'closed', '', 'cork-fitness-centre-one-life_fitness-gym-logo-mobile', '', '', '2020-02-24 09:16:55', '2020-02-24 09:16:55', '', 0, 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png', 0, 'attachment', 'image/png', 0),
(328, 1, '2020-02-24 09:17:38', '2020-02-24 09:17:38', '{\n    \"blogname\": {\n        \"value\": \"ONE L1FE FITNESS\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-24 09:16:59\"\n    },\n    \"gym-box::custom_logo\": {\n        \"value\": 329,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-24 09:17:38\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '73c0d51a-a7eb-4493-a9b4-7cbf96042b2a', '', '', '2020-02-24 09:17:38', '2020-02-24 09:17:38', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=328', 0, 'customize_changeset', '', 0),
(329, 1, '2020-02-24 09:17:18', '2020-02-24 09:17:18', 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/cropped-Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png', 'cropped-Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png', '', 'inherit', 'open', 'closed', '', 'cropped-cork-fitness-centre-one-life_fitness-gym-logo-mobile-png', '', '', '2020-02-24 09:17:18', '2020-02-24 09:17:18', '', 0, 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/cropped-Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png', 0, 'attachment', 'image/png', 0),
(330, 1, '2020-02-26 04:12:38', '2020-02-26 04:12:38', '', 'one-life-fitness', '', 'publish', 'closed', 'closed', '', 'one-life-fitness', '', '', '2020-02-26 04:13:20', '2020-02-26 04:13:20', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=330', 0, 'page', '', 0),
(331, 1, '2020-02-26 04:12:38', '2020-02-26 04:12:38', '', 'one-life-fitness', '', 'inherit', 'closed', 'closed', '', '330-revision-v1', '', '', '2020-02-26 04:12:38', '2020-02-26 04:12:38', '', 330, 'https://test.hachiweb.com/onelifefitness/2020/02/26/330-revision-v1/', 0, 'revision', '', 0),
(332, 1, '2020-02-26 04:21:18', '2020-02-26 04:21:18', '', 'riversedgefitness', '', 'publish', 'closed', 'closed', '', 'riversedgefitness', '', '', '2020-02-26 04:22:10', '2020-02-26 04:22:10', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=332', 0, 'page', '', 0),
(333, 1, '2020-02-26 04:21:18', '2020-02-26 04:21:18', '', 'riversedgefitness', '', 'inherit', 'closed', 'closed', '', '332-revision-v1', '', '', '2020-02-26 04:21:18', '2020-02-26 04:21:18', '', 332, 'https://test.hachiweb.com/onelifefitness/2020/02/26/332-revision-v1/', 0, 'revision', '', 0),
(335, 1, '2020-02-26 11:18:28', '2020-02-26 11:18:28', '', 'test-test', '', 'trash', 'closed', 'closed', '', 'test-test__trashed', '', '', '2020-02-26 11:20:14', '2020-02-26 11:20:14', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=335', 0, 'page', '', 0),
(336, 1, '2020-02-26 11:18:28', '2020-02-26 11:18:28', '', 'test-test', '', 'inherit', 'closed', 'closed', '', '335-revision-v1', '', '', '2020-02-26 11:18:28', '2020-02-26 11:18:28', '', 335, 'https://test.hachiweb.com/onelifefitness/2020/02/26/335-revision-v1/', 0, 'revision', '', 0),
(342, 1, '2020-02-27 05:59:44', '2020-02-27 05:59:44', '', 'blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2020-02-27 06:00:02', '2020-02-27 06:00:02', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=342', 0, 'page', '', 0),
(343, 1, '2020-02-27 05:59:44', '2020-02-27 05:59:44', '', 'blog', '', 'inherit', 'closed', 'closed', '', '342-revision-v1', '', '', '2020-02-27 05:59:44', '2020-02-27 05:59:44', '', 342, 'https://test.hachiweb.com/onelifefitness/2020/02/27/342-revision-v1/', 0, 'revision', '', 0),
(344, 1, '2020-02-27 06:55:28', '2020-02-27 06:55:28', '', 'add post', '', 'publish', 'open', 'open', '', 'add-post', '', '', '2020-02-27 06:55:28', '2020-02-27 06:55:28', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=344', 0, 'post', '', 0),
(345, 1, '2020-02-27 06:55:28', '2020-02-27 06:55:28', '', 'add post', '', 'inherit', 'closed', 'closed', '', '344-revision-v1', '', '', '2020-02-27 06:55:28', '2020-02-27 06:55:28', '', 344, 'https://test.hachiweb.com/onelifefitness/2020/02/27/344-revision-v1/', 0, 'revision', '', 0),
(348, 1, '2020-02-27 07:11:10', '2020-02-27 07:11:10', '', 'hello onelifefitness', '', 'publish', 'open', 'open', '', 'hello-onelifefitness', '', '', '2020-02-27 07:11:10', '2020-02-27 07:11:10', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=348', 0, 'post', '', 0),
(349, 1, '2020-02-27 07:11:10', '2020-02-27 07:11:10', '', 'hello onelifefitness', '', 'inherit', 'closed', 'closed', '', '348-revision-v1', '', '', '2020-02-27 07:11:10', '2020-02-27 07:11:10', '', 348, 'https://test.hachiweb.com/onelifefitness/2020/02/27/348-revision-v1/', 0, 'revision', '', 0),
(350, 1, '2020-02-27 07:11:45', '2020-02-27 07:11:45', '', 'hello onelifefitness', '', 'inherit', 'closed', 'closed', '', '348-autosave-v1', '', '', '2020-02-27 07:11:45', '2020-02-27 07:11:45', '', 348, 'https://test.hachiweb.com/onelifefitness/2020/02/27/348-autosave-v1/', 0, 'revision', '', 0),
(353, 1, '2020-02-27 08:40:18', '2020-02-27 08:40:18', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'hello system', '', 'publish', 'open', 'open', '', 'hello-system', '', '', '2020-02-27 08:40:18', '2020-02-27 08:40:18', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=353', 0, 'post', '', 0),
(354, 1, '2020-02-27 08:40:06', '2020-02-27 08:40:06', '', 'Andreea-Yoga-Tile', '', 'inherit', 'open', 'closed', '', 'andreea-yoga-tile', '', '', '2020-02-27 08:40:06', '2020-02-27 08:40:06', '', 353, 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png', 0, 'attachment', 'image/png', 0),
(355, 1, '2020-02-27 08:40:18', '2020-02-27 08:40:18', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'hello system', '', 'inherit', 'closed', 'closed', '', '353-revision-v1', '', '', '2020-02-27 08:40:18', '2020-02-27 08:40:18', '', 353, 'https://test.hachiweb.com/onelifefitness/2020/02/27/353-revision-v1/', 0, 'revision', '', 0),
(356, 1, '2020-02-27 08:44:10', '2020-02-27 08:44:10', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'hello system', '', 'inherit', 'closed', 'closed', '', '353-autosave-v1', '', '', '2020-02-27 08:44:10', '2020-02-27 08:44:10', '', 353, 'https://test.hachiweb.com/onelifefitness/2020/02/27/353-autosave-v1/', 0, 'revision', '', 0),
(357, 1, '2020-02-27 08:51:13', '2020-02-27 08:51:13', '{\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar-1\": [],\n            \"sidebar-2\": [\n                \"archives-2\",\n                \"categories-2\",\n                \"meta-2\"\n            ]\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-02-27 08:51:13\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '54897762-043c-4e1a-972f-1b57cb695648', '', '', '2020-02-27 08:51:13', '2020-02-27 08:51:13', '', 0, 'https://test.hachiweb.com/onelifefitness/2020/02/27/54897762-043c-4e1a-972f-1b57cb695648/', 0, 'customize_changeset', '', 0),
(359, 1, '2020-02-27 12:19:19', '2020-02-27 12:19:19', '<!-- wp:image {\"id\":361,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png\" alt=\"\" class=\"wp-image-361\"/></figure>\n<!-- /wp:image -->', 'sgdfvsdfv', '', 'publish', 'open', 'open', '', 'sgdfvsdfv', '', '', '2020-02-27 12:19:47', '2020-02-27 12:19:47', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=359', 0, 'post', '', 0),
(360, 1, '2020-02-27 12:19:19', '2020-02-27 12:19:19', '', 'sgdfvsdfv', '', 'inherit', 'closed', 'closed', '', '359-revision-v1', '', '', '2020-02-27 12:19:19', '2020-02-27 12:19:19', '', 359, 'https://test.hachiweb.com/onelifefitness/2020/02/27/359-revision-v1/', 0, 'revision', '', 0),
(361, 1, '2020-02-27 12:19:42', '2020-02-27 12:19:42', '', 'Andreea-Yoga-Tile', '', 'inherit', 'open', 'closed', '', 'andreea-yoga-tile-2', '', '', '2020-02-27 12:19:42', '2020-02-27 12:19:42', '', 359, 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png', 0, 'attachment', 'image/png', 0),
(362, 1, '2020-02-27 12:19:47', '2020-02-27 12:19:47', '<!-- wp:image {\"id\":361,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png\" alt=\"\" class=\"wp-image-361\"/></figure>\n<!-- /wp:image -->', 'sgdfvsdfv', '', 'inherit', 'closed', 'closed', '', '359-revision-v1', '', '', '2020-02-27 12:19:47', '2020-02-27 12:19:47', '', 359, 'https://test.hachiweb.com/onelifefitness/2020/02/27/359-revision-v1/', 0, 'revision', '', 0),
(364, 1, '2020-02-29 10:58:02', '2020-02-29 10:58:02', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'one life fitness', '', 'publish', 'open', 'open', '', 'one-life-fitness', '', '', '2020-02-29 10:58:02', '2020-02-29 10:58:02', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=364', 0, 'post', '', 0),
(365, 1, '2020-02-29 10:58:02', '2020-02-29 10:58:02', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'one life fitness', '', 'inherit', 'closed', 'closed', '', '364-revision-v1', '', '', '2020-02-29 10:58:02', '2020-02-29 10:58:02', '', 364, 'https://test.hachiweb.com/onelifefitness/2020/02/29/364-revision-v1/', 0, 'revision', '', 0),
(366, 1, '2020-02-29 10:59:50', '2020-02-29 10:59:50', '', 'gym class', '', 'publish', 'open', 'open', '', 'gym-class', '', '', '2020-02-29 10:59:50', '2020-02-29 10:59:50', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=366', 0, 'post', '', 0),
(367, 1, '2020-02-29 10:59:50', '2020-02-29 10:59:50', '', 'gym class', '', 'inherit', 'closed', 'closed', '', '366-revision-v1', '', '', '2020-02-29 10:59:50', '2020-02-29 10:59:50', '', 366, 'https://test.hachiweb.com/onelifefitness/366-revision-v1/', 0, 'revision', '', 0),
(368, 1, '2020-02-29 11:01:39', '2020-02-29 11:01:39', '', 'gymbox', '', 'publish', 'open', 'open', '', 'blog-gymbox', '', '', '2020-02-29 11:03:25', '2020-02-29 11:03:25', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=368', 0, 'post', '', 0),
(369, 1, '2020-02-29 11:01:39', '2020-02-29 11:01:39', '', 'gymbox', '', 'inherit', 'closed', 'closed', '', '368-revision-v1', '', '', '2020-02-29 11:01:39', '2020-02-29 11:01:39', '', 368, 'https://test.hachiweb.com/onelifefitness/368-revision-v1/', 0, 'revision', '', 0),
(370, 1, '2020-02-29 11:06:53', '2020-02-29 11:06:53', '', 'gym-example', '', 'publish', 'open', 'open', '', 'gym-example', '', '', '2020-02-29 11:07:56', '2020-02-29 11:07:56', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=370', 0, 'post', '', 0),
(371, 1, '2020-02-29 11:06:53', '2020-02-29 11:06:53', '', 'gym-example', '', 'inherit', 'closed', 'closed', '', '370-revision-v1', '', '', '2020-02-29 11:06:53', '2020-02-29 11:06:53', '', 370, 'https://test.hachiweb.com/onelifefitness/blog/370-revision-v1/', 0, 'revision', '', 0),
(372, 1, '2020-02-29 11:14:21', '2020-02-29 11:14:21', '', 'new post', '', 'publish', 'open', 'open', '', 'new-post', '', '', '2020-02-29 11:14:21', '2020-02-29 11:14:21', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=372', 0, 'post', '', 0),
(373, 1, '2020-02-29 11:14:21', '2020-02-29 11:14:21', '', 'new post', '', 'inherit', 'closed', 'closed', '', '372-revision-v1', '', '', '2020-02-29 11:14:21', '2020-02-29 11:14:21', '', 372, 'https://test.hachiweb.com/onelifefitness/372-revision-v1/', 0, 'revision', '', 0),
(374, 1, '2020-02-29 11:15:21', '2020-02-29 11:15:21', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'add post', '', 'publish', 'open', 'open', '', 'add-post-2', '', '', '2020-02-29 12:01:12', '2020-02-29 12:01:12', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=374', 0, 'post', '', 0),
(375, 1, '2020-02-29 11:15:21', '2020-02-29 11:15:21', '', 'add post', '', 'inherit', 'closed', 'closed', '', '374-revision-v1', '', '', '2020-02-29 11:15:21', '2020-02-29 11:15:21', '', 374, 'https://test.hachiweb.com/onelifefitness/blog/374-revision-v1/', 0, 'revision', '', 0),
(376, 1, '2020-02-29 11:44:53', '2020-02-29 11:44:53', '', 'post-details', '', 'publish', 'closed', 'closed', '', 'post-details', '', '', '2020-02-29 11:45:21', '2020-02-29 11:45:21', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=376', 0, 'page', '', 0),
(377, 1, '2020-02-29 11:44:53', '2020-02-29 11:44:53', '', 'post-details', '', 'inherit', 'closed', 'closed', '', '376-revision-v1', '', '', '2020-02-29 11:44:53', '2020-02-29 11:44:53', '', 376, 'https://test.hachiweb.com/onelifefitness/blog/376-revision-v1/', 0, 'revision', '', 0),
(378, 1, '2020-02-29 12:01:12', '2020-02-29 12:01:12', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->', 'add post', '', 'inherit', 'closed', 'closed', '', '374-revision-v1', '', '', '2020-02-29 12:01:12', '2020-02-29 12:01:12', '', 374, 'https://test.hachiweb.com/onelifefitness/blog/374-revision-v1/', 0, 'revision', '', 0),
(379, 1, '2020-02-29 12:06:12', '2020-02-29 12:06:12', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-02-29 12:06:12', '2020-02-29 12:06:12', '', 1, 'https://test.hachiweb.com/onelifefitness/blog/1-revision-v1/', 0, 'revision', '', 0),
(380, 1, '2020-03-02 11:58:11', '2020-03-02 11:58:11', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book. </p>\n<!-- /wp:paragraph -->', 'start post', '', 'publish', 'open', 'open', '', 'start-post', '', '', '2020-03-02 11:58:11', '2020-03-02 11:58:11', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=380', 0, 'post', '', 0),
(381, 1, '2020-03-02 11:58:11', '2020-03-02 11:58:11', '<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book. </p>\n<!-- /wp:paragraph -->', 'start post', '', 'inherit', 'closed', 'closed', '', '380-revision-v1', '', '', '2020-03-02 11:58:11', '2020-03-02 11:58:11', '', 380, 'https://test.hachiweb.com/onelifefitness/blog/380-revision-v1/', 0, 'revision', '', 0),
(382, 1, '2020-03-03 11:24:43', '2020-03-03 11:24:43', '<!-- wp:image {\"id\":383,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa-1024x683.jpg\" alt=\"\" class=\"wp-image-383\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book. </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book. </figcaption></figure>\n<!-- /wp:image -->', 'today new post', '', 'publish', 'open', 'open', '', 'today-new-post', '', '', '2020-03-03 11:24:43', '2020-03-03 11:24:43', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=382', 0, 'post', '', 0),
(383, 1, '2020-03-03 11:23:50', '2020-03-03 11:23:50', '', 'Vinyasa', '', 'inherit', 'open', 'closed', '', 'vinyasa', '', '', '2020-03-03 11:23:50', '2020-03-03 11:23:50', '', 382, 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa.jpg', 0, 'attachment', 'image/jpeg', 0),
(384, 1, '2020-03-03 11:24:43', '2020-03-03 11:24:43', '<!-- wp:image {\"id\":383,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa-1024x683.jpg\" alt=\"\" class=\"wp-image-383\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book. </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":354,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile.png\" alt=\"\" class=\"wp-image-354\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book. </figcaption></figure>\n<!-- /wp:image -->', 'today new post', '', 'inherit', 'closed', 'closed', '', '382-revision-v1', '', '', '2020-03-03 11:24:43', '2020-03-03 11:24:43', '', 382, 'https://test.hachiweb.com/onelifefitness/blog?post=/382-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(385, 1, '2020-03-03 11:53:18', '2020-03-03 11:53:18', '<!-- wp:image {\"id\":386,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer-1024x683.jpeg\" alt=\"\" class=\"wp-image-386\"/><figcaption>Fitness woman doing sit-ups exercises with male instructor in gym. Female doing abs workout with  personal trainer holding her legs.  <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":383,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa-1024x683.jpg\" alt=\"\" class=\"wp-image-383\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:gallery {\"ids\":[]} -->\n<figure class=\"wp-block-gallery columns-0 is-cropped\"><ul class=\"blocks-gallery-grid\"></ul></figure>\n<!-- /wp:gallery -->\n\n<!-- wp:image {\"id\":361,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png\" alt=\"\" class=\"wp-image-361\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'add new post today', '', 'publish', 'open', 'open', '', 'add-new-post-today', '', '', '2020-03-03 11:56:02', '2020-03-03 11:56:02', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=385', 0, 'post', '', 0),
(386, 1, '2020-03-03 11:52:01', '2020-03-03 11:52:01', '', 'Woman doing sit-ups exercises with male instructor', 'Fitness woman doing sit-ups exercises with male instructor in gym. Female doing abs workout with  personal trainer holding her legs.', 'inherit', 'open', 'closed', '', 'woman-doing-sit-ups-exercises-with-male-instructor', '', '', '2020-03-03 11:52:01', '2020-03-03 11:52:01', '', 385, 'https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer.jpeg', 0, 'attachment', 'image/jpeg', 0),
(387, 1, '2020-03-03 11:53:18', '2020-03-03 11:53:18', '<!-- wp:gallery {\"ids\":[386]} -->\n<figure class=\"wp-block-gallery columns-1 is-cropped\"><ul class=\"blocks-gallery-grid\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer-1024x683.jpeg\" alt=\"\" data-id=\"386\" data-full-url=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer-scaled.jpeg\" data-link=\"https://test.hachiweb.com/onelifefitness/?attachment_id=386\" class=\"wp-image-386\"/><figcaption class=\"blocks-gallery-item__caption\">Fitness woman doing sit-ups exercises with male instructor in gym. Female doing abs workout with  personal trainer holding her legs.</figcaption></figure></li></ul><figcaption class=\"blocks-gallery-caption\"> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:gallery -->\n\n<!-- wp:image {\"id\":383,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa-1024x683.jpg\" alt=\"\" class=\"wp-image-383\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":361,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png\" alt=\"\" class=\"wp-image-361\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'add new post today', '', 'inherit', 'closed', 'closed', '', '385-revision-v1', '', '', '2020-03-03 11:53:18', '2020-03-03 11:53:18', '', 385, 'https://test.hachiweb.com/onelifefitness/blog?post=/385-revision-v1/', 0, 'revision', '', 0),
(388, 1, '2020-03-03 11:54:32', '2020-03-03 11:54:32', '<!-- wp:gallery {\"ids\":[386]} -->\n<figure class=\"wp-block-gallery columns-1 is-cropped\"><ul class=\"blocks-gallery-grid\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer-1024x683.jpeg\" alt=\"\" data-id=\"386\" data-full-url=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer-scaled.jpeg\" data-link=\"https://test.hachiweb.com/onelifefitness/?attachment_id=386\" class=\"wp-image-386\"/></figure></li></ul><figcaption class=\"blocks-gallery-caption\"> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:gallery -->\n\n<!-- wp:image {\"id\":383,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa-1024x683.jpg\" alt=\"\" class=\"wp-image-383\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":361,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png\" alt=\"\" class=\"wp-image-361\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'add new post today', '', 'inherit', 'closed', 'closed', '', '385-revision-v1', '', '', '2020-03-03 11:54:32', '2020-03-03 11:54:32', '', 385, 'https://test.hachiweb.com/onelifefitness/blog?post=/385-revision-v1/', 0, 'revision', '', 0),
(389, 1, '2020-03-03 11:56:02', '2020-03-03 11:56:02', '<!-- wp:image {\"id\":386,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/One-Life-Personal-Trainer-1024x683.jpeg\" alt=\"\" class=\"wp-image-386\"/><figcaption>Fitness woman doing sit-ups exercises with male instructor in gym. Female doing abs workout with  personal trainer holding her legs.  <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":383,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/03/Vinyasa-1024x683.jpg\" alt=\"\" class=\"wp-image-383\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:gallery {\"ids\":[]} -->\n<figure class=\"wp-block-gallery columns-0 is-cropped\"><ul class=\"blocks-gallery-grid\"></ul></figure>\n<!-- /wp:gallery -->\n\n<!-- wp:image {\"id\":361,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"https://test.hachiweb.com/onelifefitness/wp-content/uploads/2020/02/Andreea-Yoga-Tile-1.png\" alt=\"\" class=\"wp-image-361\"/><figcaption> <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.   <em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum et Malorum for use in a type specimen book.  </figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'add new post today', '', 'inherit', 'closed', 'closed', '', '385-revision-v1', '', '', '2020-03-03 11:56:02', '2020-03-03 11:56:02', '', 385, 'https://test.hachiweb.com/onelifefitness/blog?post=/385-revision-v1/', 0, 'revision', '', 0),
(390, 1, '2020-03-05 12:14:09', '2020-03-05 12:14:09', '', 'box', '', 'inherit', 'closed', 'closed', '', '94-revision-v1', '', '', '2020-03-05 12:14:09', '2020-03-05 12:14:09', '', 94, 'https://test.hachiweb.com/onelifefitness/blog?post=/94-revision-v1/', 0, 'revision', '', 0),
(391, 1, '2020-03-05 12:18:34', '2020-03-05 12:18:34', '', 'lift', '', 'inherit', 'closed', 'closed', '', '92-revision-v1', '', '', '2020-03-05 12:18:34', '2020-03-05 12:18:34', '', 92, 'https://test.hachiweb.com/onelifefitness/blog?post=/92-revision-v1/', 0, 'revision', '', 0),
(392, 1, '2020-03-05 12:19:16', '2020-03-05 12:19:16', '', 'ride', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2020-03-05 12:19:16', '2020-03-05 12:19:16', '', 96, 'https://test.hachiweb.com/onelifefitness/blog?post=/96-revision-v1/', 0, 'revision', '', 0),
(393, 1, '2020-03-05 12:19:53', '2020-03-05 12:19:53', '', 'olf45', '', 'inherit', 'closed', 'closed', '', '112-revision-v1', '', '', '2020-03-05 12:19:53', '2020-03-05 12:19:53', '', 112, 'https://test.hachiweb.com/onelifefitness/blog?post=/112-revision-v1/', 0, 'revision', '', 0),
(394, 1, '2020-03-05 12:20:20', '2020-03-05 12:20:20', '', 'strong', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2020-03-05 12:20:20', '2020-03-05 12:20:20', '', 114, 'https://test.hachiweb.com/onelifefitness/blog?post=/114-revision-v1/', 0, 'revision', '', 0),
(395, 1, '2020-03-05 12:21:17', '2020-03-05 12:21:17', '', 'boxing-circuit', '', 'inherit', 'closed', 'closed', '', '117-revision-v1', '', '', '2020-03-05 12:21:17', '2020-03-05 12:21:17', '', 117, 'https://test.hachiweb.com/onelifefitness/blog?post=/117-revision-v1/', 0, 'revision', '', 0),
(396, 1, '2020-03-05 12:22:27', '2020-03-05 12:22:27', '', 'legs-bums-tums', '', 'inherit', 'closed', 'closed', '', '161-revision-v1', '', '', '2020-03-05 12:22:27', '2020-03-05 12:22:27', '', 161, 'https://test.hachiweb.com/onelifefitness/blog?post=/161-revision-v1/', 0, 'revision', '', 0),
(397, 1, '2020-03-05 12:23:34', '2020-03-05 12:23:34', '', 'hips-bums-tums', '', 'inherit', 'closed', 'closed', '', '163-revision-v1', '', '', '2020-03-05 12:23:34', '2020-03-05 12:23:34', '', 163, 'https://test.hachiweb.com/onelifefitness/blog?post=/163-revision-v1/', 0, 'revision', '', 0),
(398, 1, '2020-03-05 12:24:53', '2020-03-05 12:24:53', '', 'booty-blast', '', 'inherit', 'closed', 'closed', '', '165-revision-v1', '', '', '2020-03-05 12:24:53', '2020-03-05 12:24:53', '', 165, 'https://test.hachiweb.com/onelifefitness/blog?post=/165-revision-v1/', 0, 'revision', '', 0),
(399, 1, '2020-03-05 12:25:41', '2020-03-05 12:25:41', '', 'functional-circuit', '', 'inherit', 'closed', 'closed', '', '172-revision-v1', '', '', '2020-03-05 12:25:41', '2020-03-05 12:25:41', '', 172, 'https://test.hachiweb.com/onelifefitness/blog?post=/172-revision-v1/', 0, 'revision', '', 0),
(400, 1, '2020-03-05 12:26:20', '2020-03-05 12:26:20', '', 'tabata', '', 'inherit', 'closed', 'closed', '', '174-revision-v1', '', '', '2020-03-05 12:26:20', '2020-03-05 12:26:20', '', 174, 'https://test.hachiweb.com/onelifefitness/blog?post=/174-revision-v1/', 0, 'revision', '', 0),
(401, 1, '2020-03-05 12:26:54', '2020-03-05 12:26:54', '', 'hiit', '', 'inherit', 'closed', 'closed', '', '176-revision-v1', '', '', '2020-03-05 12:26:54', '2020-03-05 12:26:54', '', 176, 'https://test.hachiweb.com/onelifefitness/blog?post=/176-revision-v1/', 0, 'revision', '', 0),
(402, 1, '2020-03-05 12:27:36', '2020-03-05 12:27:36', '', 'body-tone', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-03-05 12:27:36', '2020-03-05 12:27:36', '', 208, 'https://test.hachiweb.com/onelifefitness/blog?post=/208-revision-v1/', 0, 'revision', '', 0),
(403, 1, '2020-03-05 12:28:29', '2020-03-05 12:28:29', '', 'abs-circuit', '', 'inherit', 'closed', 'closed', '', '210-revision-v1', '', '', '2020-03-05 12:28:29', '2020-03-05 12:28:29', '', 210, 'https://test.hachiweb.com/onelifefitness/blog?post=/210-revision-v1/', 0, 'revision', '', 0),
(404, 1, '2020-03-05 12:29:18', '2020-03-05 12:29:18', '', 'box-abs-circuit', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2020-03-05 12:29:18', '2020-03-05 12:29:18', '', 213, 'https://test.hachiweb.com/onelifefitness/blog?post=/213-revision-v1/', 0, 'revision', '', 0),
(405, 1, '2020-03-05 12:30:00', '2020-03-05 12:30:00', '', 'box-xpress', '', 'inherit', 'closed', 'closed', '', '238-revision-v1', '', '', '2020-03-05 12:30:00', '2020-03-05 12:30:00', '', 238, 'https://test.hachiweb.com/onelifefitness/blog?post=/238-revision-v1/', 0, 'revision', '', 0),
(406, 1, '2020-03-05 12:30:55', '2020-03-05 12:30:55', '', 'ride-xpress', '', 'inherit', 'closed', 'closed', '', '240-revision-v1', '', '', '2020-03-05 12:30:55', '2020-03-05 12:30:55', '', 240, 'https://test.hachiweb.com/onelifefitness/blog?post=/240-revision-v1/', 0, 'revision', '', 0),
(407, 1, '2020-03-05 12:31:34', '2020-03-05 12:31:34', '', 'kid-boxing', '', 'inherit', 'closed', 'closed', '', '242-revision-v1', '', '', '2020-03-05 12:31:34', '2020-03-05 12:31:34', '', 242, 'https://test.hachiweb.com/onelifefitness/blog?post=/242-revision-v1/', 0, 'revision', '', 0),
(408, 1, '2020-03-05 12:32:20', '2020-03-05 12:32:20', '', 'step-lift', '', 'inherit', 'closed', 'closed', '', '252-revision-v1', '', '', '2020-03-05 12:32:20', '2020-03-05 12:32:20', '', 252, 'https://test.hachiweb.com/onelifefitness/blog?post=/252-revision-v1/', 0, 'revision', '', 0),
(409, 1, '2020-03-05 12:33:06', '2020-03-05 12:33:06', '', 'abs-core', '', 'inherit', 'closed', 'closed', '', '254-revision-v1', '', '', '2020-03-05 12:33:06', '2020-03-05 12:33:06', '', 254, 'https://test.hachiweb.com/onelifefitness/blog?post=/254-revision-v1/', 0, 'revision', '', 0),
(410, 1, '2020-03-05 12:33:41', '2020-03-05 12:33:41', '', 'body-bootcamp', '', 'inherit', 'closed', 'closed', '', '256-revision-v1', '', '', '2020-03-05 12:33:41', '2020-03-05 12:33:41', '', 256, 'https://test.hachiweb.com/onelifefitness/blog?post=/256-revision-v1/', 0, 'revision', '', 0),
(411, 1, '2020-03-05 12:34:28', '2020-03-05 12:34:28', '', 'zumba', '', 'inherit', 'closed', 'closed', '', '266-revision-v1', '', '', '2020-03-05 12:34:28', '2020-03-05 12:34:28', '', 266, 'https://test.hachiweb.com/onelifefitness/blog?post=/266-revision-v1/', 0, 'revision', '', 0),
(412, 1, '2020-03-05 12:34:54', '2020-03-05 12:34:54', '', 'pilates', '', 'inherit', 'closed', 'closed', '', '178-revision-v1', '', '', '2020-03-05 12:34:54', '2020-03-05 12:34:54', '', 178, 'https://test.hachiweb.com/onelifefitness/blog?post=/178-revision-v1/', 0, 'revision', '', 0),
(413, 1, '2020-03-05 12:35:18', '2020-03-05 12:35:18', '', 'yoga', '', 'inherit', 'closed', 'closed', '', '268-revision-v1', '', '', '2020-03-05 12:35:18', '2020-03-05 12:35:18', '', 268, 'https://test.hachiweb.com/onelifefitness/blog?post=/268-revision-v1/', 0, 'revision', '', 0),
(414, 1, '2020-03-06 10:44:42', '2020-03-06 10:44:42', '', 'contact-us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2020-03-06 10:45:03', '2020-03-06 10:45:03', '', 0, 'https://test.hachiweb.com/onelifefitness/?page_id=414', 0, 'page', '', 0),
(415, 1, '2020-03-06 10:44:42', '2020-03-06 10:44:42', '', 'contact-us', '', 'inherit', 'closed', 'closed', '', '414-revision-v1', '', '', '2020-03-06 10:44:42', '2020-03-06 10:44:42', '', 414, 'https://test.hachiweb.com/onelifefitness/blog?post=/414-revision-v1/', 0, 'revision', '', 0),
(424, 1, '2020-03-07 14:16:08', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-03-07 14:16:08', '0000-00-00 00:00:00', '', 0, 'https://test.hachiweb.com/onelifefitness/?p=424', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'about us', 'about-us', 0),
(3, 'home', 'home', 0),
(4, 'blog', 'blog', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(77, 1, 0),
(170, 1, 0),
(344, 1, 0),
(348, 1, 0),
(353, 1, 0),
(359, 1, 0),
(364, 1, 0),
(366, 1, 0),
(368, 4, 0),
(370, 1, 0),
(370, 4, 0),
(372, 1, 0),
(374, 1, 0),
(380, 1, 0),
(382, 1, 0),
(385, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 15),
(2, 2, 'nav_menu', '', 0, 0),
(3, 3, 'nav_menu', '', 0, 0),
(4, 4, 'category', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_tour`
--

CREATE TABLE `wp_tour` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text DEFAULT NULL,
  `phone` text NOT NULL,
  `club` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_tour`
--

INSERT INTO `wp_tour` (`id`, `name`, `email`, `phone`, `club`, `created_at`, `updated_at`) VALUES
(1, 'Nell Glover', 'xyvokiwiv@mailinator.net', '3933772908', 'Covent Garden', '2020-02-19 10:54:44', '2020-02-19 10:54:44'),
(4, 'Daryl Chan', 'gexykyfo@mailinator.com', '14833626809', 'Covent Garden', '2020-02-19 12:03:24', '2020-02-19 12:03:24'),
(5, 'Kai NorrisYJHT', 'myguquk@mailinator.net', '09557820005', 'Ealing', '2020-02-19 12:07:14', '2020-02-19 12:07:14'),
(6, 'Tatiana Watkinswef', 'dhjoiujoiu@gmail.com', '09557820005', 'Sullivans Quay (Riversedge)', '2020-02-28 10:03:26', '2020-02-28 10:03:26'),
(7, 'Talon Hopkins', 'myzuloteke@mailinator.com', '13142575985', 'Sullivans Quay (Riversedge)', '2020-03-06 10:21:29', '2020-03-06 10:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(0, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"139.167.230.0\";}'),
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '424'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(20, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'session_tokens', 'a:6:{s:64:\"681795b784ddb2fc6df575ce5766f9e4a9494369fd2f3d7425f9f128adcf916a\";a:4:{s:10:\"expiration\";i:1583761031;s:2:\"ip\";s:13:\"27.63.185.132\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\";s:5:\"login\";i:1583588231;}s:64:\"ea8a188c244435b253e4be554739a345d860e2b27f610f251455d20bf09d9a0d\";a:4:{s:10:\"expiration\";i:1583761088;s:2:\"ip\";s:13:\"27.63.185.132\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\";s:5:\"login\";i:1583588288;}s:64:\"f35aeb1f0ee1f2703d6f0ef5923062a5306cc1026cc199e0c4e5eecd78dbf0e0\";a:4:{s:10:\"expiration\";i:1583762159;s:2:\"ip\";s:13:\"27.63.185.132\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\";s:5:\"login\";i:1583589359;}s:64:\"9248d5f1f876b19a3e03dfce2468a4c13ec32ec6b1b0d7d8892b268a18e350af\";a:4:{s:10:\"expiration\";i:1583767738;s:2:\"ip\";s:13:\"27.63.185.132\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\";s:5:\"login\";i:1583594938;}s:64:\"121e7a139dd6bca0943c589242e28ec8beb35f67c32360c091dde754623fe2e7\";a:4:{s:10:\"expiration\";i:1583767885;s:2:\"ip\";s:13:\"27.63.185.132\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Linux; Android 9; RMX1831) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.119 Mobile Safari/537.36\";s:5:\"login\";i:1583595085;}s:64:\"57cb01f6332ee630083bb1a3b86a3e965476139229837deb64af03e32829977c\";a:4:{s:10:\"expiration\";i:1583823075;s:2:\"ip\";s:14:\"139.167.230.72\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\";s:5:\"login\";i:1583650275;}}'),
(23, 1, 'wp_user-settings', 'libraryContent=upload'),
(24, 1, 'wp_user-settings-time', '1583591195');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BM8vm8.yLAmDQOp/0uo/n0YNxxAxTk0', 'admin', 'testhachiweb@gmail.com', '', '2020-02-04 04:12:56', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_contact`
--
ALTER TABLE `wp_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_freepass`
--
ALTER TABLE `wp_freepass`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_membership`
--
ALTER TABLE `wp_membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_tour`
--
ALTER TABLE `wp_tour`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_contact`
--
ALTER TABLE `wp_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_freepass`
--
ALTER TABLE `wp_freepass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `wp_membership`
--
ALTER TABLE `wp_membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;

--
-- AUTO_INCREMENT for table `wp_tour`
--
ALTER TABLE `wp_tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
