<?php /* Template Name: badass */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
        <h2>Box xpress</h2>
        <div class="_1-4gF4V">
            <p>The name ain’t just for fun. You’re about to squeeze, squat, lift and burn your way through a class that
                whoops your ass to give you glutes of steel and thighs of iron. Get through this on the regular and it’s
                not just your butt that feels hard as nails.</p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                <span class="Nfa775m">Onelifefitness Camden quay</span>
                <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                    </div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
        <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
            <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1308935">12:00 Sunday 16th (45 minutes)</option>
                <option value="1322357" disabled="">18:30 Wednesday 19th (45 minutes) - Bookable 7am day before</option>
                <option value="1307664" disabled="">18:30 Friday 21st (30 minutes) - Bookable 7am day before</option>
            </select><a
                href="https://indma05.clubwise.com/onelifefitness/index.html"
                target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hattie_Grover_Extra_Option_2.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hattie</h3>
                    <div>
                        <p>Hattie's training began at the age
                            of 3 when she started taking ballet lessons. As her
                            love for dance blossomed she began performing, and immersed herself in a variety
                            of different dance styles specialising in contemporary and breaking. Whilst at
                            onelifefitness she has broadened her knowledge in the Aerial Arts and is fully committed to sharing
                            her love for everything aerial, acrobatic and upside
                            down!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Geraldine_Gishen_Extra_Option_1.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Geraldine</h3>
                    <div>
                        <p>Geraldine’s classes are all about hard work and self belief. You can get through your
                            set...in fact you’re going to smash your set! Be prepared to sweat, learn new skills and
                            have fun!&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Biannca_Pegrume.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Bianca</h3>
                    <div>
                        <p>Whilst studying Zoology at university, Bianca admits she
                            missed more than one lecture to fit in a gym session. Having discovered her
                            passion for fitness, she went on to become a qualified studio instructor and
                            PT, and now loves every minute teaching at onelifefitness.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jai_Morrison.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jai</h3>
                    <div>
                        <p> Performing and dancing are Jai's passion, so helping others work on their own health and
                            fitness is a huge motivation for why she continues to work as an instructor. Jai loves
                            seeing members push past their own boundaries and leave glowing knowing they have given it
                            their all!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Matteo_Cruciani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Matteo</h3>
                    <div>
                        <p>A native Italian, Matteo was born and raised in the city of Rome.&nbsp;He grew up with a huge
                            passion for fitness, dance and movement.&nbsp;He is an&nbsp;enthusiastic and charismatic
                            person who brings a positive attitude to every situation!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                    <div>
                        <p>Through a combination of tough-love and motivation, Stevie will push you to your limits with
                            high-energy metabolic conditioning workouts that will leave you in a pool of sweat and a big
                            smile on your face.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Andy_Apollo.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Andrew</h3>
                    <div>
                        <p>Andy is a self-professed fitness addict who’s always after
                            the next endorphin hit. Training with Andy will push you to be the best version
                            of yourself and make sure you’ve got a smile on your face when you get there. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Yvette_Carter_Extra_Option_4.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Yvette C</h3>
                    <div>
                        <p>Having earned a degree in theatre dance from the London
                            Studio Centre, Yvette went on to become a qualified PT and is now a studio
                            instructor at onelifefitness. Yvette’s philosophy is that training should be fun with
                            hardwork thrown in.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lutha_Nzinga.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Luther</h3>
                    <div>
                        <p>A PT and instructor with a degreee in Sports Therapy, and a
                            background in professional powerlifting, Luther teaches Frame Fitness and Drill
                            Sergeant at onelifefitness. His focus on technique and lifting heavy will help you
                            improve both your mental and physical strength.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lawrence_Cain.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Lawrence</h3>
                    <div>
                        <p>Lawrence focuses on calisthenics, strength training and body building in his training, and
                            teaches Frame Fitness, Ripped &amp; Stripped, Drill Sergeant and Kettlebells. Lawrence’s
                            no-nonsense, hardcore attitude is sure to push you beyond your limits.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_James_Dawkins.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">James - Master Trainer</h3>
                    <div>
                        <p>London based House DJ and our Look Better Naked Master Trainer, James is passionate about
                            leading a team that will help you not only look great naked, but also feel great naked. Hop
                            into any of his classes and you'll quickly learn why we're the antidote to boring gyms! </p>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONEE1BF360F2569.JPG');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>