<?php /* Template Name: career */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>



<div>
    <div class="_1V8Vwl1 _3IPJrBv _2rbE6TC _2v5bHvx _3HiqiFI"><svg class="_33ij8ie pRZSkbx _3x6v3-y"
            preserveAspectRatio="xMidYMid" viewBox="0 0 550 110"><text vector-effect="non-scaling-stroke"
                shape-rendering="crispEdges" class="_25Sdx7V _31coI1f" x="0" y="90">Careers</text></svg>
    </div>
    <div class="_17ytWhG _2rbE6TC _2v5bHvx _3x6v3-y">
        <div class="zb0xP4p _2rbE6TC _2v5bHvx VBD7Ow3">
            <div class="_1a7xbxt _1G7RCdW _2v5bHvx VBD7Ow3">
                <div class="_2aDA4on">
                    <h3>CAREERS AT onelifefitness</h3>
                    <div>
                        <p>We want to change the way that people in the fitness industry work. Just as we didn’t
                            want to be part of an industry that served up bland, beige and boring, we also didn’t
                            want to be part of something where staff thought no-one cared about their careers.
                        </p>
                        <p>Everyone matters at onelifefitness. Today we can only continue to be a provocative,
                            ground-breaking, market leading brand with a team that’s equally proud of what we do, as
                            committed and passionate as we’ve been since that first sweaty day we opened our doors.
                            That’s why we’ve made onelifefitness the place where you can build a truly rewarding
                            career, with all the training, support and opportunities you’ll need to take you where
                            you want to go.</p>
                    </div>
                </div>
            </div>
            <div class="_3rZ3KIB _1G7RCdW _2v5bHvx">
                <div style="transform: translateY(200px);">
                    <div class="_2gpiPPm _2G4hZNU _1G7RCdW _2v5bHvx" data-aos="fade-up" data-aos-duration="3000">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png"
                            class="_3h_jpHd"></div>
                </div>
                <div style="transform: translateY(100px);">
                    <div class="_2gpiPPm _1pIlQDJ _1G7RCdW _2v5bHvx" data-aos="fade-up" data-aos-duration="3000">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png" class="_3h_jpHd">
                    </div>
                </div>
            </div>
            <div class="_2JsEYIx _2rbE6TC _2v5bHvx">
                <div class="ItM3bgw">
                    <div class="_1cT7yb7">
                        <div class="_1JMynM5 _2UxZdJd sDHKtsB">
                            <video autoplay class="_3h_jpHd _2UxZdJd sDHKtsB" playsinline loop muted
                                src="<?= get_template_directory_uri() ?>/assets/videos/Personal_Training.mp4">
                            </video>
                        </div>
                        <div class="_3-DAWjj _2QsqUxL">
                            <h2 class="_17Fvefa _2_HaYMw">Working at onelifefitness</h2>
                            <div class="_3KZTfFO _2rbE6TC _2v5bHvx">
                                <h4 class="_1qlk-1P _2DGciQP">Watch</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="_1rBG0S_ _2rbE6TC _2v5bHvx">
                <h2 class="FsabyOT _2rbE6TC _2v5bHvx">onelifefitness benefits</h2>
                <ul class="_2WTwQD9 _2rbE6TC _2v5bHvx">
                    <li class="_37M7UHC GXyzp1w _2v5bHvx">Learning &amp; development opportunities</li>
                    <li class="_37M7UHC GXyzp1w _2v5bHvx">Benefits platform full of discounts</li>
                    <li class="_37M7UHC GXyzp1w _2v5bHvx">Extra day holiday every year</li>
                    <li class="_37M7UHC GXyzp1w _2v5bHvx">Free membership</li>
                    <li class="_37M7UHC GXyzp1w _2v5bHvx">Your birthday off</li>
                    <li class="_37M7UHC GXyzp1w _2v5bHvx">Staff &amp; member parties</li>
                </ul>
            </div>
            <div class="_2gpiPPm _2-PF0o6 _2rbE6TC _2v5bHvx">
                <img src="<?= get_template_directory_uri() ?>/assets/images/ONE6K7A8104.jpg"
                    class="_3h_jpHd _3zSz_F2 _2rbE6TC _2v5bHvx"></div>
            <div class="_2umm8jj _2rbE6TC _2v5bHvx">
                <h2 class="FsabyOT _2rbE6TC _2v5bHvx">Available roles</h2>

                <!-- <div class="_3OroREi GXyzp1w _2v5bHvx">
                    <div class="_215RRp8 VBD7Ow3">
                        <div class="iBr7foy">
                            <h3>RECEPTION</h3>
                            <div>
                                <p>Does the thought of spending all day in the gym get you jumping for joy?
                                    Are you a ball of energy who gets a buzz out of life and meeting new
                                    people? Yes? Then we could have the position for you. If you're
                                    passionate and love working in a team, we want to hear from you!</p>
                            </div>
                        </div>
                    </div>
                    <a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow">
                        <span>Find out more </span><i class="fas fa-arrow-right"></i></a>
                </div>
                <div class="_3OroREi GXyzp1w _2v5bHvx">
                    <div class="_215RRp8 VBD7Ow3">
                        <div class="iBr7foy">
                            <h3>SALES GURU</h3>
                            <div>
                                <p>Is spending all day in the gym your idea of pure, unadulterated bliss?
                                    Are you an entertainer, an extrovert and an individual with passion? Are
                                    you competitive, ambitious and proactive? Did you just answer yes to all
                                    of the above? Then we have the perfect role for you!
                                </p>
                            </div>
                        </div>
                    </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
                </div>
                <div class="_3OroREi GXyzp1w _2v5bHvx">
                    <div class="_215RRp8 VBD7Ow3">
                        <div class="iBr7foy">
                            <h3>CENTRAL POSITIONS</h3>
                            <div>
                                <p>Have a look at the central positions available at onelifefitness.</p>
                            </div>
                        </div>
                    </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
                </div>
                <div class="_3OroREi GXyzp1w _2v5bHvx">
                    <div class="_215RRp8 VBD7Ow3">
                        <div class="iBr7foy">
                            <h3>STUDIO INSTRUCTORS</h3>
                            <div>
                                <p>Do you get a kick out of making a room full of people incredibly hot and
                                    sweaty? Think you have what it takes to teach the most diverse and
                                    creative classes London has to offer? Then we want to hear from you!
                                    Just drop your details below and we'll be in touch about the next steps
                                    to being one of our freelance instructors!</p>
                                <p>I understand that by submitting my details I will be contacted by onelifefitness
                                    with information about our studio product (not with spam, promise).</p>
                            </div>
                        </div>
                    </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
                </div>
                <div class="_3OroREi GXyzp1w _2v5bHvx">
                    <div class="_215RRp8 VBD7Ow3">
                        <div class="iBr7foy">
                            <h3>VERY PERSONAL TRAINERS</h3>
                            <div>
                                <p>The evolution of fitness coaching according to onelifefitness... </p>
                                <p>Training &gt; Personal Training &gt; Very Personal Training</p>
                                <p>Have you got the skills to motivate and educate London’s fittest members?
                                    Yes? Then we want to hear from you. Whether you're a graduate of the
                                    Sweet Science, fanatical about functional training or anything in
                                    between we’re looking for the best fitness coaches in London to join our
                                    team of freelance trainers to fill the best gyms in London.
                                </p>
                            </div>
                        </div>
                    </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
                </div>
                <div class="_3OroREi GXyzp1w _2v5bHvx">
                    <div class="_215RRp8 VBD7Ow3">
                        <div class="iBr7foy">
                            <h3>GENERAL MANAGER</h3>
                            <div>
                                <p>Head honcho, leader of the pack, General Manager. Are you someone who’s
                                    competitive and knows how to lead a team to greatness? Do you have
                                    incredible people, time management and organisational skills and a
                                    passion for all things onelifefitness? Yes, yes and yes? Get in touch, we have
                                    just the role for you!
                                </p>
                            </div>
                        </div>
                    </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row text-justify">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="border-set">
                <div class="_215RRp8 VBD7Ow3">
                    <div class="iBr7foy">
                        <h3>RECEPTION</h3>
                        <div>
                            <p>Does the thought of spending all day in the gym get you jumping for joy?
                                Are you a ball of energy who gets a buzz out of life and meeting new
                                people? Yes? Then we could have the position for you. If you're
                                passionate and love working in a team, we want to hear from you!</p>
                        </div>
                    </div>
                </div>
                <a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow">
                    <span>Find out more </span></a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="border-set">
                <div class="_215RRp8 VBD7Ow3">
                    <div class="iBr7foy">
                        <h3>SALES GURU</h3>
                        <div>
                            <p>Is spending all day in the gym your idea of pure, unadulterated bliss?
                                Are you an entertainer, an extrovert and an individual with passion? Are
                                you competitive, ambitious and proactive? Did you just answer yes to all
                                of the above? Then we have the perfect role for you!
                            </p>
                        </div>
                    </div>
                </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="border-set">
                <div class="_215RRp8 VBD7Ow3">
                    <div class="iBr7foy">
                        <h3>CENTRAL POSITIONS</h3>
                        <div>
                            <p>Have a look at the central positions available at onelifefitness.</p>
                        </div>
                    </div>
                </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="border-set">
                <div class="_215RRp8 VBD7Ow3">
                    <div class="iBr7foy">
                        <h3>VERY PERSONAL TRAINERS</h3>
                        <div>
                            <p>The evolution of fitness coaching according to onelifefitness... </p>
                            <p>Training &gt; Personal Training &gt; Very Personal Training</p>
                            <p>Have you got the skills to motivate and educate London’s fittest members?
                                Yes? Then we want to hear from you. Whether you're a graduate of the
                                Sweet Science, fanatical about functional training or anything in
                                between we’re looking for the best fitness coaches in London to join our
                                team of freelance trainers to fill the best gyms in London.
                            </p>
                        </div>
                    </div>
                </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="border-set">
                <div class="_215RRp8 VBD7Ow3">
                    <div class="iBr7foy">
                        <h3>STUDIO INSTRUCTORS</h3>
                        <div>
                            <p>Do you get a kick out of making a room full of people incredibly hot and
                                sweaty? Think you have what it takes to teach the most diverse and
                                creative classes London has to offer? Then we want to hear from you!
                                Just drop your details below and we'll be in touch about the next steps
                                to being one of our freelance instructors!</p>
                            <p>I understand that by submitting my details I will be contacted by onelifefitness
                                with information about our studio product (not with spam, promise).<br><br></p>
                        </div>
                    </div>
                </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="border-set">
                <div class="_215RRp8 VBD7Ow3">
                    <div class="iBr7foy">
                        <h3>GENERAL MANAGER</h3>
                        <div>
                            <p>Head honcho, leader of the pack, General Manager. Are you someone who’s
                                competitive and knows how to lead a team to greatness? Do you have
                                incredible people, time management and organisational skills and a
                                passion for all things onelifefitness? Yes, yes and yes? Get in touch, we have
                                just the role for you!<br><br><br><br><br><br><br><br><br>
                            </p>
                        </div>
                    </div>
                </div><a href="" class="_17m_Ulp _3Pq3GhV VBD7Ow3 d-flex arrow"><span>Find out more</span></a>
            </div>
        </div>
    </div>
</div>
<?php
// get_sidebar();
get_footer();
?>