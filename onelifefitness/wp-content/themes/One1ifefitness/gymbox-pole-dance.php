<?php /* Template Name: gymbox-pole-dance */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Pole Tricks</h2>
        <div class="_1-4gF4V">
            <p>They say strong is sexy, which is probably why pole dancing is fit AF. Discover the strength, toning and
                bruising it takes to climb, spin, invert and perform in this aerial-meets-gymnastics workout that werks
                you from head to toe. Sweating has never been so seductive. </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span
                        class="Nfa775m">Ealing</span><span class="Nfa775m">Old Street</span><span
                        class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                        class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Ealing">Ealing</option>
                <option value="Westfield London">Westfield London</option>
                <option value="Bank">Bank</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Victoria">Victoria</option>
                <option value="Old Street">Old Street</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1324444">13:00 Sunday 16th (60 minutes)</option>
                <option value="1302184" disabled="">20:00 Thursday 20th (60 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Serena_Ng.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Serena</h3>
                    <div>
                        <p>Since discovering her love for pole dancing, Serena has gone on to train in circus school and
                            become a qualified yoga teacher. With a love for performance and a passion for fitness,
                            Serena believes that workouts should be fun and enjoys helping her students progress.&nbsp;
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Silvio_Ghiglione.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Silvio</h3>
                    <div>
                        <p>With a background in ballroom dancing and performance,
                            Silvio began mastering pole tricks in 2016. Since then he has competed
                            professionally, and is now an instructor at Gymbox. In Silvio’s classes you can
                            expect to attempt backbends, backflips and other pole tricks. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/6gym-pole-058.jpg')"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>