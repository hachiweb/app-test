<?php /* Template Name: refer */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_3v4LBKm _2rbE6TC _2v5bHvx">
        <div class="_2xKt80P _2rbE6TC _2v5bHvx">
            <div class="_2X7P4yT _2rbE6TC _2v5bHvx">
                <h1>Friends with<br><span class="_11xSl4F">benefits</span></h1>
                <h3>REFER A FRIEND TO WIN</h3>
            </div>
        </div>
        <div class="_3qDtrYW _2rbE6TC _2v5bHvx">
            <div class="_2BcIHCQ VBD7Ow3">
                <div class="_18J8dtg _2rbE6TC _2v5bHvx">
                    <p>We know how much our members bang on about onelifefitness… so it seems only right we reward you
                        for it. Nominate a mate with training buddy potential in February and if they join,
                        you’ll be entered into a draw to win a year's supply of RX protein bars
                        <strong>plus</strong> a year’s multi-club membership here at onelifefitness. How’s that for
                        mates rates?
                    </p>
                    <p>Simply put your name below and share the link with your mate</p>
                </div>
                <input type="text" placeholder="Your name" value="" class="form-control"><br><br>
            </div>
        </div>
    </div>
</div>
<div class="_3reSXV1 _4zWqqYK sDHKtsB">Loading: 100%</div>
<div class="_12c1-Jf sDHKtsB">
    <div class="LN-nIKU I7PwxVS">
        <div class="_1L5csiF">
            <div class="_1US2FDM _2rbE6TC _2v5bHvx VBD7Ow3">
                <h3 class="LsKezX_">FREE PASS</h3><span class="_2va2FpG"></span>
            </div>
            <div class="_3ITUOra _2rbE6TC _2v5bHvx VBD7Ow3" style="width: 5366.38px; transform: translateX(0px);">
                <form>
                    <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">1/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">What's your name?</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="fromName"></label><input type="text" name="fromName"
                                value="" placeholder="Name"></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">2/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">What's your email?</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="fromEmail"></label><input type="email"
                                name="fromEmail" value="" placeholder="Email"></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">3/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">What's your phone?</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="message[mobile]"></label><input type="text"
                                name="message[mobile]" value="" placeholder="Phone"></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">4/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">Select club</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="message[gym]"></label><select name="message[gym]">
                                <option value="select">Select</option>
                                <option value="Bank">Bank</option>
                                <option value="Cannon Street">Cannon Street</option>
                                <option value="Covent Garden">Covent Garden</option>
                                <option value="Ealing">Ealing</option>
                                <option value="Elephant and Castle">Elephant and Castle</option>
                                <option value="Farringdon">Farringdon</option>
                                <option value="Finsbury Park">Finsbury Park</option>
                                <option value="Holborn">Holborn</option>
                                <option value="Old Street">Old Street</option>
                                <option value="Victoria">Victoria</option>
                                <option value="Westfield London">Westfield London</option>
                                <option value="Westfield Stratford">Westfield Stratford</option>
                            </select></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">5/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">Time to submit!</h2>
                        <p>I understand that by submitting my details I will be contacted by onelifefitness with
                            information about their services and membership options (not with spam, we promise)
                        </p><input type="submit" value="Book your free tour!">
                    </div>
                    <div class="_3ipyBdB">
                        <h2>Thanks - chat soon</h2>
                    </div>
                </form>
            </div>
            <div class="_2bIo9Yp _2rbE6TC _2v5bHvx VBD7Ow3"><button
                    class="_3pXyaiA _2S_CT_r"><span>&nbsp;</span></button><button><span>Next</span></button>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>