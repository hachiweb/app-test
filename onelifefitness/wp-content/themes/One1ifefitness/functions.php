<?php
/**
 * gym box functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gym_box
 */

if ( ! function_exists( 'gym_box_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function gym_box_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on gym box, use a find and replace
		 * to change 'gym-box' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'gym-box', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'gym-box' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'gym_box_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'gym_box_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gym_box_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'gym_box_content_width', 640 );
}
add_action( 'after_setup_theme', 'gym_box_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gym_box_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gym-box' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gym-box' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gym_box_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gym_box_scripts() {
	wp_enqueue_style( 'gym-box-style', get_stylesheet_uri() );

	wp_enqueue_script( 'gym-box-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'gym-box-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gym_box_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


// -------------------------------form submission-------------------------

add_action( 'admin_post_nopriv_my_simple_form', 'my_handle_form_submit' );
add_action( 'admin_post_my_simple_form', 'my_handle_form_submit' );


function my_handle_form_submit() {
	global $wpdb;
	// (Re)create it, if it's gone missing
	if ( ! ( $phpmailer instanceof PHPMailer ) ) {
		require_once ABSPATH . WPINC . '/class-phpmailer.php';
		require_once ABSPATH . WPINC . '/class-smtp.php';
	}
	$phpmailer = new PHPMailer;
	// SMTP configuration
	// $phpmailer->isSMTP();                    
	$phpmailer->Host = 'hachiweb.com';
	$phpmailer->SMTPAuth = true;
	$phpmailer->Username = 'care@hachiweb.com';
	$phpmailer->Password = 'Vw4&2tw4';
	$phpmailer->SMTPSecure = 'tls';
	$phpmailer->Port = 587;
	$mail->SMTPOptions = array(
		'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
		)
	);
	$phpmailer->setFrom('dev@hachiweb.com', 'One 1ife Fitness');

	// Add a recipient
	$phpmailer->addAddress('prateek.asthana@hachiweb.com');  //daraburke78@gmail.com

	// Add cc or bcc 
	$phpmailer->addCC('dev.prateek.asthana@gmail.com');

	// Set email format to HTML
	$phpmailer->isHTML(true);

	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
	{
		$protocol='https://';
	} 
	else{
		$protocol='http://';
	}
	$url = explode('/',$protocol.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
	array_pop($url);
	$url = implode('/',$url);

	if(isset($_POST['free_pass'])){		
		if(isset($_POST['name'])){
			$name = $_POST['name'];
		}
		if(isset($_POST['email'])){
			$email = $_POST['email'];
		}
		if(isset($_POST['phone'])){
			$phone = $_POST['phone'];
		}
		if(isset($_POST['club'])){
			$choose_gym = $_POST['club'];
		}

		$res = $wpdb->insert('wp_freepass', 
			array( 
				'name' => $name,
				'email' => $email,
				'phone' => $phone,
				'choose_gym' => $choose_gym,
			)
		);
		
		if(!$wpdb->print_error()){
			// Email subject
			$phpmailer->Subject = 'Free pass request';

			// Email body content
			$mailContent = "<h1>Free Pass Request</h1>
				<p>Following are the requester's details</p><ul>
				<li>Name- {$name} </li>
				<li>Email- {$email} </li>
				<li>Phone- {$phone} </li>
				<li>Gym- {$choose_gym} </li></ul>";

			$phpmailer->Body    = $mailContent;
		
			if(!$phpmailer->send()){
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $phpmailer->ErrorInfo;
				die();
			}else{
				echo 'Message has been sent';
			}
			wp_redirect( get_bloginfo('url').'?status=true' );
		}
		else{
			print_r($wpdb->print_error());
			exit();
			wp_redirect( get_bloginfo('url').'?status=false' );
		}
  	}
	
	if(isset($_POST['news_letter'])){
		if(isset($_POST['name'])){
			$name = $_POST['name'];
		}
		if(isset($_POST['email'])){
			$email = $_POST['email'];
		}
		$res = $wpdb->insert('wp_newsletter', 
			array( 
				'name' => $name,
				'email' => $email
			)
		);
		
		if(!$wpdb->print_error()){
		// Email subject
		$phpmailer->Subject = 'Newsletter subscription';

		// Email body content
		$mailContent = "<h1>Newsletter subscription</h1>
			<p>Following are the requester's details</p><ul>
			<li>Name- {$name} </li>
			<li>Email- {$email} </li>
			</ul>";

			$phpmailer->Body    = $mailContent;
			if(!$phpmailer->send()){
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $phpmailer->ErrorInfo;
				die();
			}else{
				echo 'Message has been sent';
			}
			wp_redirect( get_bloginfo('url').'?status=true' );
		}
		else{
			print_r($wpdb->print_error());
			exit();
			wp_redirect( get_bloginfo('url').'?status=false' );
		}
  	}
	

	//  MEMBER SERVICES FORM SUBMISSION 
	if(isset($_POST['Submit_membership']))
	{
		if(isset($_POST['select_club'])){
			$select_club = $_POST['select_club'];
		}
		if(isset($_POST['user_name'])){
			$user_name = $_POST['user_name'];
		}
		if(isset($_POST['email'])){
			$email = $_POST['email'];
		}
		if(isset($_POST['user_phone'])){
			$user_phone = $_POST['user_phone'];
		}

		if(isset($_POST['membership_number'])){
			$membership_number = $_POST['membership_number'];
		}
		if(isset($_POST['ticket_reasontype'])){
			$ticket_reasontype = $_POST['ticket_reasontype'];
		}
		if(isset($_POST['ticket_reasonforfreeze'])){
			$ticket_reasonforfreeze = $_POST['ticket_reasonforfreeze'];
		}
		if(isset($_POST['ticket_freeze_startdate'])){
			$ticket_freeze_startdate = $_POST['ticket_freeze_startdate'];
		}

		if(isset($_POST['ticket_freeze_enddate'])){
			$ticket_freeze_enddate = $_POST['ticket_freeze_enddate'];
		}
		if(isset($_POST['ticket_comments'])){
			$ticket_comments = $_POST['ticket_comments'];
		}
	
		$res = $wpdb->insert('wp_membership', array( 
			'select_club' => $select_club,
			'user_name' => $user_name,
			'email' => $email,
			'user_phone' => $user_phone,
			'membership_number' => $membership_number,
			'ticket_reasontype' => $ticket_reasontype,
			'ticket_reasonforfreeze' => $ticket_reasonforfreeze,
			'ticket_freeze_startdate' => $ticket_freeze_startdate,
			'ticket_freeze_enddate' => $ticket_freeze_enddate,
			'ticket_comments' => $ticket_comments,
		)
		);
		if(!$wpdb->print_error()){
			// Email subject
			$phpmailer->Subject = 'Ticket For Freeze';

			// Email body content
			$mailContent = "<h1>Ticket For Freeze</h1>
				<p>Following are the requester's details</p><ul>
				<li>Username- {$user_name} </li>
				<li>Email- {$email} </li>
				<li>Club- {$select_club} </li>
				<li>Phone- {$user_phone} </li>
				<li>Membership number- {$membership_number} </li>
				<li>Ticket reason type- {$ticket_reasontype} </li>
				<li>Reason for freeze- {$ticket_reasonforfreeze} </li>
				<li>Freeze start date- {$ticket_freeze_startdate} </li>
				<li>Freeze end date- {$ticket_freeze_enddate} </li>
				<li>Comment- {$ticket_comments} </li></ul>";
			$phpmailer->Body    = $mailContent;

			if(!$phpmailer->send()){
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $phpmailer->ErrorInfo;
				die();
			}else{
				echo 'Message has been sent';
			}
			wp_redirect( get_bloginfo('url').'?status=true' );

		}
		else{
			print_r($wpdb->print_error());
			wp_redirect( get_bloginfo('url').'?status=false' );
		}
	}


	//  CONTACT SERVICES
	if(isset($_POST['contact_submit']))
	{
		if(isset($_POST['fromName'])){
			$fromName = $_POST['fromName'];
		}
		if(isset($_POST['fromEmail'])){
			$fromEmail = $_POST['fromEmail'];
		}
		if(isset($_POST['mobile_number'])){
			$mobile_number = $_POST['mobile_number'];
		}
		if(isset($_POST['message_goals'])){
			$message_goals = $_POST['message_goals'];
		}

		if(isset($_POST['club_intrested'])){
			$club_intrested = $_POST['club_intrested'];
		}
	
		$res = $wpdb->insert('wp_contact', 
			array( 
				'fromName' => $fromName,
				'fromEmail' => $fromEmail,
				'mobile_number' => $mobile_number,
				'message_goals' => $message_goals,
				'club_intrested' => $club_intrested,
			)
		);

		if(!$wpdb->print_error()){
			// Email subject
			$phpmailer->Subject = 'Contact form';

			// Email body content
			$mailContent = "<h1>Contact form</h1>
				<p>Following are the requester's details</p><ul>
				<li>Name- {$fromName} </li>
				<li>Email- {$fromEmail} </li>
				<li>Phone- {$mobile_number} </li>
				<li>Club interested in- {$club_intrested} </li>
				<li>Message goals- {$message_goals} </li></ul>";
			$phpmailer->Body    = $mailContent;

			if(!$phpmailer->send()){
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $phpmailer->ErrorInfo;
				die();
			}else{
				echo 'Message has been sent';
			}
			wp_redirect( get_bloginfo('url').'?status=true' );
		}

		else{
			print_r($wpdb->print_error());
			wp_redirect( get_bloginfo('url').'?status=false' );
		}
	}
}


// fetch image from database post-content
function catch_that_image() {
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = isset($matches[1][0])?$matches[1][0]:'';
	if(empty($first_img)){ //Defines a default image
	  $first_img = get_template_directory_uri().'/assets/images/default.png' ;
	}
	return $first_img;
}



