<?php /* Template Name: yoga-for-lifting */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Yoga For Lifting</h2>
        <div class="_1-4gF4V">
            <p>Whatever impact your training’s had on your body, Yoga For Lifting makes the most of your motion by
                getting you stronger, stretchier and upping your range of movement. Whether it's for squats, cleans or
                overhead presses we'll get you hitting high and sinking low. One rep max PB here you come. </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Cannon
                        Street</span><span class="Nfa775m">Covent Garden</span><span class="Nfa775m">Ealing</span><span
                        class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Farringdon</span><span
                        class="Nfa775m">Old Street</span><span class="Nfa775m">Victoria</span><span
                        class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Old Street">Old Street</option>
                <option value="Bank">Bank</option>
                <option value="Victoria">Victoria</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Cannon Street">Cannon Street</option>
                <option value="Covent Garden">Covent Garden</option>
                <option value="Ealing">Ealing</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1287538" disabled="">13:00 Monday 17th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Gemma_Cousions.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Gemma - Master Trainer</h3>
                    <div>
                        <p>Gemma has worked as a professional dancer for over 10 years and found that yoga helped her to
                            find calmness and stillness, allowing her to re-balance and find space not only in the
                            physical body but also in the mind. She now wants to share her experiences and newly found
                            sensation of freedom with others, helping others to develop mindfulness and awareness of the
                            body, mind and spirit, and to find strength and overcome personal barriers.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Dylan_Salamon.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Dyl</h3>
                    <div>
                        <p>Creator of Yoga for Lifting &amp; Active Beast. Ex rugby player, now scratching that itch by
                            competing in yoga and Crossfit. I love anything that challenges you and involves the body
                            moving. Currently trying to break the stereotype that yoga isn’t for big dudes. Unless
                            you’re too scared to try ;) </p>
                        <p> </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ben_Harrison_Extra_Option_2.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ben</h3>
                    <div>
                        <p>Ben teaches modern, efficient,
                            anatomical and science based yoga. He focuses on building strength, often with hand
                            balancing - but with progressions so that every class is suitable
                            for beginners.<br></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Anna_Jackson.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hannah J</h3>
                    <div>
                        <p>Hannah was forced to give up her life as a professional
                            dancer in 2012 due to a back injury. This led her to discover her passion for
                            yoga, and having retrained as an instructor in 2014, Hannah now focuses on
                            developing her clients strength and flexibility. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url(&quot;https://gymbox-assets.enecdn.io/images/slideshow/_galleryImage/Yogaforlifters_New.jpg&quot;);"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>