<?php /* Template Name: drill-sergeant */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Drill Sergeant</h2>
        <div class="_1-4gF4V">
            <p>We won’t ask why you like a strict authoritarian crackdown on your training, you absolute sadist… but
                you’d better turn up ready to work. This is military fitness with no holds barred – hardcore circuits
                with a tongue-lashing pushing you over the finish line. It’s open to all levels of fitness, but don’t
                expect to stand at ease. </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Covent
                        Garden</span><span class="Nfa775m">Ealing</span><span class="Nfa775m">Elephant and
                        Castle</span><span class="Nfa775m">Holborn</span><span class="Nfa775m">Old Street</span><span
                        class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                        class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Holborn">Holborn</option>
                <option value="Bank">Bank</option>
                <option value="Old Street">Old Street</option>
                <option value="Covent Garden">Covent Garden</option>
                <option value="Victoria">Victoria</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Westfield London">Westfield London</option>
                <option value="Ealing">Ealing</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1280057">10:30 Sunday 16th (45 minutes)</option>
                <option value="1331950" disabled="">06:45 Monday 17th (45 minutes) - Bookable 7am day before</option>
                <option value="1266430" disabled="">06:45 Wednesday 19th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ryan_Lovett.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ryan</h3>
                    <div>
                        <p>A professional actor trained in New York, when Ryan’s not on
                            stage he’s teaching TRX, Row30 and Frame Fitness at onelifefitness. Having returned to
                            the UK a few years ago, Ryan loves his job at onelifefitness and admits that London
                            definitely has his heart now. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                    <div>
                        <p>Through a combination of tough-love and motivation, Stevie will push you to your limits with
                            high-energy metabolic conditioning workouts that will leave you in a pool of sweat and a big
                            smile on your face.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Yvette_Carter_Extra_Option_4.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Yvette C</h3>
                    <div>
                        <p>Having earned a degree in theatre dance from the London
                            Studio Centre, Yvette went on to become a qualified PT and is now a studio
                            instructor at onelifefitness. Yvette’s philosophy is that training should be fun with
                            hardwork thrown in.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lutha_Nzinga.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Luther</h3>
                    <div>
                        <p>A PT and instructor with a degreee in Sports Therapy, and a
                            background in professional powerlifting, Luther teaches Frame Fitness and Drill
                            Sergeant at onelifefitness. His focus on technique and lifting heavy will help you
                            improve both your mental and physical strength.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                    <div>
                        <p>Firas found his passion for American Football 15 years ago, and has since won numerous
                            championships as both a player and coach. Firas brings his enthusiasim and camaraderie to
                            all his classes, and will undoubtedly bring out the inner athlete in you. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lawrence_Cain.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Lawrence</h3>
                    <div>
                        <p>Lawrence focuses on calisthenics, strength training and body building in his training, and
                            teaches Frame Fitness, Ripped &amp; Stripped, Drill Sergeant and Kettlebells. Lawrence’s
                            no-nonsense, hardcore attitude is sure to push you beyond your limits.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Drill-Sergeant-1_1_1.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>