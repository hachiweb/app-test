<?php /* Template Name: login */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<!-- <section class="">
    <div class="container-fluid">
       <div class="login-top text-white">
        <div class="d-flex justify-content-between">
            <a class="" title="Go to the onelifefitness homepage"  href="<?= get_site_url(); ?>/home">GYM BOX</a>
            <a  href="<?= get_site_url(); ?>/home" title="Go to the onelifefitness homepage"><span>Return to main site</span></a>
        </div>
    </div>
    </div>
</section> -->
<form action="" method="post">
<section class="login-form">
    <div class="container">
       <div class="row">
        <div class="col-sm-7">
            <h4>Member Login</h4>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" name="email" class="form-control" id="email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" name="password" class="form-control" id="password">
            </div>
            <span class="login-btn">
                <input type="submit" id="login" class="" ><i class="fa fa-arrow-right"></i>
            </span>
            <div class="form-anchor">
                <a href="">Not yet registered?</a> <br>
                <a href="">Forgot your password?</a>
            </div>
        </div>
    </div>
    </div>
</section>
</form>