<?php /* Template Name: bike-beats */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
        <h2>Legs, Bums + Tums</h2>
        <div class="_1-4gF4V">
            <p>You’re about to squeeze, squat, lift and burn your way through a class that whoops your ass to give you glutes of steel and thighs of iron. Get through this on the regular and it’s not just your butt that feels hard as nails.</p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                <span class="Nfa775m">Onelifefitness Camden quay</span>
                <span class="Nfa775m">Onelifefitness Sullivans quay</span>
            </div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
        <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
            <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1292426">10:15 Saturday 15th (45 minutes)</option>
                <option value="1279952">11:30 Saturday 15th (45 minutes)</option>
                <option value="1298335" disabled="">13:00 Sunday 16th (45 minutes) - Bookable 7am day before</option>
                <option value="1293154" disabled="">06:45 Monday 17th (45 minutes) - Bookable 7am day before</option>
            </select>
            <a href="https://indma05.clubwise.com/onelifefitness/index.html" target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Sebastian_Edwards.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Seb</h3>
                    <div>
                        <hr>
                        <p>At one time Seb was 23 stone - his weight loss came not from following a fad diet but
                            training hard and always looking for the next goal to keep motivated. Seb's classes are fun,
                            music driven, goal oriented, and designed to stretch your limits each week. You will feel
                            yourself work hard but won't necessarily feel like you're working out! </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jenny_Lawford_1.2.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jenny - Master Trainer</h3>
                    <div>
                        <p>With a background in music, Jenny fell in love with the Cycle Club category after realising
                            the impact a banging playlist could have on a class! From Bike &amp; Beats to Connect, she
                            always brings a smile and her goofy sense of humour to the studio and promises that you’ll
                            never leave her class feeling anything less than a champion, rockstar or diva.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Vitor_Fernandes.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Vitor</h3>
                    <div>
                        <p>Former Brazilian professional dancer and Capoeira martial artist, Vitor has brought his
                            energy and enthusiasm to onelifefitness. In his classes you'll be pushed to your limits but he'll
                            ensure you have fun on that journey with his charisma and the most epic tunes carrying you
                            through. Vitor brings his vast experience and passion to teaching, making sure you walk out
                            sweating with a smile each and every class.</p>
                        <p><u></u></p>
                        <p><u><u><br></u></u></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Taofique_Folarin.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Taofique</h3>
                    <div>
                        <p>Having trained in theatre and dance from the age of 16, Taofique has been working
                            professionally on stage and television for over 15 years. His varied experience as a dancer
                            and actor led him into the world of fitness, developing skills that took him onto shows such
                            as The Lion&nbsp;King and Fame. He implements three fundamentals into his classes: stamina,
                            discipline and strength, and believes in education through movement. His classes are
                            energetic and fun, with a focus on technique, strength, conditioning and endurance. He will
                            motivate and push you to surpass your goals and live fearlessly.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Raquel_Banuls.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Raquel</h3>
                    <div>
                        <p>Raquel is a PT and studio instructor who specialises in
                            pole, aerial, indoor cycling and circuit training. Raquel’s passion for fitness
                            is contagious, and nothing makes her smile more than seeing the smiles and
                            satisfaction of her clients after a workout. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Matteo_Cruciani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Matteo</h3>
                    <div>
                        <p>A native Italian, Matteo was born and raised in the city of Rome.&nbsp;He grew up with a huge
                            passion for fitness, dance and movement.&nbsp;He is an&nbsp;enthusiastic and charismatic
                            person who brings a positive attitude to every situation!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Melissa_Powers.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Melissa</h3>
                    <div>
                        <p>&nbsp;Having taught nearly 10,000 classes in her career so far, it's safe to say Melissa
                            knows pretty much everything there is about how to get the absolute best out of her clients.
                            Whether it's pedalling furiously on a bike, lifting weights, jumping up and down or lying on
                            a mat swearing through a particularly tough ab routine, Melissa will be there, cheering
                            enthusiastically, doing it all with you, and making you feel like exercising is actually a
                            lot less horrible than you'd imagined! </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ryan_Lovett.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ryan</h3>
                    <div>
                        <p>A professional actor trained in New York, when Ryan’s not on
                            stage he’s teaching TRX, Row30 and Frame Fitness at onelifefitness. Having returned to
                            the UK a few years ago, Ryan loves his job at onelifefitness and admits that London
                            definitely has his heart now. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                    <div>
                        <p>Through a combination of tough-love and motivation, Stevie will push you to your limits with
                            high-energy metabolic conditioning workouts that will leave you in a pool of sweat and a big
                            smile on your face.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Boriss_Visokoborskis.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Boriss</h3>
                    <div>
                        <p>Boriss stepped in to sport when he was just 6 years old competing in Ballroom &amp; Latin
                            dance European championships until his late teenage years. Boriss’ love of dance developed
                            into a love of music &amp; rhythm and dancing gave him insight into hard work and
                            competitiveness, these are elements that you will experience in his spinning classes: he
                            will motivate you to work hard as a team and push yourself to go further as an individual to
                            achieve your own goals. &nbsp;And all of that to sounds of dirty beats, club bangers and
                            unheard remixes.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Amina_Bedics.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Amina</h3>
                    <div>
                        <p>If you see a tiny girl bouncing on
                            the spin bike or shouting in the studio , that’s Amina! She loves to
                            teach classes to the beat so get ready to sweat to the best tunes! Don’t be fooled by her
                            smile, she’ll be kicking asses and make sure you work hard in her classes. The motto is ass
                            to the grass!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Aaron_Cook.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Aaron C</h3>
                    <div>
                        <p>Aaron believes that everyone is an athlete, so everyone should perform and be coached like
                            one. His passion for coaching with education and enthusiasm helps give his private clients
                            and our members the results they want. Olympic lifting, gymnastics, engine work and
                            flexibility are all skills of his own training that he implements though his classes. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Layton_Taylor_1.2.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Layton</h3>
                    <div>
                        <p>Layton's passion for fitness started early, when he was crowned UK's 100m sprint champion at
                            age 16. Now, as well as teaching across the onelifefitness group, Layton oversees the Studio product
                            from Head Office. If you're a fan of Euphoric beats, awful(ly good) dad jokes and being
                            pushed to your limit, his classes are definitely for you!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Abbey_Price.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Abigail</h3>
                    <div>
                        <p>A studio cycling instructor with a neverending supply of
                            enthusiasm, Abigail’s classes are guaranteed to get you as addicted to life in
                            the saddle as she is. With multiple qualifications under her belt, Abi will
                            take you from part time Boris-biker to spinning pro.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                    <div>
                        <p>Firas found his passion for American Football 15 years ago, and has since won numerous
                            championships as both a player and coach. Firas brings his enthusiasim and camaraderie to
                            all his classes, and will undoubtedly bring out the inner athlete in you. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jason_Mellars.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jason</h3>
                    <div>
                        <p>Jason has over 20 years experience in the dance and fitness
                            industry as an international fitness presenter, master trainer and
                            choreographer. With specialities including pilates, spinning and dance, Jason’s
                            classes celebrate having fun whilst pushing you to your limits.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Tsuki_Harris.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Tsuki</h3>
                    <div>
                        <p>A fitness
                            fanatic, Tsuki with contagious energy and passion. Tsuki is a studio
                            instructor, PT and bodybuilder who will push your body and mind to places you
                            thought you were incapable of reaching.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                    <div>
                        <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                            &nbsp;classes range from Gains to Escalate, and are based around personal development in a
                            motivational team environment. His love for fitness is second only to his love of the onelifefitness
                            team.
                        </p>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>