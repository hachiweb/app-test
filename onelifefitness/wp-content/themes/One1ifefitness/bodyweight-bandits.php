<?php /* Template Name: bodyweight-bandits */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
            <h2>Bodyweight Bandits</h2>
            <div class="_1-4gF4V">
                <p>Brutally sweaty. Criminally tough. This lot are such gluttons for punishment that you probably
                    wouldn’t want to meet them in a dark alley. But hang around the frame together and you’ll soon
                    crack the fundamentals of pull ups, push ups lunges and dips with just your bodyweight. All
                    pros, no cons.</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                    <span class="Nfa775m">Onelifefitness Camden quay</span>
                    <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                        </div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Select gym</option>
                    <option value="Farringdon">Farringdon</option>
                    <option value="Cannon Street">Cannon Street</option>
                    <option value="Victoria">Victoria</option>
                    <option value="Covent Garden">Covent Garden</option>
                </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1283615" disabled="">13:00 Monday 17th (45 minutes) - Bookable 7am day before
                    </option>
                </select><a href="" target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book
                        class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                        <div>
                            <p>Firas found his passion for American Football 15 years ago, and has since won
                                numerous championships as both a player and coach. Firas brings his enthusiasim and
                                camaraderie to all his classes, and will undoubtedly bring out the inner athlete in
                                you. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Hang-Tough.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>