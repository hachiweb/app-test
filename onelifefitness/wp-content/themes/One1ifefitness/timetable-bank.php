<?php /* Template Name: timetable-bank */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
// $id = $_GET['id'];
// echo $id;
get_header(); ?>

<div class="container" style="margin-top:100px;">
    <div class="row">
        <div class="col-md-12 m-auto text-center">
            <img src="<?= get_template_directory_uri() ?>/assets/images/Camden_Timetable_Feb_2020.jpg" width="70%">
        </div>
    </div>
</div>
<?php /* 
if($id == 1) { ?>
        <div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
        <div class="hGlgE6p _2rbE6TC _2v5bHvx">
            <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
                </span>
            </button>
            <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
        </div>
        <div class="dS7Juez">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                    <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                    <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
                </div>
            </div>
        </div>
        <!-- Morning -->
        <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
            <h3 class="_3isqe69">Morning</h3>
            <div class="cQJmZSg _2rbE6TC _2v5bHvx">
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                                Club</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (40mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:45 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                                Club</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                                Sergeant</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                                Yoga</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/paddleboard-yoga">Paddleboard Yoga</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/functional-bodybuilding">Functional Bodybuilding</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                        </div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                                Club</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/cavemen-and-neandergals">Cavemen &amp; Neandergals</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                        </div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                                Sergeant</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                                Flow Yoga</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                        </div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                                Club</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                        </div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                                Sergeant</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:30 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                                Flow Yoga</a></div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <p class="_22O2e0C">No classes on this day</p>
                </div>
                <!--col-->
            </div>
        </div>
        <!-- Lunch-time -->
        <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
            <h3 class="_3isqe69">Lunch-time</h3>
            <div class="cQJmZSg _2rbE6TC _2v5bHvx">
                <!-- <div class="g2nuP7s j9cSsYQ _2rbE6TC _2v5bHvx"></div>
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div> -->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                                Barre</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/buti-yoga">Buti Yoga</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                                Club</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                                Battle</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                        </div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                                Battle</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                                Flow Yoga</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/whiplash">Whiplash</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                                Battle</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                                For Lifting</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pound">Pound</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                </div>
                <!--col-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                                For Lifting</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                                Sergeant</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                                Barre</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-beginners">Muay
                                Thai Beginners</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sword-play">Sword
                                Play</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (30mins) <a  href="<?= get_site_url(); ?>/bank">Bank</a>
                        </div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                    </div>
                </div>
                <!--div-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rampant-rehab">Rampant
                                Rehab</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling With My Yogis</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                                Beats</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                    </div>
                </div>
                <!--div-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                                Yoga</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                                Battle</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                                Flow Yoga</a></div>
                    </div>
                </div>
                <!--div-->
            </div>
        </div>
        <!-- evening -->
        <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
            <h3 class="_3isqe69">Evening</h3>
            <div class="cQJmZSg _2rbE6TC _2v5bHvx">
                <!-- <div class="g2nuP7s j9cSsYQ _2rbE6TC _2v5bHvx"></div> -->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                        </div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (90mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                                Tricks</a></div>
                    </div>
                    <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                                Thai Sparring</a></div>
                    </div>
                </div>
                <!--div-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                                 href="<?= get_site_url(); ?>/l1-white-collar-fight-club">L1 White Collar Fight Club</a></div>
                    </div>
                </div>
                <!--div-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (30mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (75mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/krav-maga">Krav Maga</a>
                        </div>
                    </div>
                </div>
                <!--div-->
                <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                                Tricks</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                                Club</a></div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                        </div>
                    </div>
                    <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                        <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                        <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (60mins) <a
                                 href="<?= get_site_url(); ?>/bank">Bank</a></div>
                        <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                                Thai Sparring</a></div>
                    </div>
                </div>
                <!--div-->
            </div>
        </div>
    </div>
<?php }
elseif($id == 2){ ?>
<!--------------------------------------- Cannon Street ----------------------------------->

<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <!-- Morning -->
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (30mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (30mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rampant-rehab">Rampant
                            Rehab</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
            </div>
            <!--div-->
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (30mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/paddleboard-yoga">Paddleboard Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
            </div>
            <!--div-->
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/functional-bodybuilding">Functional Bodybuilding</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <!--div-->
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            for lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
            </div>
            <!--div-->
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">Bodyweight Bandits</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
            </div>
            <!--div-->
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">Bodyweight Bandits</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <p class="_22O2e0C">No classes on this day</p>
            </div>
        </div>
    </div>
    <!-- Lunch-time -->
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">CounterKick</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/false-grip">False
                            Grip</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling with my Yogis</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">CounterKick</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/yogangster">Yogangster</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- evening -->
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">Bodyweight Bandits</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/superfly">Superfly</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/cannon-street">Cannon Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
</div>
<?php } 
elseif($id == 3){ ?>
<!--------------------------------------- Covent Garden ----------------------------------->

<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (30mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (30mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">Bodyweight Bandits </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/barfly">Barfly</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (60mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sweat-to-the-beat">Sweat
                            To The Beat</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (60mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (30mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/prehab">Prehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/the-fundamentals">The
                            Fundamentals </a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <p class="_22O2e0C">No classes on this day</p>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (30mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling with my Yogis</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (30mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/kettlebells-circuit">Kettlebells Circuit</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/the-fundamentals">The
                            Fundamentals</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            for Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:00 (60mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:45 (30mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                            Thai Sparring</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sword-play">Sword Play
                        </a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/covent-garden">Covent Garden</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/paddleboard-yoga">Paddleboard Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
</div>
<?php }
elseif($id == 4){?>
<!--------------------------------- Ealing----------------------------------- -->
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">Regeneration Z</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:15 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">Badass</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/paddleboard-yoga">Paddleboard Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-pilates">Aerial
                            Pilates</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:15 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:15 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">Metcon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/superfly">Superfly</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:30 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rehab">Rehab</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">Metcon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/false-grip">False
                            Grip</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/paddleboard-yoga">Paddleboard Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">Metcon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            for Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">Bodyweight Bandits</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">Counterkick</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                            Thai Sparring</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:30 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cirque-it">Cirque it</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (30mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">Metcon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymboxing-and-sparring">Gymboxing &amp; Sparring</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (90mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/krav-maga">Krav Maga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-pilates">Aerial
                            Pilates</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/dancehall">Dancehall</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats </a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/femme">Femme</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/ealing">Ealing</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
</div>
<?php }
elseif($id == 5){ ?>
<!----------------------------------------- Elephant and Castle---------------------------------- -->
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <!-- Morning -->
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/deathrow">Deathrow</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/functional-bodybuilding">Functional Bodybuilding</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling with my Yogis</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/deathrow">Deathrow</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            For Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rehab">Rehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/barfly">Barfly</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/femme">Femme</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/brazilian-jiu-jitsu">Brazilian Jiu Jitsu</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/dancehall">Dancehall</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rampant-rehab">Rampant
                            Rehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-gi">No-Gi</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/brazilian-jiu-jitsu">Brazilian Jiu Jitsu</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            For Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sword-play">Sword
                            Play</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/cavemen-and-neandergals">Cavemen &amp; Neandergals</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (30mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:45 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rehab">Rehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-gi">No-Gi</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rowingwod">RowingWOD</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">Regeneration Z</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sluggers-club">Sluggers
                            Club</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/buti-yoga">Buti Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/contortion">Contortion</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                            Thai Sparring</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:45 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/false-grip">False
                            Grip</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-pilates">Aerial
                            Pilates</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/barfly">Barfly</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/elephant-and-castle">Elephant and Castle</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
elseif($id == 6){ ?>
<!-------------------------------------- Farringdon --------------------------------------->
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <!-- Morning -->
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin’</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/deathrow">Deathrow</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/false-grip">False Grip
                        </a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">Counterkick</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/brazilian-jiu-jitsu">Brazilian Jiu Jitsu</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/deathrow">Deathrow</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/deathrow">Deathrow</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            for Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling with my Yogis</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">08:00 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:30 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/prehab">Prehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/ladies-only-brazilian-jiu-jitsu">Ladies Only Brazilian Jiu Jitsu</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/whiplash">Whiplash</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">BodyWeight Bandits</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/prehab">Prehab</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">Counterkick</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/flatline">Flatline</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-gi">No-Gi</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/femme">Femme</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sword-play">Sword
                            Play</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ocr-training">OCR
                            Training</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rowingwod">RowingWOD</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/yogangster">Yogangster</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/dancehall">Dancehall</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/prehab">Prehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-gi">No-Gi</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ocr-training">OCR
                            Training</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">CounterPunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/brazilian-jiu-jitsu">Brazilian Jiu Jitsu</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">Regeneration Z</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                            Thai Sparring</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/l1-white-collar-fight-club">L1 White Collar Fight Club</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling with my Yogis</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/the-fundamentals">The
                            Fundamentals</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (30mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            for Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/l2-white-collar-fight-club">L2 White Collar Fight Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cirque-it">Cirque It</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                            Thai Sparring</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/farringdon">Farringdon</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
elseif($id == 7){?>
<!--------------------------------------- Finsbury Park ------------------------------------>
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
</div>
<?php }
elseif($id == 8){ ?>
<!------------------------------- Holborn ------------------------------->

<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/cavemen-and-neandergals">Cavemen &amp; Neandergals</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/budokon-yoga">Budokon
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ultimate-fit">Ultimate
                            Fit</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">Regeneration Z</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ultimate-fit">Ultimate
                            Fit</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sweat-to-the-beat">Sweat
                            To The Beat</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/killer-combat">Killer
                            Combat</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/dancehall">Dancehall</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers">Swingers</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sword-play">Sword
                            Play</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/paddleboard-yoga">Paddleboard Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (30mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:00 (90mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/muay-thai-fighters-club">Muay Thai Fighters Club</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling with my Yogis</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sluggers-club">Sluggers
                            Club</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-intadv">Muay
                            Thai INT/ADV</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/muay-thai-fighters-club">Muay Thai Fighters Club</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-beginners">Muay
                            Thai Beginners</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/contortion">Contortion</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/kickboxing">Kickboxing</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:45 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/kickboxing-fighters-club">Kickboxing Fighters Club</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (90mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/krav-maga">Krav Maga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/holborn">Holborn</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/killer-combat">Killer
                            Combat</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
elseif($id == 9){ ?>
<!------------------------------------- Old Street ----------------------------------->
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <!-- Morning -->
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (30mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (30mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (30mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            for Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (30mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">ReGeneration Z</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">Badass</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (30mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/femme">Femme</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/shway">Shway</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/barfly">Barfly</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (30mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">Badass</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/b.o.l.t">B.O.L.T</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:45 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cirque-it">Cirque it</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/contortion">Contortion</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/contortion">Contortion</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-hoop">Aerial
                            Hoop</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">Badass</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:15 (60mins) <a
                             href="<?= get_site_url(); ?>/old-street">Old Street</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
        </div>
    </div>
</div>
<?php }
elseif($id == 10){ ?>
<!---------------------------------- Victoria --------------------------------------->
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <!-- Morning -->
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/threshold">Threshold</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trilactic">TriLactic</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yoga-for-lifting">Yoga
                            For Lifting</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-pilates">Aerial
                            Pilates</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/balates">Balates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trilactic">TriLactic</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trilactic">TriLactic</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/false-grip">False
                            Grip</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sweat-to-the-beat">Sweat
                            To The Beat</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">Regeneration Z</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/barfly">Barfly</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/threshold">Threshold</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bartendaz">Bartendaz</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">Counterkick</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/threshold">Threshold</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/buti-yoga">Buti Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trilactic">TriLactic</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cyclone">Cyclone</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/yogangster">Yogangster</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-pilates">Aerial
                            Pilates</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pound">Pound</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/b.o.l.t">B.O.L.T</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pound">Pound</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/invert-yourself">Invert
                            Yourself</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rowingwod">RowingWOD</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/threshold">Threshold</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cyclone">Cyclone</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/bodyweight-bandits">Bodyweight Bandits</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/the-fundamentals">The
                            Fundamentals</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trilactic">TriLactic</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/sweat-to-the-beat">Sweat
                            To The Beat</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-holds-barre">No Holds
                            Barre</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">14:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/swingers-club">Swingers
                            Club</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (90mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/muay-thai-and-sparring">Muay Thai &amp; Sparring</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/threshold">Threshold</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/aerial-yoga">Aerial
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:30 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterpunch">Counterpunch</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cocoon">Cocoon</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/escalate">Escalate</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/contortion">Contortion</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:30 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cirque-it">Cirque It</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/power-battle">Power
                            Battle</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/threshold">Threshold</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/femme">Femme</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/counterkick">Counterkick</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:45 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(39, 164, 255);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trapeze">Trapeze</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/trilactic">TriLactic</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/buti-yoga">Buti Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">21:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (30mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/cyclone">Cyclone</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/cavemen-and-neandergals">Cavemen &amp; Neandergals</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (60mins) <a
                             href="<?= get_site_url(); ?>/victoria">Victoria</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
elseif($id == 11){ ?>
<!--------------------------------------- Westfield London ---------------------------------------->
<div class="_19xlPFI _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="hGlgE6p _2rbE6TC _2v5bHvx">
        <button class="_3vuzAcT _1KP-KD_ _3xVoYzA"><span>
            </span>
        </button>
        <!-- <button class="_3PsjQmM _3xVoYzA"><span> day</span></button> -->
    </div>
    <div class="dS7Juez">
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Monday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Tuesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Wednesday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Thursday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Friday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Saturday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
        <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
            <div class="_1uOQtjy _2rbE6TC _2v5bHvx">
                <h4 class="RKva5nX _2rbE6TC _2v5bHvx">Sunday</h4>
                <h4 class="_3YdmBTx _2rbE6TC _2v5bHvx"></h4>
            </div>
        </div>
    </div>
    <!-- Morning -->
    <div class="_8SmikrR F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Morning</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad </a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/amrap">amRAP</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:15 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/metcon">MetCon</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">06:45 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">07:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">09:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hatha-yoga">Hatha
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/strongman">Strongman</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:45 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rocket-yoga">Rocket
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">10:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">11:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">12:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/battleballs">Battlebells</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3mZn29e F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Lunch-time</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/brazilian-jiu-jitsu">Brazilian Jiu Jitsu</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/callback">Callback</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/hardcore">Hardcore</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/rolling-with-my-yogis">Rolling With My Yogis</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/functional-bodybuilding">Functional BodyBuilding</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/drill-sergeant">Drill
                            Sergeant</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/laughing-therapy">Laughing Therapy</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/regeneration-z">Regeneration Z</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(129, 144, 199);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/beastmode">Beastmode</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/gymnastic-conditioning">Gymnastic Conditioning</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/dancehall">Dancehall</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/thunder">Thunder</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">17:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/the-fundamentals">The
                            Fundamentals</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/ashtanga-yoga">Ashtanga
                            Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:30 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gymboxing">Gymboxing</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:15 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pilates">Pilates</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/no-gi">No-Gi</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">18:45 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx"></div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">13:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">15:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="_2ioCve8 F8xyGuU _2rbE6TC _2v5bHvx">
        <h3 class="_3isqe69">Evening</h3>
        <div class="cQJmZSg _2rbE6TC _2v5bHvx">
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (30mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/badass">BadAss</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a
                             href="<?= get_site_url(); ?>/suspenders">Suspenders</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/yin-yoga">Yin Yoga</a>
                    </div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:30 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/gain-train">Gain
                            Train</a></div>
                </div>
                <div class="_1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai-sparring">Muay
                            Thai Sparring</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/mma">MMA</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(55, 87, 245);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/bike-beats">Bike &amp;
                            Beats</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/reppin">Reppin'</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/wod-squad">WOD Squad</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(132, 220, 240);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/vinyasa-yoga">Vinyasa
                            Flow Yoga</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rowingwod">RowingWOD</a>
                    </div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 155, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:15 (45mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/rehab">Rehab</a></div>
                </div>
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(255, 118, 197);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">20:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/pole-tricks">Pole
                            Tricks</a></div>
                </div>
            </div>
            <div class="j9cSsYQ _2rbE6TC _2v5bHvx">
                <div class="_3ZNLV9o _1NpZ_f5 _2rbE6TC _2v5bHvx">
                    <div class="_272FrfC" style="background-color: rgb(245, 66, 66);"></div>
                    <div class="_2TS6jxQ _2rbE6TC _2v5bHvx HlqSPZF _19O-0c-">19:00 (60mins) <a
                             href="<?= get_site_url(); ?>/westfield-london">Westfield London</a></div>
                    <div class="shyKwGN _2rbE6TC _2v5bHvx _25LPQhk _19O-0c-"><a  href="<?= get_site_url(); ?>/muay-thai">Muay Thai</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } */?>
<?php
get_footer();
?>