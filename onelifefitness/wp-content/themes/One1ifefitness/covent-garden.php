<?php /* Template Name: covent-garden */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<section class="_1J0RKGy _2rbE6TC _2v5bHvx">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/40I4133.jpg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/40I5089.jpg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/aerial-2.jpg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/boxing.jpg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/dance-1.jpg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/FUNCTIONAL-1.jpg');">
                    </div>
                </div>
                <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->
            </div>
        </div>
    </div>
</section>
<div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="_130-KDr">
        <div class="">
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">amRAP</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Start the day
                            right with an endurance workout that couldn’t be more self-explanatory if we tried. It’s a
                            morning<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/amrap"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Ashtanga Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Thought it was
                            badass to lift weights? Imagine a workout so deep that even your breath feels the burn. For
                            a more<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/ashtanga-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">BadAss</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">The name ain’t
                            just for fun. You’re about to squeeze, squat, lift and burn your way through a class that
                            whoops your<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/badass"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Barfly</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Get your name on
                            the list and practice your pulling technique at the only club that’ll get you
                            cleaning,<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/barfly"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Bartendaz</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Go for something
                            a bit stronger in this hardcore conditioning workout with a twist. There’s no weights at our
                            bar – just<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/bartendaz"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Beastmode</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Looking for a
                            bodyweight HIIT and weighted conditioning in a single workout? You absolute animal.
                            Thankfully, we are<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/beastmode"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Bike &amp; Beats</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Bike &amp;amp;
                            Beats is the class to make spin everyone’s thing, with playlists to get you pumped, and
                            climbs, sprints<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/bike-beats"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Bodyweight Bandits</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Brutally sweaty.
                            Criminally tough. This lot are such gluttons for punishment that you probably wouldn’t want
                            to meet<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/bodyweight-bandits"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Callback</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Live for the
                            dancefloor? Our music-video inspired commercial class is the place to choreograph
                            killer<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/callback"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">CounterPunch</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Whether you’re
                            looking forward to brushing up your agility or building the fitness to conquer a
                            staircase,<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/counterpunch"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Drill Sergeant</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">We won’t ask why
                            you like a strict authoritarian crackdown on your training, you absolute sadist… but you’d
                            better<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/drill-sergeant"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Gain Train</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">All aboard the
                            gain train. Whether you need expert tips to tone up your technique or if solo free weights
                            sessions<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/gain-train"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Gymboxing</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Starting boxing
                            from scratch? The gloves are on and London's pros are primed to pass on all the tips
                            and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/gymboxing"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Gymnastic Conditioning</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Flip the bird to
                            boring workouts. Welcome to skill-based gymnastics training for anyone who gives a tuck
                            about<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/gymnastic-conditioning"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Hardcore</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">If you thought
                            the name was inspired by the killer abs you’re going to get from this class, we’ve got bad
                            news. Try the<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/hardcore"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Hatha Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Reality kicking
                            you where it hurts? Consider this your antidote to urban life. Through a deliciously slow
                            and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/hatha-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Laughing Therapy</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Between dark
                            mornings, back to work blues and being in the red, the new year can leave you wondering
                            whether<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/laughing-therapy"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">MetCon</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Short, quick and
                            hotter than hell – there’s no messing about with MetCon. In just thirty minutes of solid
                            HIIT, you’ll test<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/metcon"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Muay Thai</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Looking to join
                            a new boxing Kru? Say Sawa Dee to Muay Thai – high energy workout with all the sweatiness
                            of<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/muay-thai"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Muay Thai Sparring</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Ready to unleash
                            those Muay Thai techniques you've been learning? Welcome to the class where the
                            more<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/muay-thai-sparring"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">No Holds Barre</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Don’t let the
                            tutu clichés fool you: Barre is the tough ballet workout that’ll hand you your derrière on a
                            plate.<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/no-holds-barre"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Paddleboard Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Hang zen, dude.
                            If you’re a yogi who wants to get wild, forget about a traditional flow – try riding the
                            wave of a<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/paddleboard-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Pilates</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Build the
                            strength to smash life, without the incredible bulk. This is all about control: precise
                            movements that stretch<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/pilates"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Power Battle</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Velodrome
                            villains, sweat fetishists and speed demons unite: this is the showdown you’ve been waiting
                            for.<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/power-battle"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Prehab</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Fail to prepare,
                            prepare to fail and all that: so give all those tweaks, pulls and strains a swerve at
                            Prehab. Working<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/prehab"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Reppin'</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">When it comes to
                            lifting, we don’t all like it heavy and slow. If it’s plenty of pumping and a cranked up
                            tempo that gets<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/reppin"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Rolling With My Yogis</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Meet the new
                            prop on the block: This cool little piece of kit opens up your chest, gets those hips
                            flexin’ and makes<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/rolling-with-my-yogis"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Sweat to the Beat</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Whether you’re a
                            slave to the rhythm or addicted to bass, this catchy little conditioning class is guaranteed
                            to get<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/sweat-to-the-beat"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Swingers Club</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Switch up your
                            functional training with a kettlebell workout that builds endurance, increases strength,
                            burns fat and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/swingers-club"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Sword Play</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Turn yourself
                            into a total weapon as you learn the way of the sword. The coming together of an ancient art
                            form with<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/sword-play"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">The Fundamentals</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">The secret to
                            perfect squats, deadlifts and shoulder presses? Crack the fundamentals and that's all you
                            need<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/the-fundamentals"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Thunder</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Blitz your lower
                            body in a session that goes down a storm with your quads and glutes. Combining bodyweight
                            booty<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/thunder"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Vinyasa Flow Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Smooth as silk.
                            Chill as you like. Vinyasa flow is your secret oasis of order amongst the chaos of modern
                            life. Knock<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/vinyasa-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">WOD Squad</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Pulling,
                            pushing, climbing, lifting. Seems so simple, doesn’t it? Well, looks can be deceiving, pal.
                            Being a Gymbox<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/wod-squad"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Yin Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Sometimes you
                            just need to take it slow – and Yin is the way to go. The strong and silent type, this
                            meditative flow<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/yin-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Yoga For Lifting</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Whatever impact
                            your training’s had on your body, Yoga For Lifting makes the most of your motion by getting
                            you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/yoga-for-lifting"></a>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>