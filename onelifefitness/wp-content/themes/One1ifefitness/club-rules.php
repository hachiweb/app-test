<?php /* Template Name: club-rules */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_2dDJkQd VBD7Ow3">
        <div>
            <h2>Club Rules</h2><br>
            <div>
                <p>All Members and Guests must comply by these Club Rules at all times. Refusal or continual
                    non-compliance with these Rules may result in the Club user being ejected from the premises
                    and termination of Membership if applicable. </p>
                <p>onelifefitness reserves the right to make changes to the Club Rules from time to time, and any
                    changes will be effective immediately. Copies of these Rules can be obtained from reception
                    or from the onelifefitness website at <a href=""></a><strong><a
                            href="">www.onelifefitness.com</a></strong></p>
                <p><strong>1. GUESTS</strong></p>
                <p>Any Guests to onelifefitness premises are required to sign in at reception. Guest visits are at
                    onelifefitness's absolute discretion, and availability may be restricted at peak times. onelifefitness
                    reserves the right to charge a Guest Fee for all non-Members wanting to use Club facilities.
                </p>
                <p>All Guests are required to provide photographic identification, and failure to provide this
                    may result in the Guest being denied entry to the Club.<br></p>
                <p>All Guests agree to abide by these Club Rules, and failure to do so may result in the Guest
                    being ejected from the Club. </p>
                <p>All new joiners will receive 5 single-use Guest passes to be used by non-Members. We strongly
                    advise any Guest visits to be booked 24 hours in advance. Guest passes will be valid for the
                    full term of the Member’s Membership. Any unused Guest passes will no longer be valid once
                    the Membership has been terminated. </p>
                <p>Guests are not able to book studio classes in advance, and may need to pay a fee to attend
                    the class, unless arranged via special appointment with the Club. </p>
                <p><strong>2. DRESS CODE</strong></p>
                <p>All Club users are expected to wear suitable attire at all times while using Club facilities.
                    onelifefitness considers suitable attire to be: a t-shirt or vest; shorts or trousers suitable for
                    training; and closed shoes suitable for training (with exceptions for certain class
                    activities).</p>
                <p>The following is NOT acceptable:</p>
                <ol>
                    <li>Open shoes or bare feet, unless partaking in certain classes as above</li>
                    <li>No t-shirt / lack of suitable clothing</li>
                    <li>Jeans or other inappropriate trousers </li>
                    <li>Getting changed in any area other than designated changing rooms</li>
                </ol>
                <p>Inappropriate attire can be offensive or intimidating to other Club users, and as such will
                    not be tolerated. Refusal to comply with the dress code may result in the Club user being
                    ejected from the facilities; continued refusal is a breach of the Rules and as such onelifefitness
                    may terminate the user’s Membership as stated in the Terms and Conditions. </p>
                <p>All Club users should use suitable measures to maintain basic standards of hygiene, such as
                    using deodorant, showering if needed, and wearing clean gym attire whilst training. </p>
                <p><strong>3. CONDUCT </strong></p>
                <p>All Members, Guests must conduct themselves in a respectful manner whilst using Club
                    facilities. Any Club user who acts in an intimidating, abusive, violent or disrespectful
                    manner may be ejected from the Club, and Members may have their Membership terminated with
                    no refund of fees already paid. </p>
                <p>During busy periods, Club users are to use only one weights station at a time. All weights
                    and equipment should be replaced correctly after use, and equipment and machines should be
                    wiped down. </p>
                <p>Any Club users who are unsure of how to use a piece of equipment should seek instruction
                    before doing so. onelifefitness will not take any responsibility where a Club user sustains injury
                    from incorrect use of equipment. If any equipment is found to be faulty or malfunctioning,
                    please report this to onelifefitness staff immediately. </p>
                <p>All Club users must comply with any reasonable request from onelifefitness staff in relation to
                    health and safety, and to ensure smooth running of the Club. </p>
                <p>No Club user may bring external personal trainers to the Club or act as a personal trainer,
                    paid or otherwise. onelifefitness Very Personal Trainers (VPT) have been confirmed as holding the
                    required certification and insurance, and as such are the only trainers permitted to work in
                    the Club. VPTs are independent professionals, and not employees or agents of onelifefitness.</p>
                <p>Only food and drink purchased at onelifefitness reception may be consumed on the premises.</p>
                <p>All Club users must refrain from bringing any bags on the gym floor.<br></p>
                <p>Club users are entitled to one towel per visit. Additional towels are available at £1.50
                    each.</p>
                <p><strong>4. FACILITIES </strong></p>
                <p>All Club users must exit the premises no later than the club closing time. </p>
                <p>No food or drink should be consumed in the Club, other than drinks in sealable containers.
                    Alcohol and illegal substances are prohibited from being brought into and/or consumed on the
                    premises; any Club user under the influence of alcohol or illegal substances may be ejected
                    from the Club. Smoking is prohibited on Club premises at all times. </p>
                <p>Club users may only use the boxing ring if supervised by a fully qualified onelifefitness combat
                    coach or VPT, or as part of a scheduled class. Unsupervised use is not permitted under any
                    circumstances. </p>
                <p><strong>5. CLASSES</strong></p>
                <p>onelifefitness timetables, instructors and classes are subject to change at any time. onelifefitness will
                    endeavour to provide notice when a class is unable to take place for any reason.</p>
                <p>Members are recommended to book in advance to ensure that a class has space for them. Members
                    can book online from 7am the previous day. A member cannot book onto classes that overlap
                    each other. onelifefitness operate with a "two strikes" non-attendance rule, whereby two no shows in
                    any 30 day rolling period will result in a one-week booking restriction. A member must
                    cancel a class booking at least one hour in advance of the class to avoid receiving a
                    strike. </p>
                <p>Class tickets should be collected from reception at least 5 minutes before class, with
                    participants expected to arrive into class on time. If a participant arrives late, entry to
                    the class may be denied at the absolute discretion of the class instructor. </p>
                <p>If a class is full a member can book onto a waiting list. An email will be sent confirming
                    the Members place on the waiting list. When a space becomes available members will be
                    automatically moved into the class and notified via email. Once on the waiting list Members
                    are committing to attend and could be auto booked into the class up to 1 hour before the
                    class start time. If you decide you can no longer attend please remove yourself from the
                    waiting list as a strike will be applied in line with our class cancellation policy.
                </p>
                <p>COMBAT / SPARRING GUIDELINES</p>
                <ul>
                    <li>Use of the boxing ring is only allowed during scheduled classes with a boxing coach or
                        with a personal trainer who has combat insurance and relevant experience or
                        qualifications. </li>
                    <li>All sparring must be supervised by a boxing coach who has combat insurance and relevant
                        experience or qualifications. </li>
                    <li>If there is any contact, the following kit must be worn when sparring:</li>
                    <li>Head guard</li>
                    <li>Gum shield</li>
                    <li>Weighted Gloves</li>
                    <li>All sparring is to be controlled and only two people can spar in the ring at any one
                        time.</li>
                    <li>Management reserve the right to stop sparring at any time.</li>
                </ul>
                <p><strong>6. SAUNA</strong></p>
                <p>All Club users should thoroughly read the Sauna Safety Poster, on display in each Sauna area,
                    before use. Club users should use a towel whilst in the Sauna, for the health, safety and
                    comfort of other Club users. No other items should be taken into the sauna, including books,
                    newspapers or other paper items which can create a fire hazard. </p>
                <p><strong>7. LOCKERS AND LOST PROPERTY</strong></p>
                <p>The majority of lockers in Club are for day use only; Club users are required to provide
                    their own padlock or can purchase one from reception. onelifefitness strongly advises all Club users
                    to lock their belongings in a locker at all times that they are using the Club's facilities.
                </p>
                <p>There is limited availability for permanent lockers, which can be hired for ongoing use.
                    Payments for permanent lockers must be kept up to date at all times. </p>
                <p>For any day lockers left padlocked overnight, or permanent lockers not paid for up-to-date,
                    onelifefitness reserves the right to cut the lock and remove items from the locker. Members will
                    need to see reception and pay a recovery fee, to gain access to their belongings. Any
                    belongings not picked up within a reasonable time will be kept in the Club for a reasonable
                    period and then may be donated to charity. </p>
                <p>Any property found left unattended in the Club will be kept for a reasonable period, and then
                    may be donated to charity. </p>
                <p>onelifefitness takes no responsibility for loss, theft or damage for any items brought into the Club,
                    unless onelifefitness is shown to have been negligent. onelifefitness strongly recommends Club users to
                    avoid bringing valuable items to the Club where at all possible. </p>
                <p><strong>8. GENERAL HEALTH AND SAFETY</strong></p>
                <p>If any Club user has concerns in relation to health and safety, they must report the issue to
                    onelifefitness staff immediately. </p>
            </div>
        </div>
    </div>
</div>

<!-- <div class="_3reSXV1 _4zWqqYK sDHKtsB">Loading: 100%</div>
<div class="_12c1-Jf sDHKtsB">
    <div class="LN-nIKU I7PwxVS">
        <div class="_1L5csiF">
            <div class="_1US2FDM _2rbE6TC _2v5bHvx VBD7Ow3">
                <h3 class="LsKezX_">FREE PASS</h3><span class="_2va2FpG"></span>
            </div>
            <div class="_3ITUOra _2rbE6TC _2v5bHvx VBD7Ow3" style="width: 5366.38px; transform: translateX(0px);">
                <form>
                    <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">1/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">What's your name?</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="fromName"></label><input type="text" name="fromName"
                                value="" placeholder="Name"></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">2/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">What's your email?</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="fromEmail"></label><input type="email"
                                name="fromEmail" value="" placeholder="Email"></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">3/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">What's your phone?</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="message[mobile]"></label><input type="text"
                                name="message[mobile]" value="" placeholder="Phone"></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">4/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">Select club</h2>
                        <div class="_2nxeEK_ _14NUrhZ"><label for="message[gym]"></label><select name="message[gym]">
                                <option value="select">Select</option>
                                <option value="Bank">Bank</option>
                                <option value="Cannon Street">Cannon Street</option>
                                <option value="Covent Garden">Covent Garden</option>
                                <option value="Ealing">Ealing</option>
                                <option value="Elephant and Castle">Elephant and Castle</option>
                                <option value="Farringdon">Farringdon</option>
                                <option value="Finsbury Park">Finsbury Park</option>
                                <option value="Holborn">Holborn</option>
                                <option value="Old Street">Old Street</option>
                                <option value="Victoria">Victoria</option>
                                <option value="Westfield London">Westfield London</option>
                                <option value="Westfield Stratford">Westfield Stratford</option>
                            </select></div>
                    </div>
                    <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                        <div class="Zd4QDDO">
                            <h4 class="Zd4QDDO">5/6</h4>
                        </div>
                        <h2 class="_1_KNBvC">Time to submit!</h2>
                        <p>I understand that by submitting my details I will be contacted by onelifefitness with
                            information about their services and membership options (not with spam, we promise)
                        </p><input type="submit" value="Book your free tour!">
                    </div>
                    <div class="_3ipyBdB">
                        <h2>Thanks - chat soon</h2>
                    </div>
                </form>
            </div>
            <div class="_2bIo9Yp _2rbE6TC _2v5bHvx VBD7Ow3"><button
                    class="_3pXyaiA _2S_CT_r"><span>&nbsp;</span></button><button><span>Next</span></button>
            </div>
        </div>
    </div>
</div> -->
<?php
// get_sidebar();
get_footer();
?>