<?php /* Template Name: timetable */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="">
    <div class="">
        
        <div class=" ">
            <div class="_13oom8M pRZSkbx">
                <h3 class="_3UPEWGx">Select a gym</h3>
                <ul class="_1Q0z3TV">
                    <a  href="<?= get_site_url(); ?>/timetable-bank"><li class="_1veOKBU border-0">
                        <h4>Onelifefitness Camden quay</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank"><li class="_1veOKBU bg-purple">
                    <h4>Onelifefitness Sullivans quay</h4>
                    </li></a>
                    <?php /*  <a  href="<?= get_site_url(); ?>/timetable-bank?id=3"><li class="_1veOKBU">
                    <h4>Covent Garden</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=4"><li class="_1veOKBU">
                         <h4>Ealing</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=5"><li class="_1veOKBU">
                        <h4>Elephant and Castle</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=6"><li class="_1veOKBU">
                         <h4>Farringdon</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=7"><li class="_1veOKBU">
                         <h4>Finsbury Park</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=8"><li class="_1veOKBU">
                         <h4>Holborn</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=9"><li class="_1veOKBU">
                         <h4>Old Street</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=10"><li class="_1veOKBU">
                         <h4>Victoria</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=11"><li class="_1veOKBU">
                         <h4>Westfield London</h4>
                    </li></a>
                    <a  href="<?= get_site_url(); ?>/timetable-bank?id=12"><li class="_1veOKBU">
                         <h4>Westfield Stratford</h4></a>
                    </li> */?>
                </ul>
            </div>
        </div>
    </div>
</div>