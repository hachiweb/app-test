<?php /* Template Name: yin-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Yin Yoga</h2>
        <div class="_1-4gF4V">
            <p>Sometimes you just need to take it slow – and Yin is the way to go. The strong and silent type, this
                meditative flow works for a much deeper connection than most, calming your mind and finding moments of
                still between the madness of daily life. It’s down time you’ll want to stretch out.
            </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Cannon
                        Street</span><span class="Nfa775m">Covent Garden</span><span class="Nfa775m">Ealing</span><span
                        class="Nfa775m">Farringdon</span><span class="Nfa775m">Holborn</span><span
                        class="Nfa775m">Westfield London</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Ealing">Ealing</option>
                <option value="Westfield London">Westfield London</option>
                <option value="Bank">Bank</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Holborn">Holborn</option>
                <option value="Covent Garden">Covent Garden</option>
                <option value="Cannon Street">Cannon Street</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1260707">10:30 Sunday 16th (60 minutes)</option>
                <option value="1260344" disabled="">13:00 Friday 21st (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ashley_Ahrens.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ashley</h3>
                    <div>
                        <p>Ashley’s classes are disciplined, yet nurturing. She places a great deal of emphasis on
                            connecting deep within your own body, using the breath as a tool to distract the mind,
                            paired with fascial tissue and energetic lines of awareness to understand postures on a very
                            personal level. With 9 years of dedicated practice, 400-hours of yoga training, and a
                            Biology degree, she has a lot of inspiration and knowledge to share!
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Gemma_Cousions.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Gemma - Master Trainer</h3>
                    <div>
                        <p>Gemma has worked as a professional dancer for over 10 years and found that yoga helped her to
                            find calmness and stillness, allowing her to re-balance and find space not only in the
                            physical body but also in the mind. She now wants to share her experiences and newly found
                            sensation of freedom with others, helping others to develop mindfulness and awareness of the
                            body, mind and spirit, and to find strength and overcome personal barriers.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Clare_Nagarajan.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Clare</h3>
                    <div>
                        <p>Clare is a yoga instructor who specialises in Vinyasa, Ashtanga, Yin, pregnancy and
                            children’s yoga. She loves to bring elements of her training in meditation and mindfulness
                            to her teaching, and her classes provide a moment for you take a break from everyday life.
                        </p>
                        <p><br></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_David_Olton.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">David</h3>
                    <div>
                        <p>David is an international dance and yoga instructor with a
                            background in pilates and ballet, who has worked for productions such as Comic
                            Relief and Sky FabTV. You can find David teaching Vinyasa Flow Yoga at Gymbox,
                            and can always expect enthusiasm in his classes. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Yinyoga.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>