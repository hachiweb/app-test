<?php /* Template Name: false-grip */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>


<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
                <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
                    <h2>False Grip</h2>
                    <div class="_1-4gF4V">
                        <p>The thought of an intense upper body sesh make your stomach flip? Come on, get a grip. This
                            studio-based gymnastics rings class is an ideal place to start – working on core strength
                            and basic movements until you have the confidence to freak out other first-timers with your
                            amazing skills. </p>
                    </div>
                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Cannon Street</span><span
                                    class="Nfa775m">Ealing</span><span class="Nfa775m">Elephant and Castle</span><span
                                    class="Nfa775m">Farringdon</span><span class="Nfa775m">Victoria</span><span
                                    class="Nfa775m">Westfield Stratford</span></div>
                        </li>
                    </ul>
                    <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="" disabled="">Select gym</option>
                            <option value="Westfield Stratford">Westfield Stratford</option>
                            <option value="Farringdon">Farringdon</option>
                            <option value="Cannon Street">Cannon Street</option>
                            <option value="Ealing">Ealing</option>
                            <option value="Elephant and Castle">Elephant and Castle</option>
                        </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                            <option value="" disabled="">Choose time</option>
                            <option value="1330088" disabled="">11:30 Saturday 15th (45 minutes) - Bookable 7am day
                                before</option>
                        </select><a
                            href=""
                            target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a>
                    </div>
                    <div class="_3srnCE8">
                        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                            <div class="_2Tb-We1 sDHKtsB">
                                <ul class="_2J71_L4 sDHKtsB">
                                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                        <h2>Your <br>instructors</h2>
                        <div class="Sa1FFQw">
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ben_Harrison_Extra_Option_2.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ben</h3>
                                <div>
                                    <p>Ben teaches modern, efficient,
                                        anatomical and science based yoga. He focuses on building strength, often with
                                        hand balancing - but with progressions so that every class is suitable
                                        for beginners.<br></p>
                                </div>
                            </div>
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Adrienn_Dombi.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Adrienn</h3>
                                <div>
                                    <p>Having grown up playing basketball and swimming competitively, Adrienn began a
                                        career in makeup and hairdressing before rediscovering her love for fitness. She
                                        then followed her passion to became an instructor, and now specialises in
                                        gymnastics, aerial fitness and yoga.</p>
                                </div>
                            </div>
                            <div class="_3fJiN_C">
                                <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                        src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Flavio_Pistori.jpg"
                                        class="_3h_jpHd _2jVkQgE"></div>
                                <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Flavio</h3>
                                <div>
                                    <p>Having trained in artistic gymnastics from an early age,
                                        Flavio believes that hard work, determination and passion are the keys to
                                        success.
                                        Flavio uses gymnastic techniques in his pilates classes to help you improve
                                        your flexibility and balance, whilst building a strong core.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wqBYWJi">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                            style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/5_180830_170325.jpg');"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>