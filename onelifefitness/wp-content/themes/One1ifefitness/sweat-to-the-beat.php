<?php /* Template Name: sweat-to-the-beat */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Sweat to the Beat</h2>
        <div class="_1-4gF4V">
            <p>Whether you’re a slave to the rhythm or addicted to bass, this catchy little conditioning class is
                guaranteed to get peak fitness pouring from every pore. This is bodyweight endurance to a beat,
                challenging your strength, sculpting your figure and making music pulse through every muscle. The floor
                is yours. </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Covent Garden</span><span
                        class="Nfa775m">Holborn</span><span class="Nfa775m">Victoria</span><span
                        class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Holborn">Holborn</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Victoria">Victoria</option>
                <option value="Covent Garden">Covent Garden</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1191147" disabled="">17:30 Monday 17th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hattie_Grover_Extra_Option_2.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hattie</h3>
                    <div>
                        <p>Hattie's training began at the age
                            of 3 when she started taking ballet lessons. As her
                            love for dance blossomed she began performing, and immersed herself in a variety
                            of different dance styles specialising in contemporary and breaking. Whilst at
                            onelifefitness she has broadened her knowledge in the Aerial Arts and is fully committed to sharing
                            her love for everything aerial, acrobatic and upside
                            down!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jai_Morrison.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jai</h3>
                    <div>
                        <p> Performing and dancing are Jai's passion, so helping others work on their own health and
                            fitness is a huge motivation for why she continues to work as an instructor. Jai loves
                            seeing members push past their own boundaries and leave glowing knowing they have given it
                            their all!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Matteo_Cruciani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Matteo</h3>
                    <div>
                        <p>A native Italian, Matteo was born and raised in the city of Rome.&nbsp;He grew up with a huge
                            passion for fitness, dance and movement.&nbsp;He is an&nbsp;enthusiastic and charismatic
                            person who brings a positive attitude to every situation!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                    <div>
                        <p>First qualified in aqua fitness aged 17, Hester worked in film production and then re-trained
                            as a contemporary dancer. Instructing pilates, fitness &amp; dance supported her through BA
                            &amp; MSc studies at Laban, and brought her to onelifefitness in 2004. Fitness, wellness and aerial
                            arts have guided Hester throughout her career. Possibly unequalled in her passion for
                            teaching and sharing the knowledge accumulated over more than 3 decades in the fitness
                            world, Hester is onelifefitness Aerial Series Master Trainer and creator of CircusFit Academy.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Sweattothebeat_New.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>