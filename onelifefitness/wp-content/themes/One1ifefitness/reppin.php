<?php /* Template Name: reppin */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Reppin'</h2>
        <div class="_1-4gF4V">
            <p>When it comes to lifting, we don’t all like it heavy and slow. If it’s plenty of pumping and a cranked up
                tempo that gets you going, these low-weight loads provide all the sweaty, sculpting, rep-fuelled rough
                n' tumble you need. It’s impossible to not look in the mirror and feel yourself after this.</p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Covent
                        Garden</span><span class="Nfa775m">Ealing</span><span class="Nfa775m">Elephant and
                        Castle</span><span class="Nfa775m">Farringdon</span><span class="Nfa775m">Holborn</span><span
                        class="Nfa775m">Old Street</span><span class="Nfa775m">Victoria</span><span
                        class="Nfa775m">Westfield London</span><span class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Old Street">Old Street</option>
                <option value="Westfield London">Westfield London</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Ealing">Ealing</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Victoria">Victoria</option>
                <option value="Bank">Bank</option>
                <option value="Covent Garden">Covent Garden</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Holborn">Holborn</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1210867">10:15 Sunday 16th (45 minutes)</option>
                <option value="1261442" disabled="">13:00 Monday 17th (45 minutes) - Bookable 7am day before</option>
                <option value="1275069" disabled="">18:30 Tuesday 18th (45 minutes) - Bookable 7am day before</option>
                <option value="1209878" disabled="">07:45 Wednesday 19th (45 minutes) - Bookable 7am day before</option>
                <option value="1210328" disabled="">17:30 Thursday 20th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Vitor_Fernandes.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Vitor</h3>
                    <div>
                        <p>Former Brazilian professional dancer and Capoeira martial artist, Vitor has brought his
                            energy and enthusiasm to onelifefitness. In his classes you'll be pushed to your limits but he'll
                            ensure you have fun on that journey with his charisma and the most epic tunes carrying you
                            through. Vitor brings his vast experience and passion to teaching, making sure you walk out
                            sweating with a smile each and every class.</p>
                        <p><u></u></p>
                        <p><u><u><br></u></u></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Natalia_Kotowska_Extra_Option_1.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Natalia</h3>
                    <div>
                        <p>Originally from
                            Poland,&nbsp;Natalia&nbsp;has represented London and Poland internationally in
                            dance battles, panels and workshops, and in 2015&nbsp;gained&nbsp;a degree in
                            Dance Urban Practice.&nbsp;Natalia teaches across our Look Better Naked, Combat
                            Sports and School of Dance categories. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Geraldine_Gishen_Extra_Option_1.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Geraldine</h3>
                    <div>
                        <p>Geraldine’s classes are all about hard work and self belief. You can get through your
                            set...in fact you’re going to smash your set! Be prepared to sweat, learn new skills and
                            have fun!&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Biannca_Pegrume.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Bianca</h3>
                    <div>
                        <p>Whilst studying Zoology at university, Bianca admits she
                            missed more than one lecture to fit in a gym session. Having discovered her
                            passion for fitness, she went on to become a qualified studio instructor and
                            PT, and now loves every minute teaching at onelifefitness.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jai_Morrison.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jai</h3>
                    <div>
                        <p> Performing and dancing are Jai's passion, so helping others work on their own health and
                            fitness is a huge motivation for why she continues to work as an instructor. Jai loves
                            seeing members push past their own boundaries and leave glowing knowing they have given it
                            their all!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Luke_Morton.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Luke</h3>
                    <div>
                        <p>As well as being a professional actor, Luke is a qualified
                            fitness instructor and PT who’s love for fitness stems from playing sports at
                            an early age. Luke intertwines elements of boxing and HIIT into his workouts,
                            and loves seeing his clients develop and push themselves. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Melissa_Powers.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Melissa</h3>
                    <div>
                        <p>&nbsp;Having taught nearly 10,000 classes in her career so far, it's safe to say Melissa
                            knows pretty much everything there is about how to get the absolute best out of her clients.
                            Whether it's pedalling furiously on a bike, lifting weights, jumping up and down or lying on
                            a mat swearing through a particularly tough ab routine, Melissa will be there, cheering
                            enthusiastically, doing it all with you, and making you feel like exercising is actually a
                            lot less horrible than you'd imagined! </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                    <div>
                        <p>Through a combination of tough-love and motivation, Stevie will push you to your limits with
                            high-energy metabolic conditioning workouts that will leave you in a pool of sweat and a big
                            smile on your face.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Amina_Bedics.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Amina</h3>
                    <div>
                        <p>If you see a tiny girl bouncing on
                            the spin bike or shouting in the studio , that’s Amina! She loves to
                            teach classes to the beat so get ready to sweat to the best tunes! Don’t be fooled by her
                            smile, she’ll be kicking asses and make sure you work hard in her classes. The motto is ass
                            to the grass!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Yvette_Carter_Extra_Option_4.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Yvette C</h3>
                    <div>
                        <p>Having earned a degree in theatre dance from the London
                            Studio Centre, Yvette went on to become a qualified PT and is now a studio
                            instructor at onelifefitness. Yvette’s philosophy is that training should be fun with
                            hardwork thrown in.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                    <div>
                        <p>Firas found his passion for American Football 15 years ago, and has since won numerous
                            championships as both a player and coach. Firas brings his enthusiasim and camaraderie to
                            all his classes, and will undoubtedly bring out the inner athlete in you. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                    <div>
                        <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                            &nbsp;classes range from Gains to Escalate, and are based around personal development in a
                            motivational team environment. His love for fitness is second only to his love of the onelifefitness
                            team.
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_James_Dawkins.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">James - Master Trainer</h3>
                    <div>
                        <p>London based House DJ and our Look Better Naked Master Trainer, James is passionate about
                            leading a team that will help you not only look great naked, but also feel great naked. Hop
                            into any of his classes and you'll quickly learn why we're the antidote to boring gyms! </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                    <div>
                        <p>First qualified in aqua fitness aged 17, Hester worked in film production and then re-trained
                            as a contemporary dancer. Instructing pilates, fitness &amp; dance supported her through BA
                            &amp; MSc studies at Laban, and brought her to onelifefitness in 2004. Fitness, wellness and aerial
                            arts have guided Hester throughout her career. Possibly unequalled in her passion for
                            teaching and sharing the knowledge accumulated over more than 3 decades in the fitness
                            world, Hester is onelifefitness Aerial Series Master Trainer and creator of CircusFit Academy.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Daisy_Rusby.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Daisy</h3>
                    <div>
                        <p>A lover of lifting heavy things and dancing to her hearts content, Daisy is a PT and studio
                            instructor who’s specialities include kettlebells, strength training and holistic
                            flexibility. Her classes are based on teamwork and guaranteed to help you reach your fitness
                            potential.&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB">
                            <span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/_galleryImage/RippedStripped_New.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>