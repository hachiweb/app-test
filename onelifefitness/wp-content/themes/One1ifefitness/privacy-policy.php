<?php /* Template Name: privacy-policy */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>


<div class="_1vdzHPH">
    <div class="_2dDJkQd VBD7Ow3">
        <div>
            <div>
                <h1>Privacy Policy</h1>
                <p>In this privacy policy, “onelifefitness” whose
                    registered office is ONE LIFE FITNESS 11 MULGRAVE ROAD CORK T23AC91.
                    onelifefitness believe in transparency, and are committed to being upfront about privacy practices,
                    including how your personal information is treated.
                    onelifefitness have undertaken an extensive exercise to ensure ongoing compliance with the European
                    Union General Data Protection Regulation (EU GPDR). onelifefitness have ensured all current business
                    processes and procedures are in line with the GDPR rights and principles and any new
                    processes involving personal information that are implemented will be thoroughly reviewed
                    through a Data Protection Impact Assessment (DPIA) prior to proceeding. All company data
                    protection procedures and policies are reviewed on an annual basis to ensure compliance.
                </p>
                <p>
                    This Privacy Policy sets out the data processing practices carried out through the use of
                    the Internet and any other electronic communication networks by onelifefitness, as well as
                    describing the types of personal information that onelifefitness may collect about you and the
                    purposes for which this information is used. </p>
                <p>
                    1. Accepting the Privacy Policy<br>
                    2. Information Collected or Received<br>3. How and why onelifefitness use your Personal
                    Information<br>4. Communication from onelifefitness<br>5. Sharing your Information<br>6. Keeping
                    your Information Secure<br>7. onelifefitness Retention Policies<br>8. Staying in Control - Your
                    Rights<br>9. Privacy Policy Changes<br>10. Contact</p>
                <h4>1. Accepting the Privacy Policy</h4>
                <p>onelifefitness need to process your personal information to run their business and provide you with
                    their services. Upon visiting onelifefitness in Club, contacting onelifefitness by the phone, or
                    visiting
                    the website, (<a href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>), you are
                    accepting and
                    consenting to the practices described in this Privacy Policy.</p>
                <p>onelifefitness are based in the UK, however process personal information both within the UK/EEA and
                    outside. Any personal information held by either onelifefitness or the designated third parties
                    comply with relevant EU GDPR legislations. Third parties which process personal information
                    outside the UK/EEA on behalf of onelifefitness have a recognised equivalent legislation in place
                    such as the US Privacy Shield, or a set of GDPR compliant Binding Corporate Rules (BCR).</p>
                <h4>2. Information Collected or Received</h4>
                <p>onelifefitness collect personal information from Club users, Staff and Suppliers/Sub-contractors to
                    run their business and provide you with their services. This personal information may
                    include the following: name, date of birth, email address, contact number, company name,
                    bank details and credit card details.
                </p>
                <p>onelifefitness also collect the following information:
                </p>
                <p>Sensitive Information: The term "sensitive information" in this context refers to information
                    related to your racial or ethnic origin, political opinions, religion or other beliefs,
                    health, criminal background or trade union membership. Whilst onelifefitness do not generally
                    collect sensitive information unless it is volunteered by you, onelifefitness do have a legal
                    requirement to collect health data for the purpose of recording your self-assessment
                    declaring readiness for physical exercise.</p>
                <p>Photographs and Identification: In the interests of security and the prevention of crime,
                    onelifefitness may take a digital photograph of each Member or guest. Each Member or guest may also
                    be required to provide a form of identification for verification and security purposes. </p>
                <p>Audio-Video: onelifefitness use CCTV in all Clubs for health and security reasons. If you have any
                    queries in relation to the use of CCTV operating in and around the clubs please contact <strong>info@onelifefitness.ie</strong>.
                    onelifefitness have a business legitimate interest to monitor service standards in Club.
                    utilise mystery shopper services in which Club users may appear in the background however
                    are never directly filmed. This footage is only used for the purposes of internal monitoring
                    and training of onelifefitness staff.</p>
                <p>Digital: When visiting the onelifefitness website (<a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>) your
                    personal information may be
                    collected, stored and used such as traffic data, location data, web logs, communication data
                    and resources that you access, as well as other personal information detailed above. If you
                    connect to onelifefitness or register for a tour of onelifefitness using an external third-party
                    application, such as Facebook, Instagram, or Twitter, these websites will have their own
                    privacy statement which onelifefitness suggest that you read before giving them your personal
                    information. Connecting to onelifefitness via a third-party application or service is optional and
                    at your own discretion.</p>
                <p>onelifefitness will only collect the relevant information required for the purposes of processing and
                    will not use this information for any other purpose without obtaining consent.</p>
                <h4>3. How and why onelifefitness use your Personal Information</h4>
                <p>The information in the above, section 2, may be used for the following purposes:</p>
                <p>- To carry out onelifefitness’s obligations arising from any contractual agreement;<br>- To contact
                    you about non-contract aspects of your Membership, such as a change in usage patterns;<br>-
                    To provide you with the information on products or services you request;<br>- To process
                    payments and maintain accounts and records;<br>- To prevent crime, fraud and aid in the
                    prosecution of offenders;<br>- To maintain Membership records;<br>- To improve the onelifefitness
                    platform;<br>- To prevent or detect abuses of the onelifefitness website;<br>- To create business
                    performance statistics and analysis;<br>- To enable third parties to carry out technical,
                    logistical, research or other functions on behalf of onelifefitness;<br>- To send you newsletters
                    and promotions, prize draws, and competitions;<br>- To conduct surveys and request
                    feedback;<br>- To notify you about urgent comms such as a sudden closure of a Club;<br>- To
                    notify you about changes to onelifefitness Services, Terms and Conditions, Privacy Policy and Club
                    rules;<br>- To process your job application if you apply for employment at onelifefitness;<br>- To
                    collect information about your tastes and preferences, both when you tell us and by analysis
                    of customer traffic, including using "cookies";<br>- To read and respond to comments made
                    regarding onelifefitness services.</p>
                <p>Other than as set out in this Privacy Policy onelifefitness will not use your personal information
                    for any other purpose without your consent, unless required to do so by law.</p>
                <h4>4. Communcation from onelifefitness</h4>
                <p>On occasion, onelifefitness may need to contact you. Primarily, these messages are delivered by
                    email, text or phone, and every individual’s record is required to keep a valid email
                    address and contact number on file to receive these messages.</p>
                <p>In response to enquiries and when communicating with Members, onelifefitness believe that the content
                    is relevant, valuable, interesting and beneficial to you. onelifefitness also believe that you would
                    reasonably expect to receive the type of content that is sent to you as part of your
                    relationship with onelifefitness. Therefore, onelifefitness’s current assessment is that the
                    communication
                    you receive is covered by the lawful basis for processing of ‘Legitimate Interest’ under
                    recital 47 within the GPDR. For all communication sent under the basis of legitimate
                    interest, opt-out options are provided and detailed later within this section.</p>
                <p>For all communication where consent is the only legal basis, preferences will be collected in
                    advance.</p>
                <p>onelifefitness recognise that you value having control over your own information, so onelifefitness
                    gives you
                    the choice of editing your communication preferences if you disagree with the above. You may
                    update these preferences by logging into the Members area through the login page of the
                    website, (<a href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>). Alternatively you
                    may email
                    <strong>info@onelifefitness.ie</strong> with your
                    request.</p>
                <p>Please note the following messages from onelifefitness fall into the category of compulsory
                    communication and therefore no opt-out options are available: </p>
                <p>- Urgent Communication (such as, unplanned closure of all or part of a Club, reduced
                    services, a change of opening times)</p>
                <p>- Automatic Class Booking communication (such as, booking confirmation, waiting list
                    movement, cancellations)</p>
                <p>- Contract related communication (such as, welcome emails, outstanding arrears, upcoming
                    renewals, communication with the onelifefitness Member Services Support Team).</p>
                <p>You will receive communication from onelifefitness via the following communication systems, all of
                    which offer an opt-out service. Please note you must opt-out of each individual
                    communication system should you wish to exercise that right;</p>
                <p>Direct email communication from onelifefitness staff via Outlook: To opt out you can do either of the
                    following, log into the members area on the website to update your communication preferences
                    within the My Profile section. You can also request this specifically to <strong>info@onelifefitness.ie</strong></p>
                <p>Texts via GlobalMessaging: To opt-out please follow the instructions within the text you
                    receive.</p>
                <p>Email communication via SendinBlue: To opt-out please use the unsubscribe link within the
                    email you receive.</p>
                <h4>5. Sharing your Information</h4>
                <p>Information about onelifefitness Club users is an important part to the business and onelifefitness
                    do not
                    sell it to others. onelifefitness shares member information only as described in this Privacy
                    Policy.</p>
                <p>onelifefitness have business partners and employ other companies and individuals to perform certain
                    functions on onelifefitness behalf. Examples include managing our Membership database, sending
                    texts, e-mails, analysing data, providing marketing assistance, providing personal training
                    and providing debt collection assistance. Third party service providers only have access to
                    the personal information relevant for performing their functions, but will not use it for
                    other purposes. Additionally, they must process the personal information in accordance with
                    this Privacy Policy and as permitted under the EU GDPR. onelifefitness will only disclose personal
                    information to reputable companies and suppliers who process data on onelifefitness behalf.</p>
                <p>Arrears Collection: onelifefitness reserves the right to forward a members information to a third
                    party debt collection agency (ARC Europe Ltd) in the event of non-payment of fees when due.
                    Further information on this process can be found in onelifefitness Terms and Conditions.</p>
                <p>Personal Training: As stated on the onelifefitness Membership Application, any Member who selects the
                    VPT Taster understands that their personal information will be provided to a designated
                    personal trainer who will contact them to arrange their taster session.</p>
                <p>Paperwork Destruction: onelifefitness schedule regular collections of paperwork from a third party
                    company (The Hill Company) who securely and confidentially shred this paperwork off site, on
                    behalf of onelifefitness.</p>
                <p>onelifefitness release account and other personal information to third parties when onelifefitness
                    believe
                    release is appropriate to comply with the law; enforce or apply membership or other
                    agreements; or protect the rights, property or safety of onelifefitness, its users or others. This
                    includes exchanging information with other companies and organisations for fraud protection
                    and credit risk reduction. This does not include selling, sharing or otherwise disclosing
                    personally identifiable information from members for commercial purposes in a way that is
                    contrary to the commitments made in this Privacy Policy.</p>
                <p>onelifefitness may share Member details with any organisation that acquires a Club to which the
                    Member has their Membership.</p>
                <p>Other Websites: The onelifefitness website may contain links to other websites that are outside
                    onelifefitness control and are not covered by this Privacy Policy. If you access other sites using
                    the links provided, the operators of these sites may collect information from you that will
                    be used by them in accordance with their privacy policy, which may differ from this policy.
                </p>
                <p>Cookies: A cookie is a small piece of information sent by a web server to a web browser,
                    which enables the server to collect information from the browser. Find out more about
                    cookies on <a href="https://onelifefitness.ie/"
                        rel="noreferrer noopener">https://onelifefitness.ie/</a>
                    onelifefitness use cookies to identify you when you visit this website and to keep track of your
                    browsing patterns and build up a demographic profile.
                    onelifefitness use cookies to allow registered users to be presented with a personalised version of
                    the site, carry out transactions and have access to information about their account.
                    Most browsers will allow you to turn off cookies. If you want to know how to do this please
                    look at the menu on your browser, or look at the instruction on <a href="https://onelifefitness.ie/"
                        rel="noreferrer noopener">https://onelifefitness.ie/</a>. Please
                    note however that turning off cookies may restrict your use of our website. By continuing to
                    use this site you are agreeing to the use of cookies as detailed within this section.</p>
                <h4>6. Keeping your Information Secure</h4>
                <p>In regards to environmental and physical security, all onelifefitness employees receive full training
                    upon commencement on employment and subsequently on an annual basis thereafter. This is
                    completed via an e-learning platform and group training sessions. Further to this, daily,
                    weekly and monthly audits are completed.</p>
                <h4><strong>7. onelifefitness Retention Policies</strong></h4>
                <p>onelifefitness have set company retention policies in place and these timescales are set in
                    accordance with any applicable legislation and/or for any agreed legitimate reasons. Where
                    none exists, then onelifefitness will keep your information for the duration of any Contract that
                    you have entered into with onelifefitness, and then for a period of 7 years after, at which time all
                    the personal information will be pseudonymised.</p>
                <p> After that 7 year duration, onelifefitness will retain and use your pseudonymised information for
                    the purpose of business statistics and analysis. onelifefitness can retract this pseudonymisation to
                    the extent necessary to comply with any legal obligations or to resolve disputes</p>
                <h4>8. Staying in Control - Your Rights</h4>
                <p>onelifefitness understand the importance of data subjects remaining in control of their personal
                    data. onelifefitness acknowledge the following rights you have under the GDPR, what they mean and
                    how you can exercise them.</p>
                <p>
                    <strong>The Right to be Informed</strong>
                </p>
                <p>This privacy policy states all uses of your personal information and the purposes for
                    processing this information. Should you have any concerns or questions about our privacy
                    policy and practices, or would like a full list of our third party processors, then please
                    email <strong>info@onelifefitness.ie</strong> </p>
                <p><strong>The Right of Access</strong>
                </p>
                <p>You have the right to access information that onelifefitness hold about you. If you wish to receive a
                    copy of the information that onelifefitness hold, please submit a Subject Access Request form (SAR)
                    which you can request from <strong>info@onelifefitness.ie</strong>. Once
                    onelifefitness
                    have received your form, they will provide a response within one month. If your request is
                    unusually complex and likely to take longer than a month, you will be informed as soon as
                    possible to tell you how long it’s likely to take.
                </p>
                <p>Please note that whilst in most cases onelifefitness will be happy to provide you with copies of the
                    information you request, onelifefitness nevertheless reserve the right, in accordance with section
                    8(2) of the DPA, not to provide you with copies of information requested if to do so would
                    take “disproportionate effort”, or in accordance with Article 12 of the GDPR to charge a fee
                    or refuse the request if it is considered to be “manifestly unfounded or excessive”.
                </p>
                <p><strong>The Right to Rectification, Restrict Processing, Erasure, and to Object</strong>
                </p>
                <p>You can ask onelifefitness at any time to change, amend or pseudonymise the information that
                    onelifefitness
                    hold about you or restrict ways in which your data may be processed.
                    You can update onelifefitness with amendments of personal information by submitting a request
                    through the member services area of our website using the following link, <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>
                </p>
                <p>To pseudonymise the information, or object/request restriction of processing then please
                    email <a href="">mypersonaldata@onelifefitness.com</a></p>
                <p>onelifefitness will aim to respond to any request as soon as possible, but no later than within 1
                    month since receipt of request. Please note that the right to erasure is not absolute and
                    only applies in certain circumstances. For further information on this please visit the
                    following link to the ICO’s website, <a
                        href="https://onelifefitness.ie/">https://onelifefitness.ie/</a>
                </p>
                <p><strong>Right to Data Portability</strong></p>
                <p>You have the right to request that your personal data is transferred by onelifefitness to another
                    organisation (this is called “data portability”). Please contact us at <strong>info@onelifefitness.ie</strong> with the
                    details of what you would like for onelifefitness to do and onelifefitness will endeavour to comply
                    with
                    your request. It may not be technically feasible, but onelifefitness will work with you to try and
                    find a possible solution.
                </p>
                <p><strong>Right to Prevent Automated Decision Making</strong>
                </p>
                <p>You have a right to ask onelifefitness to stop any automated decision making. onelifefitness do not
                    intentionally carry out such activities, but if you do have any questions or concerns onelifefitness
                    would be happy to discuss them with you so please email any concerns or queries to <strong>info@onelifefitness.ie</strong>.
                </p>
                <h4>9. Privacy Policy Changes</h4>
                <p>onelifefitness may make slight changes to the Privacy Policy, Terms and Conditions and Club Rules,
                    however, will never materially change policies and practices to make them less protective of
                    member information collected in the past without the consent of affected members. onelifefitness
                    will display signage with details of any changes to these notices and conditions, but an up
                    to date version will always be available on the website (<a
                        href="https://onelifefitness.ie/">www.onelifefitness.com</a>).</p>
                <h4>10. Contact</h4>
                <p>If you have any questions, comments or concerns about data privacy at onelifefitness, please e-mail
                    thorough a description to <strong>info@onelifefitness.ie</strong> and
                    onelifefitness
                    will endeavour to resolve the issue for you.
                    Alternatively should you wish to escalate any concerns or questions, rather than contact
                    onelifefitness directly, please contact the supervisory authority The Information Commissioner’s
                    Office (ICO). For more information please visit the ICO’s website <u><a
                            href="https://onelifefitness.ie/">https://onelifefitness.ie/</a></u>.</p>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>