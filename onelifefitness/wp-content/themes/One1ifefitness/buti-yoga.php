<?php /* Template Name: buti-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Buti Yoga</h2>
        <div class="_1-4gF4V">
            <p>If you’ve got the feet for a beat and a feel for the flow, we’ve got the class to fuel your most primal
                urges. Fusing power yoga, tribal dance and plyometrics, this high-intensity workout is flexin’ sweaty
                and so butilicious. Can you handle it?</p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Elephant
                        and Castle</span><span class="Nfa775m">Victoria</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Victoria">Victoria</option>
                <option value="Bank">Bank</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1313938" disabled="">11:30 Sunday 16th (45 minutes) - Bookable 7am day before</option>
                <option value="1318880" disabled="">19:00 Tuesday 18th (30 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Rachel_Tailbone-Potter.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Rachel</h3>
                    <div>
                        <p>Rachel is a Buti Yoga and Vinyasa trained instructor.&nbsp; She has a background in dance and
                            theatre.&nbsp; She discovered a love for Buti Yoga while living in Los Angeles, studying at
                            the American Academy of Arts.&nbsp; It was Buti yoga that kicked off a career in yoga and
                            wellness.&nbsp; Come shake your asana with Rachel!!!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Gemma_Cousions.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Gemma - Master Trainer</h3>
                    <div>
                        <p>Gemma has worked as a professional dancer for over 10 years and found that yoga helped her to
                            find calmness and stillness, allowing her to re-balance and find space not only in the
                            physical body but also in the mind. She now wants to share her experiences and newly found
                            sensation of freedom with others, helping others to develop mindfulness and awareness of the
                            body, mind and spirit, and to find strength and overcome personal barriers.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                    <div>
                        <p>First qualified in aqua fitness aged 17, Hester worked in film production and then re-trained
                            as a contemporary dancer. Instructing pilates, fitness &amp; dance supported her through BA
                            &amp; MSc studies at Laban, and brought her to onelifefitness in 2004. Fitness, wellness and aerial
                            arts have guided Hester throughout her career. Possibly unequalled in her passion for
                            teaching and sharing the knowledge accumulated over more than 3 decades in the fitness
                            world, Hester is onelifefitness Aerial Series Master Trainer and creator of CircusFit Academy.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Buti_Yoga-4.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>