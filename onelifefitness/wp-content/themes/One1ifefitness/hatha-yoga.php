<?php /* Template Name: hatha-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Hatha Yoga</h2>
        <div class="_1-4gF4V">
            <p>Reality kicking you where it hurts? Consider this your antidote to urban life. Through a deliciously slow
                and focused flow, we’ll ease your body and rebalance your mind – while more challenging poses build your
                endurance and strength. This is yoga that anyone can try, so come and hatha go if you’re calm enough.
            </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Cannon
                        Street</span><span class="Nfa775m">Covent Garden</span><span class="Nfa775m">Ealing</span><span
                        class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Farringdon</span><span
                        class="Nfa775m">Holborn</span><span class="Nfa775m">Old Street</span><span
                        class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                        class="Nfa775m">Westfield Stratford</span>
                </div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Victoria">Victoria</option>
                <option value="Cannon Street">Cannon Street</option>
                <option value="Ealing">Ealing</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Bank">Bank</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Holborn">Holborn</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Old Street">Old Street</option>
                <option value="Westfield London">Westfield London</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1314094" disabled="">12:45 Sunday 16th (60 minutes) - Bookable 7am day before</option>
                <option value="1329553" disabled="">12:00 Monday 17th (45 minutes) - Bookable 7am day before</option>
                <option value="1329604" disabled="">19:00 Monday 17th (45 minutes) - Bookable 7am day before</option>
                <option value="1315966" disabled="">07:00 Tuesday 18th (45 minutes) - Bookable 7am day before</option>
                <option value="1316330" disabled="">12:15 Tuesday 18th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ashley_Ahrens.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ashley</h3>
                    <div>
                        <p>Ashley’s classes are disciplined, yet nurturing. She places a great deal of emphasis on
                            connecting deep within your own body, using the breath as a tool to distract the mind,
                            paired with fascial tissue and energetic lines of awareness to understand postures on a very
                            personal level. With 9 years of dedicated practice, 400-hours of yoga training, and a
                            Biology degree, she has a lot of inspiration and knowledge to share!
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ben_Harrison_Extra_Option_2.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ben</h3>
                    <div>
                        <p>Ben teaches modern, efficient,
                            anatomical and science based yoga. He focuses on building strength, often with hand
                            balancing - but with progressions so that every class is suitable
                            for beginners.<br></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Devina_Vig.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Devina</h3>
                    <div>
                        <p>Devina provides a holistic approach to yoga&nbsp;offering both dynamic and gentle classes
                            promoting body, mind and breath control.<br></p>
                        <p>Her background in&nbsp;highly competitive sports acrobatics&nbsp;and cheerleading from a
                            young age has&nbsp;inspired her to include more advanced postures in some of her classes to
                            challenge students both physically and mentally.&nbsp; &nbsp;&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Paulius_Savickas.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Paulius</h3>
                    <div>
                        <p>Paulius is a multi-style yogi teacher from Lithuania, explorer of the body, mind and the
                            creative spirit. His yoga classes are the extensions of his own practice of yoga, meditation
                            and the healing arts. Paulius believes that if we take a good care of our bodies and minds –
                            our inner spirit can express itself in the infinite ways of happiness. His mission is to
                            deliver you a safe and informed guidance to reach that goal.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Clare_Nagarajan.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Clare</h3>
                    <div>
                        <p>Clare is a yoga instructor who specialises in Vinyasa, Ashtanga, Yin, pregnancy and
                            children’s yoga. She loves to bring elements of her training in meditation and mindfulness
                            to her teaching, and her classes provide a moment for you take a break from everyday life.
                        </p>
                        <p><br></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Laura_Pearce.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Laura</h3>
                    <div>
                        <p>Having studied under the renowned Swami Niralananda Giri at the
                            Sivananda Yoga centre, Laura holds the highest certification awarded by Yoga
                            Alliance UK. Laura’s classes are challenging and dynamic, based on practises
                            that detoxify and restore the body and mind.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Julie_Johnson.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Julie</h3>
                    <div>
                        <p>A professional dancer and chereographer, Julie trained in
                            performance at Stella Mann College and is a qualified Hot Power Yoga
                            instructor. Julie has taught dance for over 10 years, and her credits include
                            dance captain on ‘The World’s End’ movie. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_David_Olton.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">David</h3>
                    <div>
                        <p>David is an international dance and yoga instructor with a
                            background in pilates and ballet, who has worked for productions such as Comic
                            Relief and Sky FabTV. You can find David teaching Vinyasa Flow Yoga at Gymbox,
                            and can always expect enthusiasm in his classes. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Hatha.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>