<?php /* Template Name: gym */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div class="o15HL62 _2v5bHvx">
                <div class="_1KSLoAe">
                    <div class="_1JOjVV5 _2v5bHvx">
                        <div class="e_U31UT sDHKtsB mapboxgl-map" id="map"></div>
                    </div>
                    <div class="_2Ey81ib">
                        <ul class="_3_31gDn _2rbE6TC _2v5bHvx">
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-0">
                                <a class="_2HzoBao _2rbE6TC _2v5bHvx" href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Onelifefitness Camden quay</h4>
                                </a>

                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 10pm<br>Sat: 9am – 7pm <br>Sun: 10am - 5pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a href="tel:(021) 233 9415">(021)
                                                    233 9415</a><br><a href="mailto:info@onelifefitness.ie"
                                                    class="_2h5s4t1">info@onelifefitness.ie</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">Onelifefitness <br>11 Mulgrave Road, Cork,
                                                <br>T23AC91 <a
                                                    href="https://www.google.com/maps/place/One+Life+Fitness/@51.901338,-8.473495,17z/data=!4m5!3m4!1s0x0:0x5f4854e69cea186b!8m2!3d51.9013384!4d-8.4734947?hl=en"
                                                    target="_blank" class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Boxing Ring<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Matted Combat
                                                    Area<br></span><span>Group Cycle Studio<br></span><span>Resident
                                                    DJs<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/bank"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>

                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-1">
                                <a class="_2HzoBao _2rbE6TC _2v5bHvx" href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3 gold-clr">Onelifefitness Sullivans quay</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 10pm<br>Sat: 9am – 7pm <br>Sun: 10am - 5pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a href="tel:(021) 233 9415">(021)
                                                    233 9415</a><br><a href="mailto:info@onelifefitness.ie"
                                                    class="_2h5s4t1">info@onelifefitness.ie</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                2 Sullivan's Quay, <br>Drinan St, Centre, <br>Cork, T12 RW90
                                                <a href="https://www.google.com/maps/place/One+Life+Fitness/@51.901338,-8.473495,17z/data=!4m5!3m4!1s0x0:0x5f4854e69cea186b!8m2!3d51.9013384!4d-8.4734947?hl=en"
                                                    target="_blank" class="_1x8JHAI _3Pq3GhV _3xVoYzA">
                                                    <span>View on Map</span></a>
                                                <a href="" target="_blank" class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View
                                                        on citymapper</span></a>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span><span>Functional Fitness<br></span><span>Holistic
                                                    Studio<br></span><span>Dance &amp; Conditioning
                                                    Studio<br></span><span>Group Cycle Studio<br></span><span>Resident
                                                    DJs<br></span><span>Strength &amp; Conditioning<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/cannon-street"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>

                            <!-- <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-2"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Fit For Less</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 10am – 6pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)20 7395 0270">+44 (0)20 7395 0270</a><br><a
                                                    href="mailto:coventgardenenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">coventgardenenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">South Terrace, <br>Cork city <a
                                                    href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Boxing Ring<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Matted Combat
                                                    Area<br></span><span>Group Cycle Studio<br></span><span>Resident
                                                    DJs<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/covent-garden"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=3"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li> -->

                            <!-- <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-3"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Ealing</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am - 11pm</p>
                                                    <p>Sat to Sun: 8am - 8pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)20 3973 1118">+44 (0)20 3973 1118</a><br><a
                                                    href="mailto:ealingenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">ealingenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">Unit 15 Dickens Yard,<br>Longfield
                                                Ave<br>W5 2TD<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Sprint
                                                    Track<br></span><span>Cardio<br></span><span>Very Personal
                                                    Training<br></span><span>Olympic Lifting<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Group Cycle
                                                    Studio<br></span><span>Resident DJs<br></span><span>Boxing
                                                    Ring<br></span><span>Strength &amp; Conditioning<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/ealing"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=4"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li> -->
                            <!--<li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-4"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Elephant and Castle</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 8am – 8pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)203 912 3450">+44 (0)203 912 3450</a><br><a
                                                    href="mailto:elephantenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">elephantenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">Unit 7<br>38 New Kent Road<br>SE1
                                                6TJ<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Sprint
                                                    Track<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span><span>Functional Fitness<br></span><span>Holistic
                                                    Studio<br></span><span>Dance &amp; Conditioning
                                                    Studio<br></span><span>Matted Combat Area<br></span><span>Group
                                                    Cycle Studio<br></span><span>Resident DJs<br></span><span>Boxing
                                                    Ring<br></span><span>Strength &amp; Conditioning<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/elephant-castle"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=5"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-5"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Farringdon</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 10am – 6pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a href="tel:44 (0)203 872 5870">44
                                                    (0)203 872 5870</a><br><a
                                                    href="mailto:farringdonenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">farringdonenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">12A Leather Lane<br>London<br>EC1N
                                                7SS<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Boxing Ring<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Matted Combat
                                                    Area<br></span><span>Group Cycle Studio<br></span><span>Resident
                                                    DJs<br></span><span>Sprint Track<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/farringdon"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=6"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-6"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Finsbury Park</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>(Club Opening Summer 2020)</p>
                                                    <p>Marketing Pop-Up Now Open</p>
                                                    <p>Mon to Fri: 8am - 8pm </p>
                                                    <p>Sat to Sun: 9am - 6pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a href="tel:0203 301 6269">0203 301
                                                    6269</a><br><a href="mailto:foundermember@onelifefitness.co.uk"
                                                    class="_2h5s4t1">foundermember@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">Marketing Pop-Up<br>4 -5 Goodwin
                                                Street<br>N4 3HQ<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/finsbury-park"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=7"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-7"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Holborn</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 10am – 6pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)20 7400 1919">+44 (0)20 7400 1919</a><br><a
                                                    href="mailto:holbornenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">holbornenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">100 High Holborn<br>London<br>WC1V
                                                6RD<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Boxing Ring<br></span><span>Resident
                                                    DJs<br></span><span>Group Cycle Studio<br></span><span>Matted Combat
                                                    Area<br></span><span>Functional Fitness<br></span><span>Holistic
                                                    Studio<br></span><span>Dance &amp; Conditioning
                                                    Studio<br></span><span>Sprint Track<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/holborn"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=8"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-8"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Old Street</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 10am – 6pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)203 780 4698">+44 (0)203 780 4698</a><br><a
                                                    href="mailto:oldstreetenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">oldstreetenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">201A Old Street<br>London<br>EC1V
                                                9NP<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Resident DJs<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Group Cycle
                                                    Studio<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/old-street"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=9"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-9"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Victoria</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 10am – 6pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a href="tel:020 3780 4496">020 3780
                                                    4496</a><br><a href="mailto:victoriaenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">victoriaenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">123 Victoria
                                                Street<br>London<br>SW1E 6DE<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Resident DJs<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Group Cycle
                                                    Studio<br></span><span>Boxing Ring<br></span><span>Sprint
                                                    Track<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/victoria"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=10"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-10"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Westfield London</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm<br>Sat to Sun: 8am – 8pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)20 8735 5090">+44 (0)20 8735 5090</a><br><a
                                                    href="mailto:westfieldlondonenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">westfieldlondonenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">Westfield Shopping
                                                Centre<br>London<br>W12 7GF<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Strength &amp;
                                                    Conditioning<br></span><span>Boxing Ring<br></span><span>Functional
                                                    Fitness<br></span><span>Holistic Studio<br></span><span>Dance &amp;
                                                    Conditioning Studio<br></span><span>Matted Combat
                                                    Area<br></span><span>Group Cycle Studio<br></span><span>Resident
                                                    DJs<br></span><span>Sprint Track<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/westfield-london"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=11"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="Cq_7NWt _2rbE6TC _2v5bHvx" id="listing-11"><a class="_2HzoBao _2rbE6TC _2v5bHvx"
                                    href="javascript:void(0);">
                                    <h4 class="_2J9Yz6G _2rbE6TC _2v5bHvx VBD7Ow3">Westfield Stratford</h4>
                                </a>
                                <div class="_Wl12W- VBD7Ow3">
                                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                                                <div>
                                                    <p>Mon to Fri: 6am – 11pm <br>Sat to Sun: 8am – 8pm</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Contact</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><a
                                                    href="tel:+44 (0)20 3819 8548">+44 (0)20 3819 8548</a><br><a
                                                    href="mailto:westfieldstratfordenquiries@onelifefitness.co.uk"
                                                    class="_2h5s4t1">westfieldstratfordenquiries@onelifefitness.co.uk</a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">6A Chestnut Place<br>Westfield
                                                Stratford City<br>London E20 1GL<br><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Map</span></a><a href="" target="_blank"
                                                    class="_1x8JHAI _3Pq3GhV _3xVoYzA"><span>View on
                                                        Citymapper</span></a></div>
                                        </li>
                                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Dance &amp; Conditioning
                                                    Studio<br></span><span>Functional Fitness<br></span><span>Group
                                                    Cycle Studio<br></span><span>Holistic Studio<br></span><span>Matted
                                                    Combat Area<br></span><span>Resident DJs<br></span><span>Strength
                                                    &amp; Conditioning<br></span><span>Very Personal
                                                    Training<br></span><span>Cardio<br></span><span>Olympic
                                                    Lifting<br></span></div>
                                        </li>
                                    </ul>
                                    <div class="_2S9gFR2 _2rbE6TC _2v5bHvx"><a class="_1DHXI17 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/westfield-stratford"><span>Classes</span></a><a
                                            class="zF_pZj2 _3Pq3GhV"
                                            href="<?= get_site_url(); ?>/timetable-bank?id=12"><span>Timetable</span></a>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                    </div>

                    <div class="_1Xw6tAa">
                        <div class="_2Tb-We1 sDHKtsB">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"
                                        data-interval="3000">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                                class="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item item_left active"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');">
                                            </div>
                                            <div class="carousel-item item_left"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');">
                                            </div>
                                            <div class="carousel-item item_left"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png');">
                                            </div>
                                            <div class="carousel-item item_left"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');">
                                            </div>
                                            <div class="carousel-item item_left"
                                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h4 class="_20dJMXd">Back to map</h4>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $(".Cq_7NWt").click(function() {
        $(this).addClass("_3O95Rh5");
        $(".Cq_7NWt").hide();
        $("._3O95Rh5").show();
        $("._1KSLoAe").addClass("_3juAkpr");
    })
    $("#map").click(function() {
        $("._1KSLoAe").removeClass("_3juAkpr");
        $(".Cq_7NWt").removeClass("_3O95Rh5");
        $(".Cq_7NWt").show();
    })
});

// mapboxgl.accessToken = 'pk.eyJ1IjoiaGFjaGlkZXYiLCJhIjoiY2s2cTdkdng5MGxmODNtcW9tNGZndHN4biJ9.MWfDKVwEk5B2iLXjscpPOg';
//   var map = new mapboxgl.Map({
//     container: 'map',
//     // style: 'mapbox://styles/mapbox/streets-v11',
//     style: 'mapbox://styles/mapbox/dark-v10',
//     zoom: 13,
//     center: [4.899, 52.372],
// });
// Add zoom and rotation controls to the map.
// map.addControl(new mapboxgl.NavigationControl());

// This will let you use the .remove() function later on
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

mapboxgl.accessToken = 'pk.eyJ1IjoiaGFjaGlkZXYiLCJhIjoiY2s2cTdkdng5MGxmODNtcW9tNGZndHN4biJ9.MWfDKVwEk5B2iLXjscpPOg';
/** 
 * Add the map to the page
 */
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/dark-v10',
    center: [-8.4756834, 51.9013384],
    zoom: 13,
    scrollZoom: true
});
// map.addControl(new mapboxgl.NavigationControl());
/**
 * Wait until the map loads to make changes to the map.
 */

$(".Cq_7NWt").on('mouseover', function(e) {

    var data = {
        "type": "FeatureCollection",
        "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -8.4746834,
                        51.9030384
                    ]
                },
                "properties": {
                    "name": "One Life Fitness",
                    "phoneFormatted": "+44 (0)20 7337 9790",
                    "phone": "+44 (0)20 7337 9790",
                    "address": "Mulgrave Street",
                    "city": "Cork City",
                    "country": "United Kingdom",
                    "crossStreet": "at 15th St NW",
                    "postalCode": "EC3V 9AY",
                    "state": ""
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        -8.4748454,
                        51.8954862
                    ]
                },
                "properties": {
                    "name": "Riversedge",
                    "phoneFormatted": "(202) 507-8357",
                    "phone": "2025078357",
                    "address": "Sullivan's Quay",
                    "city": "Cork city",
                    "country": "United States",
                    "crossStreet": "Drinan Street",
                    "postalCode": "20037",
                    "state": "D.C."
                }
            }
        ]
    };

    id = $(this).attr('id');
    var parts = id.split('-');
    id = parts[1];
    var clickedListing = data.features[id];
    flyToStore(clickedListing);
    createPopUp(clickedListing);

    var activeItem = document.getElementsByClassName('active');
    if (activeItem[0]) {
        activeItem[0].classList.remove('active');
    }
    this.parentNode.classList.add('active');
});

/**
 * Use Mapbox GL JS's `flyTo` to move the camera smoothly
 * a given center point.
 **/

function flyToStore(currentFeature) {
    map.flyTo({
        center: currentFeature.geometry.coordinates,
        zoom: 15
    });
}

/**
 * Create a Mapbox GL JS `Popup`.
 **/
function createPopUp(currentFeature) {
    // var popUps = document.getElementsByClassName('mapboxgl-popup');
    // if (popUps[0]) popUps[0].remove();
    var popup = new mapboxgl.Popup({
            closeOnClick: false
        })
        .setLngLat(currentFeature.geometry.coordinates)
        .setHTML('<img src="<?= get_template_directory_uri() ?>/assets/images/pin.svg" alt="" class="locator">')
        .addTo(map);
}


map.on("load", function() {
    /* Image: An image is loaded and added to the map. */
    map.loadImage("https://i.imgur.com/MK4NUzI.png", function(error, image) {
        if (error) throw error;
        map.addImage("custom-marker", image);
        /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
        map.addLayer({
            id: "markers",
            type: "symbol",
            /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
            source: {
                type: "geojson",
                data: {
                    type: 'FeatureCollection',
                    features: [{
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: "Point",
                            coordinates: [-8.4746834,51.9030384]
                        }
                    },
                    {
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: "Point",
                            coordinates: [-8.4748454,51.8954862]
                        }
                    }]
                }
            },
            layout: {
                "icon-image": "custom-marker",
            }
        });
    });
});
</script>