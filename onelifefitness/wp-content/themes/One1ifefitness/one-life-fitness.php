<?php /* Template Name: one-life-fitness */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="o15HL62 _2v5bHvx">
    <div class="_3juAkpr _1KSLoAe">
        <div class="_2Ey81ib">
            <div>
                <h4>Gyms inShoreditch</h4>
                <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">onelifefitness Shoreditch</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                            <div>
                                <p>One of onelifefitness's most popular gyms is in Shoreditch, a vibrant hub of creativity in
                                    East London. Our Shoreditch gym is located on Old Street which is conveniently on
                                    the Central Line and with in close reach of many offices in the surrounding areas.
                                </p>
                                <p>Shoreditch also has a lively night life so if you are looking to work out before your
                                    night out, drop by for a tour!</p>
                            </div>
                        </div>
                    </li>
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Times</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                            <div>
                                <p><strong>Gym Opening Hours:</strong></p>
                                <p>Mon to Fri: 6am – 10pm<br>Sat: 9am – 7pm <br>Sun: 10am - 5pm</p>
                            </div>
                        </div>
                    </li>
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Address</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                            <div>
                                <!-- <p><strong>onelifefitness Shoreditch</strong></p> -->
                                <p>Onelifefitness <br>11 Mulgrave Road, Cork,<br>T23AC91</p>
                            </div>
                        </div>
                    </li>
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Facilities</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span>Dance &amp; Conditioning
                                Studio<br></span><span>Functional Fitness<br></span><span>Resident
                                DJs<br></span><span>Very Personal Training<br></span></div>
                    </li>
                </ul>
                <div><a href="/timetable"><span>Timetable</span></a></div>
            </div>
        </div>
        <div class="_1Xw6tAa">
            <div class="_3DJUv-O sDHKtsB">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>