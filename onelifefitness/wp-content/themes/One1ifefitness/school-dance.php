<?php /* Template Name: school-dance */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<section class="_1J0RKGy _2rbE6TC _2v5bHvx">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');">
                    </div>
                </div>
                <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->
            </div>
        </div>
    </div>
</section>
<div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="_130-KDr">
        <div class="">
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">step l1ft</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Live for the
                            dancefloor? Our music-video inspired commercial class is the place to choreograph
                            killer<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/callback"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">abs + core 15min</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Looking for the
                            perfect spot to whine and grind? It’s pure ragga, bashment and dancehall riddims on the menu
                            here<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/dancehall"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">body bootcamp</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Our Signature in house Bootcamp, designed to keep you accountable and push you to change that body shape for whatever event you have coming up.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/femme"></a>
            </div>
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>