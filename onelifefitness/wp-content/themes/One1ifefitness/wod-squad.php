<?php /* Template Name: wod-squad */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>WOD Squad</h2>
        <div class="_1-4gF4V">
            <p>Pulling, pushing, climbing, lifting. Seems so simple, doesn’t it? Well, looks can be deceiving, pal.
                Being a onelifefitness workout, you’ll mix up those four functional movements to give your body a good hiding;
                testing your strength, building your skills as well as an intense sweat. It’s fitness at its finest if
                you dig the rig. </p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Cannon
                        Street</span><span class="Nfa775m">Covent Garden</span><span class="Nfa775m">Ealing</span><span
                        class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Farringdon</span><span
                        class="Nfa775m">Holborn</span><span class="Nfa775m">Old Street</span><span
                        class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                        class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Old Street">Old Street</option>
                <option value="Victoria">Victoria</option>
                <option value="Ealing">Ealing</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Bank">Bank</option>
                <option value="Covent Garden">Covent Garden</option>
                <option value="Westfield London">Westfield London</option>
                <option value="Holborn">Holborn</option>
                <option value="Cannon Street">Cannon Street</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1157050">12:00 Saturday 15th (45 minutes)</option>
                <option value="1298179" disabled="">13:30 Sunday 16th (45 minutes) - Bookable 7am day before</option>
                <option value="1327679" disabled="">07:15 Monday 17th (45 minutes) - Bookable 7am day before</option>
                <option value="1329450" disabled="">07:30 Tuesday 18th (45 minutes) - Bookable 7am day before</option>
                <option value="1325425" disabled="">19:15 Tuesday 18th (45 minutes) - Bookable 7am day before</option>
                <option value="1297179" disabled="">13:00 Wednesday 19th (45 minutes) - Bookable 7am day before</option>
                <option value="1329981" disabled="">17:45 Wednesday 19th (45 minutes) - Bookable 7am day before</option>
                <option value="1323234" disabled="">13:00 Thursday 20th (45 minutes) - Bookable 7am day before</option>
                <option value="1274715" disabled="">19:15 Thursday 20th (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Elle_Charig.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Elle</h3>
                    <div>
                        <p>Growing up Elle loved playing almost every sport she could find and started coaching
                            basketball at age 14.&nbsp;For her it’s always been about performance and health so now she
                            specialises in functional training, gymnastics, power and Olympic lifting. Visit Elle
                            to&nbsp;learn some new skills, get fit and have fun!&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Biannca_Pegrume.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Bianca</h3>
                    <div>
                        <p>Whilst studying Zoology at university, Bianca admits she
                            missed more than one lecture to fit in a gym session. Having discovered her
                            passion for fitness, she went on to become a qualified studio instructor and
                            PT, and now loves every minute teaching at onelifefitness.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Giselle_Grant.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Giselle</h3>
                    <div>
                        <p>Having worked as a speech therapist for 10 years, Giselle
                            decided to follow her passion for fitness and become a PT and fitness
                            instructor. Giselle specialises in HIIT and functional training, and loves
                            nothing more than to push people above and beyond their limits. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ryan_Lovett.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ryan</h3>
                    <div>
                        <p>A professional actor trained in New York, when Ryan’s not on
                            stage he’s teaching TRX, Row30 and Frame Fitness at onelifefitness. Having returned to
                            the UK a few years ago, Ryan loves his job at onelifefitness and admits that London
                            definitely has his heart now. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                    <div>
                        <p>Through a combination of tough-love and motivation, Stevie will push you to your limits with
                            high-energy metabolic conditioning workouts that will leave you in a pool of sweat and a big
                            smile on your face.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Andy_Apollo.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Andrew</h3>
                    <div>
                        <p>Andy is a self-professed fitness addict who’s always after
                            the next endorphin hit. Training with Andy will push you to be the best version
                            of yourself and make sure you’ve got a smile on your face when you get there. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Aaron_Cook.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Aaron C</h3>
                    <div>
                        <p>Aaron believes that everyone is an athlete, so everyone should perform and be coached like
                            one. His passion for coaching with education and enthusiasm helps give his private clients
                            and our members the results they want. Olympic lifting, gymnastics, engine work and
                            flexibility are all skills of his own training that he implements though his classes. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lutha_Nzinga.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Luther</h3>
                    <div>
                        <p>A PT and instructor with a degreee in Sports Therapy, and a
                            background in professional powerlifting, Luther teaches Frame Fitness and Drill
                            Sergeant at onelifefitness. His focus on technique and lifting heavy will help you
                            improve both your mental and physical strength.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                    <div>
                        <p>Firas found his passion for American Football 15 years ago, and has since won numerous
                            championships as both a player and coach. Firas brings his enthusiasim and camaraderie to
                            all his classes, and will undoubtedly bring out the inner athlete in you. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Sam_Lawrence.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Sam L</h3>
                    <div>
                        <p>Sam specialises
                            in TRX, and Kettlebells, and brings this
                            diverse&nbsp;knowledge&nbsp;to&nbsp;his classes at onelifefitness. He is inspired by
                            the energy of his members when they’re pushing themselves to the limit, and
                            loves helping people reach their goals.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                    <div>
                        <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                            &nbsp;classes range from Gains to Escalate, and are based around personal development in a
                            motivational team environment. His love for fitness is second only to his love of the onelifefitness
                            team.
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Daisy_Rusby.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Daisy</h3>
                    <div>
                        <p>A lover of lifting heavy things and dancing to her hearts content, Daisy is a PT and studio
                            instructor who’s specialities include kettlebells, strength training and holistic
                            flexibility. Her classes are based on teamwork and guaranteed to help you reach your fitness
                            potential.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Alex_Pipe.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Alex</h3>
                    <div>
                        <p>
                            Alex spent 18 months in the New Zealand Rugby Academy where he was an S&amp;C coach and that
                            is what inspired him to start teaching classes. He studied an MSc in Applied Sport and
                            Exercise Nutrition and has since qualified as a Level 1 Crossfit Coach and Crossfit
                            Gymnastic Coach. His style of training is based on getting people fitter, faster and
                            stronger using either weights or just body weight.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/FrameFitness_New.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>