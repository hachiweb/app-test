<?php /* Template Name: brazilian-jui-jitsu */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
                <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
                    <h2>olf45</h2>
                    <div class="_1-4gF4V">
                        <p>Fancy getting hot and sweaty with some on-the-ground grappling? OLF45 is the
                            self-defensive martial art that means whatever your ability, opponent or size… you’ll build
                            up more strength in your fingers than some people have in their entire body. Feel the power.
                        </p>
                    </div>
                    <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                        <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                            <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                            <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                            <span class="Nfa775m">Onelifefitness Camden quay</span>
                <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                            </div>
                        </li>
                    </ul>
                    <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
                    <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
                        <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                            <option value="" disabled="">Choose time</option>
                            <option value="1268854" disabled="">13:00 Sunday 16th (60 minutes) - Bookable 7am day before
                            </option>
                            <option value="1325986" disabled="">20:00 Monday 17th (60 minutes) - Bookable 7am day before
                            </option>
                            <option value="1303912" disabled="">18:00 Wednesday 19th (60 minutes) - Bookable 7am day
                                before</option>
                        </select><a href="https://indma05.clubwise.com/onelifefitness/index.html" target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book
                                class</span></a>
                    </div>
                    <div class="_3srnCE8">
                        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                            <div class="_2Tb-We1 sDHKtsB">
                                <ul class="_2J71_L4 sDHKtsB">
                                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wqBYWJi">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                            style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>