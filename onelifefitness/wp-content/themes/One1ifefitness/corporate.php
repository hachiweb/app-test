<?php /* Template Name: corporate */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_3CTAk5g _2rbE6TC _2v5bHvx">
        <div class="_2w47Ada _2rbE6TC _2v5bHvx">
            <div class="NuYYeO4 _2rbE6TC _2v5bHvx">
                <h1 class="DqXpLQh">Add clean and jerks to your employee perks</h1>
            </div>
        </div>
        <div class="_1nsQEkX _2rbE6TC _2v5bHvx">
            <div class="_3jHhwt9 _2rbE6TC _2v5bHvx">
                <div class="_1Jm58Q3 VBD7Ow3">
                    <h3>Intro</h3><br><br>
                    <div class="_25sVHsx">
                        <div class="_3MyeZTr text-justify">Back in the day, a decent wage and a week off in Benidorm was
                            all you’d need for a happy workforce. But times have changed. Your people need more – and
                            when it comes to choosing where to spend their 9-5, they’re not afraid to play the
                            field.<br><br>So, let’s talk tactics. A cracking benefits package that includes corporate
                            gym membership is a shrewd move for employers wanting to keep the best talent all to
                            themselves. But offering One Life Fitness membership at a club that grants access to the
                            most inspiring, energising work out experience in Cork? Well, you’ll smash staff schmoozing
                            out of the park.
                        </div>
                        <div class="_3MyeZTr text-justify">We’re here to make your investment work harder than a newbie
                            in a spin class. To supercharge your productivity through sweaty training, bruising beats
                            and lung-busting fun. To be your partner in the mission for a happier, healthier and more
                            motivated team.<br><br>Stick with us and you’ll soon be hitting that workout/life balance
                            like a (literal) boss. Ready to take a look around?</div>
                    </div>
                </div>
            </div>
            <div class="_5pOUbZI _2rbE6TC _2v5bHvx">
                <ul class="_2xWkNTg VBD7Ow3">
                    <li class="_2c396T4"><a href="#who">
                            <h3>WHO?</h3>
                        </a></li>
                    <li class="_2c396T4"><a href="#why">
                            <h3>WHY?</h3>
                        </a></li>
                    <li class="_2c396T4"><a href="#what">
                            <h3>WHAT?</h3>
                        </a></li>
                    <li class="_2c396T4"><a href="#where">
                            <h3>WHERE?</h3>
                        </a></li>
                    <li class="_2c396T4"><a href="#how">
                            <h3>HOW?</h3>
                        </a></li>
                    <li class="_2c396T4"><a href="#enquire">
                            <h3>ENQUIRE</h3>
                        </a></li>
                </ul>
            </div>
            <div class="_3Vgj1HF _2rbE6TC _2v5bHvx"><a name="who"></a>
                <div class="_2eH7-qI VBD7Ow3">
                    <div class="_2FA9ISg GXyzp1w _2v5bHvx">
                        <h3>Who?</h3><br><br>
                        <p class="text-justify">
                            The fact you’ve read this far tells us a lot about you. You’re the curious type, aren’t you?
                            Someone who’s not scared to break out of the old status quo. We like that. Probably because
                            we’re that way inclined too. onelifefitness started life as a simple idea that working out
                            shouldn’t have to be another job to add the endless list we Londoners already have to chug
                            through day in, day out. And today, we’re applying that philosophy to help all kinds of
                            businesses energise their commute-weary teams.
                            <br><br>Ours is a formula that works. Why? Well, mainly, because we’ve made onelifefitness a
                            place we love to train in ourselves. When it comes to fitness clubs, we’ve seen some things,
                            man. The identikit layouts. Predictable classes. Sad, unloved facilities and equipment that
                            still give us nightmares. It’s not exactly inspiring, is it? <br><br>That was when we
                            unveiled our business plan. Unique classes.. Larger-than-life personal trainers. We might
                            not be corporate. But we know how to get the best out of your people, so they in turn give
                            their best for you. If you’d like to find out more about the business case for getting
                            really hot and sweaty, let’s talk.</p>
                    </div>
                    <div class="_3oPg3yI _1Dbvw6B _2v5bHvx">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/ONE.jpg" alt=""></div>
                </div>
            </div>
            <div class="_3VBlCoA _2rbE6TC _2v5bHvx"><a name="why"></a>
                <div class="_1AqDBWN VBD7Ow3">
                    <div class="Chl_-0Q GXyzp1w _2v5bHvx">
                        <h3>Why?</h3><br><br>
                        <p>Corporate gym membership isn’t new. In this health-conscious world, everyone knows that
                            regular exercise has its benefits – and we’re not just talking buffer Insta feeds. In
                            business terms: the fitter you are, the sharper you are, the more productive you are. Oh,
                            and less days off sick too. Boom.<br><br>But with so many gyms to partner with, why choose
                            onelifefitness? Well, to us, corporate membership isn’t just about Ian in HR building
                            eye-popping glutes and Sara in Sales bench pressing her bodyweight before breakfast. The
                            onelifefitness experience gives your people much more than that. It’s being part of a
                            lifestyle. A tribe..<br><br>Each club is original, unusual and challenging (in a really fun
                            and sweaty way). A lot of thought goes into the personalities we hire, the beats dropping
                            from the sound systems, and all classes we run every week. And the unique buzz you get out
                            of it is what comes back to your office.<br><br>This is no run-of-the-treadmill gym. We’ve
                            built our name on doing things differently. On understanding what people really want – and
                            just making life that bit more exciting. We’re here to bring more value to your staff
                            experience – boosting your rep as an employer and building serious business gains.</p>
                    </div>
                    <div class="_2DFH3kw _1Dbvw6B _2v5bHvx">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg"
                            alt="">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png" alt="">
                    </div>
                </div>
            </div>
            <div class="vZBlQpZ _2rbE6TC _2v5bHvx"><a name="what"></a>
                <div class="_3YNv-TL VBD7Ow3">
                    <div class="_3Lc7Cvz GXyzp1w _2v5bHvx">
                        <h3>What?</h3><br><br>
                        <p>So what goes on behind those doors? You need to see it believe it. But we’ve created a
                            playground designed to motivate your people through fitness, fun and full-on sweat-fests.
                            With a schedule of classes each week, there’s something for everyone – whether you’re after
                            a challenge that’ll put hairs on your chest or send you soaring to a celestial
                            plane.<br><br>Box, Pilates, dance, spin, yoga, weight pumping, functional training, all
                            kinds of circuits … an inexhaustible supply of ways to exhaust yourself, all run by an army
                            of instructors who are experts in making you knackered. Not to mention our perspirational
                            Very Personal Trainers and all the latest state-of-the-art equipment a gym should ever have.
                        </p>
                    </div>
                    <div class="_2kegIUU _1Dbvw6B _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png" alt="">
                    </div>
                </div>
            </div>
            <div class="_1rwFMn8 _2rbE6TC _2v5bHvx"><a name="where"></a>
                <div class="_2KlcqL3 VBD7Ow3">
                    <div class="_1Gm8zGd _2rbE6TC _2v5bHvx">
                        <h2>GOOD NEWS. IF YOU’VE GOT AN OFFICE CLOSE TO ANY OF THESE, THERE’S A onelifefitness READY
                            AND WAITING FOR YOU.</h2>
                    </div>
                    <ul class="_-PFmPOa">
                        <li class="b77kuZB">
                            <div class="bUFZcBv"><img class="_3xCPazc"
                                    src="<?= get_template_directory_uri(); ?>/assets/images/map1.jpg" alt=""></div>
                            <div class="_3HrC7vs  text-uppercase"><strong>One Life Fitness</strong>
                                <p>11 Mulgrave Rd, <br>Shandon, Cork, <br>T23 AC91</p>
                            </div>
                        </li>
                        <li class="b77kuZB">
                            <div class="bUFZcBv"><img class="_3xCPazc"
                                    src="<?= get_template_directory_uri(); ?>/assets/images/map2.jpg" alt=""></div>
                            <div class="_3HrC7vs text-uppercase"><strong>Rivers Edge Fitness</strong>
                                <p>2 Sullivan's Quay, <br>Drinan St, Centre, <br>Cork, T12 RW90</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="_27cKqU4 _2rbE6TC _2v5bHvx"><a name="how"></a>
                <div class="_1KFU6tA VBD7Ow3">
                    <div class="_1AyN87A _1Dbvw6B _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png" alt=""><img
                            src="<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png" alt=""></div>
                    <div class="Hd7Vux5 GXyzp1w _2v5bHvx">
                        <h3>How?</h3><br><br>
                        <p>So you’re sold on the commercial value of sending your people to a nightclub at
                            lunchtime where they can go tyre flipping or sledge pulling. Great stuff. But
                            how do you pay for it? Easy. There are two ways: either the company pays or the
                            employee does, and always at a head-turning corporate rate.</p>
                        <h4 class="_1heBBgX">FLEXIBILITY</h4>
                        <p>True to form, we’re happy to do things differently. Everything’s as flexible as a
                            3-week veteran of Vinyasa Yoga at onelifefitness. We’ll give you the option to transfer
                            memberships between departing and new employees. Add new memberships with a
                            price guarantee. And you can do it all through our uber-friendly group
                            administration department.</p>
                        <h4 class="_1heBBgX">COMPANY-PAID CORPORATE MEMBERSHIP</h4>
                        <p>Spotting the finances on behalf of your employees? Nice one. We’ll make it nice
                            and simple to set up, and you’ll have the full support of our group
                            administration department too.</p>
                        <h4 class="_1heBBgX">Ways to pay</h4>
                        <div class="_1yFUfTN _2rbE6TC _2v5bHvx"><strong>Company Invoice</strong><br>Company
                            pays by annual or monthly invoice.</div>
                        <div class="_1yFUfTN _2rbE6TC _2v5bHvx"><strong>Company Direct
                                Debit&nbsp;</strong><br>Company pays through a monthly BACS payment.&nbsp;
                        </div>
                        <div class="_1yFUfTN _2rbE6TC _2v5bHvx"><strong>Employee
                                Payroll&nbsp;</strong><br>Employee pays onelifefitness directly using one of the
                            options above, and company pays employee though payroll scheme.</div>
                        <h4 class="_1heBBgX">EMPLOYEE-PAID CORPORATE MEMBERSHIP</h4>
                        <p>Alternatively, if your employees are taking charge, we’ll set you up initially
                            through the company, based on a guaranteed number of employees signing up. From
                            there, membership can then continue directly between onelifefitness and employee.</p>
                    </div>
                </div>
            </div>
            <div class="_3I5_iDG _2rbE6TC _2v5bHvx"><a name="enquire"></a>
                <div class="_2JiiLfi _2rbE6TC _2v5bHvx">
                    <div class="_1qWZ1iU">
                        <h2>Anything goes</h2>
                        <h3>Enquire now</h3>
                    </div>
                    <div class="HRKJIWa VBD7Ow3">
                        <form class="corporate-enquiry-form">
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="fromName">Name</label>
                                <input type="text" name="fromName" value="" placeholder="Name">
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="fromEmail">Email</label>
                                <input type="email" name="fromEmail" value="" placeholder="Email">
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="message[mobile]">Phone</label>
                                <input type="text" name="message[mobile]" value="" placeholder="Phone">
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="message[company]">Company</label>
                                <input type="text" name="message[company]" value="" placeholder="Company">
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="message[employees]">Number of employees</label>
                                <input type="text" name="message[employees]" value="" placeholder="Number of employees">
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="message[gym]">Club interested in</label>
                                <select name="message[gym]" placeholder="Club interested in">
                                    <option value="select">Select</option>
                                    <option value="One Life Fitness">One Life Fitness</option>
                                    <option value="Rivers Edge Fitness">Rivers Edge Fitness</option>
                                </select>
                            </div>
                            <input type="hidden" name="subject" value="Corporate enquiry">
                            <input type="hidden" name="corporate" value="true">
                            <input type="hidden" name="onelifefitnessHP" value="">
                            <input type="submit" value="Submit" class="submit-corporate-enquiry">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>