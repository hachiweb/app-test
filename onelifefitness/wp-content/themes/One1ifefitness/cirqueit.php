<?php /* Template Name: cirqueit */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<div class="_14BrxaV">
    <div class="_1vdzHPH">
        <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
            <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
                <h2>Cirque It</h2>
                <div class="_1-4gF4V">
                    <p>Think circus tricks are for clowns? This is where fitness goes rogue. If you’re serious about
                        giving aerial a fair crack of the whip, this is exactly where you need to be. We'll help you
                        build the power of a lion, the bravery of a daredevil and the upper body strength of an acrobat
                        – there’s a reason they call it the Big Top. Please remove all jewellery beforehand. </p>
                </div>
                <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Ealing</span><span
                                class="Nfa775m">Farringdon</span><span class="Nfa775m">Old Street</span><span
                                class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield Stratford</span></div>
                    </li>
                </ul>
                <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Select gym</option>
                        <option value="Old Street">Old Street</option>
                        <option value="Victoria">Victoria</option>
                        <option value="Ealing">Ealing</option>
                        <option value="Westfield Stratford">Westfield Stratford</option>
                        <option value="Farringdon">Farringdon</option>
                    </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Choose time</option>
                        <option value="1269800" disabled="">14:45 Saturday 15th (60 minutes) - Bookable 7am day before
                        </option>
                    </select><a
                        href=""
                        target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
                <div class="_3srnCE8">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                    <h2>Your <br>instructors</h2>
                    <div class="Sa1FFQw">
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hattie_Grover_Extra_Option_2.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hattie</h3>
                            <div>
                                <p>Hattie's training began at the age
                                    of 3 when she started taking ballet lessons. As her
                                    love for dance blossomed she began performing, and immersed herself in a variety
                                    of different dance styles specialising in contemporary and breaking. Whilst at
                                    onelifefitness she has broadened her knowledge in the Aerial Arts and is fully committed to
                                    sharing her love for everything aerial, acrobatic and upside
                                    down!</p>
                            </div>
                        </div>
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                            <div>
                                <p>First qualified in aqua fitness aged 17, Hester worked in film production and then
                                    re-trained as a contemporary dancer. Instructing pilates, fitness &amp; dance
                                    supported her through BA &amp; MSc studies at Laban, and brought her to onelifefitness in
                                    2004. Fitness, wellness and aerial arts have guided Hester throughout her career.
                                    Possibly unequalled in her passion for teaching and sharing the knowledge
                                    accumulated over more than 3 decades in the fitness world, Hester is onelifefitness Aerial
                                    Series Master Trainer and creator of CircusFit Academy.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wqBYWJi">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Circus.jpg')"></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>