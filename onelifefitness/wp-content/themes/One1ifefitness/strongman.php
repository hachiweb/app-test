<?php /* Template Name: strongman */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
            <h2>Strongman</h2>
            <div class="_1-4gF4V">
                <p>Tyre flipping. Log pressing. Sled dragging. If there’s heavy sh*t you’ve ever wanted to lift,
                    push or fling, we’re about to make all your dreams come true. Bringing together many of the
                    disciplines used in competitive Strongman, this is cardio with a heavy twist. Grunting optional.
                </p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Ealing</span><span
                            class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Farringdon</span><span
                            class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                            class="Nfa775m">Westfield Stratford</span></div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Select gym</option>
                    <option value="Westfield London">Westfield London</option>
                    <option value="Elephant and Castle">Elephant and Castle</option>
                    <option value="Victoria">Victoria</option>
                    <option value="Westfield Stratford">Westfield Stratford</option>
                    <option value="Farringdon">Farringdon</option>
                    <option value="Ealing">Ealing</option>
                </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1280004">11:00 Saturday 15th (45 minutes)</option>
                </select><a href="" target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book
                        class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Aaron_Cook.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Aaron C</h3>
                        <div>
                            <p>Aaron believes that everyone is an athlete, so everyone should perform and be coached
                                like one. His passion for coaching with education and enthusiasm helps give his
                                private clients and our members the results they want. Olympic lifting, gymnastics,
                                engine work and flexibility are all skills of his own training that he implements
                                though his classes. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Daisy_Rusby.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Daisy</h3>
                        <div>
                            <p>A lover of lifting heavy things and dancing to her hearts content, Daisy is a PT and
                                studio instructor who’s specialities include kettlebells, strength training and
                                holistic flexibility. Her classes are based on teamwork and guaranteed to help you
                                reach your fitness potential.&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Strongman_Web.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>