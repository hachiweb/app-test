<?php /* Template Name: services */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="container-fluid _1vdzHPH">
    <div class="_3VQRo5k _2rbE6TC _2v5bHvx">
        <div class="_2iUzBam _2rbE6TC _2v5bHvx _3HiqiFI">
            <div class="LZPk4LN _3s-__sC _3x6v3-y text-center"><svg class="_3RdppjM" preserveAspectRatio="xMidYMid"
                    viewBox="0 0 540 110"><text vector-effect="non-scaling-stroke" shape-rendering="crispEdges"
                        class="_1D8oKRW _31coI1f" x="0" y="90">Services</text></svg></div>
        </div>
        <div class="_1zDADC9 _2rbE6TC _2v5bHvx _3x6v3-y">
            <div class="_3HyXefo _2rbE6TC _2v5bHvx VBD7Ow3">
                <div class="_2Ofop5C">
                    <h3>Member services</h3>
                    <!-- <button id="" class="btn btn-primary btn-lg toastrDefaultSuccess"></button> -->

                </div>
                <div class="_2DQBLZb">
                    <div class="_1U5x0G0 _1MNkAOW _2v5bHvx">
                        <a class="_3r1Y9_D _Y55-bj _2rbE6TC _2v5bHvx service1" href="javascript:void(0);">Membership
                            Freeze</a>
                        <!-- <a class="_Y55-bj _2rbE6TC _2v5bHvx service2" href="javascript:void(0);">Package Change</a>
                        <a class="_Y55-bj _2rbE6TC _2v5bHvx service3" href="javascript:void(0);">Cancellation</a>
                        <a class="_Y55-bj _2rbE6TC _2v5bHvx service4" href="javascript:void(0);">Change of Personal Details</a> -->
                    </div>
                    <div class="_2x_S9gP _2eBzx2S _2v5bHvx ser1">
                        <form class="_81firkY" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>"
                            method="POST">
                            <input type="hidden" name="action" value="my_simple_form">
                            <p>Getting over an injury, going overseas or just plain overworked. We know there'll be
                                times you simply can't make it to the gym. So if you want to freeze your Gymbox
                                membership, it's never a problem and this is the place to do it.</p>
                            <div>
                                <div class="_2nxeEK_ _14NUrhZ">
                                    <label for="select_club">Home club</label>
                                    <select name="select_club" class="select_club" required>
                                        <option selected disabled value="">Choose Home Club</option>
                                        <option value="One Life Fitness">One Life Fitness</option>
                                        <option value="Rivers Edge Fitness">Rivers Edge Fitness</option>
                                    </select>
                                </div>
                                <div class="_2nxeEK_ _14NUrhZ">
                                    <label for="user_name">Name<span> (must match the named used for the
                                            membership)</span></label>
                                    <input name="user_name" placeholder="Name" type="text" value="" required>
                                </div>
                                <div class="_2nxeEK_ _14NUrhZ">
                                    <label for="email">Email<span> (must be the address used to login)</span></label>
                                    <input name="email" placeholder="Email" type="email" value="" required>
                                </div>
                                <div class="_2nxeEK_ _14NUrhZ">
                                    <label for="user_phone">Phone number</label>
                                    <input name="user_phone" placeholder="Phone number" type="text" value="" required>
                                </div>
                                <div class="_2nxeEK_ _14NUrhZ">
                                    <label for="membership_number">Membership number</label>
                                    <input name="membership_number" placeholder="Membership number" type="text" value=""
                                        required>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="subject" value="Membership Freeze"> -->
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="ticket_reasontype">What would you like to do</label>
                                <select name="ticket_reasontype" placeholder="What would you like to do" required>
                                    <option value="" selected disabled>Select</option>
                                    <option value="Request a freeze">Request a freeze</option>
                                    <option value="Request to unfreeze">Request to unfreeze</option>
                                </select>
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="ticket_reasonforfreeze">Reason for freeze</label>
                                <select name="ticket_reasonforfreeze" placeholder="Reason for freeze" required>
                                    <option value="" selected disabled>Select</option>
                                    <option value="Relocation">Relocation</option>
                                    <option value="Work relocation">Work relocation</option>
                                    <option value="Financial">Financial</option>
                                    <option value="Travel">Travel</option>
                                    <option value="Lack of time">Lack of time</option>
                                    <option value="Personal">Personal</option>
                                    <option value="Season">Season</option>
                                    <option value="Medical/Injury">Medical/Injury</option>
                                    <option value="Pregnancy">Pregnancy</option>
                                </select>
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="ticket_freeze_startdate">Freeze start date</label>
                                <select name="ticket_freeze_startdate" placeholder="Freeze start date" required>
                                    <option value="" selected disabled>Select</option>
                                    <option value="01 April 2020">01 April 2020</option>
                                    <option value="01 May 2020">01 May 2020</option>
                                    <option value="01 June 2020">01 June 2020</option>
                                    <option value="01 July 2020">01 July 2020</option>
                                    <option value="01 August 2020">01 August 2020</option>
                                    <option value="01 September 2020">01 September 2020</option>
                                </select></div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="ticket_freeze_enddate">Freeze end date</label>
                                <select name="ticket_freeze_enddate" placeholder="Freeze end date" required>
                                    <option value="" selected disabled>Select</option>
                                    <option value="30 April 2020">30 April 2020</option>
                                    <option value="31 May 2020">31 May 2020</option>
                                    <option value="30 June 2020">30 June 2020</option>
                                    <option value="31 July 2020">31 July 2020</option>
                                    <option value="31 August 2020">31 August 2020</option>
                                    <option value="30 September 2020">30 September 2020</option>
                                </select>
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <label for="ticket_comments">Please enter any comments below</label>
                                <textarea name="ticket_comments" placeholder="Please enter any comments below"
                                    required></textarea>
                            </div>
                            <div class="_2nxeEK_ _14NUrhZ">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="check" name="check"
                                        required>
                                    <label class="custom-control-label" for="check">I fully agree to
                                        receive contact via telephone & give consent that my data will be stored
                                        and handled by this website</label>
                                </div>
                            </div>
                            <!-- <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[charge]">I accept the £20/pm charge<span>
                                        (unless pregnancy or injury was specified)</span></label><select
                                    name="ticket[charge]" placeholder="I accept the £20/pm charge">
                                    <option value="select">Select</option>
                                    <option value="Agreed">Agreed</option>
                                </select></div>-->
                            <input type="submit" name="Submit_membership" value="Submit">
                        </form>
                    </div>

                    <!-- <div class="_2x_S9gP _2eBzx2S _2v5bHvx dis-none ser2">
                        <form class="_81firkY">
                            <p>There are things you don't want to tweak (your hamstring, for example) and things you do,
                                like your Gymbox membership. So if you want to add something extra like access to all
                                clubs or a permanent locker, this is the place to tweak.</p>
                            <div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[homeClub]">Home club</label><select
                                        name="user[homeClub]" placeholder="Home club">
                                        <option value="select">Select</option>
                                        <option value="Bank">Bank</option>
                                        <option value="Cannon Street">Cannon Street</option>
                                        <option value="Covent Garden">Covent Garden</option>
                                        <option value="Ealing">Ealing</option>
                                        <option value="Elephant and Castle">Elephant and Castle</option>
                                        <option value="Farringdon">Farringdon</option>
                                        <option value="Finsbury Park">Finsbury Park</option>
                                        <option value="Holborn">Holborn</option>
                                        <option value="Old Street">Old Street</option>
                                        <option value="Victoria">Victoria</option>
                                        <option value="Westfield London">Westfield London</option>
                                        <option value="Westfield Stratford">Westfield Stratford</option>
                                    </select></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[name]">Name<span> (must match the named
                                            used for the membership)</span></label><input name="user[name]"
                                        placeholder="Name" type="text" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[email]">Email<span> (must be the address
                                            used to login)</span></label><input name="user[email]" placeholder="Email"
                                        type="email" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[phone]">Phone number</label><input
                                        name="user[phone]" placeholder="Phone number" type="text" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[membershipNumber]">Membership
                                        number</label><input name="user[membershipNumber]"
                                        placeholder="Membership number" type="text" value=""></div>
                            </div><input type="hidden" name="subject" value="Package Change">
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[typeOfChange]">Type of
                                    change</label><select name="ticket[typeOfChange]" placeholder="Type of change">
                                    <option value="select">Select</option>
                                    <option value="Locker Hire Options">Locker Hire Options</option>
                                    <option value="Link membership to access all clubs">Link membership to access all
                                        clubs</option>
                                    <option value="Renew my membership">Renew my membership</option>
                                    <option value="Change my Home Club">Change my Home Club</option>
                                    <option value="Other">Other</option>
                                </select></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[changeStartDate]">Start
                                    date</label><select name="ticket[changeStartDate]" placeholder="Start date">
                                    <option value="select">Select</option>
                                    <option value="Immediately">Immediately</option>
                                    <option value="01 April 2020">01 April 2020</option>
                                    <option value="01 May 2020">01 May 2020</option>
                                    <option value="01 June 2020">01 June 2020</option>
                                    <option value="01 July 2020">01 July 2020</option>
                                    <option value="01 August 2020">01 August 2020</option>
                                    <option value="01 September 2020">01 September 2020</option>
                                </select></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[comments]">Please enter any comments
                                    below</label><textarea name="ticket[comments]"
                                    placeholder="Please enter any comments below"></textarea></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[terms]">I have read and understand the
                                    Terms and Conditions</label><select name="ticket[terms]"
                                    placeholder="I have read and understand the Terms and Conditions">
                                    <option value="No" disabled="">Select</option>
                                    <option value="Agreed">Agreed</option>
                                </select></div><input type="submit" value="Submit">
                        </form>
                    </div>

                    <div class="_2x_S9gP _2eBzx2S _2v5bHvx dis-none ser3">
                        <form class="_81firkY">
                            <p>Nothing hurts more than a 36th pull up, except when someone tells us they're leaving. So
                                if you've decided to cancel your membership, please tell us why here and we'll see if
                                there's something we can do to change your mind… even a 37th pull up.</p>
                            <div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[homeClub]">Home club</label><select
                                        name="user[homeClub]" placeholder="Home club">
                                        <option value="select">Select</option>
                                        <option value="Bank">Bank</option>
                                        <option value="Cannon Street">Cannon Street</option>
                                        <option value="Covent Garden">Covent Garden</option>
                                        <option value="Ealing">Ealing</option>
                                        <option value="Elephant and Castle">Elephant and Castle</option>
                                        <option value="Farringdon">Farringdon</option>
                                        <option value="Finsbury Park">Finsbury Park</option>
                                        <option value="Holborn">Holborn</option>
                                        <option value="Old Street">Old Street</option>
                                        <option value="Victoria">Victoria</option>
                                        <option value="Westfield London">Westfield London</option>
                                        <option value="Westfield Stratford">Westfield Stratford</option>
                                    </select></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[name]">Name<span> (must match the named
                                            used for the membership)</span></label><input name="user[name]"
                                        placeholder="Name" type="text" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[email]">Email<span> (must be the address
                                            used to login)</span></label><input name="user[email]" placeholder="Email"
                                        type="email" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[phone]">Phone number</label><input
                                        name="user[phone]" placeholder="Phone number" type="text" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[membershipNumber]">Membership
                                        number</label><input name="user[membershipNumber]"
                                        placeholder="Membership number" type="text" value=""></div>
                            </div><input type="hidden" name="subject" value="Cancellation">
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[reasonForCancellation]">Reason for
                                    cancellation</label><select name="ticket[reasonForCancellation]"
                                    placeholder="Reason for cancellation">
                                    <option value="select">Select</option>
                                    <option value="Facilities">Facilities</option>
                                    <option value="Financial">Financial</option>
                                    <option value="Gym/Classes too busy">Gym/Classes too busy</option>
                                    <option value="Quality of classes">Quality of classes</option>
                                    <option value="Location no longer convenient">Location no longer convenient</option>
                                    <option value="Medical/Pregnancy">Medical/Pregnancy</option>
                                    <option value="Service">Service</option>
                                    <option value="Travel">Travel</option>
                                    <option value="Joined another gym">Joined another gym</option>
                                    <option value="Lack of use">Lack of use</option>
                                </select></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[cancellationDate]">Cancellation
                                    date</label><select name="ticket[cancellationDate]" placeholder="Cancellation date">
                                    <option value="select">Select</option>
                                    <option value="30 April 2020">30 April 2020</option>
                                    <option value="31 May 2020">31 May 2020</option>
                                    <option value="30 June 2020">30 June 2020</option>
                                    <option value="31 July 2020">31 July 2020</option>
                                    <option value="31 August 2020">31 August 2020</option>
                                    <option value="30 September 2020">30 September 2020</option>
                                </select></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[comments]">Please enter any comments
                                    below</label><textarea name="ticket[comments]"
                                    placeholder="Please enter any comments below"></textarea></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[terms]">I have read and understand the
                                    Terms and Conditions</label><select name="ticket[terms]"
                                    placeholder="I have read and understand the Terms and Conditions">
                                    <option value="select">Select</option>
                                    <option value="Agreed">Agreed</option>
                                </select></div>
                            <p>We're sad to see you go but we'll still keep you in the loop about all the amazing
                                parties we're having, the new classes we've created and new gyms we've designed. Really
                                want this to be the end? You can visit our privacy policy for information on how to
                                change your preferences.</p><input type="submit" value="Submit">
                        </form>
                    </div>

                    <div class="_2x_S9gP _2eBzx2S _2v5bHvx dis-none ser4">
                        <form class="_81firkY">
                            <p>Moved home? Changed your mobile number? Been elevated to the peerage ("Arise Lord Pushup
                                of Farringdon")? If your personal details have changed, please make sure you tell us.
                            </p>
                            <div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[homeClub]">Home club</label><select
                                        name="user[homeClub]" placeholder="Home club">
                                        <option value="select">Select</option>
                                        <option value="Bank">Bank</option>
                                        <option value="Cannon Street">Cannon Street</option>
                                        <option value="Covent Garden">Covent Garden</option>
                                        <option value="Ealing">Ealing</option>
                                        <option value="Elephant and Castle">Elephant and Castle</option>
                                        <option value="Farringdon">Farringdon</option>
                                        <option value="Finsbury Park">Finsbury Park</option>
                                        <option value="Holborn">Holborn</option>
                                        <option value="Old Street">Old Street</option>
                                        <option value="Victoria">Victoria</option>
                                        <option value="Westfield London">Westfield London</option>
                                        <option value="Westfield Stratford">Westfield Stratford</option>
                                    </select></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[name]">Name<span> (must match the named
                                            used for the membership)</span></label><input name="user[name]"
                                        placeholder="Name" type="text" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[email]">Email<span> (must be the address
                                            used to login)</span></label><input name="user[email]" placeholder="Email"
                                        type="email" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[phone]">Phone number</label><input
                                        name="user[phone]" placeholder="Phone number" type="text" value=""></div>
                                <div class="_2nxeEK_ _14NUrhZ"><label for="user[membershipNumber]">Membership
                                        number</label><input name="user[membershipNumber]"
                                        placeholder="Membership number" type="text" value=""></div>
                            </div><input type="hidden" name="subject" value="Change of Personal Details">
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[updateEmailAddress]">Update email
                                    address</label><input name="ticket[updateEmailAddress]"
                                    placeholder="Update email address" type="email" value=""></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[updateTelephoneNumber]">Update telephone
                                    number</label><input name="ticket[updateTelephoneNumber]"
                                    placeholder="Update telephone number" type="text" value=""></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[updateMobileNumber]">Update mobile
                                    number</label><input name="ticket[updateMobileNumber]"
                                    placeholder="Update mobile number" type="text" value=""></div>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="ticket[updateAddress]">Update
                                    address</label><textarea name="ticket[updateAddress]"
                                    placeholder="Update address"></textarea></div><input type="submit" value="Submit">
                        </form>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $("._Y55-bj").click(function() {
        $("._2rbE6TC").removeClass("_3r1Y9_D");
        $(this).addClass("_3r1Y9_D");
    });
    $(".service1").click(function() {
        $(".ser1").show();
        $(".ser2").hide();
        $(".ser3").hide();
        $(".ser4").hide();
        $(".ser5").hide();
        $(".ser6").hide();
        $(".ser7").hide();
        $(".ser8").hide();
    });
    $(".service2").click(function() {
        $(".ser1").hide();
        $(".ser2").show();
        $(".ser3").hide();
        $(".ser4").hide();
        $(".ser5").hide();
        $(".ser6").hide();
        $(".ser7").hide();
        $(".ser8").hide();
    });
    $(".service3").click(function() {
        $(".ser1").hide();
        $(".ser2").hide();
        $(".ser3").show();
        $(".ser4").hide();
        $(".ser5").hide();
        $(".ser6").hide();
        $(".ser7").hide();
        $(".ser8").hide();
    });
    $(".service4").click(function() {
        $(".ser4").show();
        $(".ser1").hide();
        $(".ser2").hide();
        $(".ser3").hide();
        $(".ser5").hide();
        $(".ser6").hide();
        $(".ser7").hide();
        $(".ser8").hide();
    });
});
</script>
<?php get_footer(); ?>