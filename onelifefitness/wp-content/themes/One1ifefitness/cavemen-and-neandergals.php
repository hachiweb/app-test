<?php /* Template Name: cavemen-and-neandergals */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_14BrxaV">
    <div class="_1vdzHPH">
        <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
            <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
                <h2>Cavemen &amp; Neandergals</h2>
                <div class="_1-4gF4V">
                    <p>Ditch your prehistoric training programme and listen to your body’s most primal urges. This
                        Caveman-style circuit class goes back to basics – using intense obstacle-based HIIT circuit
                        training to give you hunter-gatherer agility, animalistic speed and the strength to take down an
                        angry mammoth if required. All essential skills for London life.</p>
                </div>
                <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                    <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                        <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                        <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span
                                class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Holborn</span><span
                                class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield Stratford</span></div>
                    </li>
                </ul>
                <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Select gym</option>
                        <option value="Holborn">Holborn</option>
                        <option value="Westfield Stratford">Westfield Stratford</option>
                        <option value="Bank">Bank</option>
                        <option value="Victoria">Victoria</option>
                    </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                        <option value="" disabled="">Choose time</option>
                        <option value="1263000" disabled="">12:15 Monday 17th (45 minutes) - Bookable 7am day before
                        </option>
                    </select><a
                        href=""
                        target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
                <div class="_3srnCE8">
                    <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                        <div class="_2Tb-We1 sDHKtsB">
                            <ul class="_2J71_L4 sDHKtsB">
                                <li class="NPRZnJb _1kNfguI sDHKtsB">
                                    <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                    <h2>Your <br>instructors</h2>
                    <div class="Sa1FFQw">
                        <div class="_3fJiN_C">
                            <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                                    class="_3h_jpHd _2jVkQgE"></div>
                            <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                            <div>
                                <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                                    &nbsp;classes range from Gains to Escalate, and are based around personal
                                    development in a motivational team environment. His love for fitness is second only
                                    to his love of the onelifefitness team.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wqBYWJi">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Caveman6.jpg');"></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_3reSXV1 _4zWqqYK sDHKtsB">Loading: 100%</div>
    <div class="_12c1-Jf sDHKtsB">
        <div class="LN-nIKU I7PwxVS">
            <div class="_1L5csiF">
                <div class="_1US2FDM _2rbE6TC _2v5bHvx VBD7Ow3">
                    <h3 class="LsKezX_">FREE PASS</h3><span class="_2va2FpG"></span>
                </div>
                <div class="_3ITUOra _2rbE6TC _2v5bHvx VBD7Ow3" style="width: 5366.38px; transform: translateX(0px);">
                    <form>
                        <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                            <div class="Zd4QDDO">
                                <h4 class="Zd4QDDO">1/6</h4>
                            </div>
                            <h2 class="_1_KNBvC">What's your name?</h2>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="fromName"></label><input type="text"
                                    name="fromName" value="" placeholder="Name"></div>
                        </div>
                        <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                            <div class="Zd4QDDO">
                                <h4 class="Zd4QDDO">2/6</h4>
                            </div>
                            <h2 class="_1_KNBvC">What's your email?</h2>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="fromEmail"></label><input type="email"
                                    name="fromEmail" value="" placeholder="Email"></div>
                        </div>
                        <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                            <div class="Zd4QDDO">
                                <h4 class="Zd4QDDO">3/6</h4>
                            </div>
                            <h2 class="_1_KNBvC">What's your phone?</h2>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="message[mobile]"></label><input type="text"
                                    name="message[mobile]" value="" placeholder="Phone"></div>
                        </div>
                        <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                            <div class="Zd4QDDO">
                                <h4 class="Zd4QDDO">4/6</h4>
                            </div>
                            <h2 class="_1_KNBvC">Select club</h2>
                            <div class="_2nxeEK_ _14NUrhZ"><label for="message[gym]"></label><select
                                    name="message[gym]">
                                    <option value="select">Select</option>
                                    <option value="Bank">Bank</option>
                                    <option value="Cannon Street">Cannon Street</option>
                                    <option value="Covent Garden">Covent Garden</option>
                                    <option value="Ealing">Ealing</option>
                                    <option value="Elephant and Castle">Elephant and Castle</option>
                                    <option value="Farringdon">Farringdon</option>
                                    <option value="Finsbury Park">Finsbury Park</option>
                                    <option value="Holborn">Holborn</option>
                                    <option value="Old Street">Old Street</option>
                                    <option value="Victoria">Victoria</option>
                                    <option value="Westfield London">Westfield London</option>
                                    <option value="Westfield Stratford">Westfield Stratford</option>
                                </select></div>
                        </div>
                        <div class="_26Hb2eR _2cjZ1PT _1G7RCdW _2v5bHvx">
                            <div class="Zd4QDDO">
                                <h4 class="Zd4QDDO">5/6</h4>
                            </div>
                            <h2 class="_1_KNBvC">Time to submit!</h2>
                            <p>I understand that by submitting my details I will be contacted by onelifefitness with information
                                about their services and membership options (not with spam, we promise)</p><input
                                type="submit" value="Book your free tour!">
                        </div>
                        <div class="_3ipyBdB">
                            <h2>Thanks - chat soon</h2>
                        </div>
                    </form>
                </div>
                <div class="_2bIo9Yp _2rbE6TC _2v5bHvx VBD7Ow3"><button
                        class="_3pXyaiA _2S_CT_r"><span>&nbsp;</span></button><button><span>Next</span></button></div>
            </div>
        </div>
    </div>
</div>