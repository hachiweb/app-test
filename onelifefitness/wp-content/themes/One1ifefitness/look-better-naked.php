<?php /* Template Name: look-better-naked*/
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<section class="_1J0RKGy _2rbE6TC _2v5bHvx">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                </ol>
                <div class="carousel-inner" role="listbox">
                <div class="carousel-item active"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="_3SYd3N0 _2rbE6TC _2v5bHvx">
    <div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
        <div class="_130-KDr">
            <div class="">
                <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                    <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                        <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box xpress</h3>
                        <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                            <p class="LinesEllipsis LinesEllipsis--clamped ">The name
                                ain’t just for fun. You’re about to squeeze, squat, lift and burn your way through a
                                class that whoops your<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                        </div>
                    </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/badass"></a>
                </div>
                <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                    <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                        <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">r1de xpress</h3>
                        <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                            <p class="LinesEllipsis LinesEllipsis--clamped ">All aboard
                                the gain train. Whether you need expert tips to tone up your technique or if solo free
                                weights sessions<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                        </div>
                    </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/gain-train"></a>
                </div>
                <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                    <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                    <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                        <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">k1ds boxing</h3>
                        <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                            <p class="LinesEllipsis LinesEllipsis--clamped ">If you
                                thought the name was inspired by the killer abs you’re going to get from this class,
                                we’ve got bad news. Try the<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                        </div>
                    </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/hardcore"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
// get_sidebar();
get_footer();
?>