<?php /* Template Name: vinyasa-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
        <h2>Vinyasa Flow Yoga</h2>
        <div class="_1-4gF4V">
            <p>Smooth as silk. Chill as you like. Vinyasa flow is your secret oasis of order amongst the chaos of modern
                life. Knock the frenzy down a few notches by channelling your energies into a dynamic flow that builds
                strength, improves flexibility and restores sanity one delicious breath at a time.</p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Bank</span><span class="Nfa775m">Cannon
                        Street</span><span class="Nfa775m">Covent Garden</span><span class="Nfa775m">Ealing</span><span
                        class="Nfa775m">Elephant and Castle</span><span class="Nfa775m">Farringdon</span><span
                        class="Nfa775m">Holborn</span><span class="Nfa775m">Old Street</span><span
                        class="Nfa775m">Victoria</span><span class="Nfa775m">Westfield London</span><span
                        class="Nfa775m">Westfield Stratford</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Select gym</option>
                <option value="Farringdon">Farringdon</option>
                <option value="Elephant and Castle">Elephant and Castle</option>
                <option value="Cannon Street">Cannon Street</option>
                <option value="Holborn">Holborn</option>
                <option value="Victoria">Victoria</option>
                <option value="Old Street">Old Street</option>
                <option value="Westfield Stratford">Westfield Stratford</option>
                <option value="Westfield London">Westfield London</option>
                <option value="Bank">Bank</option>
                <option value="Covent Garden">Covent Garden</option>
                <option value="Ealing">Ealing</option>
            </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1324751">11:00 Sunday 16th (60 minutes)</option>
                <option value="1293261" disabled="">19:15 Monday 17th (60 minutes) - Bookable 7am day before</option>
                <option value="1251373" disabled="">07:00 Thursday 20th (60 minutes) - Bookable 7am day before</option>
                <option value="1297880" disabled="">13:00 Friday 21st (45 minutes) - Bookable 7am day before</option>
            </select><a
                href=""
                target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ashley_Ahrens.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ashley</h3>
                    <div>
                        <p>Ashley’s classes are disciplined, yet nurturing. She places a great deal of emphasis on
                            connecting deep within your own body, using the breath as a tool to distract the mind,
                            paired with fascial tissue and energetic lines of awareness to understand postures on a very
                            personal level. With 9 years of dedicated practice, 400-hours of yoga training, and a
                            Biology degree, she has a lot of inspiration and knowledge to share!
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Dylan_Salamon.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Dyl</h3>
                    <div>
                        <p>Creator of Yoga for Lifting &amp; Active Beast. Ex rugby player, now scratching that itch by
                            competing in yoga and Crossfit. I love anything that challenges you and involves the body
                            moving. Currently trying to break the stereotype that yoga isn’t for big dudes. Unless
                            you’re too scared to try ;) </p>
                        <p> </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ellisif_Bendiksen_1.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ellisif</h3>
                    <div>
                        <p>Ellisif has more than 15 years experience studying movement
                            through dance, calisthenics, acrobatics and yoga, and specialises in yoga and
                            skill based training. With a mantra of ‘life is better upside-down’, you can
                            expect her classes to be challenging, playful and fun.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Anna_Jackson.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hannah J</h3>
                    <div>
                        <p>Hannah was forced to give up her life as a professional
                            dancer in 2012 due to a back injury. This led her to discover her passion for
                            yoga, and having retrained as an instructor in 2014, Hannah now focuses on
                            developing her clients strength and flexibility. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Clare_Nagarajan.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Clare</h3>
                    <div>
                        <p>Clare is a yoga instructor who specialises in Vinyasa, Ashtanga, Yin, pregnancy and
                            children’s yoga. She loves to bring elements of her training in meditation and mindfulness
                            to her teaching, and her classes provide a moment for you take a break from everyday life.
                        </p>
                        <p><br></p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ryan_CM.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ryan C</h3>
                    <div>
                        <p>A PT with over 10 years experience teaching yoga, martial
                            arts and functional fitness, Ryan has a passion for helping people create
                            freedom in their bodies and achieve their goals. His classes are playful,
                            athletic and challenging, with sweat and smiles in abundance. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Laura_Pearce.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Laura</h3>
                    <div>
                        <p>Having studied under the renowned Swami Niralananda Giri at the
                            Sivananda Yoga centre, Laura holds the highest certification awarded by Yoga
                            Alliance UK. Laura’s classes are challenging and dynamic, based on practises
                            that detoxify and restore the body and mind.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_David_Olton.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">David</h3>
                    <div>
                        <p>David is an international dance and yoga instructor with a
                            background in pilates and ballet, who has worked for productions such as Comic
                            Relief and Sky FabTV. You can find David teaching Vinyasa Flow Yoga at Gymbox,
                            and can always expect enthusiasm in his classes. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Vinyasa.jpg');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div></div>