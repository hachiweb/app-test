<?php /* Template Name: hardcore */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
    <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
        <h2>K1ds Boxing</h2>
        <div class="_1-4gF4V">
            <p>If you thought the name was inspired by the killer abs you’re going to get from this class, we’ve got bad
                news. Try the killer workout you crunch, twist and plank through to get there. This is one mad assault
                on your middle that definitely earns that 6 pack. (And the one in the fridge.)</p>
        </div>
        <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
            <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                <span class="Nfa775m">Onelifefitness Camden quay</span>
                <span class="Nfa775m">Onelifefitness Sullivans quay</span></div>
            </li>
        </ul>
        <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
        <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
            <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                <option value="" disabled="">Choose time</option>
                <option value="1236472" disabled="">07:30 Monday 17th (30 minutes) - Bookable 7am day before</option>
                <option value="1304733" disabled="">17:45 Monday 17th (30 minutes) - Bookable 7am day before</option>
                <option value="1290659" disabled="">17:45 Tuesday 18th (30 minutes) - Bookable 7am day before</option>
            </select><a
                href="https://indma05.clubwise.com/onelifefitness/index.html"
                target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
        <div class="_3srnCE8">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
            <h2>Your <br>instructors</h2>
            <div class="Sa1FFQw">
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Mark_Lenihan.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Mark</h3>
                    <div>
                        <p>Mark has been in the fitness industry since 2010, and
                            specialises in bodyweight calisthenics, gymnastics and HIIT. He believes that
                            the first step to any transformation is being aware of the choices you make,
                            and how they effect yourself and others. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Giselle_Grant.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Giselle</h3>
                    <div>
                        <p>Having worked as a speech therapist for 10 years, Giselle
                            decided to follow her passion for fitness and become a PT and fitness
                            instructor. Giselle specialises in HIIT and functional training, and loves
                            nothing more than to push people above and beyond their limits. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Matteo_Cruciani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Matteo</h3>
                    <div>
                        <p>A native Italian, Matteo was born and raised in the city of Rome.&nbsp;He grew up with a huge
                            passion for fitness, dance and movement.&nbsp;He is an&nbsp;enthusiastic and charismatic
                            person who brings a positive attitude to every situation!</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                    <div>
                        <p>Through a combination of tough-love and motivation, Stevie will push you to your limits with
                            high-energy metabolic conditioning workouts that will leave you in a pool of sweat and a big
                            smile on your face.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Aaron_Cook.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Aaron C</h3>
                    <div>
                        <p>Aaron believes that everyone is an athlete, so everyone should perform and be coached like
                            one. His passion for coaching with education and enthusiasm helps give his private clients
                            and our members the results they want. Olympic lifting, gymnastics, engine work and
                            flexibility are all skills of his own training that he implements though his classes. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Yvette_Carter_Extra_Option_4.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Yvette C</h3>
                    <div>
                        <p>Having earned a degree in theatre dance from the London
                            Studio Centre, Yvette went on to become a qualified PT and is now a studio
                            instructor at Gymbox. Yvette’s philosophy is that training should be fun with
                            hardwork thrown in.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Firas_Iskandarani.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Firas - Master Trainer</h3>
                    <div>
                        <p>Firas found his passion for American Football 15 years ago, and has since won numerous
                            championships as both a player and coach. Firas brings his enthusiasim and camaraderie to
                            all his classes, and will undoubtedly bring out the inner athlete in you. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Lawrence_Cain.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Lawrence</h3>
                    <div>
                        <p>Lawrence focuses on calisthenics, strength training and body building in his training, and
                            teaches Frame Fitness, Ripped &amp; Stripped, Drill Sergeant and Kettlebells. Lawrence’s
                            no-nonsense, hardcore attitude is sure to push you beyond your limits.</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Danielle_Linton.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Danielle</h3>
                    <div>
                        <p>With a degree in Exercise Science, and qualifications in
                            Keiser Cycle, kettlebells and suspension training, Danielle is a PT and studio
                            instructor who’s eclectic classes will get you feeling motivated, inspired and
                            empowered. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ryan_CM.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ryan C</h3>
                    <div>
                        <p>A PT with over 10 years experience teaching yoga, martial
                            arts and functional fitness, Ryan has a passion for helping people create
                            freedom in their bodies and achieve their goals. His classes are playful,
                            athletic and challenging, with sweat and smiles in abundance. </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jamie_Shaw.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jamie</h3>
                    <div>
                        <p>A PT and instructor with an extensive background in the fitness industry, Jamie’s
                            &nbsp;classes range from Gains to Escalate, and are based around personal development in a
                            motivational team environment. His love for fitness is second only to his love of the Gymbox
                            team.
                        </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_James_Dawkins.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">James - Master Trainer</h3>
                    <div>
                        <p>London based House DJ and our Look Better Naked Master Trainer, James is passionate about
                            leading a team that will help you not only look great naked, but also feel great naked. Hop
                            into any of his classes and you'll quickly learn why we're the antidote to boring gyms! </p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Daisy_Rusby.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Daisy</h3>
                    <div>
                        <p>A lover of lifting heavy things and dancing to her hearts content, Daisy is a PT and studio
                            instructor who’s specialities include kettlebells, strength training and holistic
                            flexibility. Her classes are based on teamwork and guaranteed to help you reach your fitness
                            potential.&nbsp;</p>
                    </div>
                </div>
                <div class="_3fJiN_C">
                    <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                            src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Chitose_Paine.jpg"
                            class="_3h_jpHd _2jVkQgE"></div>
                    <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Chitose</h3>
                    <div>
                        <p>Chi considers herself lucky to have found a love for the fitness industry, and appreciates
                            the positive effect it has both on the mind and the body. Her passion is teaching, and she
                            loves to share her extensive fitness knowledge with her clients and class-goers.&nbsp; </p>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <div class="wqBYWJi">
        <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
            <div class="_2Tb-We1 sDHKtsB">
                <ul class="_2J71_L4 sDHKtsB">
                    <li class="NPRZnJb _1kNfguI sDHKtsB">
                        <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OScreenshot-2019.png');"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>