<?php /* Template Name: post-details */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3l5upd8 _2rbE6TC _2v5bHvx">
        <div class="_2DStQxp _1G7RCdW _2v5bHvx VBD7Ow3">
            <h4>Categories</h4>
            <!-- <ul class="Y6VQYPu">
                <li class="_3VAxuP5"><a href="/blog">All</a></li>
                <li class="_2PCsNDZ _3VAxuP5"><a href="/blog">Classes</a></li>
                <li class="_3VAxuP5"><a href="/blog">Reviews</a></li>
                <li class="_3VAxuP5"><a href="/blog">Interviews</a></li>
                <li class="_3VAxuP5"><a href="/blog">News</a></li>
                <li class="_3VAxuP5"><a href="/blog">Advice</a></li>
                <li class="_3VAxuP5"><a href="/blog">Nutrition</a></li>
                <li class="_3VAxuP5"><a href="/blog">Events</a></li>
                <li class="_3VAxuP5"><a href="/blog">Training</a></li>
                <li class="_3VAxuP5"><a href="/blog">Stories</a></li>
            </ul> -->
            <?php $categories = get_categories();
            foreach($categories as $category) {
            echo '<div class="col-md-4"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';
            } ?>
        </div>
        <div class="_2uaX48K _1G7RCdW _2v5bHvx VBD7Ow3">
            <div class="_2fGVUXV _2rbE6TC _2v5bHvx _3HiqiFI">
                <div>
                    <h2 class="_3L7FtF7">The Collab You've All Been Waiting For...</h2>
                    <p>27th February</p>
                </div>
                <div>
                    <p><em>If you follow our socials you’ll have already seen our big news, if not, here it comes..
                            We’ve teamed up with our mates over at Gymshark at their month-long pop up store that’s
                            going to take Covent Garden by storm for three weeks and we’ll be bringing with us some of
                            London’s most unique classes. Keep reading for everything you need to know.</em></p>
                    <p><img src="https://gymbox-assets.enecdn.io/images/blog/Assets-with-white-logo_200225_110600.jpg?mtime=20200225110600"
                            alt="Assets-with-white-logo_200225_110600.jpg?mtime=20200225110600#asset:398077"><br></p>
                    <p>Throwing the doors open on Friday 28th February for their longest pop up EVER, Gymshark are
                        bringing their URL to IRL for 3 wonderful weeks in Covent Garden. Upstairs will be a full retail
                        store with the shop being refreshed with new releases EVERY week. Downstairs is where we come
                        in, and things are about to get a lot sweatier…</p>
                    <p>For 3 weeks, we’ll be bringing 16 of our unique classes to Long Acre and each week will be
                        showcasing a different style of training. Week one is all things MetCon, week two is combat
                        focussed and we’ll be bringing some holistic vibes to finish the collab in week three.
                    </p>
                    <p><img src="https://gymbox-assets.enecdn.io/images/blog/metcon-gymshark.jpg?mtime=20200225110145"
                            alt="metcon-gymshark.jpg?mtime=20200225110145#asset:398073"><br></p>
                    <p><br></p>
                    <p>Not sure what you're into, check out the full class descriptions below:
                    </p>
                    <p><strong>METCON HIIT</strong> | Short, quick and hotter than hell – there’s no messing about with
                        MetCon. It’s the no-nonsense way to walk in and crawl out.
                    </p>
                    <p><strong>BEASTMODE</strong> | If you’re after bodyweight HIIT and weighted conditioning in a
                        single workout this ones for you, prepare to go full beast. </p>
                    <p><strong>CAVEMEN AND NEANDERGALS</strong> | A circuit class, but not as you know it. Alternate
                        between bodyweight and obstacle based exercises... kettlebell swings, box jumps, sandbag slams +
                        more.
                    </p>
                    <p><strong>AMRAP</strong> | A morning session to help you bust as many reps as possible with a speed
                        rap soundtrack to help you on your way.
                    </p>
                    <p><strong>BATTLEBELLS</strong> | Whether you wage war with ropes, sandbags or kettlebells, this is
                        a workout for true warriors focussing on power, endurance and HIIT.
                    </p>
                    <p><strong>WOD SQUAD</strong> | Pulling, pushing, climbing, lifting. Simple, right? You’ll mix up
                        these four functional movements to give your body a good hiding. </p>
                    <p><img src="https://gymbox-assets.enecdn.io/images/blog/combat-gymshark.jpg?mtime=20200225110205"
                            alt="combat-gymshark.jpg?mtime=20200225110205#asset:398074"><br></p>
                    <p>Fancy getting all fight club, check out what they’re all about below:<br></p>
                    <p><strong>COUNTERKICK </strong>| A cardio based kickboxing class. Expect to find rounds of HIIT
                        training and bag work which take you through the basic boxing and kicking techniques.
                    </p>
                    <p><strong>COUNTERPUNCH</strong> | A cardio based boxing class. Slightly different to Counterkick,
                        this class revolves more around gymboxing; taking you through the basic boxing techniques with
                        HIIT training and bag work.
                    </p>
                    <p><strong>KILLER COMBAT</strong> | Whether you’re kicking up a few notches in prep for a fight or
                        just like more workouts a bit tougher than most, try a combat circuit that would make even Rocky
                        cry.
                    </p>
                    <p><strong>SWORDPLAY </strong>| Combining discipline, focus and a graceful touch through dance,
                        Swordplay is a coming together of ancient art form with choreographed combat.
                    </p>
                    <p><img src="https://gymbox-assets.enecdn.io/images/blog/flow-gymshark.jpg?mtime=20200225110313"
                            alt="flow-gymshark.jpg?mtime=20200225110313#asset:398075"><br></p>
                    <p>Swole and flexy more your vibe? Check out the flow classes below:</p>
                    <p><strong>ROLLING WITH MY YOGIS </strong>| A dynamic yoga flow with a yoga wheel created to help
                        perfect various yoga poses and backbends.
                    </p>
                    <p><strong>CONTORTION</strong> | Focusing on backbends and the splits through progressive
                        stretching, this class utilises props such as blocks and ropes to help improve all-round
                        flexibility.
                    </p>
                    <p><strong>YOGA FOR LIFTING </strong>| Whatever impact your training has had on your body, Yoga for
                        Lifting makes the most of your motion by getting stronger, stretchier and increasing your range
                        of movement. </p>
                    <p><strong>STRETCH AND MASSAGE </strong>| Feeling a little sore and tight from your training
                        session? You're in luck - this class guides you through a variety of stretching ropes and foam
                        rolling exercises; increasing flexibility and promoting myofascial release.
                    </p>
                    <p><strong>SWEAT TO THE BEAT </strong>| This is bodyweight endurance to a beat, challenging your
                        strength, sculpting your figure and making music pulse through every muscle. This catchy
                        conditioning class is guaranteed to get peak fitness pouring from every pore.
                    </p>
                    <p><em>For all the info on when the next ticket drop is happening keep an eye on our <a
                                href="https://www.instagram.com/gymboxofficial/" target="_blank"
                                rel="noreferrer noopener">socials</a>, you don’t want to miss this. </em>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>