<?php /* Template Name: categories */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div id="root">
    <div class="_14BrxaV">
        <div class="_1vdzHPH">
            <div>
                <div class="_24oGOi1 _2rbE6TC _2v5bHvx" id="blurb-box">
                    <h1 class="_2BnL5rS I7PwxVS _1w_OEYg" style="opacity: 1;">Select classes</h1>
                </div>
                <ul class="_2i5ovmW _2rbE6TC _2v5bHvx">
                    <!-- <li class="_2fBxcy1 _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB">
                                <video autoplay="" class="_3h_jpHd _20CZYOH" playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Andrea_12-com.mp4"></video>
                            </div>
                        </div>
                        <a class="_3C8M2qT" href="<?= get_site_url(); ?>/classes">
                            <h2 class="E7hZVuN">show all classe</h2>
                            <h4 class="_1qvAHdq">24 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/classes"></a>
                    </li>
                    <li class="_1Ogv5Gv _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB">
                                <video autoplay="" class="_3h_jpHd _20CZYOH" playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Box_12-com.mp4"></video>
                            </div>
                        </div>
                        <a class="_3C8M2qT" href="<?= get_site_url(); ?>/combat-sports">
                            <h2 class="E7hZVuN">Combat Sports</h2>
                            <h4 class="_1qvAHdq">10 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/combat-sports"></a>
                    </li>

                    <li class="_2fBxcy1 _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB"><video autoplay="" class="_3h_jpHd _20CZYOH"
                                    playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/R1DE1_2-com.mp4"></video>
                            </div>
                        </div><a class="_3C8M2qT" href="<?= get_site_url(); ?>/cycle-club">
                            <h2 class="E7hZVuN">Cycle Club</h2>
                            <h4 class="_1qvAHdq">4 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/cycle-club"></a>
                    </li>
                    <li class="_1Ogv5Gv _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB">
                                <video autoplay="" class="_3h_jpHd _20CZYOH" playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Andrea_32-com.mp4"></video>
                            </div>
                        </div>
                        <a class="_3C8M2qT" href="<?= get_site_url(); ?>/functional-training">
                            <h2 class="E7hZVuN">Functional Training</h2>
                            <h4 class="_1qvAHdq">23 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/functional-training"></a>
                    </li>

                    <li class="_2fBxcy1 _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB"><video autoplay="" class="_3h_jpHd _20CZYOH"
                                    playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Nora_21-com.mp4"></video>
                            </div>
                        </div><a class="_3C8M2qT" href="<?= get_site_url(); ?>/holistic-retreat">
                            <h2 class="E7hZVuN">Holistic Retreat</h2>
                            <h4 class="_1qvAHdq">17 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/holistic-retreat"></a>
                    </li>
                    <li class="_1Ogv5Gv _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB">
                                <video autoplay="" class="_3h_jpHd _20CZYOH" playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Jorge_22-com.mp4"></video>
                            </div>
                        </div>
                        <a class="_3C8M2qT" href="<?= get_site_url(); ?>/look-better-naked">
                            <h2 class="E7hZVuN">Look Better Naked</h2>
                            <h4 class="_1qvAHdq">7 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/look-better-naked"></a>
                    </li>

                    <li class="_2fBxcy1 _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB"><video autoplay="" class="_3h_jpHd _20CZYOH"
                                    playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Box_122-com.mp4"></video>
                            </div>
                        </div><a class="_3C8M2qT" href="<?= get_site_url(); ?>/school-dance">
                            <h2 class="E7hZVuN">School of Dance</h2>
                            <h4 class="_1qvAHdq">8 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/school-dance"></a>
                    </li>
                    <li class="_1Ogv5Gv _38t4RXz GXyzp1w _2v5bHvx kNWVZpn _3x6v3-y">
                        <div class="_1HhJyk9 sDHKtsB" style="transform: translateY(83.9521px);">
                            <div class="_1JMynM5 _3NMx_pK sDHKtsB">
                                <video autoplay="" class="_3h_jpHd _20CZYOH" playsinline="" loop="" muted=""
                                    src="<?= get_template_directory_uri() ?>/assets/videos/Jorge_13-com.mp4"></video>
                            </div>
                        </div>
                        <a class="_3C8M2qT" href="<?= get_site_url(); ?>/sweat-drench">
                            <h2 class="E7hZVuN">Sweat Drench</h2>
                            <h4 class="_1qvAHdq">12 classes</h4><span class="_2p0oNuR"></span>
                        </a><a class="_2sogq4p sDHKtsB" href="<?= get_site_url(); ?>/sweat-drench"></a>
                    </li> -->
                </ul> 
            </div>
            <div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
                <div class="_130-KDr">
                    <div class="">
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Lift</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">When it comes to lifting, we don’t
                                        all like it heavy and slow. If it’s plenty of pumping and a cranked up tempo
                                        that gets you going, these low-weight loads provide all the sweaty, sculpting,
                                        rep-fuelled rough n' tumble you need. It’s impossible to not look in the mirror
                                        and feel yourself after this.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/lift"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Box</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Whether you’re looking forward to
                                        brushing up your agility or building the fitness to conquer a staircase, BOX is
                                        the class you need in your corner. From hard HIITing cardio to heavy water bag
                                        work, you’re going to batter those basics and punch up your power like the champ
                                        you are. Ding Ding BYO gloves.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/box"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Ride</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">R1DE is the class to make spin
                                        everyone’s thing, with playlists to get you pumped, and climbs, sprints and
                                        jumps to seriously crank your cardio training up a gear. Whether you’re more
                                        used to pushing pedals or pounding the dancefloor, you’ll soon find your rhythm
                                        at this multicoloured mash up.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/ride"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">olf45</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Fancy getting
                                        hot and sweaty with some on-the-ground grappling? Brazilian Jiu Jitsu is the
                                        self-defensive
                                        martial<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/olf45"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">strong</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Kick the sh*t
                                        out of bad form and fight your way towards phenomenal fitness with a training
                                        session that
                                        gets you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/strong"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box1ng circuit</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">I would loose.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/boxing-circuit"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">legs, bums + tums</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">You’re about to squeeze, squat,
                                        lift and burn your way through a class that whoops your ass to give you glutes
                                        of steel and thighs of iron. Get through this on the regular and it’s not just
                                        your butt that feels hard as nails.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/legs-bums-tums"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">h1ps, bums, & tums</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Toto, this spin
                                        class ain’t in Kansas no more. Brace yourself for 30 minutes of extreme sweating
                                        in the
                                        saddle as you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/hips-bums-tums"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">booty blast</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Blitz your lower body in a session
                                        that goes down a storm with your glutes. Combining bodyweight booty-busters and
                                        ruthless resistance work, it's the conditioning class where the thigh's the
                                        limit. Come on, giz us a squeeze.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/booty-blast"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">funct1onal circuit</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">combines cardiovascular
                                        conditioning (heart and lung endurance), functional strength training, balance
                                        activities, and flexibility and stretching exercises in an intense and
                                        encouraging atmosphere.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/functional-circuit"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">tabata</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Tabata - is a high-intensity
                                        interval training that consists of eight sets of fast-paced exercises each
                                        performed for 20 seconds interspersed with a brief rest of 10 seconds.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/tabata"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">h11t</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">This Caveman-style circuit class
                                        goes back to basics – using intense based HIIT training to give you
                                        hunter-gatherer agility, animalistic speed and the strength to take down an
                                        angry mammoth if required. All essential skills for Cork life.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/hiit"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">body tone</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Id bring under L1FT </p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/body-tone"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">abs circuit</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">If you thought the name was
                                        inspired by the killer abs you’re going to get from this class, we’ve got bad
                                        news. Try the killer workout you crunch, twist and plank through to get there.
                                        This is one mad assault on your middle that definitely earns that 6 pack. (And
                                        the one in the fridge.)</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/abs-circuit"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box + abs circuit</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">You know how it
                                        is. Sometimes you want to feel the strength of a deep yogic stretch. Other
                                        times, you want
                                        to<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/box-abs-circuit"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box xpress</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">The name
                                        ain’t just for fun. You’re about to squeeze, squat, lift and burn your way
                                        through a
                                        class that whoops your<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/box-xpress"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">r1de xpress</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">All aboard
                                        the gain train. Whether you need expert tips to tone up your technique or if
                                        solo free
                                        weights sessions<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/ride-xpress"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">k1ds boxing</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">If you
                                        thought the name was inspired by the killer abs you’re going to get from this
                                        class,
                                        we’ve got bad news. Try the<wbr><span class="LinesEllipsis-ellipsis">…</span>
                                    </p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/kid-boxing"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">step l1ft</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Live for the
                                        dancefloor? Our music-video inspired commercial class is the place to
                                        choreograph
                                        killer<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/step-lift"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">abs + core 15min</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Looking for the
                                        perfect spot to whine and grind? It’s pure ragga, bashment and dancehall riddims
                                        on the menu
                                        here<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/abs-core"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">body bootcamp</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Our Signature in house Bootcamp,
                                        designed to keep you accountable and push you to change that body shape for
                                        whatever event you have coming up.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/body-bootcamp"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">zumba</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Start the day
                                        right with an endurance workout that couldn’t be more self-explanatory if we
                                        tried. It’s a
                                        morning<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/zumba"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">pilates</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">pilates a form of low-impact
                                        exercise that aims to strengthen muscles while improving postural alignment and
                                        flexibility. Pilates moves tend to target the core, although the exercises work
                                        other areas of your body as well. </p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/pilates"></a>
                        </div>
                        <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                            <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                            <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                                <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">yoga</h3>
                                <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                                    <p class="LinesEllipsis LinesEllipsis--clamped ">Yoga is an old discipline from
                                        India. It is both spiritual and physical. Yoga uses breathing techniques,
                                        exercise and meditation. It helps to improve health and happiness, Namaste.</p>
                                </div>
                            </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                                href="<?= get_site_url(); ?>/yoga"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(window).scroll(function() {
    $("#blurb-box").css("opacity", 1 - $(window).scrollTop() / 300);
});
</script>
<?php
get_footer();
?>