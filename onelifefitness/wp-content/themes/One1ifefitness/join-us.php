<?php /* Template Name: join-us */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>

<div class="_1vdzHPH">
    <div class="_3G5m_gR _2rbE6TC _2v5bHvx">
        <div class="_2o5PW8a _2rbE6TC _2v5bHvx _3HiqiFI">
            <div class="_1JG4awS _3s-__sC _3x6v3-y text-center">
                <svg class="_3RdppjM" preserveAspectRatio="xMidYMid" viewBox="0 0 784 110">
                    <text vector-effect="non-scaling-stroke" shape-rendering="crispEdges" class="_1D8oKRW _31coI1f" x="0" y="90">Memberships</text>
                </svg>
                <h3 class="_2Lb3AVC _3x6v3-y">Where working out is as fun as going out</h3>
            </div>
        </div>
        <div class="container mb-5">
            <div class="row">
                <div class="col-md-6 m-auto">
                    <select name="select_club" class="select_club" placeholder="Club interested in">
                        <!-- <option value="select">Select</option> -->
                        <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life Fitness
                        </option>
                        <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                        </option>
                    </select>
                </div>
            </div>
        </div>
        <div class="_2VOZknn _2rbE6TC _2v5bHvx _3x6v3-y">
            <div class="_3iOZpcx _2rbE6TC _2v5bHvx VBD7Ow3">
                <ul>
                    <li class="pv0_4Ma GXyzp1w _2v5bHvx">
                        <div class="_3GQ28fT VBD7Ow3">
                            <div class="IiXBDIM _2_HaYMw">01</div>
                            <div class="_3va3BJW">
                                <h3>Pay-As-You-Go</h3>
                                <div>
                                    <p>Afraid of commitment? No problem. Pay monthly by direct debit and cancel anytime
                                        with a calendar month's notice. There’ll be no questions asked but we may rib
                                        you a bit.</p>
                                </div>
                                <ul style="list-style-type:square;margin:0px 20px;">
                                    <li>No contract 30days notice </li>
                                    <li>€39.99 per month </li>
                                    <li>€30 admin fee</li>
                                </ul>
                            </div>
                        </div>
                        <a class="attri" href="https://indma05.clubwise.com/onelifefitness/index.html">
                            <div class="_1tb_CAe _3Pq3GhV VBD7Ow3"><span>BOOKING</span></div>
                        </a>
                    </li>
                    <li class="pv0_4Ma GXyzp1w _2v5bHvx">
                        <div class="_3GQ28fT VBD7Ow3">
                            <div class="IiXBDIM _2_HaYMw">02</div>
                            <div class="_3va3BJW">
                                <h3>3-Month Membership</h3>
                                <div>
                                    <p>Wanna slightly better monthly rate - no problem, just give us an initial
                                        commitment for 3 months</p>
                                </div>
                                <ul style="list-style-type:square;margin:0px 20px;">
                                    <li>€129 Full Acess</li>
                                    <li>€99 Off Peak hours</li>
                                </ul>
                            </div>
                        </div>
                        <a class="attri" href="https://indma05.clubwise.com/onelifefitness/index.html">
                            <div class="_1tb_CAe _3Pq3GhV VBD7Ow3"><span>BOOKING</span></div>
                        </a>
                    </li>
                    <li class="pv0_4Ma GXyzp1w _2v5bHvx">
                        <div class="_3GQ28fT VBD7Ow3">
                            <div class="IiXBDIM _2_HaYMw">03</div>
                            <div class="_3va3BJW">
                                <h3>6-Month Membership</h3>
                                <div>
                                    <p>Wanna even better monthly rate - no problem, just give us an initial commitment
                                        for 6 months.</p>
                                </div>
                                <ul style="list-style-type:square;margin:0px 20px 30px;">
                                    <li>€229 Full Access </li>
                                </ul>
                            </div>
                        </div>
                        <a class="attri" href="https://indma05.clubwise.com/onelifefitness/index.html">
                            <div class="_1tb_CAe _3Pq3GhV VBD7Ow3"><span>BOOKING</span></div>
                        </a>
                    </li>
                    <li class="pv0_4Ma GXyzp1w _2v5bHvx">
                        <div class="_3GQ28fT VBD7Ow3">
                            <div class="IiXBDIM _2_HaYMw">04</div>
                            <div class="_3va3BJW">
                                <h3>12 month contract 12-Month Membership</h3>
                                <div>
                                    <p>Monthly by direct debit or if you are feeling flush pay the year upfront and get
                                        1 month free. Ker-ching.</p>
                                </div>
                                <ul style="list-style-type:square;margin:0px 20px 30px;">
                                    <li>€29.99 per month</li>
                                    <li>€30 admin fee</li>
                                    <li>€329 Per year for 13 months</li>
                                </ul>
                            </div>
                        </div>
                        <a class="attri" href="https://indma05.clubwise.com/onelifefitness/index.html">
                            <div class="_1tb_CAe _3Pq3GhV VBD7Ow3"><span>BOOKING</span></div>
                        </a>
                    </li>
                    <!-- <li class="pv0_4Ma GXyzp1w _2v5bHvx">
                        <div class="_3GQ28fT VBD7Ow3">
                            <div class="IiXBDIM _2_HaYMw">05</div>
                            <div class="_3va3BJW">
                                <h3>18-Month Membership</h3>
                                <div>
                                    <p>That's what we like, someone who takes their sweat-fest seriously. Pay
                                        for the whole lot upfront and we'll give you 2 months free, or pay
                                        monthly by direct debit we'll give you a fist pump.</p>
                                </div>
                            </div>
                        </div>
                        <a class="attri" href="https://indma05.clubwise.com/onelifefitness/index.html"><div class="_1tb_CAe _3Pq3GhV VBD7Ow3"><span>BOOKING</span></div></a>
                    </li> -->
                    <li class="pv0_4Ma GXyzp1w _2v5bHvx">
                        <div class="_3GQ28fT VBD7Ow3">
                            <div class="IiXBDIM _2_HaYMw">05</div>
                            <div class="_3va3BJW">
                                <h3>Student and Corporate </h3>
                                <div>
                                    <p>People know their company's got their back if we're one of the perks of the job.
                                        Be the coolest kid around the water cooler and the classroom. </p>
                                </div>
                                <ul style="list-style-type:square;margin:0px 20px 30px;">
                                    <li>€34.99 per month</li>
                                    <li>€30 admin fee</li>
                                </ul>
                            </div>
                        </div>
                        <a class="attri" href="https://indma05.clubwise.com/onelifefitness/index.html">
                            <div class="_1tb_CAe _3Pq3GhV VBD7Ow3"><span>BOOKING</span></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="_1FX8Gs8 _2rbE6TC _2v5bHvx">
            <div class="_3x7PkHV">
                <div class="_1cT7yb7">
                    <div class="_1JMynM5 _2UxZdJd sDHKtsB">
                        <video autoplay="autoplay" class="_3h_jpHd _2UxZdJd sDHKtsB" playsinline="" loop="loop" id="vid"
                            preload="auto" muted="" src="<?= get_template_directory_uri() ?>/assets/videos/Jorge_2.mp4">
                        </video>
                    </div>
                    <div class="_3-DAWjj _2QsqUxL">
                        <h2 class="_17Fvefa _2_HaYMw">Anything goes</h2>
                        <div class="_3KZTfFO _2rbE6TC _2v5bHvx">
                            <h4 class="_1qlk-1P _2DGciQP">Watch</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
setTimeout(function() {
    document.getElementById('vid').play();
}, 1000);


</script>
<?php
// get_sidebar();
get_footer();
?>