<?php /* Template Name: pilates*/
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
            <h2>Pilates</h2>
            <div class="_1-4gF4V">
                <p>Build the strength to smash life, without the incredible bulk. This is all about control: precise
                    movements that stretch and tone to improve your posture, increase your flexibility and reinforce
                    your core. You know what they say. Hit the mat and you’ll never go back. </p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                        <span class="Nfa775m">Onelifefitness Camden quay</span>
                        <span class="Nfa775m">Onelifefitness Sullivans quay</span>
                    </div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
                <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                        Fitness
                    </option>
                    <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                    </option>
                </select>
                <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1319192" disabled="">06:45 Monday 17th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1306392" disabled="">12:15 Thursday 20th (45 minutes) - Bookable 7am day before
                    </option>
                </select><a href="https://indma05.clubwise.com/onelifefitness/index.html" target="_blank"
                    class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Kristy_Suzuki.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Christine</h3>
                        <div>
                            <p>Christine’s pilates and yoga career began as a means to manage her hypermobility, and
                                she has a deep interest in helping clients, especially on the hypermobile spectrum.
                                Her sessions are mindful and methodical, and focus on encouraging each individual to
                                find their flow.</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Jason_Mellars.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Jason</h3>
                        <div>
                            <p>Jason has over 20 years experience in the dance and fitness
                                industry as an international fitness presenter, master trainer and
                                choreographer. With specialities including pilates, spinning and dance, Jason’s
                                classes celebrate having fun whilst pushing you to your limits.</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Laura_Pearce.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Laura</h3>
                        <div>
                            <p>Having studied under the renowned Swami Niralananda Giri at the
                                Sivananda Yoga centre, Laura holds the highest certification awarded by Yoga
                                Alliance UK. Laura’s classes are challenging and dynamic, based on practises
                                that detoxify and restore the body and mind.</p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Hester_Campbell.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Hester - Master Trainer</h3>
                        <div>
                            <p>First qualified in aqua fitness aged 17, Hester worked in film production and then
                                re-trained as a contemporary dancer. Instructing pilates, fitness &amp; dance
                                supported her through BA &amp; MSc studies at Laban, and brought her to onelifefitness
                                in
                                2004. Fitness, wellness and aerial arts have guided Hester throughout her career.
                                Possibly unequalled in her passion for teaching and sharing the knowledge
                                accumulated over more than 3 decades in the fitness world, Hester is onelifefitness
                                Aerial
                                Series Master Trainer and creator of CircusFit Academy.</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Pilates4.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>