<?php /* Template Name: deathrow */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis">
            <h2>Deathrow</h2>
            <div class="_1-4gF4V">
                <p>Long stretches of cardio a sentence you’d rather avoid? In thirty short minutes, get a full-body
                    workout that smashes through interval training, builds your strength and makes you sweat like your
                    life depends on it. Whether you’re a newbie or an old-timer, you’ll find retribution here.</p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx"><span class="Nfa775m">Elephant and Castle</span><span
                            class="Nfa775m">Farringdon</span></div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx"><select class="acLo9hG _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Select gym</option>
                    <option value="Farringdon">Farringdon</option>
                    <option value="Elephant and Castle">Elephant and Castle</option>
                </select><select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1243569" disabled="">08:00 Monday 17th (30 minutes) - Bookable 7am day before
                    </option>
                    <option value="1281686" disabled="">12:45 Tuesday 18th (30 minutes) - Bookable 7am day before
                    </option>
                    <option value="1284344" disabled="">07:15 Wednesday 19th (30 minutes) - Bookable 7am day before
                    </option>
                </select><a
                    href=""
                    target="_blank" class="_26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Mark_Lenihan.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Mark</h3>
                        <div>
                            <p>Mark has been in the fitness industry since 2010, and
                                specialises in bodyweight calisthenics, gymnastics and HIIT. He believes that
                                the first step to any transformation is being aware of the choices you make,
                                and how they effect yourself and others. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Katherin_Berlie.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Katherine</h3>
                        <div>
                            <p>&nbsp;<em>Rowing is
                                    where it all began for Katherine as a member of Lea Rowing Club where
                                    she&nbsp;currently&nbsp;runs and teaches their Row to Fitness
                                    programme.&nbsp;</em><em>She moved
                                    away from&nbsp;training with crews on the water&nbsp;and&nbsp;into Strength and
                                    Conditioning and now&nbsp;uses land based rowing as way of re-enforcing core
                                    strength gained from moving and lifting all things heavy,&nbsp;and for getting
                                    on a great sweat.</em></p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ryan_Lovett.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ryan</h3>
                        <div>
                            <p>A professional actor trained in New York, when Ryan’s not on
                                stage he’s teaching TRX, Row30 and Frame Fitness at onelifefitness. Having returned to
                                the UK a few years ago, Ryan loves his job at onelifefitness and admits that London
                                definitely has his heart now. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Stevie_Christian.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Stevie C</h3>
                        <div>
                            <p>Through a combination of tough-love and motivation, Stevie will push you to your limits
                                with high-energy metabolic conditioning workouts that will leave you in a pool of sweat
                                and a big smile on your face.&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/Row30_New.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>