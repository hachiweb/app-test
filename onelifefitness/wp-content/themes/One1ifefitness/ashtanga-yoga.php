<?php /* Template Name: ashtanga-yoga */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>
<div class="_1vdzHPH">
    <div class="_3pBQZ7U _2rbE6TC _2v5bHvx">
        <div class="_27wt8R7 _1G7RCdW _2v5bHvx VBD7Ow3 y5XwMis pb-5">
            <h2>body tone</h2>
            <div class="_1-4gF4V">
                <p>Id bring under L1FT </p>
            </div>
            <ul class="M6EUSUT _2rbE6TC _2v5bHvx">
                <li class="_31cCPJV _2rbE6TC _2v5bHvx">
                    <div class="_2l5my07 _2HhTmXD _2v5bHvx">Location</div>
                    <div class="_1Fu6UbF _1dohwpN _2v5bHvx">
                    <span class="Nfa775m">Onelifefitness Camden quay</span>
                    <span class="Nfa775m">Onelifefitness Sullivans quay</span></div>
                </li>
            </ul>
            <div class="_1uYqZmj _2rbE6TC _2v5bHvx">
            <select name="select_club" class="select_club acLo9hG _1G7RCdW _2v5bHvx">
                            <option value="https://indma05.clubwise.com/onelifefitness/index.html" slected>One Life
                                Fitness
                            </option>
                            <option value="https://indma10.clubwise.com/onelifefitnessriversedge">Rivers Edge Fitness
                            </option>
                        </select>
                <select class="_1LDVOxt _1G7RCdW _2v5bHvx">
                    <option value="" disabled="">Choose time</option>
                    <option value="1255013" disabled="">13:00 Sunday 16th (60 minutes) - Bookable 7am day before
                    </option>
                    <option value="1333798" disabled="">07:00 Monday 17th (45 minutes) - Bookable 7am day before
                    </option>
                    <option value="1298486" disabled="">07:30 Tuesday 18th (60 minutes) - Bookable 7am day before
                    </option>
                </select><a
                    href="https://indma05.clubwise.com/onelifefitness/index.html"
                    target="_blank" class="attri _26pOGra _2cT-kSp _3xVoYzA _3Pq3GhV"><span>Book class</span></a></div>
            <div class="_3srnCE8">
                <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                    <div class="_2Tb-We1 sDHKtsB">
                        <ul class="_2J71_L4 sDHKtsB">
                            <li class="NPRZnJb _1kNfguI sDHKtsB">
                                <div class="_2gpiPPm lC7b5Zk sDHKtsB"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="_3BtndY2 _2rbE6TC _2v5bHvx">
                <h2>Your <br>instructors</h2>
                <div class="Sa1FFQw">
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Ashley_Ahrens.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Ashley</h3>
                        <div>
                            <p>Ashley’s classes are disciplined, yet nurturing. She places a great deal of emphasis on
                                connecting deep within your own body, using the breath as a tool to distract the mind,
                                paired with fascial tissue and energetic lines of awareness to understand postures on a
                                very personal level. With 9 years of dedicated practice, 400-hours of yoga training, and
                                a Biology degree, she has a lot of inspiration and knowledge to share!
                            </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Fleur_Poad.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Fleur</h3>
                        <div>
                            <p>Fleur has trained in various yoga styles including Ashtanga,
                                Mandala Vinyasa Flow, Yin and pre and post natal, and is always working on
                                enhancing her knowledge. Fleur’s classes are fun and challenging, and open to
                                both beginners and those at a more advanced level. </p>
                        </div>
                    </div>
                    <div class="_3fJiN_C">
                        <div class="_2gpiPPm _3jJP_-6 _2rbE6TC _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/Gym_Box_Instructors_Beko_Kaygee.jpg"
                                class="_3h_jpHd _2jVkQgE"></div>
                        <h3 class="_2MUcDo2 _2rbE6TC _2v5bHvx">Beko</h3>
                        <div>
                            <p>As an international presenter, Beko has been promoting
                                dance, martial arts and holistic health for over 22 years, and has been at Gymbox
                                since its opening day. Accredited with the creation of sword-fighting inspired
                                workout ‘Blade’, Beko’s sessions are anything but boring.</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="wqBYWJi">
            <div class="_3GMtneJ _1G7RCdW _2v5bHvx">
                <div class="_2Tb-We1 sDHKtsB">
                    <ul class="_2J71_L4 sDHKtsB">
                        <li class="NPRZnJb _1kNfguI sDHKtsB">
                            <div class="_2gpiPPm lC7b5Zk sDHKtsB"><span class="_3h_jpHd"
                                    style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-SDER6K7A8104.jpg');"></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>