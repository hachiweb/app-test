<?php /* Template Name: cannon-street */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>


<section class="_1J0RKGy _2rbE6TC _2v5bHvx">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');">
                    </div>
                </div>
                <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->
            </div>
        </div>
    </div>
</section>

<div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="_130-KDr">
        <div class="">
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Lift</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">When it comes to lifting, we don’t
                            all like it heavy and slow. If it’s plenty of pumping and a cranked up tempo
                            that gets you going, these low-weight loads provide all the sweaty, sculpting,
                            rep-fuelled rough n' tumble you need. It’s impossible to not look in the mirror
                            and feel yourself after this.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/lift"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Box</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Whether you’re looking forward to
                            brushing up your agility or building the fitness to conquer a staircase, BOX is
                            the class you need in your corner. From hard HIITing cardio to heavy water bag
                            work, you’re going to batter those basics and punch up your power like the champ
                            you are. Ding Ding BYO gloves.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/box"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Ride</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">R1DE is the class to make spin
                            everyone’s thing, with playlists to get you pumped, and climbs, sprints and
                            jumps to seriously crank your cardio training up a gear. Whether you’re more
                            used to pushing pedals or pounding the dancefloor, you’ll soon find your rhythm
                            at this multicoloured mash up.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/ride"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">olf45</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Fancy getting
                            hot and sweaty with some on-the-ground grappling? Brazilian Jiu Jitsu is the
                            self-defensive
                            martial<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/olf45"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">strong</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Kick the sh*t
                            out of bad form and fight your way towards phenomenal fitness with a training
                            session that
                            gets you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/strong"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box1ng circuit</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">I would loose.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/boxing-circuit"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">legs, bums + tums</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">You’re about to squeeze, squat,
                            lift and burn your way through a class that whoops your ass to give you glutes
                            of steel and thighs of iron. Get through this on the regular and it’s not just
                            your butt that feels hard as nails.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/legs-bums-tums"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">h1ps, bums, & tums</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Toto, this spin
                            class ain’t in Kansas no more. Brace yourself for 30 minutes of extreme sweating
                            in the
                            saddle as you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/hips-bums-tums"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">booty blast</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Blitz your lower body in a session
                            that goes down a storm with your glutes. Combining bodyweight booty-busters and
                            ruthless resistance work, it's the conditioning class where the thigh's the
                            limit. Come on, giz us a squeeze.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/booty-blast"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">funct1onal circuit</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">combines cardiovascular
                            conditioning (heart and lung endurance), functional strength training, balance
                            activities, and flexibility and stretching exercises in an intense and
                            encouraging atmosphere.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/functional-circuit"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">tabata</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Tabata - is a high-intensity
                            interval training that consists of eight sets of fast-paced exercises each
                            performed for 20 seconds interspersed with a brief rest of 10 seconds.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/tabata"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">h11t</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">This Caveman-style circuit class
                            goes back to basics – using intense based HIIT training to give you
                            hunter-gatherer agility, animalistic speed and the strength to take down an
                            angry mammoth if required. All essential skills for Cork life.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/hiit"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">body tone</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Id bring under L1FT </p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/body-tone"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">abs circuit</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">If you thought the name was
                            inspired by the killer abs you’re going to get from this class, we’ve got bad
                            news. Try the killer workout you crunch, twist and plank through to get there.
                            This is one mad assault on your middle that definitely earns that 6 pack. (And
                            the one in the fridge.)</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/abs-circuit"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box + abs circuit</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">You know how it
                            is. Sometimes you want to feel the strength of a deep yogic stretch. Other
                            times, you want
                            to<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/box-abs-circuit"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">box xpress</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">The name
                            ain’t just for fun. You’re about to squeeze, squat, lift and burn your way
                            through a
                            class that whoops your<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/box-xpress"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">r1de xpress</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">All aboard
                            the gain train. Whether you need expert tips to tone up your technique or if
                            solo free
                            weights sessions<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/ride-xpress"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">k1ds boxing</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">If you
                            thought the name was inspired by the killer abs you’re going to get from this
                            class,
                            we’ve got bad news. Try the<wbr><span class="LinesEllipsis-ellipsis">…</span>
                        </p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/kid-boxing"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">step l1ft</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Live for the
                            dancefloor? Our music-video inspired commercial class is the place to
                            choreograph
                            killer<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/step-lift"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">abs + core 15min</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Looking for the
                            perfect spot to whine and grind? It’s pure ragga, bashment and dancehall riddims
                            on the menu
                            here<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/abs-core"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">body bootcamp</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Our Signature in house Bootcamp,
                            designed to keep you accountable and push you to change that body shape for
                            whatever event you have coming up.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/body-bootcamp"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">zumba</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Start the day
                            right with an endurance workout that couldn’t be more self-explanatory if we
                            tried. It’s a
                            morning<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/zumba"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">pilates</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">pilates a form of low-impact
                            exercise that aims to strengthen muscles while improving postural alignment and
                            flexibility. Pilates moves tend to target the core, although the exercises work
                            other areas of your body as well. </p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/pilates"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Yoga is an old discipline from
                            India. It is both spiritual and physical. Yoga uses breathing techniques,
                            exercise and meditation. It helps to improve health and happiness, Namaste.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/yoga"></a>
            </div>
            <!-- <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Aerial Hoop</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">If you’re after
                            a full-body workout that leaves your comfort zone six feet down, hop onto the hoop and
                            unleash your<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/aerial-hoop"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Aerial Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">We all know Yoga
                            helps you to feel more grounded, but we love turning fitness on its head. Say Namast-hey to
                            the<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/aerial-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Ashtanga Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Thought it was
                            badass to lift weights? Imagine a workout so deep that even your breath feels the burn. For
                            a more<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/ashtanga-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">BadAss</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">The name ain’t
                            just for fun. You’re about to squeeze, squat, lift and burn your way through a class that
                            whoops your<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/badass"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Battlebells</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped "> Grab your
                            weapons. Whether you wage war with ropes, sandbells or kettlebells, this is a workout for
                            true warriors.<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/battleballs"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Beastmode</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Looking for a
                            bodyweight HIIT and weighted conditioning in a single workout? You absolute animal.
                            Thankfully, we are<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/beastmode"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Bike &amp; Beats</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Bike &amp;amp;
                            Beats is the class to make spin everyone’s thing, with playlists to get you pumped, and
                            climbs, sprints<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/bike-beats"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Bodyweight Bandits</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Brutally sweaty.
                            Criminally tough. This lot are such gluttons for punishment that you probably wouldn’t want
                            to meet<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/bodyweight-bandits"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Callback</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Live for the
                            dancefloor? Our music-video inspired commercial class is the place to choreograph
                            killer<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/callback"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">CounterKick</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Kick the sh*t
                            out of bad form and fight your way towards phenomenal fitness with a training session that
                            gets you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/counterkick"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 66, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">CounterPunch</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Whether you’re
                            looking forward to brushing up your agility or building the fitness to conquer a
                            staircase,<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/counterpunch"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Escalate</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Functional,
                            HIIT, circuits and track sprints? Ouch. If you’re looking for a class that’ll leave you
                            knackered, aching and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/escalate"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">False Grip</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">The thought of
                            an intense upper body sesh make your stomach flip? Come on, get a grip. This
                            studio-based<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/false-grip"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Functional Bodybuilding</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">A wise Austrian
                            terminator once said that the best activities for your health are pumping and humping. While
                            we can<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/functional-bodybuilding"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Gain Train</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">All aboard the
                            gain train. Whether you need expert tips to tone up your technique or if solo free weights
                            sessions<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/gain-train"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Hardcore</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">If you thought
                            the name was inspired by the killer abs you’re going to get from this class, we’ve got bad
                            news. Try the<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/hardcore"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Hatha Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Reality kicking
                            you where it hurts? Consider this your antidote to urban life. Through a deliciously slow
                            and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/hatha-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Invert Yourself</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">It doesn’t
                            matter how experienced you are. This is where flexual deviants come to swing every which way
                            and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/invert-yourself"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Laughing Therapy</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Between dark
                            mornings, back to work blues and being in the red, the new year can leave you wondering
                            whether<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/laughing-therapy"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">MetCon</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Short, quick and
                            hotter than hell – there’s no messing about with MetCon. In just thirty minutes of solid
                            HIIT, you’ll test<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/metcon"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(255, 118, 197);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">No Holds Barre</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Don’t let the
                            tutu clichés fool you: Barre is the tough ballet workout that’ll hand you your derrière on a
                            plate.<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/no-holds-barre"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Paddleboard Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Hang zen, dude.
                            If you’re a yogi who wants to get wild, forget about a traditional flow – try riding the
                            wave of a<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/paddleboard-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Pilates</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Build the
                            strength to smash life, without the incredible bulk. This is all about control: precise
                            movements that stretch<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/pilates"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(55, 87, 245);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Power Battle</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Velodrome
                            villains, sweat fetishists and speed demons unite: this is the showdown you’ve been waiting
                            for.<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/power-battle"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Rampant Rehab</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">When your
                            workout leaves you feeling done in, our Theraguns will help you hit the spot. In these
                            recovery<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/rampant-rehab"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Rocket Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Burning out your
                            fuse at both ends? You wanna try Rocket, man. Developed in 80s San Fran, this is a more
                            playful<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/rocket-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Rolling With My Yogis</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Meet the new
                            prop on the block: This cool little piece of kit opens up your chest, gets those hips
                            flexin’ and makes<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"
                    href="<?= get_site_url(); ?>/rolling-with-my-yogis"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Superfly</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">There are many
                            ways to fly (and not many of them are legal) but at Superfly, it’s your body not your brain
                            doing all the<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/superfly"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Suspenders</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">When it comes to
                            class bookings, not all suspensions are bad. Challenging your physical strength and mental
                            nerve,<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/suspenders"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Swingers Club</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Switch up your
                            functional training with a kettlebell workout that builds endurance, increases strength,
                            burns fat and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/swingers-club"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(235, 225, 95);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Thunder</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Blitz your lower
                            body in a session that goes down a storm with your quads and glutes. Combining bodyweight
                            booty<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/thunder"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(39, 164, 255);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Trapeze</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Every Londoner’s
                            had it. That urge to run off to the circus. We’ve made it easy trapezy, with an aerial class
                            that’ll get<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/trapeze"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Vinyasa Flow Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Smooth as silk.
                            Chill as you like. Vinyasa flow is your secret oasis of order amongst the chaos of modern
                            life. Knock<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/vinyasa-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">WOD Squad</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Pulling,
                            pushing, climbing, lifting. Seems so simple, doesn’t it? Well, looks can be deceiving, pal.
                            Being a Gymbox<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/wod-squad"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Yin Yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Sometimes you
                            just need to take it slow – and Yin is the way to go. The strong and silent type, this
                            meditative flow<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/yin-yoga"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Yoga For Lifting</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Whatever impact
                            your training’s had on your body, Yoga For Lifting makes the most of your motion by getting
                            you<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/yoga-for-lifting"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(132, 220, 240);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Yogangster</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">The Yogangster
                            Dynamic 45min Flow will calm your mind, energise your spine and mobilise your body.
                            Traditional<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB" href="<?= get_site_url(); ?>/yogangster"></a>
            </div> -->
        </div>
    </div>
</div>
<?php
get_footer();
?>