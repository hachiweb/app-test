<?php /* Template Name: sweat-drench */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */

get_header(); ?>

<section class="_1J0RKGy _2rbE6TC _2v5bHvx">
    <div class="row">
        <div class="col-lg-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                </ol>
                <div class="carousel-inner" role="listbox">
                <div class="carousel-item active"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ON-Andreea-Yoga-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png');">
                    </div>
                    <div class="carousel-item"
                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png');">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="_39EiYTl _2rbE6TC _2v5bHvx VBD7Ow3">
    <div class="_130-KDr">
        <div class="">
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">zumba</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Start the day
                            right with an endurance workout that couldn’t be more self-explanatory if we tried. It’s a
                            morning<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/amrap"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(245, 155, 66);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">pilates</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">pilates a form of low-impact exercise that aims to strengthen muscles while improving postural alignment and flexibility. Pilates moves tend to target the core, although the exercises work other areas of your body as well. </p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/battleballs"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">yoga</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Yoga is an old discipline from India. It is both spiritual and physical. Yoga uses breathing techniques, exercise and meditation. It helps to improve health and happiness, Namaste.</p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/beastmode"></a>
            </div>
            <!-- <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Deathrow</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Long stretches
                            of cardio a sentence you’d rather avoid? In thirty short minutes, get a full-body workout
                            that smashes<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/deathrow"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Drill Sergeant</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">We won’t ask why
                            you like a strict authoritarian crackdown on your training, you absolute sadist… but you’d
                            better<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/drill-sergeant"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Escalate</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Functional,
                            HIIT, circuits and track sprints? Ouch. If you’re looking for a class that’ll leave you
                            knackered, aching and<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/escalate"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Flatline</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">We call it that
                            because it leaves you for dead. The evil brainchild of onelifefitness sadists and endurance experts,
                            this<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/flatline"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">MetCon</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Short, quick and
                            hotter than hell – there’s no messing about with MetCon. In just thirty minutes of solid
                            HIIT, you’ll test<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/metcon"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Pound</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Prefer to bang
                            workouts out to the beat of your own drum? Meet cardio, Pilates, isometric and plyometric in
                            the pit<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/pound"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Swingers</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Swap your
                            same-old cardio routine for a group session that'll really test your stamina. Working you
                            hard from top<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/swingers"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Threshold</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Well, with a
                            name like that, you know it’s going to be more gnarly than nice. Using self-powered cardio
                            equipment<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/threshold"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Trilactic</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">Discover what it
                            means to be a true triple threat at Trilactic. Taking over the Escalate studio, you’ll move
                            through three<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/trilactic"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Ultimate Fit</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">If you thought
                            that any class with ‘ultimate’ in the name is going to give you a workout so tough that your
                            eyeballs<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/ufc-fit"></a>
            </div>
            <div class="pavfCl7 _2rbE6TC _2v5bHvx">
                <div class="_1XA6QCl" style="background-color: rgb(129, 144, 199);"></div>
                <div class="_1nGuswI _2rbE6TC _2v5bHvx">
                    <h3 class="rfn1m5O _2rbE6TC _2v5bHvx">Whiplash</h3>
                    <div class="_2FOvB3a _2rbE6TC _2v5bHvx">
                        <p class="LinesEllipsis LinesEllipsis--clamped ">It may be
                            skipping but there’s no playtime here. Progress your rope technique, crack your footwork
                            co-ordination<wbr><span class="LinesEllipsis-ellipsis">…</span></p>
                    </div>
                </div><span class="_GcKgjb"></span><a class="_1nR3q81 sDHKtsB"  href="<?= get_site_url(); ?>/whiplash"></a>
            </div> -->
        </div>
    </div>
</div>

<?php
// get_sidebar();
get_footer();
?>