<?php /* Template Name: about */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
get_header(); ?>


<div class="_1vdzHPH">
    <div class="_2k0LrXm _2rbE6TC _2v5bHvx">
        <div class="_2z6ElmY _2rbE6TC _2v5bHvx _3HiqiFI">
            <div class="_1oMY_Ys _3s-__sC _3x6v3-y text-center"><svg class="_3RdppjM" preserveAspectRatio="xMidYMid"
                    viewBox="0 0 640 110"><text vector-effect="non-scaling-stroke" shape-rendering="crispEdges"
                        class="_1D8oKRW _31coI1f" x="0" y="90">Our
                        story</text></svg></div>
        </div>
        <div class="bMWNK1B _2rbE6TC _2v5bHvx _3x6v3-y">
            <div class="_2dd9I7F _2rbE6TC _2v5bHvx VBD7Ow3">
                <div class="IeMLLTl _1G7RCdW _2v5bHvx VBD7Ow3">
                    <div class="_1NA_h7i">
                        <h3>The onelifefitness Story</h3>
                        <div>
                            <p>Ok imagine this. It’s 2019, there is only one style of gym in Cork City . A one size fits
                                all kinda gym. OK for everyone kinda gym. ‘It's fine I guess’ kinda gym.<br></p>
                            <p>When uninspiring becomes normal, there is a problem. There was only one option.
                            </p>
                            <p>It was easy to say, there was no other fitness space like this. Exercise blended
                                perfectly with pleasure. You could be anyone you wanted to be. Be any shape you happened
                                to be.. It was simply just, serious fun.</p>
                            <p><em>Join us, and see for yourself - </em></p>
                        </div>
                    </div>
                </div>
                <div class="_363tr7g _1G7RCdW _2v5bHvx">
                    <div style="transform: translateY(136.456px);" data-aos="fade-up" data-aos-duration="3000">
                        <div class="_2gpiPPm _3gv6oAs _1G7RCdW _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png"
                                class="_3h_jpHd"></div>
                    </div>
                    <div style="transform: translateY(66.8276px);" data-aos="fade-down" data-aos-easing="linear"
                        data-aos-duration="1500">
                        <div class="_2gpiPPm _1Qxe_9c _1G7RCdW _2v5bHvx"><img
                                src="<?= get_template_directory_uri() ?>/assets/images/OScreenshot-2019.png"
                                class="_3h_jpHd"></div>
                    </div>
                </div>
                <div class="_-WrE9rl _2rbE6TC _2v5bHvx"><svg class="_3RdppjM" preserveAspectRatio="xMidYMid"
                        viewBox="0 0 700 110"><text vector-effect="non-scaling-stroke" shape-rendering="crispEdges"
                            class="_1D8oKRW _31coI1f" x="0" y="90">Philosophy</text></svg></div>
                <div class="_18e7Ks9 _1G7RCdW _2v5bHvx VBD7Ow3">
                    <div class="_2P81XQQ">
                        <h3>OUR PHILOSOPHY</h3>
                        <div>
                            <p>It was like no other gym in town. You could be anyone you wanted to be. Be any shape you
                                happened to be. And be a man, woman or completely undecided. We didn't care. If you were
                                serious about working out and having a laugh we were happy to accommodate. There were no
                                disapproving glances, rolled eyeballs, tutts, sighs or Gym Cliques round our place!</p>
                            <p>Just pure, unadulterated serious fun.</p>
                            <p>We don’t take ourselves to seriously and don’t expect our members to either, One Life
                                Fitness was designed to be an add on to your life, not to take it over, make exercise
                                fun again and come give us a try. </p>
                        </div>
                    </div>
                </div>
                <div class="ErJhEG8 _1G7RCdW _2v5bHvx" data-aos="zoom-in-down">
                    <div>
                        <div class="_2Mohl0x _1G7RCdW _2v5bHvx">
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down">
                                <img src="<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png"
                                    class="_3h_jpHd"></div>
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/One-Life-Personal-Trainer.jpeg"
                                    class="_3h_jpHd"></div>
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/ONE-Jorge-IG-Tile.png"
                                    class="_3h_jpHd"></div>
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/Andreea-Yoga-Tile.png"
                                    class="_3h_jpHd"></div>
                        </div>
                    </div>
                    <div>
                        <div class="_1Aa09nZ _1G7RCdW _2v5bHvx" data-aos="zoom-in-down">
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/ONE-Screenshot.png"
                                    class="_3h_jpHd"></div>
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/ONE-SDER6K7A8104.jpg"
                                    class="_3h_jpHd"></div>
                            <div class="_2gpiPPm _1MzFp9n" data-aos="zoom-in-down"><img
                                    src="<?= get_template_directory_uri() ?>/assets/images/ONE.jpg" class="_3h_jpHd">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="k3d6s8r _2rbE6TC _2v5bHvx">
                    <h2>Anything goes</h2>
                    <h2><a class="_2nUA0vX" href="<?= get_site_url(); ?>/join-us">Join us</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
// get_sidebar();
get_footer();
?>