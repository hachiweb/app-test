<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gym_box
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" type="image/x-icon" href="<?= get_template_directory_uri() ?>/assets/images/favicon.ico">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.css' rel='stylesheet' />
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <!-- aos acript -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.js'></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
    function openNav() {
        document.getElementById("myNav").style.height = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }

    function openNavform() {
        document.getElementById("myNav-form").style.height = "100%";
    }

    function closeNavform() {
        document.getElementById("myNav-form").style.height = "0%";
    }

    $(document).on('change', ".select_club", function(e) {
        var optVal = $(".select_club option:selected").val();
        $(".attri").attr("href", optVal);
    });
    </script>

    <?php wp_head(); ?>
</head>

<body>
    <header class="_3KR0JuQ VBD7Ow3">
        <a class="_1ff-7Sh" title="GO TO THE ONE L1FE FITNESS HOMEPAGE" href="<?= get_site_url(); ?>">
            <img src="<?= get_template_directory_uri() ?>/assets/images/Cork-Fitness-Centre-One-Life_Fitness-Gym-Logo-Mobile.png"
                alt="">
        </a>
        <div class="ijfRzqG pt-4">
            <!-- <span class="_2CJPpcw pRZSkbx"></span> -->
            <span class="" style="font-size:30px;cursor:pointer;margin-top:20px;" onclick="openNav()">&#9776;</span>
        </div>
        <ul class="_3XOFvvi">
            <li class="_2GaQ8vJ"><a class="m7LKENr _25LPQhk _2DGciQP" href="javascript:void(0);" onclick="openNavform()">Free Pass</a></li>
            <li class="_2GaQ8vJ"><a class="m7LKENr _25LPQhk _2DGciQP" href="<?= get_site_url(); ?>/categories">Classes</a></li>
            <li class="_2GaQ8vJ">
            <div class="dropdown m7LKENr _25LPQhk _2DGciQP">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">Join</a>
                <div class="dropdown-menu sele_club">
                    <a href="https://secure12.clubwise.com/onelifefitness/pos.asp" class="dropdown-item">One life Fitness Cork City</a>
                    <a href="https://secure16.clubwise.com/onelifefitnessriversedge/pos.asp" class="dropdown-item">One Life Fitness Riversedge</a>
                </div>
            </div>
            </li>
            <li class="_2GaQ8vJ"><a class="m7LKENr _25LPQhk _2DGciQP" href="<?= get_site_url(); ?>/contact-us">Contact</a></li>
        </ul>
    </header>

    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="overlay-content">
            <div class="modal-side-set">
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/categories">Classes</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/gym">Gyms</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/timetable">Timetable</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/join-us">Membership</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/personal-training">Personal
                    Training</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/about">Our Story</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/career">Careers</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/corporate">Corporate</a>
                <a class="_2Vj-OOq _2_HaYMw _1w_OEYg" href="<?= get_site_url(); ?>/blog">Blog</a>
                <ul class="list-inline social_site">
                    <li class="list-inline-item">
                        <a href="https://www.instagram.com/one_life_fitness_/" class="" target="_blank">Instagram</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://twitter.com/onel1fefitness" class="_3rmm2J7 _2BYuTc8 _2DGciQP _25LPQhk"
                            target="_blank">Twitter</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/One-Life-Fitness-2233876316629833/" class=""
                            target="_blank">Facebook</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="myNav-form" class="overlay" style="background-color:#000000">
        <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNavform()">&times;</a> -->
        <div class="overlay-content">
            <div class="modal-side-set">
                <div class="content">
                    <!--content inner-->
                    <div class="content__inner">
                        <div class="container">
                            <!--content title-->
                            <!-- <h2 class="content__title content__title--m-sm">Pick animation type</h2> -->
                            <!--animations form-->
                            <!-- <form class="pick-animation my-4">
                            <div class="form-row">
                            <div class="col-5 m-auto">
                                <select class="pick-animation__select form-control">
                                <option value="scaleIn" selected="selected">ScaleIn</option>
                                <option value="scaleOut">ScaleOut</option>
                                <option value="slideHorz">SlideHorz</option>
                                <option value="slideVert">SlideVert</option>
                                <option value="fadeIn">FadeIn</option>
                                </select>
                            </div>
                            </div>
                        </form> -->
                            <!--content title-->

                            <div class="_1US2FDM _2rbE6TC _2v5bHvx VBD7Ow3">
                                <button type="hidden" class="btn btn-lg toastrDefaultSuccess d-none"></button>

                                <h3 class="LsKezX_">FREE PASS</h3>
                                <span class="_2va2FpG" onclick="closeNavform()"></span>
                            </div>
                            <!-- <h1 class="content__title">FREE PASS</h1> -->
                        </div>
                        <div class="container overflow-hidden">
                            <!--multisteps-form-->
                            <div class="multisteps-form">
                                <!--progress bar-->
                                <div class="row d-none">
                                    <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
                                        <div class="multisteps-form__progress">
                                            <button class="multisteps-form__progress-btn js-active" type="button"
                                                title="User Info">User Info</button>
                                            <button class="multisteps-form__progress-btn" type="button"
                                                title="Address">Address</button>
                                            <button class="multisteps-form__progress-btn" type="button"
                                                title="Order Info">Order Info</button>
                                            <button class="multisteps-form__progress-btn" type="button"
                                                title="Comments">Comments </button>
                                        </div>
                                    </div>
                                </div>
                                <!--form panels-->
                                <div class="row">
                                    <div class="col-12 col-lg-8 m-auto">
                                        <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST" class="multisteps-form__form">
                                            <!--single form panel-->
                                            <input type="hidden" name="action" value="my_simple_form">
                                            <div class="multisteps-form__panel shadow p-4 rounded js-active"
                                                data-animation="scaleIn">
                                                <div class="multisteps-form__content">
                                                    <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                                                        <div class="Zd4QDDO">
                                                            <h4 class="Zd4QDDO">1/5</h4>
                                                        </div>
                                                        <h2 class="_1_KNBvC">What's your name?</h2>
                                                        <div class="_2nxeEK_ _14NUrhZ">
                                                            <label for="name"></label>
                                                            <input type="text" name="name" value="" id="your_name"
                                                                placeholder="Name" autocomplete="off" required>
                                                            <span class="float-left alert_sms text-danger"><span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <button type="button" class="btn_name"><span>Next</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--single form panel-->
                                            <div class="multisteps-form__panel shadow p-4 rounded"
                                                data-animation="scaleIn">
                                                <div class="multisteps-form__content">
                                                    <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                                                        <div class="Zd4QDDO">
                                                            <h4 class="Zd4QDDO">2/5</h4>
                                                        </div>
                                                        <h2 class="_1_KNBvC">What's your email?</h2>
                                                        <div class="_2nxeEK_ _14NUrhZ">
                                                            <label for="email"></label>
                                                            <input type="email" name="email" value="" id="email"
                                                                placeholder="Email" required autocomplete="off">
                                                            <span class="float-left alert_sms text-danger"><span>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="button-row d-flex mt-4 col-12">
                                                        <button type="button" class="_2S_CT_r js-btn-prev"
                                                                title="Prev"><span>Prev</span>
                                                            </button>
                                                            <button type="button"
                                                                class="btn_email"><span>Next</span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--single form panel-->
                                            <div class="multisteps-form__panel shadow p-4 rounded"
                                                data-animation="scaleIn">
                                                <div class="multisteps-form__content">
                                                    <div class="row">
                                                        <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                                                            <div class="Zd4QDDO">
                                                                <h4 class="Zd4QDDO">3/5</h4>
                                                            </div>
                                                            <h2 class="_1_KNBvC">What's your phone?</h2>
                                                            <div class="_2nxeEK_ _14NUrhZ">
                                                                <label for="phone"></label>
                                                                <input type="text" name="phone" value="" id="phone"
                                                                    placeholder="Phone" autocomplete="off"
                                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                                                <span class="float-left alert_sms text-danger"><span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="button-row d-flex mt-4 col-12">
                                                            <button type="button" class="_2S_CT_r js-btn-prev"
                                                                title="Prev"><span>Prev</span>
                                                            </button>
                                                            <button type="button"
                                                                class="btn_phone"><span>Next</span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--single form panel-->
                                            <div class="multisteps-form__panel shadow p-4 rounded"
                                                data-animation="scaleIn">
                                                <div class="multisteps-form__content">
                                                    <div class="row">
                                                        <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                                                            <div class="Zd4QDDO">
                                                                <h4 class="Zd4QDDO">4/5</h4>
                                                            </div>
                                                            <h2 class="_1_KNBvC">Choose Your Gym (Required)</h2>
                                                            <div class="_2nxeEK_ _14NUrhZ">
                                                                <label for="club"></label>
                                                                <select name="club" id="club" class="">
                                                                    <option value=""selected disabled>Select Club</option>
                                                                    <option value="onelife fitness camden quay">onelife fitness camden quay</option>
                                                                    <option value="onelife fitness sullivans quay">onelife fitness sullivans quay</option>
                                                                </select>
                                                                <span class="float-left alert_sms text-danger"><span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="button-row d-flex mt-4 col-12">
                                                        <button type="button" class="_2S_CT_r js-btn-prev"
                                                                title="Prev"><span>Prev</span>
                                                            </button>
                                                            <button type="button"
                                                                class="btn_club"><span>Next</span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--single form panel-->
                                            <div class="multisteps-form__panel shadow p-4 rounded"
                                                data-animation="scaleIn">
                                                <div class="multisteps-form__content">
                                                    <div class="_2cjZ1PT _1G7RCdW _2v5bHvx">
                                                        <div class="Zd4QDDO">
                                                            <h4 class="Zd4QDDO">5/5</h4>
                                                        </div>
                                                        <h2 class="_1_KNBvC">Time to submit!</h2>
                                                        <p>I understand that by submitting my details I will be
                                                            contacted by ONE L1FE FITNESS with information about their
                                                            services
                                                            and membership options (not with spam, we promise)</p>
                                                        <input type="submit" name="free_pass" value="free pass">
                                                    </div>

                                                    <div class="row d-flex mt-4 col-12">
                                                        <div class="button-row d-flex mt-4">
                                                            <button type="button" class="_2S_CT_r js-btn-prev"
                                                                title="Prev"><span>Prev</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $("#showToast").click(function() {
            $('.toast').toast('show');
        });
        $('.btn_name').click(function() {
            if ($('#your_name').val() == "") {
                $(".alert_sms").text('This field is required.');
            }
        });
        $('#your_name').keyup(function() {
            name = $(this).val();
            if (name == "") {
                $('.btn_name').removeClass("js-btn-next");
                $(".alert_sms").text('This field is required.');
            } else {
                $('.btn_name').addClass("js-btn-next");
                $(".alert_sms").text('');
            }
        });

        $('.btn_email').click(function() {
            if ($('#email').val() == "") {
                $(".alert_sms").text('This field is required.');
            }
        });
        $('#email').on('keyup', function() {
            var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
            if (!re) {
                $('.btn_email').removeClass("js-btn-next");
                $(".alert_sms").text('Invalid Email Format');
            } else {
                $('.btn_email').addClass("js-btn-next");
                $(".alert_sms").text('');
            }
        })


        $('.btn_phone').click(function() {
            if ($('#phone').val() == "") {
                $(".alert_sms").text('This field is required.');
            }
        });
        $('#phone').keyup(function() {
            phone = $(this).val();
            if (phone == "") {
                $('.btn_phone').removeClass("js-btn-next");
                $(".alert_sms").text('This field is required.');
            } else {
                $('.btn_phone').addClass("js-btn-next");
                $(".alert_sms").text('');
            }
        });


        $('.btn_club').click(function() {
            if ($('#club').val() == "") {
                $(".alert_sms").text('This field is required.');
            }
        });
        $('#club').change(function() {
            club = $(this).val();
            if (club == "") {
                $('.btn_club').removeClass("js-btn-next");
                $(".alert_sms").text('This field is required.');
            } else {
                $('.btn_club').addClass("js-btn-next");
                $(".alert_sms").text('');
            }
        });

    });


    //DOM elements
    const DOMstrings = {
        stepsBtnClass: 'multisteps-form__progress-btn',
        stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
        stepsBar: document.querySelector('.multisteps-form__progress'),
        stepsForm: document.querySelector('.multisteps-form__form'),
        stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
        stepFormPanelClass: 'multisteps-form__panel',
        stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
        stepPrevBtnClass: 'js-btn-prev',
        stepNextBtnClass: 'js-btn-next'
    };


    //remove class from a set of items
    const removeClasses = (elemSet, className) => {
        elemSet.forEach(elem => {
            elem.classList.remove(className);
        });
    };

    //return exect parent node of the element
    const findParent = (elem, parentClass) => {
        let currentNode = elem;

        while (!currentNode.classList.contains(parentClass)) {
            currentNode = currentNode.parentNode;
        }
        return currentNode;
    };

    //get active button step number
    const getActiveStep = elem => {
        return Array.from(DOMstrings.stepsBtns).indexOf(elem);
    };

    //set all steps before clicked (and clicked too) to active
    const setActiveStep = activeStepNum => {

        //remove active state from all the state
        removeClasses(DOMstrings.stepsBtns, 'js-active');

        //set picked items to active
        DOMstrings.stepsBtns.forEach((elem, index) => {
            if (index <= activeStepNum) {
                elem.classList.add('js-active');
            }
        });
    };

    //get active panel
    const getActivePanel = () => {
        let activePanel;
        DOMstrings.stepFormPanels.forEach(elem => {
            if (elem.classList.contains('js-active')) {
                activePanel = elem;
            }
        });
        return activePanel;
    };

    //open active panel (and close unactive panels)
    const setActivePanel = activePanelNum => {

        //remove active class from all the panels
        removeClasses(DOMstrings.stepFormPanels, 'js-active');

        //show active panel
        DOMstrings.stepFormPanels.forEach((elem, index) => {
            if (index === activePanelNum) {
                elem.classList.add('js-active');
                setFormHeight(elem);
            }
        });
    };

    //set form height equal to current panel height
    const formHeight = activePanel => {
        const activePanelHeight = activePanel.offsetHeight;
        DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;
    };

    const setFormHeight = () => {
        const activePanel = getActivePanel();

        formHeight(activePanel);
    };

    //STEPS BAR CLICK FUNCTION
    // DOMstrings.stepsBar.addEventListener('click', e => {

    //     //check if click target is a step button
    //     const eventTarget = e.target;

    //     if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    //         return;
    //     }

    //     //get active button step number
    //     const activeStep = getActiveStep(eventTarget);

    //     //set all steps before clicked (and clicked too) to active
    //     setActiveStep(activeStep);

    //     //open active panel
    //     setActivePanel(activeStep);
    // });

    //PREV/NEXT BTNS CLICK
    DOMstrings.stepsForm.addEventListener('click', e => {

        const eventTarget = e.target;

        //check if we clicked on `PREV` or NEXT` buttons
        if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList
                .contains(`${DOMstrings.stepNextBtnClass}`))) {
            return;
        }

        //find active panel
        const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);

        let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);

        //set active step and active panel onclick
        if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
            activePanelNum--;

        } else {
            activePanelNum++;
        }
        setActiveStep(activePanelNum);
        setActivePanel(activePanelNum);
    });

    //SETTING PROPER FORM HEIGHT ONLOAD
    window.addEventListener('load', setFormHeight, false);

    //SETTING PROPER FORM HEIGHT ONRESIZE
    window.addEventListener('resize', setFormHeight, false);

    //changing animation via animation select !!!YOU DON'T NEED THIS CODE (if you want to change animation type, just change form panels data-attr)

    const setAnimationType = newType => {
        DOMstrings.stepFormPanels.forEach(elem => {
            elem.dataset.animation = newType;
        });
    };

    //selector onchange - changing animation
    const animationSelect = document.querySelector('.pick-animation__select');

    animationSelect.addEventListener('change', () => {
        const newAnimationType = animationSelect.value;
        setAnimationType(newAnimationType);
    });
    </script>