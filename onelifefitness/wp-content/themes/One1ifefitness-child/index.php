<?php /* Template Name: home-page */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gym_box
 */
?>
<?php get_header(); 
if(isset($_GET['status']) && $_GET['status']=='true'){
    // echo '<h6 class="alert alert-success text-center deletesuccess">Record added</h6>';
    echo "<script>
        $('document').ready(function(){ 
        $('.toastrDefaultSuccess').click(); 
        // $('#modalContactForm').addClass('d-none');
        }); 
    </script>"?>
    <?php }
?>

<div class="_1vdzHPH">
    <div>
        <section class="_3UoOzEJ _2rbE6TC _2v5bHvx _1CqZaJN">
            <div class="_1JMynM5 _3-6A3ch _1CqZaJN">
                <video class="_3h_jpHd _21UD1KG _3HiqiFI" style="width:100%;" autoplay muted playsinline loop
                    src="<?= get_template_directory_uri() ?>/assets/videos/BoxRideLiftLong-com.mp4"
                    oncanplay="this.muted=true"></video>
            </div>
            <div class="_3d137aW sDHKtsB" style="transform: translateY(35.4125px);">
                <!-- <div class="_3NcCK2a _2rbE6TC _2v5bHvx pRZSkbx VBD7Ow3">
                    <h1 class="_1jnoPLs _3x6v3-y">Anything Goes</h1>
                    <h4 class="MgRzFsz _3x6v3-y">Home to big hitters, heavy lifters, slick dancers, zen masters,
                        first timers, seasoned sadists, movers and rule-breakers of every kind</h4>
                    <div class="FBl2DCz _2rbE6TC _2v5bHvx _3x6v3-y">
                        <h4 class="_3KWTFTz _2DGciQP" id="video1">Watch</h4>
                    </div>
                </div> -->
            </div><span class="_3nHfkWi sDHKtsB" style="opacity: 0;"></span><span class="FUTUrGJ"></span>
            <div class="_3VqQwD6 sDHKtsB">
                <div class="_2tbaljn pRZSkbx">
                    <div class="YvuJKdN"></div>
                </div>
            </div>
        </section>
        <section class="_1UgSuTH _2rbE6TC _2v5bHvx">
            <h2 class="_3fhPOEA _2_HaYMw">A New Breed Of Gym In Cork City</h2>
            <div class="_9r7AgCX _2rbE6TC _2v5bHvx">
                <div class="kcyDmek _3m-B4DB"
                    style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png')">
                    <a href="<?= get_site_url(); ?>/regeneration-z">
                        <!-- <button class="_3xulBbp _3Pq3GhV"><span>View class</span></button> -->
                        <button class="_3xulBbp _3Pq3GhV"><span>View class</span></button>
                    </a></div>
                <div class="_3m-B4DB"
                    style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/One-Life-R1DE.png')">
                    <a href="<?= get_site_url(); ?>/rampant-rehab"><button class="_3xulBbp _3Pq3GhV"><span>View
                                class</span></button></a></div>
                <div class="_3m-B4DB"
                    style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/Andreea-Yoga-Tile.png')">
                    <a href="<?= get_site_url(); ?>/sword-play"><button class="_3xulBbp _3Pq3GhV"><span>View
                                class</span></button></a></div>
                <div class="_3m-B4DB"
                    style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/OBox-Tile.png')">
                    <a href="<?= get_site_url(); ?>/laughing-therapy"><button class="_3xulBbp _3Pq3GhV"><span>View
                                class</span></button></a></div>
                <div class="_3m-B4DB"
                    style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/OFB-L1FT-Photo.png')">
                    <a href="<?= get_site_url(); ?>/rolling-with-my-yogis"><button class="_3xulBbp _3Pq3GhV"><span>View
                                class</span></button></a></div>
            </div>
        </section>
        <section class="AFpgIcy _2rbE6TC _2v5bHvx">
            <div></div>
            <div class="_1JMynM5 YSLFsES sDHKtsB">
                <video autoplay="" class="_3h_jpHd _1fwVBhX" playsinline="" loop="" muted="muted"
                    src="<?= get_template_directory_uri() ?>/assets/videos/map.mp4"></video></div><span
                class="_2hv5MQP sDHKtsB"></span>
            <a class="_2HHd2sE I7PwxVS" href="<?= get_site_url(); ?>/gym">
                <h2 class="_1uQNznK _2_HaYMw">Find your gym</h2>
                <h3>CORK CITY</h3><button class="_2wN8HCL _3Pq3GhV"><span>Find your gym</span></button>
            </a>
        </section>
        <div class="_3sdzKXV _2rbE6TC _2v5bHvx VBD7Ow3">
            <div>
                <div class="M5KqVYK _2rbE6TC _2v5bHvx">
                    <h3 class="_2Ii007W">Instagram</h3>
                    <!-- <a class="zxpnPmO _25LPQhk" href="">@gymboxofficial</a> -->
                </div>
                <div class="_2lcgMde _2rbE6TC _2v5bHvx">
                    <div class="_1fy4wK0 _2rbE6TC _2v5bHvx">
                        <div class="_1yfuZQ0 _1XSaRkn">
                            <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE41976D15-4EBF-47AA-8413-C81599A1FAE7.JPG');"></span>
                                </div>
                            </a>
                        </div>
                        <div class="_1XSaRkn">
                            <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE41C1-A0A0-B2DDC64C498D.JPG');"></span>
                                </div>
                            </a>
                        </div>
                        <div class="_1yfuZQ0 _1XSaRkn">
                            <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONEIMG_1075.JPG');"></span>
                                </div>
                            </a>
                        </div>
                        <div class="_1XSaRkn">
                            <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONE-A07717B5E353.JPG');"></span>
                                </div>
                            </a>
                        </div>
                        <div class="_1XSaRkn">
                            <a href="https://www.instagram.com/one_life_fitness_/" target="_blank">
                                <div class="_2gpiPPm _1UIRqyl"><span class="_3h_jpHd _30CnrX2 sDHKtsB"
                                        style="background-image: url('<?= get_template_directory_uri() ?>/assets/images/ONEE1BF360F2569.JPG');"></span>
                                </div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="DlAw8Ep">
            <a class="_3ULa4Rt" href="javascript:void(0)" onclick="openNavform()" >
                <h2 class="_1Uq5Y7q"> One Life Sullivans Quay </h2>
                <h4> Book a tour now </h4>
                <p class="_1K9a2Cg">Limited Founder Rates Available</p>
            </a>
            <span class="_13J05Gx"></span>
        </div>
    </div>
</div>
<!-- home page popup form -->
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content bg-clr-black" style="border-top: 4px solid #fecf3b;border-bottom: 4px solid #fecf3b;">
            <div class="modal-header mx-3">
                <h4 class="modal-title w-100 m-0 font-weight-bold text-uppercase" style="color:#fecf3b;">free gym pass</h4>
                <input type="button" class="btn btn-gym close" title="Close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-white">&times;</span>
            </div>
            <div class="modal-body mx-0 bg-clr-black">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row box-width">
                            <div class="HRKJIWa VBD7Ow3">
                                <form class="pt-enquiry-form" id="form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
                                <!-- Add a hidden form field with the name "action" and a unique value that you can use to handle the form submission  -->
                                    <input type="hidden" name="action" value="my_simple_form" >
                                    <div class="_2nxeEK_ _14NUrhZ">
                                        <label for="name">Name</label>
                                        <input name="name" placeholder="Name" type="text" value="" required>
                                    </div>
                                    <div class="_2nxeEK_ _14NUrhZ">
                                        <label for="email">Email</label>
                                        <input name="email" placeholder="Email" type="email" value="" required>
                                    </div>
                                    <div class="_2nxeEK_ _14NUrhZ">
                                        <label for="phone">Phone</label>
                                        <input name="phone" placeholder="Phone" type="text" value="" required>
                                    </div>
                                    <div class="_2nxeEK_ _14NUrhZ">
                                        <label for="choose_gym">Choose Your Gym (Required)</label>
                                        <select name="choose_gym" placeholder="Club interested in" required>
                                            <option value="" disabled selected> Select</option>
                                            <option value="onelife fitness sullivans quay">onelife fitness sullivans quay</option>
                                            <option value="onelife fitness camden quay">onelife fitness camden quay</option>
                                        </select>
                                    </div>
                                    <div class="_2nxeEK_ _14NUrhZ">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check"
                                                name="check" required>
                                            <label class="custom-control-label" for="check">I fully agree to
                                                receive contact via telephone & give consent that my data will be stored
                                                and handled by this website</label>
                                        </div>
                                    </div>
                                    <input type="submit" class="submit" name="free_pass" value="Submit" >
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="text-center">
    <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm">LaunchModal Contact Form</a>
</div> -->
<!-- <div id="vidBox">
    <div id="videCont">
        <video autoplay id="v1" loop controls>
            <source src="<?= get_template_directory_uri() ?>/assets/videos/gymbox-hero-v2.mp4" type="video/mp4">
        </video>
    </div>
</div> -->
<script>
let searchParams = new URLSearchParams(window.location.search);
let param = searchParams.get('status')


$(document).ready(function() {
    $('video, audio').prop('muted', true);
    if(param == 'true'){
        setTimeout(myFunctionhide);
        
    }
    else{
        setTimeout(myFunction, 7000);
    }
});

function myFunction () {
    $("#modalContactForm").modal("show");
}
function myFunctionhide () {
    $("#modalContactForm").modal("hide");
}



// $('#check').on('change', function() {
//     this.value = this.checked ? 'Yes' : 'No';
// }).change();

// $(function() {
//     $('#vidBox').VideoPopUp({
//         backgroundColor: "#17212a",
//         opener: "video1",
//         maxweight: "340",
//         idvideo: "v1"
//     });
// });

// (function($) {
//     $.fn.VideoPopUp = function(options) {
//         var defaults = {
//             backgroundColor: "#000000",
//             opener: "video",
//             maxweight: "640",
//             pausevideo: false,
//             idvideo: ""
//         };
//         var patter = this.attr('id');

//         var settings = $.extend({}, defaults, options);

//         var video = document.getElementById(settings.idvideo);

//         function stopVideo() {
//             var tag = $('#' + patter + '').get(0).tagName;
//             if (tag == 'video') {
//                 video.pause();
//                 video.currentTime = 0;
//             }
//         }

//         $('#' + patter + '').css("display", "none");
//         $('#' + patter + '').append('<div id="opct"></div>');
//         $('#opct').css("background", settings.backgroundColor);
//         $('#' + patter + '').css("z-index", "100001");
//         $('#' + patter + '').css("position", "fixed")
//         $('#' + patter + '').css("top", "0");
//         $('#' + patter + '').css("bottom", "0");
//         $('#' + patter + '').css("right", "0");
//         $('#' + patter + '').css("left", "0");
//         $('#' + patter + '').css("padding", "auto");
//         $('#' + patter + '').css("text-align", "center");
//         $('#' + patter + '').css("background", "none");
//         $('#' + patter + '').css("vertical-align", "vertical-align");
//         $("#videCont").css("z-index", "100002");
//         $('#' + patter + '').append('<div id="closer_videopopup">&otimes;</div>');
//         $("#" + settings.opener + "").on('click', function() {
//             $('#' + patter + "").show();
//             $('#' + settings.idvideo + '').trigger('play');

//         });
//         $("#closer_videopopup").on('click', function() {
//             if (settings.pausevideo == true) {
//                 $('#' + settings.idvideo + '').trigger('pause');
//             } else {
//                 stopVideo();
//             }
//             $('#' + patter + "").hide();
//         });
//         return this.css({

//         });
//     };
// }(jQuery));


$('._13J05Gx').on('click', function() {
    $('.DlAw8Ep').remove();
});
</script>
<?php
// get_sidebar();
get_footer();
?>