<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gym_box
 */

?>

<footer class="_24zAESA _2rbE6TC _2v5bHvx">
    <div class="_2revefY _2rbE6TC _2v5bHvx _25LPQhk">
       <div class="p-HDv6u" onclick="openNavform()">
            <span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span
                class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span
                class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span
                class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span class="DGKlJgu">FREE PASS</span><span
                class="DGKlJgu">FREE PASS</span>
        </div>
    </div>
    <div class="_1B_-c0t VBD7Ow3">
        <div class="_3sadWOc _2HhTmXD _2v5bHvx">
            <h3>Pages</h3>
            <ul class="_1aeQHSW">
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/career">Careers</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/services">Member Services</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/faqs">FAQs</a></li>
                <?php /*<li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/refer">Refer a Friend</a></li> */ ?>
                <?php /*<li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/">Press &amp; Marketing Contact</a></li> */ ?>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/contact-us">Studio Contact</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/">Club rules</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/terms-and-conditions">Terms and Conditions</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/privacy-policy">Privacy Policy</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/resources">Resources</a></li>
            </ul>
        </div>
        <div class="_3sadWOc _2HhTmXD _2v5bHvx">
            <h3>Gyms</h3>
            <ul class="_1aeQHSW">
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Onelifefitness Camden quay</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Onelifefitness Sullivans quay</a></li>
                <?php /* <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Covent Garden</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Ealing</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Elephant and Castle</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Farringdon</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Holborn</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Old Street</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Victoria</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Westfield London</a></li>
                <li><a class="_3FnoLbj _25LPQhk _2DGciQP"  href="<?= get_site_url(); ?>/gym">Westfield
                        Stratford</a></li> */?>
            </ul>
        </div>
        <div class="_3sadWOc _2HhTmXD _2v5bHvx">
            <h3>Social</h3>
            <ul class="_1aeQHSW">
                <li><a href="https://www.instagram.com/one_life_fitness_/" class="_3FnoLbj _25LPQhk _2DGciQP">Instagram</a>
                </li>
                <li><a href="https://www.facebook.com/One-Life-Fitness-2233876316629833/" class="_3FnoLbj _25LPQhk _2DGciQP">Facebook</a>
                </li>
                <li><a href="https://twitter.com/onel1fefitness" class="_3FnoLbj _25LPQhk _2DGciQP">Twitter</a>
                </li>
            </ul>
        </div>
        <div class="_3sadWOc _2HhTmXD _2v5bHvx">
            <h3>Newsletter</h3>
            <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST" >
                <!--single form panel-->
                <input type="hidden" name="action" value="my_simple_form">
                <div class="_2nxeEK_ _14NUrhZ">
                    <label for="name"></label>
                    <input type="text" name="name" value="" placeholder="Your name" required>
                </div>
                <div class="_2nxeEK_ _14NUrhZ">
                    <label for="email"></label>
                    <input type="email" name="email" value="" placeholder="Your email" required>
                </div>
                    <input type="submit" name="news_letter" placeholder="Submit">
            </form>
        </div>
        <div class="_3QyZoZ_ _2rbE6TC _2v5bHvx">
            <h4 class="HLzP6ji">©2020 onelifefitness</h4>
            <h4 class="_3oiFvb3"><a href="https:www.hachiweb.com" target="_blank">Hachiweb</a></h4>
        </div>
    </div>
</footer>
<script>
  AOS.init();

  $('.toastrDefaultSuccess').click(function() {
    toastr.success('You have successfully submitted!')
  });
</script>
<?php wp_footer(); ?>
</body>
</html>
