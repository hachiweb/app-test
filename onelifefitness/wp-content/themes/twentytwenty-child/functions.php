<?php
  add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
  function enqueue_parent_styles() {
      wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
      // wp_enqueue_style( 'custom-css', get_stylesheet_directory_uri().'/assets/css/custom-style.css', array( 'jquery' ), '1.0', true );
      wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), '1.0', true );
  }
  // add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
  // function enqueue_child_theme_styles() {
  //   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
  // }

  // Use your hidden "action" field value when adding the actions
  add_action( 'admin_post_nopriv_my_simple_form', 'my_handle_form_submit' );
  add_action( 'admin_post_my_simple_form', 'my_handle_form_submit' );
