/*
Plugin Name: One Life Popup Banner
Plugin URI:  https://http://onelifefitness.ie/
Description: Creates a bottom sliding banner
Version:     1.0
Author:      Hachiweb
Author URI:  https://hachiweb.com/
License:     GPL2 etc
License URI: https://hachiweb.com/license/wp-plugins
*/

Copyright 2020 HACHIWEB (email : dev@hachiweb.com)
(Plugin Name) is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
(Plugin Name) is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with (Plugin Name). If not, see (https://hachiweb.com/license/wp-plugins).
*/

function olf_content_replace( $content ) {
	$search  = array( 'wordpress', 'goat', 'Easter', '70', 'sensational' );
	$replace = array( 'WordPress', 'coffee', 'Easter holidays', 'seventy', 'extraordinary' );
	return str_replace( $search, $replace, $content );
}
add_filter( 'the_content', 'olf_content_replace' );

function olf_content_footer_note( $content ) {
	$content .= '<footer class="olf-content-footer">All rights reserved. <a href="https://http://onelifefitness.ie/">One Life Fitness</a></footer>';
	return $content;
}
add_filter( 'the_content', 'olf_content_footer_note' );