<!DOCTYPE html>
<html lang="en">

<head>
    <title>Entry and Exit Animation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</head>

<body>

    <section class="container" id="shoe_show">
        <div class="select_shoe">
            <h3>Select the Shoe Part</h3>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="part">
                    <h6 id="part1">PART 1</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part">
                    <h6>PART 2</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part">
                    <h6>PART 3</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part">
                    <h6>PART 4</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part">
                    <h6>PART 5</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part">
                    <h6>PART 6</h6>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
    </section>
    <!--section-->

    <section class="container display-hidden" id="material">
        <div class="select_shoe">
            <h3>Select Material Name</h3>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="part">
                    <h6 id="part_change">PART 1</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part_leather">
                    <h6>Genuine Leather</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part_leather">
                    <h6>Manmade Leather</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part_leather">
                    <h6>Velvet</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part_leather">
                    <h6>Canvas</h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="part_leather">
                    <h6>Denim</h6>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
    </section>
    <!--section-->

    <section class="container display-hidden" id="texture">
        <div class="select_shoe">
            <h3>Select Color</h3>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="part_leather">
                    <h6 id="texture_change"></h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="texture_part_leather clr1">
                    <h6></h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="texture_part_leather cl2">
                    <h6></h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="texture_part_leather clr3">
                    <h6></h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="texture_part_leather clr4">
                    <h6></h6>
                </div>
            </div>
            <!--col-->
            <div class="col-md-2">
                <div class="texture_part_leather clr5">
                    <h6></h6>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
    </section>
    <section class="container mt-4">
        <div class="row">
            <div class="col-12 text-right">
                <button type="button" class="btn btn-success start" >Start Over</button>
                <!-- <button type="button" class="btn btn-success">Go Back</button> -->
            </div>
        </div>
    </section>
    <!--section-->
    <!-- <div class="container mt-5">
        <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                <div class="carousel-item col-md-3 col-sm-3 col-3 active">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                            <div class="part">
                                <h6 id="part1">PART 1</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                            <div class="part">
                                <h6 id="">PART 2</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part">
                                <h6 id="">PART 3</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part">
                                <h6 id="">PART 4</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part">
                                <h6 id="">PART 5</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part">
                                <h6 id="">PART 6</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part_leather">
                            <h6>Genuine Leather</h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part_leather">
                            <h6>Manmade Leather</h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part_leather">
                            <h6>Velvet</h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part_leather">
                            <h6>Canvas</h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="part_leather">
                            <h6>Denim</h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="texture_part_leather clr1">
                          <h6></h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                         <div class="texture_part_leather clr2">
                           <h6></h6>
                         </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                            <div class="texture_part_leather clr3">
                                <h6></h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="texture_part_leather clr4">
                            <h6></h6>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item col-md-3 col-sm-3 col-3">
                    <div class="panel panel-default">
                        <div class="panel-thumbnail">
                        <div class="texture_part_leather clr5">
                            <h6></h6>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon text-dark" aria-hidden="true"></span>
                <span class="sr-only text-dark">Previous</span>
            </a>
            <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                <span class="carousel-control-next-icon text-dark" aria-hidden="true"></span>
                <span class="sr-only text-dark">Next</span>
            </a>
        </div>
    </div> -->
    <div class="margin-bottom"></div>

    <script>
    $('#carouselExample').on('slide.bs.carousel', function(e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 4;
        var totalItems = $('.carousel-item').length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $('.carousel-item').eq(i).appendTo('.carousel-inner');
                } else {
                    $('.carousel-item').eq(0).appendTo('.carousel-inner');
                }
            }
        }
    });

    $('#carouselExample').carousel({
        interval: 2000
    });

    $(document).ready(function() {
        /* show lightbox when clicking a thumbnail */
        $('a.thumb').click(function(event) {
            event.preventDefault();
            var content = $('.modal-body');
            content.empty();
            var title = $(this).attr("title");
            $('.modal-title').html(title);
            content.html($(this).html());
            $(".modal-profile").modal({
                show: true
            });
        });

    });
    </script>
</body>

</html>