$(document).on('click',".part", function () {
    var text = $(this).children().text();
    var boxWidth = $("#shoe_show").width();

    $('#texture').hide(2000);
    if (text=='PART 1') {
        $("#part_change").text(text);
        $('#material').show(2000);
        // $("#material").animate({
        //     width: boxWidth
        // });
        $('#shoe_show').hide(2000);
        // $("#shoe_show").animate({
        //     width: 0
        // });
    } 
    else if (text=='PART 2') {
        $("#part_change").text(text);
        $('#material').show(2000);
        $('#shoe_show').hide(2000);
    }
    else if (text=='PART 3') {
        $("#part_change").text(text);
        $('#material').show(2000);
        $('#shoe_show').hide(2000);
    }
    else if (text=='PART 4') {
        $("#part_change").text(text);
        $('#material').show(2000);
        $('#shoe_show').hide(2000);
    }
    else if (text=='PART 5') {
        $("#part_change").text(text);
        $('#material').show(2000);
        $('#shoe_show').hide(2000);
    }
    else if (text=='PART 6') {
        $("#part_change").text(text);
        $('#material').show(2000);
        $('#shoe_show').hide(2000);
    }
});

$(document).on('click',".part_leather", function () {
    var material = $(this).children().text();
    if (material=='Genuine Leather') {
        $("#texture_change").text(material);
        $('#texture').show(2000);
        $('#material').hide(2000);
    } 
    else if (material=='Manmade Leather') {
        $("#texture_change").text(material);
        $('#texture').show(2000);
        $('#material').hide(2000);
    }
    else if (material=='Velvet') {
        $("#texture_change").text(material);
        $('#texture').show(2000);
        $('#material').hide(2000);
    }
    else if (material=='Canvas') {
        $("#texture_change").text(material);
        $('#texture').show(2000);
        $('#material').hide(2000);
    }
    else if (material=='Denim') {
        $("#texture_change").text(material);
        $('#texture').show(2000);
        $('#material').hide(2000);
    }
});
$(document).on('click',".btn-success", function () {
 $("#shoe_show").hide(2000);
 $('#material').show(2000);
 $('#texture').hide(2000);
});
$(document).on('click',".start", function () {
    $("#shoe_show").show(2000);
    $('#material').hide();
    $('#texture').hide();
   });

//   $(document).ready(function(){
//         var boxWidth = $("#shoe_show").width();
//         $(".part").click(function(){
//             $("#shoe_show").animate({
//                 width: 0
//             });
//             $("#material").animate({
//                 width: boxWidth
//             });
//         });
//     });