<?php /* Template Name: cpm */
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CPM
 */

get_header(); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Advertising</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/all.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
    * {
        padding: 0px;
        margin: 0px;
    }

    body {
        position: relative;
        background-color: #eaeef0;
    }

    .navbar .navbar-brand img {
        width: 10%;
    }

    .navbar {
        padding: 12px 60px;
    }

    .sticky {
        box-shadow: 0px 3px 16px 0px rgba(0, 0, 0, 0.1) !important;
        position: fixed;
        width: 100%;
        top: -70px;
        left: 0;
        right: 0;
        z-index: 990;
        transform: translateY(70px);
        transition: transform 500ms ease, background 500ms ease;
        -webkit-transition: transform 500ms ease, background 500ms ease;
        box-shadow: 0px 3px 16px 0px rgba(0, 0, 0, 0.1);
        background: rgba(255, 255, 255, 0.96);
        opacity: 0.9;
    }

    .seach_icon_scroll {
        padding: 0px 60px;
    }

    .navbar-light .navbar-nav .nav-link {
        color: #000;
        font-weight: 400;
    }

    .left_box_right .box img {
        width: 100%;
        font-size: 18px;
        text-align: center;
        margin: 10px 0px;
        position: relative;
        transition: 0.3s;
    }

    .center-box .box img {
        font-size: 18px;
        text-align: center;
        margin: 10px 0px;
        position: relative;
        transition: 0.3s;
        width: 100%;
        height: 200px;
    }

    .center_div {
        margin: auto;
        width: 800px;
    }

    .center_div img {
        text-align: center;
        margin: 0px 0px;
        position: relative;
        transition: 0.3s;
        width: 100%;
        object-fit: fill;
    }

    .left_box_right {
        border: 2px solid lightgrey;
        padding: 0px 10px;
    }

    .left_box_right .box img:hover {
        cursor: pointer;
    }

    .top-head {
        background-color: #d1d1d1;
        padding: 10px;
        color: white;
        text-align: center;
    }

    .bottom-ads {
        border: 2px solid lightgrey;
        /* padding: 10px 5px; */
        margin: 10px 0px;
    }

    .box img:hover {
        cursor: pointer;
    }


    /* footer css */
    .footer {
        background-repeat: no-repeat;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        background: #040E27;
        position: relative;
        z-index: 0;
        margin-top: 30px;
    }

    .footer_top {
        padding: 115px 0px 129px;
    }

    .footer_logo a img {
        width: 31%;
    }

    .footer_logo {
        margin-bottom: 44px;
    }

    .footer_widget_logo p {
        font-size: 14px;
        font-weight: 400;
        line-height: 26px;
        color: #AAB1B7;
    }

    .footer_widget_logo p a {
        font-size: 14px;
        font-weight: 400;
        line-height: 26px;
        color: #AAB1B7;
        text-decoration: none;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        -o-transition: 0.3s;
        transition: 0.3s;
    }

    .footer_widget_logo p a:hover {
        color: #FF4A52;

    }

    .footer .socail_links_set {
        margin-top: 47px;
    }

    .socail_links_set ul {
        padding-left: 0px;
    }

    .socail_links_set ul li {
        display: inline;
        line-height: 36px;
        list-style-type: none;
        margin: 0px 25px 0px 0px;
    }

    .socail_links_set ul li a i {
        color: rgb(184, 191, 197);
        font-size: 20px;
    }

    .socail_links_set ul li:hover a i {
        color: #FF4A52;
        position: relative;
        transition: 0.3s;
    }

    .footer_widget_set h3 {
        font-size: 20px;
        font-weight: 400;
        color: #fff;
        text-transform: capitalize;
        margin-bottom: 45px;
        font-family: "Rubik", sans-serif;
    }

    .footer_widget_set ul li a {
        color: #ACACAC;
        font-size: 14px;
        line-height: 36px;
        text-decoration: none;
    }

    .footer_widget_set ul li {
        list-style-type: none;
    }

    .footer_widget_set .links {
        padding-left: 0px;
    }

    .footer_double_links li {
        width: 50%;
        float: left;
    }

    .single_insta a img {
        border-radius: 10px;
        width: 100%;
    }

    .footer_widget_set .row .col-lg-4 {
        padding: 10px !important;
    }

    .footer_border_top {
        border-top: 1px solid #363E52;
        padding-bottom: 26px;
    }

    .footer_widget_set p {
        color: #ACACAC;
    }

    .copy-right_text {
        padding-bottom: 26px;
    }

    .copy_right_footer {
        font-size: 14px;
        color: #7A838B;
        margin-bottom: 0;
        font-weight: 400;
    }

    .copy_right_footer a {
        text-decoration: none;
        color: #FF4A52;
    }

    .links li:hover a {
        color: #FF4A52;
    }
    </style>
</head>

<body>
    <header id="masthead" class="site-header sticky-header shadow">
        <nav class="navbar navbar-expand-md bg-light navbar-light">
            <!-- Brand -->
            <a class="navbar-brand" href="#">
                <img src="<?= get_template_directory_uri() ?>/assets/img/AD-Logo.svg" alt="">
            </a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">CONTACT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link2</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link3</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header><!-- #masthead -->
    <div class="" style="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row bottom-ads">
                        <div class="col-md-8">
                            <div class="center-box">
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif9.gif" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="center-box">
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/ads/ad2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="left_box_right">
                                <div class="top-head">Sponsors</div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif1.gif" alt="">
                                </div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif2.gif" alt="">
                                </div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif3.gif" alt="">
                                </div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif5.gif" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 my-auto">
                            <div class="row">
                                <div class="center_div">

                                    <img src="<?= get_template_directory_uri(); ?>/assets/img/funny_img.jpg" alt="">

                                </div>
                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="left_box_right">
                                <div class="top-head">Sponsors</div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif4.gif" alt="">
                                </div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif6.gif" alt="">
                                </div>
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif1.gif" alt="">
                                </div>
                                <!-- <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/ads/ad7.png" alt="">
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-11 offset-md-1">
                    <div class="row bottom-ads">
                        <div class="col-md-6">
                            <div class="center-box">
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/ads/ad1.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="center-box">
                                <div class="box">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/gif/gif7.gif" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer section-->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="footer_widget_logo">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/AD-Logo.svg" alt="">
                                </a>
                            </div>
                            <p>ABC <br> XYZ <br>
                                <a href="#">+00 123 567 7890</a> <br>
                                <a href="#">xyz@gmail.com</a>
                            </p>
                            <div class="socail_links_set">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="footer_widget_set">
                            <h3 class="footer_title">
                                About
                            </h3>
                            <ul class="links">
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#"> Link</a></li>
                                <li><a href="#"> Link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="footer_widget_set">
                            <h3 class="footer_title">
                                Popular Link
                            </h3>
                            <ul class="links footer_double_links">
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="footer_widget_set">
                            <h3 class="footer_title">
                                Description
                            </h3>
                            <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print,
                                graphic or web designs. The passage is attributed to an unknown typesetter in the 15th
                                century.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border_top"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right_footer text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="far fa-heart"
                                area-hedden="true"></i> by <a href="https://www.hachiweb.com"
                                target="_blank">Hachiweb</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- #colophon -->
    <script>
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".sticky-header").removeClass("sticky");
            $('.navbar').removeClass("seach_icon_scroll");
        } else {
            $(".sticky-header").addClass("sticky");
            $('.navbar').addClass("seach_icon_scroll");
        }
    });
    </script>
</body>

</html>