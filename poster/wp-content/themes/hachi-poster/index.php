<?php
    echo 'Reach out to us on Whatsapp +91 7895632912'.'<br>'.'or email us at prateek.asthana@hachiweb.com';
    exit; 
?>
<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package poster
 */

get_header();
?>
<section class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 left-part style-trip-tab">
        <div class="serial">
            <h2>1. Add Your Spots</h2>
            <span class="choose-destination">Please enter the city name in search and press enter</span>
            <div class="main">
                <div class="form-group mb-0">
                    <span class="fa fa-search form-control-icon"></span>
                    <input type="text" class="form-control" placeholder="Search" autocomplete="on">
                </div>
            </div>
            <span class="">
                <img id="travel_flight" class="">
                <img id="travel_car" class="active">
            </span>
            <div class="mt-4">
                <input type="checkbox" id="looped_map"> <label class="choose-destination" for="looped_map">Loop
                    Back</label>
            </div>
            <h2>2. Choose Your Style</h2>
            <ul class="styler">
                <li class="red active">
                    <img src="<?= get_template_directory_uri() ?>/assets/img/redposter.png">
                    <span>RED</span>
                </li>
                <li class="green">
                    <img src="<?= get_template_directory_uri() ?>/assets/img/greenposter.png">
                    <span>GREEN</span>
                </li>
                <li class="blue">
                    <img src="<?= get_template_directory_uri() ?>/assets/img/blueposter.png">
                    <span>BLUE</span>
                </li>
            </ul>
            <h2>3. Add Some Details</h2>
            <div class="detail-checkboxes">
                <input id="tripName_txt" type="text" placeholder="AUSTRALIA" maxlength="13"
                    style="background-color:#273C50;color:#FFFFFF;font-family: -apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue';border:none;"
                    class="form-control">
                <input id="tripDetail_txt" type="text" placeholder="May - July 2018" maxlength="30"
                    style="background-color:#273C50;color:#FFFFFF;font-family: -apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue';border:none;margin-top:4px;"
                    class="form-control">
            </div>
            <h2 class="mt-4">4. Select Orientation</h2>
            <ul class="portraIte-landscape">
                <li class="portrait active">Portrait</li>
                <li class="landscape">Landscape</li>
            </ul>
            <h2>4. Select the size</h2>
            <ul class="sizes">
                <li class="a">
                    <div class="size-container">
                        <span class="big size">
                            PDF
                        </span>
                        <span class="xsmall">
                            <div class="tool-tip">
                                <i class="fa fa-info-circle fa-4x" data-toggle="tooltip" title="We will send you a high resolution png so that you
                                        can print the poster at your local print shop." style="font-size: 20px;"></i>
                                <!-- <p class="tool-tip__info">
                                    <span class="info">We will send you a high resolution png so that you
                                        can print the poster at your local print shop.</span>
                                </p> -->
                            </div>
                        </span>
                        <span class="big price pt-0">
                            € 4.95
                        </span>
                        <br>
                    </div>
                </li>
                <li class="b">
                    <div class="size-container">
                        <span class="big size">
                            S
                        </span>
                        <span class="small">
                            [ A4 ]
                        </span>
                        <br>
                        <span class="big price">
                            € 24.95
                        </span>
                        <br>
                    </div>
                </li>
                <li class="c">
                    <div class="size-container">
                        <span class="big size">
                            M
                        </span>
                        <span class="small">
                            [ A3 ]
                        </span>
                        <br>
                        <span class="big price">
                            € 29.95
                        </span>
                        <br>
                    </div>
                </li>
            </ul>
            <ul class="new_row"></ul>
            <ul class="sizes">
                <li class="d">
                    <div class="size-container">
                        <span class="big size">
                            L
                        </span>
                        <span class="small">
                            [ A2 ]
                        </span>
                        <span class="small evergreen">
                            POPULAR
                        </span>
                        <span class="big price">
                            € 34.95
                        </span>
                        <br>
                    </div>
                </li>
                <li class="e">
                    <div class="size-container">
                        <span class="big size">
                            XL
                        </span>
                        <span class="small">
                            [ A1 ]
                        </span>
                        <br>
                        <span class="big price">
                            € 39.95
                        </span>
                        <br>
                    </div>
                </li>
            </ul>
            <form enctype="multipart/form-data" method="POST" action="img.php" name="fileinfo">
                <span id="menu">
                    <textarea style="display:none" type="text" id="img" name="img"></textarea>
                    <textarea style="display:none" type="text" id="img2" name="img2"></textarea>
                    <textarea style="display:none" type="text" id="size" name="size"></textarea>
                    <input type="button" id="btnPrint" class="btn btn-outline-success btn-block print-order"
                        value="Order">
                    <!-- input type="button" id="btnDownload" class="btn btn-outline-success btn-block print-order" 
				value="Download" -->
                </span>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9 right-area">
        <div id="map-container" class="map-container mapboxgl-map map-portrait">
            <div class="map-responsive">
                <div id="map" class="mapboxgl-canvas">
                    <div id="state-legend" class="gradient blue-gradient">
                        <span id="tcrTripName"
                            style="font-size: 34px; line-height: 41px; letter-spacing: 9px;">AUSTRALIA</span>
                        <p id="tcrTripDetails" style="font-size: 17px; line-height: 21px; letter-spacing: 6px;"> May -
                            July 2018</p>
                        <p id="distancelbl" style="display:none"> - km </p>
                    </div>
                </div>
            </div>
            <!-- <div class="mapboxgl-canary" style="visibility: hidden;"></div>
            <div
                class="mapboxgl-canvas-container mapboxgl-interactive mapboxgl-touch-drag-pan mapboxgl-touch-zoom-rotate">
                <canvas class="mapboxgl-canvas" tabindex="0" aria-label="Map" width="642" height="448"
                    style="position: absolute; width: 642px; height: 448px;"></canvas></div>
            <div class="mapboxgl-control-container">
                <div class="mapboxgl-ctrl-top-left"></div>
                <div class="mapboxgl-ctrl-top-right">
                    <div class="mapboxgl-ctrl mapboxgl-ctrl-group"><button class="mapboxgl-ctrl-zoom-in" type="button"
                            title="Zoom in" aria-label="Zoom in"><span class="mapboxgl-ctrl-icon"
                                aria-hidden="true"></span></button><button class="mapboxgl-ctrl-zoom-out" type="button"
                            title="Zoom out" aria-label="Zoom out"><span class="mapboxgl-ctrl-icon"
                                aria-hidden="true"></span></button></div>
                </div>
                <div class="mapboxgl-ctrl-bottom-left">
                    <div class="mapboxgl-ctrl" style="display: block;"><a class="mapboxgl-ctrl-logo" target="_blank"
                            rel="noopener nofollow" href="https://www.mapbox.com/" aria-label="Mapbox logo"></a></div>
                </div>
                <div class="mapboxgl-ctrl-bottom-right">
                    <div class="mapboxgl-ctrl mapboxgl-ctrl-attrib">
                        <div class="mapboxgl-ctrl-attrib-inner"><a href="https://www.mapbox.com/about/maps/"
                                target="_blank">© Mapbox</a> <a href="http://www.openstreetmap.org/about/"
                                target="_blank">© OpenStreetMap</a> <a class="mapbox-improve-map"
                                href="https://apps.mapbox.com/feedback/?owner=thijssondag&amp;id=cjdsigxek0yxx2ss2d29xfatn&amp;access_token=pk.eyJ1IjoidGhpanNzb25kYWciLCJhIjoiY2phOHI2MXNuMDh3dzMzanVhZXlzanU4byJ9.L3vNl1ehNadAt1JWPJqgiA"
                                target="_blank" rel="noopener nofollow">Improve this map</a></div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
	$(".mapboxgl-canvas").addClass("mapboxgl-portrait");

    $("#travel_flight").click(function() {
        $(this).addClass("active");
        $("#travel_car").removeClass("active");
    });
    $("#travel_car").click(function() {
        $(this).addClass("active");
        $("#travel_flight").removeClass("active");
    });

    $(".red").click(function() {
        $(this).addClass("active");
        $(".green").removeClass("active");
        $(".blue").removeClass("active");
    });
    $(".green").click(function() {
        $(this).addClass("active");
        $(".red").removeClass("active");
        $(".blue").removeClass("active");
    });
    $(".blue").click(function() {
        $(this).addClass("active");
        $(".green").removeClass("active");
        $(".red").removeClass("active");
    });

    $(".portrait").click(function() {
        $(this).addClass("active");
        $(".landscape").removeClass("active");
    });
    $(".landscape").click(function() {
        $(this).addClass("active");
        $(".portrait").removeClass("active");
    });

    $(".a").click(function() {
        $(this).addClass("active");
        $(".b").removeClass("active");
        $(".c").removeClass("active");
        $(".d").removeClass("active");
        $(".e").removeClass("active");
    });
    $(".b").click(function() {
        $(this).addClass("active");
        $(".a").removeClass("active");
        $(".c").removeClass("active");
        $(".d").removeClass("active");
        $(".e").removeClass("active");
    });
    $(".c").click(function() {
        $(this).addClass("active");
        $(".b").removeClass("active");
        $(".a").removeClass("active");
        $(".d").removeClass("active");
        $(".e").removeClass("active");
    });
    $(".d").click(function() {
        $(this).addClass("active");
        $(".b").removeClass("active");
        $(".c").removeClass("active");
        $(".a").removeClass("active");
        $(".e").removeClass("active");
    });
    $(".e").click(function() {
        $(this).addClass("active");
        $(".b").removeClass("active");
        $(".c").removeClass("active");
        $(".d").removeClass("active");
        $(".a").removeClass("active");
    });
	
	$(".landscape ").click(function() {
		$(".mapboxgl-map").removeClass("map-portrait").addClass("map-lanscape");
		$(".mapboxgl-canvas").removeClass("mapboxgl-portrait").addClass("mapboxgl-landscape");
		$(".mapboxgl-canvas").removeClass("mapboxgl-portrait").addClass("mapboxgl-landscape");
    });

	$(".portrait ").click(function() {
        $(".mapboxgl-map").addClass("map-portrait").removeClass("map-lanscape");
		$(".mapboxgl-canvas").addClass("mapboxgl-portrait").removeClass("mapboxgl-landscape");
		$(".mapboxgl-canvas").addClass("mapboxgl-portrait").removeClass("mapboxgl-landscape");
    });

    mapboxgl.accessToken =
        'pk.eyJ1IjoiaGFjaGlkZXYiLCJhIjoiY2s2cTdkdng5MGxmODNtcW9tNGZndHN4biJ9.MWfDKVwEk5B2iLXjscpPOg';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [-74.5, 40], // starting position
        zoom: 9 // starting zoom
    });

    // Add zoom and rotation controls to the map.
    map.addControl(new mapboxgl.NavigationControl());
});

$("#tripName_txt").keyup(function(){
	var txt = $(this).val();
	$("#tcrTripName").text(txt);
});
$("#tripDetail_txt").keyup(function(){
	var txt = $(this).val();
	$("#tcrTripDetails").text(txt);
});
</script>
<?php
// get_sidebar();
get_footer();